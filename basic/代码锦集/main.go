package main

//==============================
// 异步爬虫
//==============================

import (
	"fmt"
	"io/ioutil"
	"net/http"
)

func fetchData(url string, ch chan<- string) {
	resp, err := http.Get(url)
	if err != nil {
		ch <- fmt.Sprintf("Error fetching %s: %s", url, err)
		return
	}

	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		ch <- fmt.Sprintf("Error reading %s: %s", url, err)
		return
	}

	ch <- string(body)
}

func main() {
	urls := []string{
		"https://www.example.com/page1",
		"https://www.example.com/page2",
		"https://www.example.com/page3",
	}

	ch := make(chan string)

	// 遍历URL列表，启动goroutine并发获取数据
	for _, url := range urls {
		go fetchData(url, ch)
	}

	// 主goroutine从channel中接收结果并处理
	for range urls {
		data := <-ch
		fmt.Println(data)
	}
}
