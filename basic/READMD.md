
## 安装Golang
```shell
#****************** Golang **************************
export GOROOT=/usr/local/go1.22
export GOPATH=$HOME/.go
export PATH=$PATH:$GOROOT/bin:$GOPATH/bin

# 根据情况设定：
export GO111MODULE=on
export GOPROXY=https://goproxy.cn,direct
#****************************************************
```


