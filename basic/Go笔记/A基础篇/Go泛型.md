在2022年，Go语言发布了1.18版本，这个版本是Go的一个重要里程碑，因为它在语言中引入了泛型。在这之前，Go程序员通常需要使用接口和类型断言来处理这类问题，但这种方法在许多情况下都不够安全，不够方便。

Go语言的泛型**基于约束**（constraints）和**类型参数**（type parameters）。让我们一步步来看看如何使用。





## 1. 泛型函数

最简单的泛型是泛型函数，其格式如下：

```go
func Print[T any](t T) {
	fmt.Println(t)
}

func main() {
	Print(true)
	Print('a')
	Print(12.3)
	Print("hello")
	Print(1 + 2i)
}
```
```go
func Add[T rune | int | float64](a, b T) T {
	return a + b
}

func main() {
	fmt.Println(Add('a', 'b'))
	fmt.Println(Add(1, 2))
	fmt.Println(Add(1.2, 3.4))
}
```



## 2. 泛型类型

```go
type Slice[T any] []T

func (s Slice[T]) Print() {
	for _, v := range s {
		fmt.Println(v)
	}
}

func main() {
	a := Slice[int]{1, 2, 3}
	b := Slice[string]{"apple", "banana"}
	a.Print()
	b.Print()
}
```

```go
type Map[K rune | int, V any] map[K]V

func main() {
	var m1 = Map[int, float64]{1: 3.14, 2: 9.8}
	fmt.Println(m1)

	var m2 = Map[rune, string]{'a': "a", 'b': "b"}
	fmt.Println(m2)
}
```





## 3. 自定义约束

我们也可以定义自己的约束。格式如下：

```go
type Integer int

// Numeric 泛型约束(波浪号表示相似类型)
type Numeric interface {
	~int | float64
}

func Print[T Numeric](n T) {
	fmt.Println(n)
}

func main() {
	Print[int](10)          // int
	Print[float64](20.5)    // float
	Print(Integer(30))      // Integer 相似类型
	// Print(int32(1))      // 不支持
}
```



**排序示例：**

```go
type Ordered interface {
	~int | ~int8 | ~int16 | ~int32 | ~int64 |
		~uint | ~uint8 | ~uint16 | ~uint32 | ~uint64 | ~uintptr |
		~float32 | ~float64 |
		~string
}

// 实现Sort排序,必须实现方法 Len、Less、Swap
type orderedSlice[T Ordered] []T

func (order orderedSlice[T]) Len() int {
	return len(order)
}

func (order orderedSlice[T]) Less(i, j int) bool {
	return order[i] < order[j]
}
func (order orderedSlice[T]) Swap(i, j int) {
	order[i], order[j] = order[j], order[i]
}

func OrderedSlice[T Ordered](s []T) {
	sort.Sort(orderedSlice[T](s))
}

func main() {
	s1 := []int{5, -2, 8, 7, 9}
	OrderedSlice(s1)
	fmt.Println(s1)

	s2 := []uint{9, 1, 0, 8}
	OrderedSlice(s2)
	fmt.Println(s2)

	s3 := []string{"how", "are", "you"}
	OrderedSlice(s3)
	fmt.Println(s3)
}
```



总结



- 泛型基本格式：`Identifier[T any]`



在这个例子中，我们创建了一个Adder接口，定义了Add方法，然后创建了Int和Float类型，使它们

实现了Adder接口。然后我们创建了一个Add函数，它可以接受任何实现了Adder接口的类型。

以上就是Go语言泛型的基础使用，希望能对你有所帮助！