

**Sync包简述：**

`Sync包同步提供基本的同步原语，如互斥锁`。 除了Once和WaitGroup类型之外，大多数类型都是供低级库例程使用的。 通过Channel和沟通可以更好地完成更高级别的同步。并且此包中的值在使用过后不要拷贝。



## 1. 同步协作

```go
func main() {
    var wg sync.WaitGroup
    wg.Add(2)
    wg.Done()
    wg.Done()
    wg.Add(1)
    wg.Done()
    wg.Wait() 
    fmt.Println("ok")
}
```

```go
func main() {
    var wg sync.WaitGroup
    download := func(n int) {
        rd := rand.Intn(10)
        <-time.After(time.Second * time.Duration(rd))
        fmt.Printf("文件【%2d】下载完成。\n", n)
        wg.Done()
    }

    for i := 0; i < 10; i++ {
        wg.Add(1)
        go download(i + 1)
    }
    wg.Wait()
    fmt.Println("====全部下载完毕====")
}
```



## 2. 互斥锁

>   `Mutex`是一种独占锁，即`写写互斥`，`读写互斥`，`读读互斥`。

```go
//  Mutex相当于一个全局互斥锁(Lock)
func main() {
    var mx sync.Mutex
    
    foo := func(msg string) {
        mx.Lock()
        defer mx.Unlock()
        for {
            <-time.After(time.Second)
            log.Println(msg)
        }
    }

    go foo("A")
    go foo("B")
    time.Sleep(time.Second * 10)
}
```


## 3. 读写锁

- `RWMutex`有两种粒度锁，即`Lock/Unlock`和`RLock/RUnlock`,它遵循：`写写互斥`、`读写互斥`,`读读共享`。
```go

var rw sync.RWMutex

// 互斥锁: Lock和Unlock
func Set() {
    rw.Lock()
    defer rw.Unlock()
    for {
        fmt.Println("写入：", (<-time.Tick(time.Second)).Second())
    }
}

// 共享锁: RLock和RUnlock
func Get(tip string) {
    rw.RLock()
    defer rw.RUnlock()
    for {
        fmt.Printf("读取(%s)：%d\n", tip, (<-time.Tick(time.Second)).Second())
    }
}

func main() {
    // // 1. 读写互斥
    // go Get("A")
    // time.Sleep(time.Second)
    // go Set()

    // // 2. 写读互斥
    // go Set()
    // time.Sleep(time.Second)
    // go Get("B")

    // 3. 读读共享
    go Get("AA")
    go Get("BB")

    <-time.Tick(time.Second * 10)
}
```


## 4. 单例锁 - sync.Once

- `sync.Once`相当于一个全局锁(Lock)， `once.Do(...)`只会被执行一次。常用与**单例模式**。
```go
var once sync.Once

func main() {
    once.Do(func() { fmt.Println("第一次") })
    go once.Do(func() { fmt.Println("第二次") })
    time.Sleep(time.Second)
}
```

```go
var (
    cfg  *Config
    once sync.Once
)

// 单例对象
type Config struct{}

// 获取单例对象
func DefaultConfig() *Config {
    once.Do(func() { cfg = &Config{} })
    return cfg
}
```


## 5. 条件锁
- 条件锁(`sync.NewCond`)有单信号(`Signal`)和广播(`Broadcast`)两种模式。
```go
// 场景：大爷大妈们超市领大米(先排号，再叫号领礼品)
func main() {
   cond := sync.NewCond(new(sync.Mutex))

   // 创建30个goroutine，同时进入等待状态
   fmt.Println("=================领号牌================")
   for i := 0; i < 30; i++ {
      go func(x int) {
         cond.L.Lock()
         defer cond.L.Unlock()

         fmt.Printf("%-2d 已领到号\n", x)
         cond.Wait() // 暂时阻塞,等待通知吧...

         fmt.Printf("%-2d 成功领取领一袋大米\n", x)
      }(i)
   }

   time.Sleep(time.Second)
   fmt.Println("=================第一位================")
   cond.Signal()

   time.Sleep(time.Second)
   fmt.Println("=================下一位================")
   cond.Signal()

   time.Sleep(time.Second * 3)
   fmt.Println("================剩余全部===============")
   cond.Broadcast()

   var quit string
   fmt.Scanln(&quit)
}
```
<br/>

## 6. 对象池
- 临时对象池(sync.Pool)中使用`GC`，将销毁整个对象池对象。
```go
func main() {
   // New:是一个func()interface{}的委托, 用于确定池对象类型
   intPool := &sync.Pool{New: func() interface{} { return 0 }}
   fmt.Println(intPool.Get().(int))

   intPool.Put(1)
   intPool.Put(2) //覆盖赋值是无效的
   //runtime.GC()
   fmt.Println(intPool.Get().(int))
}
```
<br/>

## 7. 同步字典
```go
//https://www.jianshu.com/p/b09853ecd39d
// 读写删改遍历
func main() {
   var m sync.Map
   m.Store("a", 10)                    //写
   m.Store("a", 11)                    //改(覆盖)
   m.Store("b", 20)                    //
   fmt.Println(m.LoadOrStore("d", 30)) //写或读
   fmt.Println(m.LoadOrStore("d", 31)) //(不覆盖)

   fmt.Println(m.Load("a")) //读
   m.Delete("b")            //删
   m.Range(func(key, value interface{}) bool { //遍历
      fmt.Println(key, value)
      return true
   })
}
```

## 参考链接
https://www.jianshu.com/p/4e2922f68991
