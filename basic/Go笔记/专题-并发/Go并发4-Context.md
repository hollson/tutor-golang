

<br/>

```golang
func WithoutCancel(parent Context) Context
func WithTimeout(parent Context, timeout time.Duration) (Context, CancelFunc) 
func WithDeadline(parent Context, d time.Time) (Context, CancelFunc)
func WithValue(parent Context, key, val any) Context



// A Context carries a deadline, a cancellation signal, and other values across
// API boundaries.
//
// Context's methods may be called by multiple goroutines simultaneously.
type Context interface {
	// 返回：到期还有多久，是否到期
	Deadline() (deadline time.Time, ok bool)

	// Done returns a channel that's closed when work done on behalf of this
	// context should be canceled. Done may return nil if this context can
	// never be canceled. Successive calls to Done return the same value.
	// The close of the Done channel may happen asynchronously,
	// after the cancel function returns.
	//
	// WithCancel arranges for Done to be closed when cancel is called;
	// WithDeadline arranges for Done to be closed when the deadline
	// expires; WithTimeout arranges for Done to be closed when the timeout
	// elapses.
	//
	// Done is provided for use in select statements:
	//
	//  // Stream generates values with DoSomething and sends them to out
	//  // until DoSomething returns an error or ctx.Done is closed.
	//  func Stream(ctx context.Context, out chan<- Value) error {
	//  	for {
	//  		v, err := DoSomething(ctx)
	//  		if err != nil {
	//  			return err
	//  		}
	//  		select {
	//  		case <-ctx.Done():
	//  			return ctx.Err()
	//  		case out <- v:
	//  		}
	//  	}
	//  }
	//
	// See https://blog.golang.org/pipelines for more examples of how to use
	// a Done channel for cancellation.
	Done() <-chan struct{}

	// If Done is not yet closed, Err returns nil.
	// If Done is closed, Err returns a non-nil error explaining why:
	// Canceled if the context was canceled
	// or DeadlineExceeded if the context's deadline passed.
	// After Err returns a non-nil error, successive calls to Err return the same error.
	Err() error

	// Value returns the value associated with this context for key, or nil
	// if no value is associated with key. Successive calls to Value with
	// the same key returns the same result.
	//
	// Use context values only for request-scoped data that transits
	// processes and API boundaries, not for passing optional parameters to
	// functions.
	//
	// A key identifies a specific value in a Context. Functions that wish
	// to store values in Context typically allocate a key in a global
	// variable then use that key as the argument to context.WithValue and
	// Context.Value. A key can be any type that supports equality;
	// packages should define keys as an unexported type to avoid
	// collisions.
	//
	// Packages that define a Context key should provide type-safe accessors
	// for the values stored using that key:
	//
	// 	// Package user defines a User type that's stored in Contexts.
	// 	package user
	//
	// 	import "context"
	//
	// 	// User is the type of value stored in the Contexts.
	// 	type User struct {...}
	//
	// 	// key is an unexported type for keys defined in this package.
	// 	// This prevents collisions with keys defined in other packages.
	// 	type key int
	//
	// 	// userKey is the key for user.User values in Contexts. It is
	// 	// unexported; clients use user.NewContext and user.FromContext
	// 	// instead of using this key directly.
	// 	var userKey key
	//
	// 	// NewContext returns a new Context that carries value u.
	// 	func NewContext(ctx context.Context, u *User) context.Context {
	// 		return context.WithValue(ctx, userKey, u)
	// 	}
	//
	// 	// FromContext returns the User value stored in ctx, if any.
	// 	func FromContext(ctx context.Context) (*User, bool) {
	// 		u, ok := ctx.Value(userKey).(*User)
	// 		return u, ok
	// 	}
	Value(key any) any
}
```



```golang
func main() {
	_ = http.ListenAndServe(":8081", http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		select {
		case <-time.After(time.Second * 3):
			w.Write([]byte("请求被执行..."))
		case <-r.Context().Done():
			fmt.Fprintf(os.Stderr, " 请求被取消...\n")
			fmt.Println(r.Context().Err())
		}
	}))
}
```



## 1. 取消

```go
func main() {
	// 创建一个Cancel的Context
	ctx, cancel := context.WithCancel(context.Background())

	// 在子协程中执行cancel操作
	go func() {
		time.Sleep(2 * time.Second)
		cancel()
		println("ddd")
	}()

	<-ctx.Done()
	fmt.Println("Context cancelled.")
}
```
<br/>

## 2. 超时
```go
func main() {
	done := make(chan struct{})

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*1)
	defer cancel()

	go func() {
		time.Sleep(3 * time.Second)
		close(done)
	}()

	select {
	case <-ctx.Done():
		fmt.Println("操作超时...")
	case <-done:
		fmt.Println("操作完成...")
	}
}
```
<br/>

## 3. 过期
```go
func main() {
   dead := time.Now().Add(3 * time.Second) //截止时间
   ctx, cancel := context.WithDeadline(context.Background(), dead)
   defer cancel()

   // 会员有效期检测
   select {
   case <-time.After(1 * time.Second):
      fmt.Println("欢迎光临,尽情畅玩吧...")
   case <-ctx.Done():
      fmt.Println("会员失效，请充值...", ctx.Err())
   }
   fmt.Println("over")
}
```

<br/>

## 4. 传值

```go
func main() {
	// 创建一个Value的Context
	ctx := context.WithValue(context.Background(), "greet", "Hello World")

	// 子协程读取改Value
	go func(c context.Context) {
		result := c.Value("greet")
		fmt.Println(result)
	}(ctx)

	_, _ = fmt.Scanln()
}
```
