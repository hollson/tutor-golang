+++
title= " 🦑 Go语言中的原子操作"
url= "/posts/0742b474a643d63a774461726a6201203/"  
aliases = ["/post/15865201203/"]
keywords= "优雅,退出,并发"
description= "原子操作中，针对某个值的操作在被进行的过程中，CPU绝不会再去进行其他的针对该值的操作。为了实现这样的严谨性，原子操作仅会由一个独立的CPU指令代表和完成。原子操作是无锁的，常常直接通过 CPU 指令直接实现。 事实上，其它同步技术的实现常常依赖于原子操作"
image= "/img/res/blog.jpg"
date= 2020-12-02T22:03:06+08:00
lastmod= 2020-12-02T22:03:06+08:00
categories= ["GO高级篇"]
tags= ["并发编程"]
archives= "2020"
author= "史布斯"
height= 1586527386
draft= false

+++



## 原子操作概要

### 什么是原子操作

**原子操作**是在执行中不能被中断的操作，通常由CPU芯片级能力来保证，并由操作系统提供调用，golang基于操作系统的能力，也提供了基于原子操作的支持。

在Go中，一条普通的赋值语句其实不是一个原子操作。列如，在32位机器上写int64类型的变量就会有中间状态，因为他会被拆成两次写操作(MOV)——写低32位和写高32位。

### atonic与mutex

- 目的不同：Mutex用于保护一段逻辑操作，atomic用于保护对变量的更新；
- 实现不同：Mutex由操作系统的调度器实现；atomic由底层硬件指令直接提供支持;

## Atomic基础操作

>    `sync/atomic` 提供了原子级的内存操作。

- 原子操作共有五种：`store,load,add,cas,swap`;
- 原子操作支持:`int32、int64、uint32、uint64、uintptr、unsafe.Pointer`;

### 1. Store/Load

> 载入和存储相对

```go
func Store() {
	// 类型操作
	var n int32
	atomic.StoreInt32(&n, 2)          // 存储
	fmt.Println(atomic.LoadInt32(&n)) // 加载

	// 泛化类型
	var a atomic.Value
	a.Store(1)            // 存储
	fmt.Println(a.Load()) // 加载
}
```

### 2.Add

> 增减操作都是以`Add`为前缀, 减即`加负数`。
>
> `func AddInt32(addr *int32, delta int32) (new int32)`

```go
func Add() {
	var n int32 = 0
	for i := 0; i < 10000; i++ {
		go atomic.AddInt32(&n, 1) // 原子操作
	}

	time.Sleep(time.Second)
	fmt.Println(n)
}
```

### 3.Cas

> `func CompareAndSwapInt32(addr *int32, old, new int32) (swapped bool)`

```go
func main() {
	var num int32 = 10 // 初始值为10

	// 使用CompareAndSwap进行值的比较和交换
	swapped := atomic.CompareAndSwapInt32(&num, 10, 20)
	fmt.Println("Swap successful?", swapped) // 期望的旧值为10，设置的新值为20

	fmt.Println("Current value:", num) // 输出变量的当前值
}
```

### 4.Swap

>   `func SwapInt32(addr *int32, new int32) (old int32)`

```go
func main() {
	var num int32 = 10 // 初始值为10

	// 使用Swap进行值的交换
	oldValue := atomic.SwapInt32(&num, 20)
	fmt.Println(oldValue, num) // 输出旧值和新值
}
```

## 参考连接

> https://liyuxing.blog.csdn.net/article/details/113886238
> https://blog.csdn.net/alwaysrun/article/details/125023283
> https://www.sixt.tech/golangs-atomic
> https://blog.csdn.net/h_l_f/article/details/118739317
>
> https://blog.csdn.net/xingzuo_1840/article/details/124575768
>
> https://www.kancloud.cn/digest/batu-go/153537
>
> https://github.com/lxmgo/learngo/blob/master/atomic/atomic.go
