+++
title= "Gorm快速指南"
url= "/post/1597480249/"
image= "/img/res/bg4.jpg"
date= 2020-08-14
categories= ["Go基础篇"]
archives= "2020"
author= "史布斯"
height= 1597480249
draft= false

+++

[TOC]


## Gorm介绍

> [Github Gorm](https://github.com/jinzhu/gorm)是一个使用Go语言编写的ORM框架。[中文官方网站](https://gorm.io/zh_CN/)内含十分齐全的中文文档 ，对开发者友好，支持主流数据库。

**安装Gorm：**

```bash
go get -u github.com/jinzhu/gorm
```

## Gorm连接

> 连接不同的数据库，需要导入对应数据的驱动程序，`GORM`支持如下的数据库驱动：

```go
import _ "github.com/jinzhu/gorm/dialects/mysql"
import _ "github.com/jinzhu/gorm/dialects/postgres"
import _ "github.com/jinzhu/gorm/dialects/sqlite"
import _ "github.com/jinzhu/gorm/dialects/mssql"
```

### 1. 连接MySQL

```go
import (
  "github.com/jinzhu/gorm"
  _ "github.com/jinzhu/gorm/dialects/mysql"
)

func main() {
  db, err := gorm.Open("mysql", "user:password@(localhost)/dbname?charset=utf8mb4&parseTime=True&loc=Local")
  defer db.Close()
}
```

### 2. 连接Postgresql

> 基本代码同上，注意引入对应`postgres`驱动并正确指定`gorm.Open()`参数。

```go
import (
  "github.com/jinzhu/gorm"
  _ "github.com/jinzhu/gorm/dialects/postgres"
)

func main() {
  db, err := gorm.Open("postgres", "host=myhost port=myport user=gorm dbname=gorm password=mypassword")
  defer db.Close()
}
```



## GORM基本示例

### 创建数据库

在使用GORM前手动创建数据库`db1`：

```mysql
CREATE DATABASE db1;
```

### 基本CURD

使用GORM连接上面的`db1`进行创建、查询、更新、删除操作。

```go
package main

import (
	"fmt"

	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
)

// UserInfo 用户信息
type UserInfo struct {
	ID      uint
	Name    string
	Age     int
	Address string
}

// 表名称
func (p UserInfo) TableName() string {
	return "account"
}

// db.Cmd,返回DB，如何立即执行
func main() {
	db, err := gorm.Open("mysql", "root:123456@(127.0.0.1:3306)/db1?charset=utf8mb4&parseTime=True&loc=Local")
	if err != nil {
		panic(err)
	}
	defer db.Close()

	// 自动迁移: 只增不减(表和字段)
	// db.Exec("drop table if exists account;")
	// db.Exec("drop table if exists user_infos;")
	db.AutoMigrate(&UserInfo{})

	// 1.插入
	db.Create(&UserInfo{Name: "Tom", Age: 22, Address: "China"})
	db.Create(&UserInfo{Name: "Lucy", Age: 18, Address: "Korea"})
	db.Create(&UserInfo{Name: "Jack", Age: 22, Address: "USA"})

	// 2.查询
	var u = new(UserInfo)
	db.First(u)
	fmt.Printf("%#v\n", u) // 打印结构字段和值

	var u2 []UserInfo
	db.Find(&u2, "age=?", 22)
	fmt.Printf("%v\n", u2)

	// 3.更新,如何更新多个字段
	// u.ID=2
	// db.Model(&u).Update("address", "Japan","age",22)

	// 4.删除,空对象则清空表
	// db.Delete(&u)
}
```

## Gorm Model定义

> GORM内置了一个`gorm.Model`结构体与数据库中的数据表进行`映射`，在GORM中模型(Models)通常是正常定义的`结构体、基本类型或它们的指针`。 同时也支持`sql.Scanner`及`driver.Valuer`接口(interfaces）。

**Model定义：**

```go
// gorm.Model 定义
type Model struct {
	ID        uint `gorm:"primary_key"`
	CreatedAt time.Time
	UpdatedAt time.Time
	DeletedAt *time.Time
}
```

### 模型定义示例

```go
type User struct {
  gorm.Model
  Name         string
  Age          sql.NullInt64
  Birthday     *time.Time
  Email        string  `gorm:"type:varchar(100);unique_index"`
  Role         string  `gorm:"size:255"` // 设置字段大小为255
  MemberNumber *string `gorm:"unique;not null"` // 设置会员号（member number）唯一并且不为空
  Num          int     `gorm:"AUTO_INCREMENT"` // 设置 num 为自增类型
  Address      string  `gorm:"index:addr"` // 给address字段创建名为addr的索引
  IgnoreMe     int     `gorm:"-"` // 忽略本字段
}
```

### 结构体标记（tags）

#### 支持的结构体标记（Struct tags）

| 结构体标记（Tag） | 描述                                                     |
| ----------------- | ----------------------- |
| Column            | 指定列名                                                 |
| Type              | 指定列数据类型                                           |
| Size              | 指定列大小, 默认值255                                    |
| PRIMARY_KEY       | 将列指定为主键                                           |
| UNIQUE            | 将列指定为唯一                                           |
| DEFAULT           | 指定列默认值                                             |
| PRECISION         | 指定列精度                                               |
| NOT NULL          | 将列指定为非 NULL                                        |
| AUTO_INCREMENT    | 指定列是否为自增类型                                     |
| INDEX             | 创建具有或不带名称的索引, 如果多个索引同名则创建复合索引 |
| UNIQUE_INDEX      | 和 `INDEX` 类似，只不过创建的是唯一索引                  |
| EMBEDDED          | 将结构设置为嵌入                                         |
| EMBEDDED_PREFIX   | 设置嵌入结构的前缀                                       |
| -                 | 忽略此字段                                               |

#### 关联相关标记（tags）

| 结构体标记（Tag）                | 描述                               |
| ------------------ | ------------------ |
| MANY2MANY                        | 指定连接表                         |
| FOREIGNKEY                       | 设置外键                           |
| ASSOCIATION_FOREIGNKEY           | 设置关联外键                       |
| POLYMORPHIC                      | 指定多态类型                       |
| POLYMORPHIC_VALUE                | 指定多态值                         |
| JOINTABLE_FOREIGNKEY             | 指定连接表的外键                   |
| ASSOCIATION_JOINTABLE_FOREIGNKEY | 指定连接表的关联外键               |
| SAVE_ASSOCIATIONS                | 是否自动完成save的相关操作       |
| ASSOCIATION_AUTOUPDATE           | 是否自动完成update的相关操作     |
| ASSOCIATION_AUTOCREATE           | 是否自动完成create的相关操作     |
| ASSOCIATION_SAVE_REFERENCE       | 是否自动完成引用的save的相关操作 |
| PRELOAD                          | 是否自动完成预加载的相关操作       |

## 主键、表名、列名的约定

### 主键（Primary Key）

GORM 默认会使用名为ID的字段作为表的主键。

```go
type User struct {
  ID   string // 名为`ID`的字段会默认作为表的主键
  Name string
}

// 使用`AnimalID`作为主键
type Animal struct {
  AnimalID int64 `gorm:"primary_key"`
  Name     string
  Age      int64
}
```

### 表名（Table Name）

表名默认就是结构体名称的复数，例如：

```go
type User struct {} // 默认表名是 `users`

// 将 User 的表名设置为 `profiles`
func (User) TableName() string {
  return "profiles"
}

func (u User) TableName() string {
  if u.Role == "admin" {
    return "admin_users"
  } else {
    return "users"
  }
}

// 禁用默认表名的复数形式，如果置为 true，则 `User` 的默认表名是 `user`
db.SingularTable(true)
```

也可以通过`Table()`指定表名：

```go
// 使用User结构体创建名为`deleted_users`的表
db.Table("deleted_users").CreateTable(&User{})

var deleted_users []User
db.Table("deleted_users").Find(&deleted_users)
//// SELECT * FROM deleted_users;

db.Table("deleted_users").Where("name = ?", "jinzhu").Delete()
//// DELETE FROM deleted_users WHERE name = 'jinzhu';
```

GORM还支持更改默认表名称规则：

```go
gorm.DefaultTableNameHandler = func (db *gorm.DB, defaultTableName string) string  {
  return "prefix_" + defaultTableName;
}
```

### 列名（Column Name）

列名由字段名称进行下划线分割来生成

```go
type User struct {
  ID        uint      // column name is `id`
  Name      string    // column name is `name`
  Birthday  time.Time // column name is `birthday`
  CreatedAt time.Time // column name is `created_at`
}
```

可以使用结构体tag指定列名：

```go
type Animal struct {
  AnimalId    int64     `gorm:"column:beast_id"`         // set column name to `beast_id`
  Birthday    time.Time `gorm:"column:day_of_the_beast"` // set column name to `day_of_the_beast`
  Age         int64     `gorm:"column:age_of_the_beast"` // set column name to `age_of_the_beast`
}
```

### 时间戳跟踪

#### CreatedAt

如果模型有 `CreatedAt`字段，该字段的值将会是初次创建记录的时间。

```go
db.Create(&user) // `CreatedAt`将会是当前时间

// 可以使用`Update`方法来改变`CreateAt`的值
db.Model(&user).Update("CreatedAt", time.Now())
```

#### UpdatedAt

如果模型有`UpdatedAt`字段，该字段的值将会是每次更新记录的时间。

```go
db.Save(&user) // `UpdatedAt`将会是当前时间

db.Model(&user).Update("name", "jinzhu") // `UpdatedAt`将会是当前时间
```

#### DeletedAt

如果模型有`DeletedAt`字段，调用`Delete`删除该记录时，将会设置`DeletedAt`字段为当前时间，而不是直接将记录从数据库中删除。