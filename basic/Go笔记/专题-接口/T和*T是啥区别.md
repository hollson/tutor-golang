



## 结构体方法

>   `结构体方法的第一个参数就是隐含的接受者本身`

```go
type Hello struct{}

func (Hello) Greet(name string) {
	println("Hello", name)
}

func main() {
	h := Hello{}
	Hello.Greet(h, "World") // 原生语法
	h.Greet("World")        // 语法糖
}
```

### 函数签名

- 如果两个函数的签名相同，那么它们的类型相同；
- 如果两个函数的**参数列表**、**返回值类型**和**参数顺序**相同，则函数签名相同 (与函数名和包名无关)；
```go
func F1(int) {}
func F2(int) {}

// 验证函数类型是否相同
func main() {
	f1 := reflect.TypeOf(F1)
	f2 := reflect.TypeOf(F2)
	fmt.Println(f1, f2, f1 == f2)
}
```



## 方法接收器

> 方法接收器可以是值类型或指针类型，它们可以互相调用(**语法糖**)。
```go
type T struct{}

// T会生成同名的*T包装方法
func (T) A() { println("A") }

func (*T) B() { println("B") }

func main() {
	var t T = T{} // 值类型
	t.A()         // 副本传递
	t.B()         // (&f).Bar()

	var pt *T = &T{} // 指针类型
	pt.A()           // 解引用：(*f).Foo()
	pt.B()           // 指针传递
}
```



## 结构体与接口

```golang
type Cache interface {
	Set(string)
	Get() string
}

type Memory struct{ Value string }

func (m Memory) Set(v string) { m.Value = v }
func (m *Memory) Get() string { return m.Value }

func main() {
	var c Cache = Memory{}
	c.Set("hello world")
	fmt.Println(c.Get())
}
```



https://studygolang.com/articles/13920 
http://legendtkl.com/2017/07/01/golang-interface-implement/

