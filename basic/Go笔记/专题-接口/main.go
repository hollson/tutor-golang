import "fmt"

type T struct{}

func (T) F() {
	println("hello")
}

func F(T) {} // 原生函数

func main() {
	t := T{}
	t.F()  // 语法糖
	T.F(t) // 原生语法

	// 验证
	f1 := reflect.TypeOf(T.F)
	f2 := reflect.TypeOf(F)
	fmt.Println(f1, f2, f1 == f2)
}