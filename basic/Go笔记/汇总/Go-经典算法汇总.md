+++
title= "经典算法汇总(Go版本)"
url= "/post/1585633378/"
#aliases = ["/posts/cae4f512a6ec8fdc32cef78612e55ac7/"]
keywords= "Go编程,练习题,面试题"
description= "「Golang版本」十道经典编程练习题"
image= "/img/res/bg9.jpg"
date= 2018-02-18
categories= ["Golang"]
tags= ["面试","算法"]
archives= "2018"
author= "史布斯"
height= 1585633378
draft= false
+++


## 1.排列组合
- 题目：有1、2、3、4个数字，能组成多少个互不相同且无重复数字的三位数？都是多少？
- 分析：三层嵌套循环，时间复杂度为O(n^3)
```go
func main() {
  arr := []int{1, 2, 3, 4}
  for i := 0; i < len(arr); i++ {
    for j := 0; j < len(arr); j++ {
      for k := 0; k < len(arr); k++ {
        if arr[i] != arr[j] && arr[j] != arr[k] {
          fmt.Printf("%d%d%d\n", arr[i], arr[j], arr[k])
        }
      }
    }
  }
}
```
<br/>

## 2.三元方程
- 题目：一个整数(10万以内)，它加上100后是一个完全平方数，再加上168又是一个完全平方数，请问该数是多少？
- 分析：将多元方程的`数学求解方法转换为计算机程序`, 即：
设该数为x，加100后得到的完全平方数为`y^2`，再加168后得到的完全平方数为`z^2`
- 参考：https://blog.csdn.net/baishuiniyaonulia/article/details/77922601
```go
func main() {
  var x = -100  //x+100=y*y (可知：x>=-100)；
  var y, z float64 = 0, 0
  for ; (z+y) >= 0 && (z+y) <= 168; x++ {
    y = math.Sqrt(float64(x + 100))
    z = math.Sqrt(float64(x + +100 + 168))
    // 判断y,z是整数
    if math.Ceil(y) == math.Floor(y) && math.Ceil(z) == math.Floor(z) {
      fmt.Println(x)
    }
  }
}
```
<br/>

## 3.判断闰年
- 题目：输入某年某月某日，判断这一天是这一年的第几天？
- 分析：闰年：四年剔百一闰，或四百年一闰
```go
func foo(year, month, day int) {
  year, month, day = 2018, 3, 1
  counts := []int{31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31} //平年

  var sum = day
  for i := 0; i < month-1; i++ {
    sum += counts[i]
  }

  if (year%4 == 0 && year%100 != 0) || (year%400 == 0) && month > 2 {
    sum += 1
  }
  fmt.Println(sum)
}
```

<br/>

## 4.乘法口诀
- 题目：输出九九乘法表
- 分析：注意`坐标系变换`和`打印控制`
```go
func main() {
  for x := 1; x < 10; x++ {
    for y := 1; y < 10; y++ {
      if y<= x {
        fmt.Printf("%d*%d=%-3d", y, x, x*y)  //左对齐占3位
      }
    }
    fmt.Println()
  }
}
```
<br/>

## 5.打印棋盘
- 题目：要求输出国际象棋棋盘
- 分析：定义黑白两个字符色块；根据奇偶控制输出
```go
func main() {
  black := "\033[40;37m  \033[0m" //空格字符色块
  white := "\033[40;47m  \033[0m"

  for i := 0; i < 8; i++ {
    for j := 0; j < 8; j++ {
      if (i+j)%2 == 0 {
        fmt.Printf(white)
      } else {
        fmt.Printf(black)
      }
    }
    fmt.Println()
  }
}
```
<br/>

## 6.打印楼梯
- 分析：借鉴`打印棋盘`和`乘法口诀`，控制输出条件
```go
func main() {
  black := "\033[40;37m  \033[0m" 
  white := "\033[40;47m  \033[0m"

  for i := 0; i < 8; i++ {
    for j := 0; j < 8 && j < i; j++ {
      if (i+j)%2 == 0 {
        fmt.Printf(white)
      } else {
        fmt.Printf(black)
      }
    }
    fmt.Println()
  }
}
```
<br/>

## 7.兔子问题
- 题目：有一对兔子，从出生后第3个月起每个月都生一对兔子，小兔子长到第三个月后每个月又生一对兔子，假如兔子都不死，问每个月的兔子总数为多少？
- 分析：兔子问题即`斐波那契序列`问题，可循环或递归实现
```go
// 循环方式
func main() {
  var f1, f2 int = 1, 1
  var i int
  for i = 1; i <= 10; i++ {
    fmt.Printf("%12d %12d", f1, f2)
    if i%2 == 0 {
      fmt.Printf("\n")
    }
    f1 = f1 + f2 /*前两个月加起来赋值给第三个月*/
    f2 = f1 + f2
  }
}
```
```go
//递归方式
func Fibonacci(num int) int {
  if num < 3 {
    return 1
  }
  return Fibonacci(num-1) + Fibonacci(num-2)
}
```
<br/>

## 8. 素数个数
- 题目： 判断101-200之间有多少个素数，并输出所有素数。
- 分析：如果概数能被`2 ~ sqrt(n)`中的某个数，则此数不是素数，反之是素数。 　
- 注意边界取值
```go
func main() {
  for i, n := 100, 0; i < 200; i++ {
    sqt :=int( math.Sqrt(float64(i+1))) 
    for j := 2; j <=sqt; j++ {
      if i%j == 0 {
        goto NEXT
      }
    }
    fmt.Printf("%-4d", i)
    n++
    if n%10 == 0 {
      fmt.Println()
    }
  NEXT:
  }
} 
```

<br/>

## 9. 水仙花数
- 题目：打印出所有的“水仙花数”(指一个三位数，其各位数字立方和等于该数本身),例如：`153=1^3+5^3+3^3`
- 分析：利用for循环控制100-999个数，每个数分解出个位，十位，百位。
```go
func main() {
  for n := 100; n < 1000; n++ {
    i := n / 100
    j := (n % 100) / 10
    k := (n % 100) % 10
    if i*100+j*10+k == i*i*i+j*j*j+k*k*k {
      fmt.Printf("%-4d", n)
    }
  }
}
```
<br/>

## 10.数学求解
- 题目：一球从100米高度落下，每次落地后反跳回原高度的一半；再落下，求第10次落地时，共经过多少米？第10次反弹多高？
- 分析：`定位变量和变量关系`，`循环10次，变量为sum和mid，结果累加`
```go
func main() {
  var sum, mid float64 = 100, 50
  for n := 2; n <= 10; n++ {
    sum += mid * 2  //反跳高度
    mid /= 2
  }
  fmt.Println(sum, mid)
}
```

## 参考链接
> https://www.zybuluo.com/Gestapo/note/32082#golang%E7%BC%96%E7%A8%8B%E7%99%BE%E4%BE%8B

