+++
title= "Go-经典编程面试题"
url= "/post/1585726288/"
#aliases = ["/posts/fb0136bfbdf5bfc06d2cd38ce526b7f2/"]
#description= "用于SEO的内容说明"
image= "/img/res/blog.jpg"
date= 2019-04-09
categories= ["Golang"]
tags= ["面试"]
archives= "2019"
author= "史布斯"
height= 1585726239
draft= false
+++


## 1. 写出下面代码输出内容？
```go
func main() {
  defer func() { fmt.Println("打印前") }()
  defer func() { fmt.Println("打印中") }()
  defer func() { fmt.Println("打印后") }()
  panic("触发异常")
}

// output:
// 打印后
// 打印中
// 打印前
// panic: 触发异常...
```
> 解答：panic需要等defer结束后才会向上传递。


## 2. 以下代码有什么问题？
```go
type person struct {
  Id   int
  Name string
}

func (p *person) String() string {
  return p.Name
}

func main() {
  m := make(map[int]*person)
  s := []person{{1, "tom"}, {2, "lucy"}, {3, "lily"}}
  for _, v := range s {
    m[v.Id] = &v
  }
  fmt.Println(m)
}

// output:
// map[1:lily 2:lily 3:lily]
```
> 解答：🚫 `for-range`的元素最终为最后一个struct的值拷贝。


## 3. 下面的代码会输出什么?
```go
func main() {
  runtime.GOMAXPROCS(1)
  wg := sync.WaitGroup{}

  for i := 0; i < 10; i++ {
    wg.Add(1)
    go func() {
      fmt.Println("A: ", i)  // 外部变量(共享变量)
      wg.Done()
    }()
  }

  for i := 0; i < 10; i++ {
    wg.Add(1)
    go func(i int) {
      fmt.Println("B: ", i)  // 内部变量(两个变量)
      wg.Done()
    }(i)
  }
  wg.Wait()
}
```
> 考点：Goroutine执行的「随机性」和「闭包」
> 
> 解答：⛔️ A:均为输出10；B:从0~9输出(顺序不定)

## 4. 下面代码会输出什么？
```go
type Person struct{}

func (p *Person) Talk() {
  fmt.Println("Talk")
  p.Sing()
}
func (p *Person) Sing() {
  fmt.Println("Sing")
}

type Student struct {
  Person
}
func (t *Student) Sing() {
  fmt.Println("Student Sing")
}

func main() {
  t := Student{}
  t.Talk()
}

// output：
// Talk
// Sing
```
> 考点：组合继承。


## 5. 下面代码输出什么？
```go
func main() {
  c1 := make(chan string, 1)
  c2 := make(chan string, 1)
  c1 <- "hello"
  c2 <- "world"

  select {
  case v := <-c1:
    fmt.Println(v)
  case v := <-c2:
    fmt.Println(v)
  }
}
```
> 考点: select随机性。
>
> 解析: `make(chan string)`不等于`make(chan string, 1)`。


## 6. 下面代码输出什么？
```go
func foo(index string, a, b int) int {
  ret := a + b
  fmt.Println(index, a, b, ret)
  return ret
}
func main() {
  a := 1
  b := 2
  defer foo("1", a, foo("10", a, b)) // 3
  a = 0
  defer foo("2", a, foo("20", a, b)) // 2
  b = 1
}

// 解析：
// 10 1 2 =>  10 1 2 3
// 20 0 2 =>  20 0 2 2
//  2 0 2 =>   2 0 2 2
//  1 1 3 =>   1 1 3 4
```
> 考点： defer的参数与函数执行
>
> 解答： defer的参数是在程序加载时确定的。


## 7. 以下输出什么内容?
```go
func main() {
  s := make([]int, 5)
  s = append(s, 1, 2, 3)
  fmt.Println(s)
}

// output：
// [0 0 0 0 0 1 2 3]
```
> 考点：make默认值和append
>
> 解答：make初始化是有默认值的，此处默认值为0



## 8. 下面的代码有什么问题?
```go
func main() {
  var (
    dic = make(map[int]int)
    mux = sync.Mutex{}
  )

  go func() {
    for i := 0; i < math.MaxInt; i++ {
      mux.Lock()
      dic[i] = i // 并发写
      mux.Unlock()
    }
  }()

  go func() {
    for i := 0; i < math.MaxInt; i++ {
      _ = dic[i] // 并发读
    }
  }()

  time.Sleep(time.Second * 1)
}
```
> 考点：map线程安全
>
> 解答：可能会出现 `fatal error: concurrent map read and map write.`, 
>
> 即map的并发「`写写`」或「`读写`」都会引发异常, 可使用`sync.RWMutex`或`sync.Map`解决。

**可使用`race`做竞争检测：如：**
```go
//go:generate go run -race ddd.go
func main() {
  var a = 0
  go func() {
    a = 1
  }()
  a = 2
}
```

## 9. 下面的迭代会有什么问题？
```go
type Set struct {
  sync.RWMutex
  Ele []interface{}
}

func (p *Set) Iter() <-chan interface{} {
  // ch := make(chan interface{}) // 解除注释看看！
  ch := make(chan interface{}, len(p.Ele))
  go func() {
    p.RLock()
    for k, v := range p.Ele {
      ch <- k
      println("Iter:", k, v)
    }
    close(ch)
    p.RUnlock()
  }()
  return ch
}
func main() {
  th := Set{
    Ele: []interface{}{"1", "2"},
  }
  v := <-th.Iter()
  fmt.Println(v)
}
```

## 10. 以下代码能编译通过吗？
```go
type Person interface {
  Speak() string
}

type Student struct{}

func (stu *Student) Speak() string {
  return "hello"
}

func main() {
  var peo Person = Student{}
  fmt.Println(peo.Speak())
}

// output：
// Student does not implement Person (Speak method has pointer receiver)
```
> 考点： golang的方法集
> 
> 解答： `T`会生成`*T`的包装方法，反之不可。

## 11. 以下代码打印出来什么内容？

```go
type Student struct{}

func (Student) String() string {
  return "student"
}

func main() {
  var s *Student
  var obj fmt.Stringer = s

  if obj == nil {
    fmt.Println("A")
  } else {
    fmt.Println("B")
  }
}

// output:
// B
```
>   考点：interface内部结构
>
>   解答：go中的接口分为eface和iface两种，底层结构如下：

```go
type eface struct { // 空接口
  _type *_type         // 类型信息
  data  unsafe.Pointer // 指向数据的指针(go语言中特殊的指针类型unsafe.Pointer类似于c语言中的void*)
}
type iface struct { // 带有方法的接口
  tab  *itab          // 存储type信息还有结构实现方法的集合
  data unsafe.Pointer // 指向数据的指针(go语言中特殊的指针类型unsafe.Pointer类似于c语言中的void*)
}

type itab struct {
  inter  *interfacetype // 接口类型
  _type  *_type         // ⚠️ 结构类型
  link   *itab
  bad    int32
  inhash int32
  fun    [1]uintptr // ⚠️ 可变大小方法集合
}

type _type struct {
  size       uintptr // 类型大小
  ptrdata    uintptr // 前缀持有所有指针的内存大小
  hash       uint32  // 数据hash值
  tflag      tflag
  align      uint8    // 对齐
  fieldalign uint8    // 嵌入结构体时的对齐
  kind       uint8    // kind 有些枚举值kind等于0是无效的
  alg        *typeAlg // 函数指针数组，类型实现的所有方法
  gcdata     *byte
  str        nameOff
  ptrToThis  typeOff
}
```


## 12. 以下代码可以编译通过？
```go
func main() {
  o := GetValue()
  switch o.(type) {
  case int:
    fmt.Println("int")
  default:
    fmt.Println("other")
  }
}
func GetValue() int {
  return 1
}

// 编译失败：
// cannot type switch on non-interface value o (type int)
```
> 考点：类型断言，即类型断言的对象只能是 **interface{}** 类型

## 13. 下面代码有什么问题？
```go
func foo(x,y int)(sum int,error){    
  return x+y,nil
}
```

> 考点: 函数返回值命名
> 解析: 函数多返回值时，要么全部命名，要么全部不命名。

## 14. 下面代码输出什么？
```go
func foo1(i int) (t int) {
  t = i
  defer func() {
    t += 3
  }()
  return t
}

func foo2(i int) int {
  t := i
  defer func() {
    t += 3
  }()
  return t
}

func foo3(i int) (t int) {
  defer func() {
    t += i
  }()
  return 2
}

func main() {
  println(foo1(1))
  println(foo2(1))
  println(foo3(1))
}

// output:
// 4
// 1
// 3
```
> 考点： defer和函数返回值
>
> 解析： return可分解为三步：**保存返回值 => 执行defer => 返回结果**。 

## 15. 是否可以编译通过？
```go
func main() {
  list := new([]int)
  list = append(list,1)
  fmt.Println(list)
}
```
> 考点： new与make
> 
> 解析: 指针类型`new => &slice{}`区别于引用类型`make => slice{}`。

## 16. 是否可以编译通过？
```go
func main() {
  s1 := []int{1, 2, 3}
  s2 := []int{4, 5}
  s1 = append(s1, s2)
  fmt.Println(s1)
}
```
> 考点: append
>
> 解析: append切片时候别漏了`…`

## 17. 是否可以编译通过？
```go
func main() {
  t1 := struct{ name string }{"go"}
  t2 := struct{ name string }{"go"}

  if t1 == t2 {
    fmt.Println("t1 == t2")
  }

  t3 := struct{ dic map[int]string }{map[int]string{1: "a"}}
  t4 := struct{ dic map[int]string }{map[int]string{1: "a"}}

  if t3 == t4 {
    fmt.Println("t3 == t4")
  }

  if reflect.DeepEqual(t3, t4) {
    fmt.Println("t3 deep equal t4")
  }
}
```
> 考点: 结构体比较
>
> 解析: 
>
> 类型相同：**字段「类型相同」+「顺序相同」**；
>
> 数据相等：**「类型相同」+「字段可比较」**， 如指针，函数，clise，map等不可比大小。


## 18. 是否可以编译通过？
```go
func foo(x interface{}) {
  if x == nil {
    fmt.Println("A")
    return
  }
  fmt.Println("B")
}

func main() {
  var x *int = nil
  foo(x)
}
```
> 考点：interface内部结构，同11


## 19. 是否可以编译通过？
```go
func foo(m map[int]string, id int) (string, bool) {
  if _, exist := m[id]; exist {
    return "存在数据", true
  }
  return nil, false
}

// output:
// cannot use nil as type string in return argument
```
> 考点： 类型赋值


## 20. 下面代码输出什么？
```go
const (
  a = iota
  b
  c = "go"
  d
  e = iota
)

func main() {
  fmt.Println(a,b,c,d,e)
}

// output:
// 0 1 go go 4
```
> 考点：iota

## 21. 下面代码有什么问题？
```go
var (
  a    := 1024
  b = size * 2
)

func main() {
  println(a, b)
}

// output: syntaxerror: unexpected :=
```
> 考点: 变量简短模式。


## 22. 下面代码有什么问题？
```go
const a = 100
var b = 100

func main() {
  println(&a, a)
  println(&b, b)
}
```
> 考点: 常量不能取地址。

## 23. 下面代码有什么问题？
```go
func main() {
  for i := 0; i < 10; i++ {
  loop:
    println(i)
  }
  goto loop
}
```
> 考点：goto
>
> 解析: goto只能在其作用域内跳转。


## 24. 下面代码有什么问题？
```go
type TypeA int
type TypeB = int

func main() {
  var i int = 9
  var a TypeA = i
  var b TypeB = i

  fmt.Println(a, b)
}

// output:  cannot use i (type int) as type TypeA in assignment
```
> 考点：「别名」与「类型」。


## 25. 下面代码输出什么？
```go
type User struct{}
type UserA User
type UserB = User

func (i UserA) m1() {
  fmt.Println("UserA.m1")
}

func (i User) m2() {
  fmt.Println("User.m2")
}
func main() {
  var a UserA
  var b UserB

  a.m1()
  b.m2()
}

// output:
// UserA.m1
// User.m2
```
> 考点：「别名」与「类型」。

## 26. 下面代码有什么问题？
```go
type User struct{}

func (t User) Name() {
  fmt.Println("User.Name")
}

type Person = User
type Student struct {
  User
  Person
}

func main() {
  s := Student{}
  s.Name()
}

// output:
// ambiguous selector s.Name (模棱两可的选择器 s.Name)
```
> 解析: 编译异常。**方法**和**字段**都有可能ambiguous异常，可改为`s.Person.Name()`或`s.User.Name()`。


## 27. 下面代码输出什么？
```go
func foo(ok bool) (err error) {
  if ok {
    result, err := func() (int, error) {
      return -1, errors.New("sorry")
    }()
    _, _ = result, err
  }
  return err
}

func main() {
  fmt.Println(foo(true))
  fmt.Println(foo(false))
}
```
> 考点: 变量作用域
> 
> 解析: 因为if语句块的err变量会覆盖函数级err变量，即**内层变量优先级高于外层变量优先级**。

## 28. 编译执行下面代码会出现什么?
```go

func foo() []func() {
  var fs []func()
  for i := 0; i < 2; i++ {
    fs = append(fs, func() {
      fmt.Println(&i, i)
    })
  }
  return fs
}

func main() {
  fs := foo()
  for _, f := range fs {
    f()
  }
}

// output:
// 0xc0000b2008 2
// 0xc0000b2008 2
```
> 考点：闭包延迟求值
> 
> 解析：for循环复用局部变量i，每一次放入匿名函数的应用都是一个变量。可优化为：

```go
func foo() []func() {
  var fs []func()
  for i := 0; i < 2; i++ {
    x := i
    fs = append(fs, func() {
      fmt.Println(&x, x)
    })
  }
  return fs
}
```
## 29. 编译执行下面代码会出现什么?
```go
func foo(x int) (func(), func()) {
  return func() {
      fmt.Println(x)
      x += 10
    }, func() {
      fmt.Println(x)
    }
}

func main() {
  a, b := foo(100)
  a()
  b()
}

// output:
// 100
// 110
```
> 考点：闭包引用相同变量

## 30. 编译执行下面代码会出现什么?
```go
func main() {
  defer func() {
    if err := recover(); err != nil {
      f := err.(func() string) // error是一个函数类型
      fmt.Println(err, f(), reflect.TypeOf(err).Kind().String())
      return
    }
    fmt.Println("fatal")
  }()

  defer func() {
    panic(func() string { return "second panic" })
  }()
  
  panic("first panic")
}

// output:
// 0x108a3e0 second panic func
```

> 考点: revover仅捕获最后一个panic
>
> 解析: **`panic(interface{})`**, 最后一个panic抛出一个函数类型的异常。

## 31. 编译执行下面代码会出现什么?
```go
func Add() {
  d := map[string]string{
    "1001": "aaa",
    "1002": "bbb",
  }
  for k, v := range d {
    d[v] = k
  }
  fmt.Println(d)
}

func Append() {
  v := []int{1, 2, 3}
  for i := range v {
    v = append(v, i)
  }
  fmt.Println(v)
}

func Delete() {
  m := map[int]int{1: 1, 2: 2, 3: 3}
  for key := range m {
    if key%2 == 1 {
      delete(m, key)
    }
  }
  fmt.Println(m)
}
func main() {
  Add()
  Append()
  Delete()
}
```
> 考点: for-range
>
> 解析: range会对切片或字典做拷贝，新增的数据并不在拷贝内容中，并不会发生死循环。