
+++
title= "18. Go语言MySQL数据库操作"
url= "/post/1597480218/"
image= "/img/res/bg6.jpg"
date= 2020-08-14
categories= ["Go应用篇"]
archives= "2020"
author= "史布斯"
height= 1597480249
draft= false
+++

[TOC]

## 一、MySQL安装驱动

**Go官方没有提供数据库驱动**，最常用的开源MySQL数据库驱动非`Go-MySQL-Driver`莫属:

> - 维护比较好。
>
> - 完全支持database/sql接口。
>
> - 支持keepalive，保持长连接。
```shell
# 安装mysql驱动
go get -u github.com/go-sql-driver/mysql
```



## 二、MySQL基本操作

_测试数据：_

```sql
-- 创建库
create database if not exists demo default charset utf8;

-- 创建表
drop table if exists student;
CREATE TABLE `student` (
`id` int(10) NOT NULL AUTO_INCREMENT,
`name` varchar(20) DEFAULT NULL,
`age` int(10) DEFAULT NULL,
PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
```

### 1、打开连接

```go
import (
	"database/sql"
	_ "github.com/go-sql-driver/mysql"
)

// 建立数据库连接
func Open() *sql.DB {
	db, err := sql.Open("mysql", "root:123456@tcp(localhost:3306)/demo?charset=utf8mb4")
	errorHandler(err)
	return db
}
```

### 2、即时SQL

> 可使用`sql.DB`直接执行写入操作或查询操作：
>
> `func (db *DB) Exec(query string, args ...interface{}) (Result, error)`
> `func (db *DB) Query(query string, args ...interface{}) (*Rows, error)`

```go
// 插入数据
func TestInsert(t *testing.T) {
	db := Open()
	defer db.Close()

	db.Exec(`INSERT INTO student(name,age) VALUES(?,?),(?,?);`, "Lucy", 18, "Jack", 22)
}

// 修改数据
func TestUpdate(t *testing.T) {
	db := Open()
	defer db.Close()

	db.Exec(`UPDATE student SET age=18 WHERE name="Tom";`)
}

// 删除记录
func TestDelete(t *testing.T) {
	db := Open()
	defer db.Close()

	db.Exec(`DELETE FROM student WHERE age>20`)
}

// 查询记录
func TestQuery(t *testing.T) {
	db := Open()
	defer db.Close()

	rows, err := db.Query(`SELECT * FROM student;`)
	if err != nil {
		fmt.Println(err)
	}
	for rows.Next() {
		var name string
		var id int
		var age int
		if err := rows.Scan(&id, &name, &age); err != nil {
			fmt.Println(err)
		}
		fmt.Printf("%-4d %-10s %4d\n", id, name, age)
	}
}
```

### 3、预处理SQL

> 预处理SQL(`Prepared Statements`) 将SQL做了模版化/参数化处理 ，避免每次执行SQL都去执行`词法语义解析、语句优化、制定执行计划等`重复工作。
>
> 预处理SQL实现了：`一次编译、多次运行`，省去了`解析优化等过程`；此外预编译语句能`防止 SQL 注入`。
>
> 
>
> 使用`sql.DB`创建预处理语句，然后执行写入和查询命令
>
> `func (db *DB) Prepare(query string) (*Stmt, error)`
> `func (s *Stmt) Exec(args ...interface{}) (Result, error)`
> `func (s *Stmt) Query(args ...interface{}) (*Rows, error)`

```go

// 插入数据
func TestInsert(t *testing.T) {
	db := Open()
	defer db.Close()

	stmt, _ := db.Prepare("INSERT INTO student(name,age) VALUES(?,?);")
	stmt.Exec("如花", "22")
}

// 查询记录
func TestQuery(t *testing.T) {
	db := Open()
	stmt, _ := db.Prepare("SELECT * FROM student limit 1;")
    defer stmt.Close()
	defer db.Close()
    
	row := stmt.QueryRow()
	var name string
	var id int
	var age int
	row.Scan(&id, &name, &age)

	fmt.Println(id, name, age)
}

// 其余操作类似...
```



## 三、MySQL事务操作

> 事务是一个完整的RW单元，事务相关的方法如下：
>
> 
>
> `func (db *DB) Begin() (*Tx, error)`
> `func (tx *Tx) Commit() error`
> `func (tx *Tx) Rollback() error`
> `func (tx *Tx) Exec(query string, args ...interface{}) (Result, error)`
> `func (tx *Tx) Query(query string, args ...interface{}) (*Rows, error)`

```go
func TestTransaction(t *testing.T) {
    db := Open()
	defer db.Close()

	tx, err := db.Begin()
	if err != nil {
		panic(err)
	}
	values := [][]interface{}{{ "迪迦", 20}, { "赛罗", 16}, { "欧布", 10}}
	stmt, _ := tx.Prepare("INSERT INTO student(name,age) VALUES(?,?);")
	for _, val := range values {
		_, err := stmt.Exec(val...)
		if err != nil {
			fmt.Printf("INSERT failed：%v", err)
			tx.Rollback()
		}
	}
	tx.Commit()
}
```

## 四、MySQL操作的效率分析

### 1、sql接口效率分析

`func sql.Open(driverName, dataSourceName string) (*DB, error)`
sql.Open返回一个DB对象，DB对象对于多个goroutines并发使用是安全的，DB对象内部封装了连接池。Open函数并没有创建连接，只是验证参数是否合法，然后开启一个单独goroutine去监听是否需要建立新的连接，当有请求建立新连接时就创建新连接。
`func (db *DB) Exec(query string, args ...interface{}) (Result, error)`
执行不返回行（row）的查询，比如INSERT，UPDATE，DELETE
DB交给内部的exec方法负责查询。exec会首先调用DB内部的conn方法从连接池里面获得一个连接。然后检查内部的driver.Conn是否实现了Execer接口，如果实现了Execer接口，会调用Execer接口的Exec方法执行查询；否则调用Conn接口的Prepare方法负责查询。
`func (db *DB) Query(query string, args ...interface{}) (*Rows, error)`
用于查询，DB交给内部的query方法负责查询。query首先调用DB内部的conn方法从连接池里面获得一个连接，然后调用内部的queryConn方法负责查询。
`func (db *DB) Prepare(query string) (*Stmt, error)`
返回一个Stmt。Stmt对象可以执行Exec,Query,QueryRow等操作。DB交给内部的prepare方法负责查询。prepare首先调用DB内部的conn方法从连接池里面获得一个连接，然后调用driverConn的prepareLocked方法负责查询。
`func (db *DB) Begin() (*Tx, error)`
开启事务，返回Tx对象。调用Begin方法后，TX会与指定的连接绑定，一旦事务提交或者回滚，事务绑定的连接就还给DB的连接池。DB交给内部的begin方法负责处理。begin首先调用DB内部的conn方法从连接池里面获得一个连接，然后调用Conn接口的Begin方法获得一个TX。
**进行MySQL数据库操作时，如果每次SQL操作都从DB对象的连接池中获取连接，则会在很大程度上损耗效率。因此，必须尽量在一个连接上执行SQL操作。**

### 2、效率分析示例

```go
package main

import (
   "database/sql"
   "fmt"
   "strconv"
   "time"

   _ "github.com/go-sql-driver/mysql"
)

var db = &sql.DB{}

func init() {
   db, _ = sql.Open("mysql", "root:xxxxxx@tcp(118.24.159.133:3306)/student?charset=utf8")
   CREATE_TABLE := "CREATE TABLE student(" +
      "sid INT(10) NOT NULL AUTO_INCREMENT," +
      "sname VARCHAR(64) NULL DEFAULT NULL," +
      "age INT(10) DEFAULT NULL,PRIMARY KEY (sid))" +
      "ENGINE=InnoDB DEFAULT CHARSET=utf8;"
   db.Exec(CREATE_TABLE)
}

func update() {
   //方式1 update
   start := time.Now()
   for i := 1001; i <= 1100; i++ {
      db.Exec("UPDATE student set age=? where sid=? ", i, i)
   }
   end := time.Now()
   fmt.Println("db.Exec update total time:", end.Sub(start).Seconds())

   //方式2 update
   start = time.Now()
   for i := 1101; i <= 1200; i++ {
      stm, _ := db.Prepare("UPDATE student set age=? where sid=? ")
      stm.Exec(i, i)
      stm.Close()
   }
   end = time.Now()
   fmt.Println("db.Prepare 释放连接 update total time:", end.Sub(start).Seconds())

   //方式3 update
   start = time.Now()
   stm, _ := db.Prepare("UPDATE student set age=? where sid=?")
   for i := 1201; i <= 1300; i++ {
      stm.Exec(i, i)
   }
   stm.Close()
   end = time.Now()
   fmt.Println("db.Prepare 不释放连接 update total time:", end.Sub(start).Seconds())

   //方式4 update
   start = time.Now()
   tx, _ := db.Begin()
   for i := 1301; i <= 1400; i++ {
      tx.Exec("UPDATE student set age=? where sid=?", i, i)
   }
   tx.Commit()

   end = time.Now()
   fmt.Println("tx.Exec 不释放连接 update total time:", end.Sub(start).Seconds())

   //方式5 update
   start = time.Now()
   for i := 1401; i <= 1500; i++ {
      tx, _ := db.Begin()
      tx.Exec("UPDATE student set age=? where sid=?", i, i)
      tx.Commit()
   }
   end = time.Now()
   fmt.Println("tx.Exec 释放连接 update total time:", end.Sub(start).Seconds())
}

func delete() {
   //方式1 delete
   start := time.Now()
   for i := 1001; i <= 1100; i++ {
      db.Exec("DELETE FROM student WHERE sid=?", i)
   }
   end := time.Now()
   fmt.Println("db.Exec delete total time:", end.Sub(start).Seconds())

   //方式2 delete
   start = time.Now()
   for i := 1101; i <= 1200; i++ {
      stm, _ := db.Prepare("DELETE FROM student WHERE sid=?")
      stm.Exec(i)
      stm.Close()
   }
   end = time.Now()
   fmt.Println("db.Prepare 释放连接 delete total time:", end.Sub(start).Seconds())

   //方式3 delete
   start = time.Now()
   stm, _ := db.Prepare("DELETE FROM student WHERE sid=?")
   for i := 1201; i <= 1300; i++ {
      stm.Exec(i)
   }
   stm.Close()
   end = time.Now()
   fmt.Println("db.Prepare 不释放连接 delete total time:", end.Sub(start).Seconds())

   //方式4 delete
   start = time.Now()
   tx, _ := db.Begin()
   for i := 1301; i <= 1400; i++ {
      tx.Exec("DELETE FROM student WHERE sid=?", i)
   }
   tx.Commit()

   end = time.Now()
   fmt.Println("tx.Exec 不释放连接 delete total time:", end.Sub(start).Seconds())

   //方式5 delete
   start = time.Now()
   for i := 1401; i <= 1500; i++ {
      tx, _ := db.Begin()
      tx.Exec("DELETE FROM student WHERE sid=?", i)
      tx.Commit()
   }
   end = time.Now()
   fmt.Println("tx.Exec 释放连接 delete total time:", end.Sub(start).Seconds())

}

func query() {

   //方式1 query
   start := time.Now()
   rows, _ := db.Query("SELECT sid,sname FROM student")
   defer rows.Close()
   for rows.Next() {
      var name string
      var id int
      if err := rows.Scan(&id, &name); err != nil {
         fmt.Println(err)
      }
   }
   end := time.Now()
   fmt.Println("db.Query query total time:", end.Sub(start).Seconds())

   //方式2 query
   start = time.Now()
   stm, _ := db.Prepare("SELECT sid,sname FROM student")
   defer stm.Close()
   rows, _ = stm.Query()
   defer rows.Close()
   for rows.Next() {
      var name string
      var id int
      if err := rows.Scan(&id, &name); err != nil {
         fmt.Println(err)
      }
   }
   end = time.Now()
   fmt.Println("db.Prepare query total time:", end.Sub(start).Seconds())

   //方式3 query
   start = time.Now()
   tx, _ := db.Begin()
   defer tx.Commit()
   rows, _ = tx.Query("SELECT sid,sname FROM student")
   defer rows.Close()
   for rows.Next() {
      var name string
      var id int
      if err := rows.Scan(&id, &name); err != nil {
         fmt.Println(err)
      }
   }
   end = time.Now()
   fmt.Println("tx.Query query total time:", end.Sub(start).Seconds())
}

func insert() {

   //方式1 insert
   start := time.Now()
   for i := 1001; i <= 1100; i++ {
      //每次循环内部都会去连接池获取一个新的连接，效率低下
      db.Exec("INSERT INTO student(sid,sname,age) values(?,?,?)", i, "student"+strconv.Itoa(i), i-1000)
   }
   end := time.Now()
   fmt.Println("db.Exec insert total time:", end.Sub(start).Seconds())

   //方式2 insert
   start = time.Now()
   for i := 1101; i <= 1200; i++ {
      //Prepare函数每次循环内部都会去连接池获取一个新的连接，效率低下
      stm, _ := db.Prepare("INSERT INTO student(sid,sname,age) values(?,?,?)")
      stm.Exec(i, "student"+strconv.Itoa(i), i-1000)
      stm.Close()
   }
   end = time.Now()
   fmt.Println("db.Prepare 释放连接 insert total time:", end.Sub(start).Seconds())

   //方式3 insert
   start = time.Now()
   stm, _ := db.Prepare("INSERT INTO student(sid,sname,age) values(?,?,?)")
   for i := 1201; i <= 1300; i++ {
      //Exec内部并没有去获取连接，为什么效率还是低呢？
      stm.Exec(i, "user"+strconv.Itoa(i), i-1000)
   }
   stm.Close()
   end = time.Now()
   fmt.Println("db.Prepare 不释放连接 insert total time:", end.Sub(start).Seconds())

   //方式4 insert
   start = time.Now()
   //Begin函数内部会去获取连接
   tx, _ := db.Begin()
   for i := 1301; i <= 1400; i++ {
      //每次循环用的都是tx内部的连接，没有新建连接，效率高
      tx.Exec("INSERT INTO student(sid,sname,age) values(?,?,?)", i, "user"+strconv.Itoa(i), i-1000)
   }
   //最后释放tx内部的连接
   tx.Commit()

   end = time.Now()
   fmt.Println("tx.Exec 不释放连接 insert total time:", end.Sub(start).Seconds())

   //方式5 insert
   start = time.Now()
   for i := 1401; i <= 1500; i++ {
      //Begin函数每次循环内部都会去连接池获取一个新的连接，效率低下
      tx, _ := db.Begin()
      tx.Exec("INSERT INTO student(sid,sname,age) values(?,?,?)", i, "user"+strconv.Itoa(i), i-1000)
      //Commit执行后释放连接
      tx.Commit()
   }
   end = time.Now()
   fmt.Println("tx.Exec 释放连接 insert total time:", end.Sub(start).Seconds())
}

func main() {
   insert()
   query()
   update()
   query()
   delete()
}

// output:
// db.Exec insert total time: 2.069104068
// db.Prepare 释放连接 insert total time: 1.869348813
// db.Prepare 不释放连接 insert total time: 1.447833105
// tx.Exec 不释放连接 insert total time: 1.098540307
// tx.Exec 释放连接 insert total time: 3.465670469

// db.Query query total time: 0.005803479
// db.Prepare query total time: 0.010966584
// tx.Query query total time: 0.011800843
// db.Exec update total time: 2.117122871

// db.Prepare 释放连接 update total time: 2.132430998
// db.Prepare 不释放连接 update total time: 1.523685366
// tx.Exec 不释放连接 update total time: 1.346163272
// tx.Exec 释放连接 update total time: 3.129312377

// db.Query query total time: 0.00848425
// db.Prepare query total time: 0.013472261
// tx.Query query total time: 0.012418198

// db.Exec delete total time: 2.100008271
// db.Prepare 释放连接 delete total time: 1.9821439490000001
// db.Prepare 不释放连接 delete total time: 1.429259466
// tx.Exec 不释放连接 delete total time: 1.103143464
// tx.Exec 释放连接 delete total time: 2.863670582
```

从示例结果看，执行SQL操作时如果不释放连接，则效率比释放连接要高。