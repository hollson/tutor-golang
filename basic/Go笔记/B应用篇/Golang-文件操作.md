+++
title= "Golang文件与文本操作"
url= "/posts/gofile/" 
#keywords= ""
#description= ""
#image= "/img/res/xx.jpg"
date= 2022-02-08
categories= ["Go基础篇"]
tags= ["文件"]
archives= "2022"
author= "史布斯"
height= 1
#draft= false
+++

# 一. 操作系统
- Unix中，`目录即文件`，包括`存在、创建、读取(遍历)、修改、删除`

**1. 系统信息**
```go
func SysInfo() {
    //机器信息
   fmt.Println(os.Hostname()) //主机名

    //用户信息
   fmt.Println(os.Getuid())  //用户ID
   fmt.Println(os.Getgid())  //组ID
   fmt.Println(os.Geteuid()) //有效用户
   fmt.Println(os.Getegid()) //有效组

    //进程信息
   fmt.Println(os.Getpid())  //当前ID
   fmt.Println(os.Getppid()) //当前父ID

    //环境变量
   fmt.Println(os.Getenv("GOPATH"))                //获取环境变量的值
   os.Setenv("TEST", "test")                       //设置环境变量的值
   fmt.Println(strings.Join(os.Environ(), "\r\n")) //获取所有环境变量
}
```
**2. 目录操作**
```go
func MunuInfo() {
   fmt.Println(os.Getwd())   //当前目录
   fmt.Println(os.TempDir()) //临时目录

   //多个文件增删带All
   os.Mkdir("A", os.ModePerm)        //具体创建
   os.MkdirAll("A/B/C", os.ModePerm) //多级创建
   os.Remove("A/B/C")                //具体删除
   //os.RemoveAll("A")                 //多级删除

   os.Rename("A/B", "A/D")   //重命名目录
   os.Chdir("/Users/sybs/A") //切换目录
   fmt.Println(os.Getwd())
}
```
**3. 遍历文件**
```go
func Trave() {
   f, _ := os.Open("./A")
   defer f.Close()

   //f.Readdir：(目录+文件)
   fi, _ := f.Readdir(-1)
   for _, v := range fi {
      fmt.Println(v.Name())
   }

   //f.Readdirnames：遍历文件名
   names, _ := f.Readdirnames(-1)
   fmt.Println(names)
}
```
**4. 创建文件**
- `os.Create()`和`os.Open`都指向底层都`os.OpenFile()`方法
- `chmod 777 file (chmod u=rwx,g=rwx,o=rwx file)`
- `chmod 600 file (chmod u=rw,g=---,o=--- file)`
```go
// 是否存在
func IsExist(path string) bool {
   _, err := os.Stat(path)
   return err == nil || os.IsExist(err)
   //return !os.IsNotExist(err)
}

// 创建文件
func CreateFile() {
   path := "./a.txt"
   //1.是否存在：IsNotExist查找使用，可参考系统的各种实现
   if _, err := os.Stat(path); err != nil || os.IsNotExist(err) {
      log.Fatal("file not exist.")
   }

   //原始写法（O_TRUNC：打开并清空文件 ）
   f, _ := os.OpenFile("./a.txt", os.O_RDWR|os.O_CREATE|os.O_TRUNC, 0666)
   //2.创建文件
   f, _ = os.Create(path) //完全调用OpenFile

   //3.只读打开
   f, _ = os.Open(path) //只读调用OpenFile
   defer f.Close()

   //4.修改权限
   //f.Chmod(0777)
   f.Chmod(0644)
   f.Chown(0, 0)

   //5.重命名
   os.Rename(path, "./b.txt")
}
```
**5. 管道操作**
```go
func main() {
   r, w, _ := os.Pipe()
   w.WriteString("hello,world!")
   
   var s = make([]byte, 20)
   n, _ := r.Read(s)
   fmt.Println(string(s[:n])) //  hello,world!
}
```

<br/>

# 二. File系统

- 句柄：标识项目的唯一`16位无符号整数`,如：`模块、任务、内存块、菜单、字体、光标、调色板以及设备描述表`等
- 文件包含`设备文件`和`磁盘文件`
- File对象为`文件对象`，FileInfo为`文件信息` 
- `file.Stat`和`os.Stat`都能获取文件信息(`公共信息+系统依赖信息`)
**文件信息**
```go
//os.Stat根据操作系统的不同，引用不同的底层实现
//如 mac：usr/local/go/src/os/stat_darwin.go
//Darwin(达尔文)是由苹果电脑于2000年所释出的一个开放原始码操作系统
func main() {
   //文件对象
   path := "/Users/sybs/a.txt" //文件或目录
   file, _ := os.Open(path)
   defer file.Close()
   fmt.Println("文件句柄：", file.Fd())

   //文件详情
   info, _ := os.Stat(path) //方式1
   info, _ = file.Stat()    //方式2
   fmt.Println("文件名称：", info.Name())
   fmt.Println("文件大小：", info.Size())
   fmt.Println("文件权限：", info.Mode())
   fmt.Println("修改时间：", info.ModTime())
   fmt.Println("是目录吗：", info.IsDir())
   //spew.Dump(info) //格式化打印

   //系统依赖信息
   sys := info.Sys() //跟随操作系统的信息
   fmt.Println("系统类型", reflect.TypeOf(sys))
   if val, ok := sys.(*syscall.Stat_t); ok {
      fmt.Printf("%+v", val)
   }
}
```
**读取字节**
```go
func main() {
   path := "./a.txt"
   f, _ := os.Open(path)
   defer f.Close()

   //读内容：缓冲器
   buf := make([]byte, 1024)
   for n, _ := f.Read(buf); n != 0; n, _ = f.Read(buf) {
      fmt.Println(string(buf[:n]))
   }
}
```

## 2. 创建文件

```go
func main() {
   file, err := os.Create("a.txt")
   if err != nil {
      log.Fatalln(err)
   }
   defer file.Close()
}
```
```go
// 创建目录（参考bilibili）
func CreateDir(path string) (err error) {
   _, err = os.Stat(path)
   defer func() {
      if os.IsExist(err) {
         err = nil
      }
   }()
   if os.IsNotExist(err) {
      err = os.Mkdir(path, os.ModePerm)
   }
   return
}
```
# 文件读写
- N个读写器`os,file,io,ioutil,bufio`

  ## 3. 输入输出
```go
func main() {
   buf := make([]byte, 1024)
   n, _ := os.Stdin.Read(buf) //标准输入
   os.Stdout.Write(buf[:n])   //标准输出
}
```
# File文件
OS下的具体的磁盘文件

# 方式1：os包

```go
func main() {
   file, _ := os.Create("d:/test.log") //创建文件
   defer file.Close()

   num, _ := file.Write([]byte("hello"))  
   fmt.Printf("写入 %d 个字节n", num)
   file.Sync()
}
```

# 方式2：io包

```go
func main() {
   var filename= "d:/test.txt"
   file, _ := OpenOrCreateFile(filename)
   defer file.Close()
   num, _ := io.WriteString(file, "hello")
   fmt.Printf("写入%d个字节n", num)
}

func OpenOrCreateFile(filename string) (file *os.File, err error) {
   if FileExists(filename) {
      file, err = os.OpenFile(filename, os.O_APPEND, 0666)
   } else {
      file, err = os.Create(filename) //flag: O_RDWR|O_CREATE|O_TRUNC
   }
   return file, err
}
```

# 方式3：ioutil包

```go
func main() {
   err:= ioutil.WriteFile("d:/test.log", []byte("hello"), 0644)  //写数据
   check(err)

   bts, err := ioutil.ReadFile("d:/test.log")            //读数据
   check(err)
   fmt.Println(string(bts))
}
```
# 方式4：bufio包

```go
func main() {
   file, _ := OpenOrCreateFile("d:/test.log")
   defer file.Close()
   writer := bufio.NewWriter(file)
   defer writer.Flush()

   num, _ := writer.WriteString("hello golang")
   fmt.Printf("写入%d个字节n", num)
}
```

---

```go
package main

import (
   "fmt"
   "io"
   "os"
)

func Text(filename string) string {
   f, err := os.Open(filename)
   CheckErr(err)
   defer f.Close()

   var txt []byte
   buf := make([]byte, 1024)
   x:=0
   for {
      n, err := f.Read(buf)
      if CheckEof(err){break}
      txt = append(txt, buf[:n]...)
      x++
   }
   fmt.Println(x)
   return string(txt)
}
func main() {
   //fileurl := os.Getenv("HOME")
   //filename := fileurl + "/test.txt"
    h:= os.Getenv("HOME")
    fmt.Println(h)
   filename := "d:/test.txt"
   fmt.Println(Text(filename))
}

func CheckErr(err error) {
   if err != nil {
      fmt.Println(err.Error())
   }
}

func CheckEof(err error) bool {
   if err != nil && err == io.EOF {return true}
   return false
}
```

# 读写接口
https://www.cnblogs.com/shiluoliming/p/8312928.html
https://studygolang.com/articles/10552
https://studygolang.com/articles/12044?fr=sidebar
https://www.cnblogs.com/jkko123/p/7146474.html?utm_source=itdadao&utm_medium=referral


# 电子表格
```go
func main() {
   f, err := os.Create("test.csv")//创建文件
   if err != nil {
      panic(err)
   }
   defer f.Close()

   f.WriteString("\xEF\xBB\xBF") // 写入UTF-8 BOM

   w := csv.NewWriter(f)//创建一个新的写入文件流
   data := [][]string{
      {"1", "中国", "23"},
      {"2", "美国", "23"},
      {"3", "bb", "23"},
      {"4", "bb", "23"},
      {"5", "bb", "23"},
   }
   w.WriteAll(data)//写入数据
   w.Flush()
}
```

```go
func main() {
   file, _ := os.OpenFile("test.csv", os.O_WRONLY|os.O_CREATE, os.ModePerm)
   w := csv.NewWriter(file)
   w.Write([]string{"123", "234234", "345345", "234234"})
   w.Flush()
   file.Close()
   rfile, _ := os.Open("test.csv")
   r := csv.NewReader(rfile)
   strs, _ := r.Read()
   for _, str := range strs {
      fmt.Print(str, "\t")
   }
}
```

```go
func main() {
 // 1.创建XLS文件
 var xlsx_file = xlsx.NewFile()
 sheet, _ := xlsx_file.AddSheet("订单报表")

 // 添加标题
 titles := []string{"序号", "单号", "金额", "时间"}
 row := sheet.AddRow()
 for _, v := range titles {
  row.AddCell().Value = v
 }

 // 添加数据
 row = sheet.AddRow()
 row.AddCell().Value = "1001"
 row.AddCell().Value = "DD01"
 row.AddCell().Value = "11"
 row.AddCell().Value = "2019-01-01 00:00:00"

 row = sheet.AddRow()
 row.AddCell().Value = "1002"
 row.AddCell().Value = "DD02"
 row.AddCell().Value = "22"
 row.AddCell().Value = "2019-01-02 00:00:00"

 // 2.将文件写入Buffer
 buf := bytes.NewBuffer(make([]byte, 1024))
 if err := xlsx_file.Write(bufio.NewWriter(buf)); err != nil {
  log.Fatal(err)
 }

 // 3.存入阿里云或本地文件
 var xls_path = fmt.Sprintf("%s.xlsx", time.Now().Format("20060102150405"))
 if err := oss.AliOSS.Client.PutObject(oss.AliOSS.BucketName, xls_path,
  strings.NewReader(buf.String())); err != nil {
 }
 backUrl := oss.GetAliOssURL(fmt.Sprintf("%s%s", alioss.AliossPrefix, xls_path))
 fmt.Println(backUrl)

 //xls_path := fmt.Sprintf("./%s.xlsx", time.Now().Format("20060102150405"))
 //xlsx_file.Write(bufio.NewWriter(buf))
}
```

# sdfaf
https://www.jianshu.com/p/758c4e2b4ab8

# 遍历目录
```go
func main() {
    //方式一
    filepath.Walk("temp/", func (path string, info os.FileInfo, err error) error {
        fmt.Println(path)
        return nil
    })

    //方式二
    getFileList("temp/")
}

func getFileList(path string) {
    fs,_:= ioutil.ReadDir(path)
    for _,file:=range fs{
        if file.IsDir(){
            fmt.Println(path+file.Name())
            getFileList(path+file.Name()+"/")
        }else{
            fmt.Println(path+file.Name())
        }
    }
}
```



