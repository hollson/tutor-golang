+++
title= "sync.errgroup 源码解读"
url= "/posts/f0e1186f6b7f094003d14539f38425d6/"  
#aliases = ["/post/1586871522/"]
keywords= "errgroup,错误,异常"
description= "包errgroup可以为处理常见任务的子任务的goroutine组提供同步，错误传播和上下文取消。"
image= "/img/res/bg5.jpg"
date= 2020-03-08T21:38:42+08:00
lastmod= 2020-03-08T21:38:42+08:00
categories= ["Go高级篇"]
tags= ["Go解密"]
archives= "2020"
author= "史布斯"
height= 1586871522
draft= false
+++





## 前言
在启动多个Go协程并发的时候，处理子协程的异常是一件麻烦的事，你可能得需要通过多个类似`<-error.New(...)`向main协程发送错误，然后使用`select`IO监听器集中处理。然大量的子协程，使得`select`模板臃肿不堪，也极易发生疏漏。好在go为我们提供了`sync.errgroup`来优雅的处理此事。


## 简单示例
```go
import "golang.org/x/sync/errgroup"

func main() {
    var eg errgroup.Group

    var urls = []string{
        "http://www.baidu.com/",
        "http://www.errors01.com/", //错误地址
        "http://www.errors02.com/", //错误地址
    }

    for _, url := range urls {
        url := url  //注意 ⚠️
        eg.Go(func() error {
            fmt.Println(url)
             resp, err := http.Get(url)
            if err == nil {
                resp.Body.Close()  //注意关闭
            }
            return err
        })
    }

    err := eg.Wait()
    if err != nil {
        fmt.Printf("err: %v", err)
        return
    }
    fmt.Println("Successfully fetched all URLs.")
}
```
> errgroup使用很简单，声明一个`errgroup.Group`变量，使用`eg.Go`回调函数启动多个并发协程（也不用类型`wg.Done()`的处理），然后eg.Wait()等待接受错误。


## Group对象
[errgroup.Group源码](https://github.com/golang/sync/blob/43a5402ce75a/errgroup/errgroup.go#L18-L25)也是非常简单的：
```go
type Group struct {
    cancel func()

    wg sync.WaitGroup

    errOnce sync.Once
    err     error
}
```
- `err error` 保存第一个错误
- `errOnce sync.Once` errOnce可以保证它的回调函数只执行一次，这正是我们需要的，只返回第一个错误
- `wg sync.WaitGroup` 可以等待所有go程结束
- `cancel func()` 这里保存的是contex.WithCancel返回的第二个参数。
  总结: 变量`wg`可以保证在所有go程结束，这时候返回err的值。在赋值的时候，只需要第一个值，这里用到`errOnce`变量保证(可以看下上篇的`sync.Once`源代码解析)。`cancel`有值的情况下，回调函数出错会被调用(`context.Context`现在有点chan里面软中断的意味)。



## WithContext接口

这接口主要是保存context.WithCancel返回的结果



```go
func WithContext(ctx context.Context) (*Group, context.Context) {
    ctx, cancel := context.WithCancel(ctx)
    return &Group{cancel: cancel}, ctx
}
```

## Wait接口

这里使用了`g.wg.Wait`函数等待所有go程结束，err有值，意味着出错，我们通过return告诉调用端。如果设置了`cancel`，我们通过调用`cancel`关闭这个`context.Context`



```go
func (g *Group) Wait() error {
    g.wg.Wait()
    if g.cancel != nil {
        g.cancel()
    }
    return g.err
}
```

## Go接口

- 首先，要保证g.wg.Wait调用正确，在go程起之前调用`g.wg.Add(1)`加上1个，结束之后通过`defer g.wg.Done()`减去1个。
- 无错是相安无事，这里看下出错。`g.errOnce.DO`保证回调函数会被执行一次。只做一次的事，就是把第一个错误收集起来`g.err = err`， 如果设置了cancel也关闭了`g.cancel()`



```go
func (g *Group) Go(f func() error) {
    g.wg.Add(1)

    go func() {
        defer g.wg.Done()

        if err := f(); err != nil {
            g.errOnce.Do(func() {
                g.err = err
                if g.cancel != nil {
                    g.cancel()
                }
            })
        }
    }()
}
```

## 参考链接
https://pkg.go.dev/golang.org/x/sync/errgroup?tab=doc
https://golang.org/doc/faq#closures_and_goroutines
https://blog.csdn.net/Raily_Qi/article/details/99979570
https://blog.csdn.net/henreash/article/details/103025067

