+++
title= "Option编程模式"
url= "/posts/848cd59ca42c13602cf7b0ddf6ca1d1f/"  
#aliases = ["/post/1594301647/"]
keywords= "option,编程模式"
description= "Option模式的专业术语为：Functional Options Pattern（函数式选项模式）"
image= "/img/res/blog.jpg"
date= 2020-07-09T21:34:07+08:00
lastmod= 2020-07-09T21:34:07+08:00
categories= ["Go高级篇"]
tags= []
archives= "2020"
author= "史布斯"
height= 1594301647
draft= false
+++

## 什么是Option模式
> Option模式的专业术语为：Functional Options Pattern（函数式选项模式）
Option模式为golang的开发者提供了将一个函数的参数设置为可选的功能，也就是说我们可以选择参数中的某几个，并且可以按任意顺序传入参数。
比如针对特殊场景需要不同参数的情况，C++可以直接用重载来写出任意个同名函数，在任意场景调用的时候使用同一个函数名即可；但同样情况下，在golang中我们就必须在不同的场景使用不同的函数，并且参数传递方式可能不同的人写出来是不同的样子，这将导致代码可读性差，维护性差。


## 代码示例
**./mylog/logger.go**
```go
package mylog

import (
  "fmt"
  "io"
  "os"
  "time"
)

// 模拟一个redis客户端对象,一般为私有类型
type logger struct {
  Level  int // 级别
  Writer io.Writer
}

// 创建主体对象，使用【回调函数】设置对象值
func New(opts ...Option) *logger {
  instance := &logger{
    Level:  1,
    Writer: os.Stdout,
  }

  // 循环执行回调函数，达到覆盖默认值等目的
  for _, v := range opts {
    v(instance)
  }

  return instance
}

func (p *logger) Println(v ...interface{}) {
  s:= fmt.Sprintf("消息: %s %v\n",time.Now().Format("2006-01-02 15:04:05"),v)
  p.Writer.Write([]byte(s))
}

func (p *logger) Fatalln(v ...interface{}) {
  s:= fmt.Sprintf("错误: %s %v\n",time.Now().Format("2006-01-02 15:04:05"),v)
  p.Writer.Write([]byte(s))
}
```
**./mylog/option.go**
```go
package mylog

import (
  "io"
)

// 函数式编程，是一个函数类型，把主体关联起来
type Option func(opt *logger)

// 【闭包函数】修改主体字段值
func WithLevel(level int) Option {
  return func(opt *logger) {
    opt.Level = level
  }
}

// 【闭包函数】修改主体字段值
func WithWriter(w io.Writer) Option {
  return func(opt *logger) {
    opt.Writer = w
  }
}
```
**./main.go**
```go
func main() {
  log := mylog.New(mylog.WithLevel(1))
  log.Println("hello")
  log.Fatalln("sorry")

  file, err := os.OpenFile("./dump.log", os.O_CREATE|os.O_WRONLY|os.O_APPEND, 766)
  if err!=nil{
    panic(err)
  }
  log = mylog.New(mylog.WithLevel(3), mylog.WithWriter(file))
  log.Println("你好")
  log.Fatalln("抱歉")
}
```

## 参考：
> https://www.cnblogs.com/zhichaoma/p/12509110.html
>
> https://halls-of-valhalla.org/beta/articles/functional-options-pattern-in-go,54/
