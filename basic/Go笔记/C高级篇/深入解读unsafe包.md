+++
title= "深入解读unsafe包"
url= "/post/1586082803/"
#aliases = ["/posts/54d1416de84b6a95cfcf7b0c475cd742/"]
keywords= "unsafe,转换"
description= "unsafe提供了类型转换，通过指针修改字段值，修改私有变量等能力，可以通过unsafe查看类型大小、内存偏移量、对齐值等。它是一种危险的操作，但是在某些情况下有特别有用。"
image= "/img/res/bg2.jpg"
date= 2020-04-05
lastmod= 2020-04-06T18:33:23+08:00
categories= ["Go高级篇"]
tags= ["Go解密"]
archives= "2020"
author= "史布斯"
height= 1586082803
draft= false

+++

## 一. 关于 unsafe 包

Unsafe 包主要用于`指针计算`和指`针类型转换`(具体可以查阅[**unsafe 源码**](https://github.com/golang/go/blob/master/src/unsafe/unsafe.go)或[**unsafe 文档**](https://golang.org/pkg/unsafe/)）。unsafe 包源码如下：

```go
package unsafe

// 它表示任意Go表达式的类型。
type ArbitraryType int

//任意类型指针，类似于语言的*void
type Pointer *ArbitraryType

// 大小
func Sizeof(x ArbitraryType) uintptr

// 偏移量
func Offsetof(x ArbitraryType) uintptr

// 对齐值
func Alignof(x ArbitraryType) uintptr
```

## 二. 地址类型 uintptr

`uintptr`是 Go 语言`内置数据类型`，源码注释如下：

> uintptr is an integer type that is large enough to hold the bit pattern of any pointer.
>
> 即：uintptr 是一个足够大的`整数类型`，可以`容纳`任何`指针`的位模式。

说明：

1. uintptr 不属于指针，也不属于 unsafe，它只是指针运算的`中间变量或容器`。

1. uintptr 是整数类型，所以它可以参与数据(内存地址)计算,也可能被 GC 回收。

1. unsafe 中的 uintptr 是`常量`，类似的还有`cap`、`len`、`complex`等。

```go
const size = unsafe.Sizeof(int64(1))
const com = complex(1.2, 3.4)
const lenght= len([3]int{}) //数组长度(相当于常量)
const capacity= cap([3]int{})
```

## 三. 任意类型 Arbitrary

> Arbitrary`[ˈɑːrbɪtreri]` 即任意的。`ArbitraryType`它只是声明了一个 int 的类型，相当于一个`占位符`。

### 1. 类型大小 Sizeof

返回类型描述符的字节大小（与数据无关）

```go
func main() {
    type T struct {
        t1 bool
        t2 byte
        t3 int64
        t4 string
    }
    t := T{true, 1, 2, ""}

    fmt.Println("===字段尺寸===")
    fmt.Println(unsafe.Sizeof(t.t1)) //1 => 4
    fmt.Println(unsafe.Sizeof(t.t2)) //1 => 4
    fmt.Println(unsafe.Sizeof(t.t3)) //8
    fmt.Println(unsafe.Sizeof(t.t4)) //16

    fmt.Println("===结构体尺寸===")
    fmt.Println(unsafe.Sizeof(t)) //32(发生了4倍对齐)

    fmt.Println("===指针尺寸===")
    fmt.Println(unsafe.Sizeof(&t)) //4或8，跟系统相关

    fmt.Println("===引用尺寸===")
    s1 := []byte{1, 2, 3}
    s2 := []int64{1, 2, 3, 4, 5, 6}
    m1 := map[byte]float32{1: 1.1}
    m2 := map[int32]float64{1: 1.1, 2: 2.2}
    fmt.Println(unsafe.Sizeof(s1), unsafe.Sizeof(s2)) //24 24
    fmt.Println(unsafe.Sizeof(m1), unsafe.Sizeof(m2)) //8 8
}
```

> 特别注意的是，结构体尺寸发生了`2^n`对齐；其次，指针类型跟平台相关，即 uintptr 是跟平台相关的值。

### 2. 字段对齐 Alignof

```go
func main() {
    type T struct {
        t1 byte
        t2 int32
    }
    var t T
    var tp *T

    fmt.Println(unsafe.Alignof(t.t1)) //1
    fmt.Println(unsafe.Alignof(t.t2)) //4
    fmt.Println(unsafe.Alignof(t))    //4
    fmt.Println(unsafe.Alignof(tp))   //4或8，跟系统相关

    fmt.Println("===反射对齐===")
    fmt.Println(reflect.TypeOf(t).Align())         //4  类型对齐
    fmt.Println(reflect.TypeOf(t.t1).Align())      //1
    fmt.Println(reflect.TypeOf(tp).Elem().Align()) //4
}
```

### 3. 偏移量 Offsetof

```go
func main() {
    type T struct {
        t1 int64   // => 0
        t2 byte    // => 8
        t3 string  // 8+1=9 => 16
        t4 []int32 // 16+16=32
        t5 float64 // 32+16=48 => 56
    }
    t := T{}
    fmt.Println(unsafe.Offsetof(t.t1))
    fmt.Println(unsafe.Offsetof(t.t2))
    fmt.Println(unsafe.Offsetof(t.t3))
    fmt.Println(unsafe.Offsetof(t.t4))
    fmt.Println(unsafe.Offsetof(t.t5))
}
```

> 注意，偏移量是以`2^n`对齐方式递增的。

## 四. 空指针 Pointer

Pointer 表示**指向任意类型的指针**(类似于 C 语言里的`void*`指针)。 Pointer 类型有四种特殊操作 ：

> - 任何类型的`指针值`都可以转换为 Pointer。
> - Pointer 可以转换为任何类型的`指针值`。
> - uintptr 可以转换为 Pointer。
> - Pointer 可以转换为 uintptr。

![unsafe](/img/post/640.png)

因此，指针允许程序破坏类型系统并读写任意内存。 使用时应格外小心。

**示例 1: 通过 Pointer 修改对象**

```go
func main() {
    var v = struct {
        A int32
        B int64
    }{1, 2}

    p := unsafe.Pointer(&v) // *T     ->  Pointer
    i32 := (*int32)(p)      // Pointer -> *T
    *i32 += 10
    fmt.Println(v)

    up := uintptr(p) + unsafe.Offsetof(v.B) // Pointer -> uintptr
    p = unsafe.Pointer(up)                  // uintptr -> Pointer
    i64 := (*int64)(p)
    *i64 += 10
    fmt.Println(v)
}
```

> Pointer 充当了一个`通用类型转换器`的角色, 不同类型的互转都得通过 Pointer 转接。

**示例 2: 结构体类型换转**

```go
type A struct {
    A int8
    B string
    C float32

}

type B struct {
    D int8
    E string
    F float32
}

func main() {
    a := A{1,  `foo`,  1.23}
    //b := B(a) //不能以这种方式转换
    b := *(*B)(unsafe.Pointer(&a))
    println(b.D, b.E, b.F)
}
```

> 太无耻了，字段名不一样也能转换 😂😂😂

**示例 3: 修改私有成员**

```go
// 定义一个和 strings包中的Reader相同的本地结构体
type Reader struct {
    s        string
    i        int64
    prevRune int
}

func main() {
    sr := strings.NewReader("abcdef")
    p := unsafe.Pointer(sr)

    pR := (*Reader)(p) //将源结构体转换成系统结构的本地Reader结构体
    (*pR).i = 3        //修改sr中的私有成员了

    fmt.Println(sr)
    b, err := sr.ReadByte()
    fmt.Printf("%c, %v\n", b, err)
}
```

## 参考链接：

> https://golang.org/pkg/unsafe/ > https://go101.org/article/unsafe.html > https://medium.com/a-journey-with-go/go-what-is-the-unsafe-package-d2443da36350 > https://segmentfault.com/a/1190000017389782 > https://www.pixelstech.net/article/1584241521-Understand-unsafe-in-GoLang
