+++
title= "深度揭秘之sync.Pool"
url= "/posts/702022020553/" 
#keywords= "golang,c常量"
description= "Pool是一组可以单独保存和读取的临时对象,Pool中的对象可能(在未通知的情况下)随时被自动删除。如果在发生这种情况时 Pool 拥有唯一的引用，则该项目可能会被释放。"
image= "/img/res/bg1.jpg"
date= 2022-02-07T15:36:02+08:00
categories= ["Go高级篇"]
tags= ["Go解密"]
archives= "2022"
author= "史布斯"
height= 1
#draft= false
+++

## Pool定义

```go
// Pool是一组可以单独保存和读取的临时对象。
// 
// Pool中的对象可能(在未通知的情况下)随时被自动删除。如果在发生这种情况时 Pool 拥有唯一的引用，则该项目可能会被释放。
// 
// Pool在多协程下使用是安全的。
// 
// Pool用于缓存未使用的对象，以减轻GC压力。即Pool用于构建高效、线程安全的空闲列表。
// 
// 在同一package中独立运行的多个独立线程之间静默共享一组临时元素才是pool的合理使用场景。Pool提供在多个独立client之间共享临时元素的机制。
// 
// 在fmt包中有一个使用Pool的例子，它维护了一个动态大小的输出buffer。
// 
// 一些短生命周期的对象不适合用Pool，因为使用它们自己的free list可能更高效。
// 
// 首次使用后不得复制Pool。
type Pool struct {
    noCopy noCopy //空结构，用来防止pool在第一次使用时被复制。

    local     unsafe.Pointer // 本地固定大小的pool池，其实类型为[P]poolLocal
    localSize uintptr        // 本地固定pool池的大小

    victim     unsafe.Pointer // 表示上一个周期的local
    victimSize uintptr        // 同上

    // New在pool中没有获取到，调用该方法生成一个变量
    New func() interface{}
}
```



## Get方法

-   Pool会为每个P维护一个本地池，P的本地池分为「`私有池private`」和「`共享池shared`」。

-   私有池中的元素只能被本地P使用，共享池中的元素可能会被其他P偷走，所以**使用私有池private时不用加锁，而使用共享池shared时需加锁**。

-   Get**优先查找本地private，再查找本地shared，最后查找其他P的shared**，如果以上全部没有可用元素，最后会调用`New`函数获取新元素。

```go
func (p *Pool) Get() interface{} {
    if race.Enabled {
        race.Disable()
    }
    // 获取当前p的pool（池）
    l, pid := p.pin()
    x := l.private
    l.private = nil
    if x == nil {
        // 从当前 P 的 shared 末尾取一个
        x, _ = l.shared.popHead()
        if x == nil {
        // 还没有取到 则去其他P的shared 取
            x = p.getSlow(pid)
        }
    }
    runtime_procUnpin()
    if race.Enabled {
        race.Enable()
        if x != nil {
            race.Acquire(poolRaceAddr(x))
        }
    }
    // 最后还没取到 调用 NEW 方法生成一个
    if x == nil && p.New != nil {
        x = p.New()
    }
    return x
}
```

```go
//从其他 P 中的 shared 池中获取可用元素
func (p *Pool) getSlow() (x interface{}) {
   // See the comment in pin regarding ordering of the loads.
   size := atomic.LoadUintptr(&p.localSize) // load-acquire
   local := p.local                         // load-consume
   // Try to steal one element from other procs.
   pid := runtime_procPin()
   runtime_procUnpin()
   for i := 0; i < int(size); i++ {
      l := indexLocal(local, (pid+i+1)%int(size))
      // 对应 pool 需加锁
      l.Lock()
      last := len(l.shared) - 1
      if last >= 0 {
         x = l.shared[last]
         l.shared = l.shared[:last]
         l.Unlock()
         break
      }
      l.Unlock()
   }
   return x
}
```

## Put方法

-   Put优先把元素放在private池中；如果private不为空，则放在shared池中。有趣的是，在入池之前，该元素有`1/4`可能被丢掉。

```go
func (p *Pool) Put(x interface{}) {
    ...
    //获取当前p的pool(池)
    l, _ := p.pin()
    if l.private == nil {
    // 如果私有属性为nil，那么将x写入到private
        l.private = x
        x = nil
    }
    if x != nil {
    //如果x没有复制给私有属性，则将其推到共享属性中
        l.shared.pushHead(x)
    }
    runtime_procUnpin()
    if race.Enabled {
        race.Enable()
    }
}
```

## poolCleanup

-   GC将要开始时， poolCleanup会被调用。该函数内不能分配内存且不能调用任何运行时函数。
-   如果GC发生时，某个goroutine正在访问 l.shared，整个Pool将会保留，下次执行时将会有双倍内存

```go
func poolCleanup() {  
   for i, p := range allPools {
      allPools[i] = nil
      for i := 0; i < int(p.localSize); i++ {
         l := indexLocal(p.local, i)
         l.private = nil
         for j := range l.shared {
            l.shared[j] = nil
         }
         l.shared = nil
      }
      p.local = nil
      p.localSize = 0
   }
   allPools = []*Pool{}
}
```

## 案例1: gin中的Context pool

>   在web应用中，后台在处理用户的每条请求时都会为当前请求创建一个上下文环境Context,用于存储请求信息信息。Context满足长生命周期的特点，且用户请求也是属于并发环境，所以对于线程安全的 Pool 非常适合用来维护Context的临时对象池。

gin 在结构体 Engine 中定义了一个 pool:

```go
type Engine struct {
   //... 
   pool             sync.Pool
}
```

初始化engine时定义了pool的New函数：

```go
engine.pool.New = func() interface{} {
   return engine.allocateContext()
}

// allocateContext
func (engine *Engine) allocateContext() *Context {
   // 构造新的上下文对象
   return &Context{engine: engine}
}
```

ServeHttp:

```go
// 从 pool 中获取，并转化为 *Context
c := engine.pool.Get().(*Context)
c.writermem.reset(w)
c.Request = req
c.reset()  // reset

engine.handleHTTPRequest(c)

// 再扔回 pool 中
engine.pool.Put(c)
```

## 案例2: fmt中的printer pool

>   printer也符合长生命周期的特点，同时也会可能会在多goroutine中使用，所以也适合使用pool来维护。

printer与它的临时对象池

```go
// pp 用来维护 printer 的状态
// 它通过 sync.Pool 来重用，避免申请内存
type pp struct {
   //... 字段已省略
}

var ppFree = sync.Pool{
   New: func() interface{} { return new(pp) },
}
```

获取与释放：

```go
func newPrinter() *pp {
   p := ppFree.Get().(*pp)
   p.panicking = false
   p.erroring = false
   p.fmt.init(&p.buf)
   return p
}

func (p *pp) free() {
   p.buf = p.buf[:0]
   p.arg = nil
   p.value = reflect.Value{}
   ppFree.Put(p)
}
```

## 总结
- Pool用于缓存未使用的对象，以减轻GC压力
- Pool有1个私有池和多个共享池
- 适合长生命周期的对象，如http.Context
- 并发安全(私有池不加锁,共享池加锁)
- 先访问私有池，再访问其他公共池
- Pool会被GC回收，不能用于数据库连接等
