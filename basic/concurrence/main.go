package main

import "fmt"

// Payment 函数签名
type Payment func()

// Paypal 支付函数
func Paypal() {
	fmt.Println("Paypal支付成功")
}

// PayDecorator 添加函数装饰器
func PayDecorator(f Payment) Payment {
	return func() {
		fmt.Println("=> 开始执行...")
		f()
		fmt.Println("=> 执行结束...")
	}
}

// 装饰器模式是一种结构型设计模式，它允许在不修改现有对象结构的情况下，动态地向对象添加新的功能。
func main() {
	pay := PayDecorator(Paypal)
	pay()
}
