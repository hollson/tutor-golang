以下是`context`在实际项目中的一些应用案例：

**一、HTTP服务器中的请求处理**

1. **超时控制**
	- 在一个Web服务器中，当处理一个HTTP请求时，可能会涉及到多个子操作，如查询数据库、调用外部API等。如果某个操作耗时过长，可能会导致整个服务器的资源被占用，影响其他请求的处理。
	- 示例：
   ```go
   package main

   import (
       "context"
       "fmt"
       "net/http"
       "time"
   )

   func handleRequest(w http.ResponseWriter, r *http.Request) {
       // 为每个请求创建一个上下文，设置超时时间为5秒
       ctx, cancel := context.WithTimeout(r.Context(), 5*time.Second)
       defer cancel()

       // 模拟一个耗时的操作，比如查询数据库
       go func() {
           time.Sleep(6 * time.Second)
           fmt.Println("Database query completed.")
       }()

       // 在主处理逻辑中等待操作完成或者超时
       select {
       case <-ctx.Done():
           http.Error(w, "Request timed out", http.StatusGatewayTimeout)
       }
   }

   func main() {
       http.HandleFunc("/", handleRequest)
       http.ListenAndServe(":8080", nil)
   }
   ```
   在这个示例中，对于每个HTTP请求，创建了一个带有5秒超时的上下文。如果模拟的数据库查询操作（这里通过`time.Sleep`模拟耗时操作）超过了5秒，请求就会被视为超时，返回`504 Gateway Timeout`错误给客户端。

2. **请求级别的数据传递**
	- 假设在一个Web应用中，需要在多个中间件和处理函数之间传递用户相关的信息（如用户ID）。
	- 示例：
   ```go
   package main

   import (
       "context"
       "fmt"
       "net/http"
   )

   func middleware(next http.Handler) http.Handler {
       return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
           // 从请求中获取用户ID（这里简化为从查询参数获取）
           userID := r.URL.Query().Get("user_id")
           // 将用户ID添加到上下文
           ctx := context.WithValue(r.Context(), "user_id", userID)
           next.ServeHTTP(w, r.WithContext(ctx))
       })
   }

   func handleRequest(w http.ResponseWriter, r *http.Request) {
       // 从上下文中获取用户ID
       userID := r.Context().Value("user_id").(string)
       fmt.Fprintf(w, "Hello, user %s", userID)
   }

   func main() {
       mux := http.NewServeMux()
       mux.Handle("/", middleware(http.HandlerFunc(handleRequest)))
       http.ListenAndServe(":8080", mux)
   }
   ```
   这里，中间件`middleware`从HTTP请求中获取用户ID，并将其添加到请求的上下文中。然后在处理函数`handleRequest`中，可以从上下文中获取用户ID并进行相应的操作。

**二、分布式系统中的任务调度**

1. **任务取消**
	- 在一个分布式任务调度系统中，可能会有多个工作节点（`worker`）执行任务。如果某个任务需要被提前终止（例如，因为用户取消了操作或者系统资源不足），可以使用`context`来传播取消信号。
	- 示例：
   ```go
   package main

   import (
       "context"
       "fmt"
       "sync"
       "time"
   )

   func worker(ctx context.Context, taskID int, wg *sync.WaitGroup) {
       defer wg.Done()
       for {
           select {
           case <-ctx.Done():
               fmt.Printf("Worker %d: Task cancelled\n", taskID)
               return
           default:
               fmt.Printf("Worker %d: Working...\n", taskID)
               time.Sleep(1 * time.Second)
           }
       }
   }

   func main() {
       var wg sync.WaitGroup
       ctx, cancel := context.WithCancel(context.Background())

       // 启动3个工作节点
       for i := 1; i <= 3; i++ {
           wg.Add(1)
           go worker(ctx, i, &wg)
       }

       // 假设3秒后需要取消任务
       time.Sleep(3 * time.Second)
       cancel()

       wg.Wait()
       fmt.Println("All tasks cancelled.")
   }
   ```
   在这个例子中，创建了3个工作节点（`worker`），每个`worker`在一个独立的`goroutine`中运行。主函数创建了一个可取消的上下文，3秒后调用`cancel`函数，这会导致所有`worker`中的`ctx.Done()`通道被关闭，从而使`worker`停止工作。