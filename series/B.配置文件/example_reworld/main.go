// Copyright 2021 Hollson. All rights reserved.
// Use of this source code is governed by a MIT style
// license that can be found in the LICENSE file.

package main

import (
	"fmt"
	"log"

	"config_demos/example_reworld/config"
)

func main() {
	if err := config.Init("./config/example.toml"); err != nil {
		log.Fatalf("配置文件路径错误,%v", err)
	}

	fmt.Println(config.Name)
	fmt.Println(config.System)
	fmt.Println(config.Service)
	fmt.Println(config.Repo["logic"].Slave)
	fmt.Println(config.Log)
	fmt.Println(config.Redis)
	fmt.Println(config.Kafka)
}
