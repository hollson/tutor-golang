// Copyright 2021 Hollson. All rights reserved.
// Use of this source code is governed by a MIT style
// license that can be found in the LICENSE file.

package config

import (
	"io/fs"

	"github.com/BurntSushi/toml"
)

type TomlFiles struct{}

// Open 从path路径下读取所有toml文件，合并为一个fs.File对象
func (t *TomlFiles) Open(path string) (fs.File, error) {

	return nil, nil
}

// 文件加载优先级
// 命令行
// 环境变量
// 默认家目录
// 全局目录
// 项目目录
func Init(path ...string) error {
	// if _, err := os.Stat(f); err != nil {
	// 	f = "_example/example.toml"
	// }

	var c global
	_path := "./config/server.toml"
	if len(path) > 0 {
		_path = path[0]
	}
	// toml.DecodeFS(&TomlFiles{}, "/", &c)

	// toml.Decode()

	if _, err := toml.DecodeFile(_path, &c); err != nil {
		return err
	}

	// App = c.App
	System = c.System
	Service = c.Service
	Log = c.Log
	Repo = c.Repo
	Redis = c.Redis
	Kafka = c.Kafka
	return nil
}
