// Copyright 2021 Hollson. All rights reserved.
// Use of this source code is governed by a MIT style
// license that can be found in the LICENSE file.

package config

import (
	"github.com/BurntSushi/toml"
)

// 全局变量
var (
	App    *app
	WeChat *weChat
	GPT    *gpt
)

func Load(path ...string) error {
	_path := "./config.toml"
	if len(path) > 0 {
		_path = path[0]
	}

	var model configModel
	if _, err := toml.DecodeFile(_path, &model); err != nil {
		return err
	}

	App = model.App
	WeChat = model.Wechat
	GPT = model.GPT
	return nil
}
