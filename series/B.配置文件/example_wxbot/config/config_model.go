package config

import (
	"fmt"
)

// ===========================================================
//  根据业务定义的配置模板，须跟toml文件配置项保持一致
// ===========================================================

// 全局Model
type configModel struct {
	App    *app    `toml:"app"`
	Wechat *weChat `toml:"wechat"`
	GPT    *gpt    `toml:"gpt"`
}

func (p *configModel) String() string {
	return fmt.Sprintf("%+v", *p)
}

// ===========================================================

// 应用
type app struct {
	Name      string   `toml:"app_name"`   // 应用名称
	DataPath  string   `toml:"data_path"`  // 数据目录
	LogPath   string   `toml:"log_path"`   // 日志目录
	BotNick   string   `toml:"bot_nick"`   // 机器人昵称
	BotPrefix []string `toml:"bot_prefix"` // 机器人唤醒前缀
}

func (p *app) String() string {
	return fmt.Sprintf("%+v", *p)
}

// ===========================================================

// 微信
type weChat struct {
	AutoPass bool `toml:"auto_pass"` // 自动通过好友
}

func (p *weChat) String() string {
	return fmt.Sprintf("%+v", *p)
}

// ===========================================================

// ChatGPT
type gpt struct {
	Model   string   `toml:"model"`    // GPT模型
	ApiKeys []string `toml:"api_keys"` // 安全密钥
}

func (p *gpt) String() string {
	return fmt.Sprintf("%+v", *p)
}
