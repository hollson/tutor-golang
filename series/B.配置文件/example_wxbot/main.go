// Copyright 2023 Hollson. All rights reserved.
// Use of this source code is governed by a MIT style
// license that can be found in the LICENSE file.

package main

import (
	"embed"
	"fmt"
	"os"

	"config_demos/example_wxbot/config"
)

func main() {
	cfg := "./config.toml"
	if err := createExampleConfig(cfg); err != nil {
		fmt.Println(err)
		return
	}

	if err := config.Load(cfg); err != nil {
		panic(err)
	}
	fmt.Println(config.App)
	fmt.Println(config.WeChat)
	fmt.Println(config.GPT)
}

//go:embed config/config.toml
var configExample embed.FS

// CreateExampleConfig 如果正式配置文件cfg不存在，则创建示例配置文件
func createExampleConfig(cfg string) error {
	if fileExists("./config.toml") {
		return nil // 文件存在
	}

	// 创建示例文件
	file, err := os.Create("./config.example.toml")
	if err != nil {
		return err
	}
	defer file.Close()

	// 写入Embed数据
	data, err := configExample.ReadFile("config/config.toml")
	if _, err := file.Write(data); err != nil {
		return err
	}

	return fmt.Errorf("配置文件不存在，请根据示例文件重新创建")
}

// 判断文件是否存在
func fileExists(filename string) bool {
	_, err := os.Stat(filename)
	return !os.IsNotExist(err)
}
