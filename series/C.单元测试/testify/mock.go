// Copyright 2022 Hollson. All rights reserved.
// Use of this source code is governed by a MIT style
// license that can be found in the LICENSE file.

package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
)

type Product struct {
	Name  string
	Price float64
}

// ICrawler 爬虫
type ICrawler interface {
	CrawlProducts() ([]Product, error) // 爬取商品列表
}

// ====================================================

// Crawler 业务中的爬虫实例
type Crawler struct {
	url string
}

// CrawlProducts 通过Http爬取商品列表，这是一个外部依赖
func (c *Crawler) CrawlProducts() ([]Product, error) {
	resp, err := http.Get(c.url)
	if err != nil {
		return nil, err
	}

	defer resp.Body.Close()
	data, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	var products []Product
	err = json.Unmarshal(data, &products)
	if err != nil {
		return nil, err
	}

	return products, nil
}

// CrawlerProcess 启动爬虫程序
func CrawlerProcess(crawler ICrawler) {
	users, err := crawler.CrawlProducts()
	if err != nil {
		return
	}

	for _, u := range users {
		fmt.Println(u)
	}
}
