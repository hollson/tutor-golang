// Copyright 2022 Hollson. All rights reserved.
// Use of this source code is governed by a MIT style
// license that can be found in the LICENSE file.

package main

import (
	"fmt"
	"net/http"

	"github.com/stretchr/testify/suite"
)

type MyTestSuit struct {
	suite.Suite
	testCount uint32
}

// SetupSuite 安装套件
func (s *MyTestSuit) SetupSuite() {
	fmt.Println("SetupSuite")
}

// TearDownSuite 拆解套件
func (s *MyTestSuit) TearDownSuite() {
	fmt.Println("TearDownSuite")
}

// SetupTest 安装测试
func (s *MyTestSuit) SetupTest() {
	fmt.Printf("SetupTest test count:%d\n", s.testCount)
}

// TearDownTest 拆解测试
func (s *MyTestSuit) TearDownTest() {
	s.testCount++
	fmt.Printf("TearDownTest test count:%d\n", s.testCount)
}

// BeforeTest 测试前事件
func (s *MyTestSuit) BeforeTest(suiteName, testName string) {
	fmt.Printf("BeforeTest suite:%s test:%s\n", suiteName, testName)
}

// AfterTest 测试后事件
func (s *MyTestSuit) AfterTest(suiteName, testName string) {
	fmt.Printf("AfterTest suite:%s test:%s\n", suiteName, testName)
}

func (s *MyTestSuit) TestExample() {
	fmt.Println("TestExample")
}

// ============================================================
func index(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintln(w, "Hello World")
}

func greeting(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "welcome, %s", r.URL.Query().Get("name"))
}

// func main() {
// 	mux := http.NewServeMux()
// 	mux.HandleFunc("/", index)
// 	mux.HandleFunc("/greeting", greeting)
//
// 	server := &http.Server{
// 		Addr:    ":8080",
// 		Handler: mux,
// 	}
//
// 	if err := server.ListenAndServe(); err != nil {
// 		log.Fatal(err)
// 	}
// }
