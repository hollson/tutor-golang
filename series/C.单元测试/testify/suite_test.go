// Copyright 2022 Hollson. All rights reserved.
// Use of this source code is governed by a MIT style
// license that can be found in the LICENSE file.

package main

import (
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/suite"
)

func TestExample(t *testing.T) {
	suite.Run(t, new(MyTestSuit))
}

func TestIndex(t *testing.T) {
	recorder := httptest.NewRecorder()
	request, _ := http.NewRequest("GET", "/", nil)
	mux := http.NewServeMux()
	mux.HandleFunc("/", index)
	mux.HandleFunc("/greeting", greeting)

	mux.ServeHTTP(recorder, request)

	assert.Equal(t, recorder.Code, 200, "get index error")
	assert.Contains(t, recorder.Body.String(), "Hello World", "body error")
}

func TestGreeting(t *testing.T) {
	recorder := httptest.NewRecorder()
	request, _ := http.NewRequest("GET", "/greeting", nil)
	request.URL.RawQuery = "name=dj"
	mux := http.NewServeMux()
	mux.HandleFunc("/", index)
	mux.HandleFunc("/greeting", greeting)

	mux.ServeHTTP(recorder, request)

	assert.Equal(t, recorder.Code, 200, "greeting error")
	assert.Contains(t, recorder.Body.String(), "welcome, dj", "body error")
}
