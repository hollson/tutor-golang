// Copyright 2022 Hollson. All rights reserved.
// Use of this source code is governed by a MIT style
// license that can be found in the LICENSE file.

package main

import (
	"testing"

	"github.com/stretchr/testify/mock"
)

// Mock模拟数据
var mockProducts []Product

func init() {
	mockProducts = append(mockProducts, Product{"小米9号平衡车PLUS", 3399.00})
	mockProducts = append(mockProducts, Product{"微星GTX 1050显卡", 1599.00})
}

// Mock模拟实例
type MockCrawler struct {
	mock.Mock
}

func (m *MockCrawler) CrawlProducts() ([]Product, error) {
	args := m.Called()
	return args.Get(0).([]Product), args.Error(1)
}

func TestCrawlProducts(t *testing.T) {
	// Mock实例
	_crawler := new(MockCrawler)
	_crawler.On("CrawlProducts").Return(mockProducts, nil)

	CrawlerProcess(_crawler)
	_crawler.AssertExpectations(t)
}
