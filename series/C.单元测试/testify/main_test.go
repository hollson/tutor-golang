package main

import (
	"errors"
	"fmt"
	"io"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestSquare(t *testing.T) {
	assert.Equal(t, 4, Square(2), "Square函数验证失败")
}

func TestAssertions(t *testing.T) {
	_assert := assert.New(t)

	// 不需要再传入TestingT参数了
	_assert.Equal(1, 1, "不相等")
}

// 断言
func TestEqual(t *testing.T) {
	// 相等
	assert.Equal(t, 1, 1, "整数 - 验证失败")
	assert.Equal(t, "hello", "hello", "字符串 - 验证失败")
	assert.Equal(t, [2]int{1, 2}, [2]int{1, 2}, "数组 - 验证失败")
	assert.Equal(t, []int{1, 2}, []int{1, 2}, "切片 - 验证失败")
	assert.Equal(t, map[int]string{1: "a"}, map[int]string{1: "a"}, "字典 - 验证失败")
	assert.Equal(t, 1, interface{}(1), "接口 - 验证失败")

	// 不相等
	assert.NotEqual(t, 1, 2, "NotEqual - 验证失败")
	assert.NotEqual(t, "a", "b", "NotEqual - 验证失败")
	assert.NotEqual(t, []int{1}, []int{2}, "NotEqual - 验证失败")
}

// 包含
func TestContains(t *testing.T) {
	assert.Contains(t, "hello world", "hello")
	assert.Contains(t, []string{"Hello", "World"}, "World")
	assert.Contains(t, map[string]int{"hello": 1}, "hello", "字典中不存在该Key")

	// 格式化
	assert.Containsf(t, "hello world", "hello", "%v不存在", "Hello")
}

// 元素匹配： 判断数组或切片的元素是否匹配，忽略元素顺序
func TestElements(t *testing.T) {
	assert.ElementsMatch(t, []int{1, 2, 3}, [3]int{2, 1, 3}, "类型不匹配")
	assert.ElementsMatch(t, []int{1, 2, 3}, []int{2, 1, 3, 4}, "类型不匹配")
}

// 表格驱动
func TestCalculateTable(t *testing.T) {
	_assert := assert.New(t)

	var tests = []struct {
		input    int
		expected int
	}{
		{2, 4},
		{-1, 1},
		{0, 0},
		{-5, 25},
		{10, 99},
	}

	for _, test := range tests {
		_assert.Equal(Square(test.input), test.expected, "与结果不符")
	}
}

// 空值/零值
func TestEmpty(t *testing.T) {
	var a, b, c, d, e, f = new(int), 0, 0.0, "", false, make([]int, 0)
	assert.Empty(t, a, "指针 - 不为空")
	assert.Empty(t, b, "整数 - 不为空")
	assert.Empty(t, c, "实数 - 不为空")
	assert.Empty(t, d, "字符串 - 不为空")
	assert.Empty(t, e, "布尔 - 不为空")
	assert.Empty(t, f, "切片 - 不为空")
}

// 断言Error
func TestError(t *testing.T) {
	var err = errors.New("sorry")

	// 判断err不为nil
	assert.NotNil(t, err, "err==nil")
	assert.Error(t, err, "err为nil")

	assert.ErrorIs(t, err, io.EOF, "Error类型不匹配-1")
	assert.ErrorIs(t, err, fmt.Errorf("wrap: %w", io.EOF), "Error类型不匹配-2")
	assert.ErrorIs(t, io.EOF, io.ErrClosedPipe, "Error类型不匹配-3")

	err = io.EOF
	assert.ErrorIs(t, err, io.EOF, "OK")
}
