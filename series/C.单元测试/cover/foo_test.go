// Copyright 2022 Hollson. All rights reserved.
// Use of this source code is governed by a MIT style
// license that can be found in the LICENSE file.

package cover

import (
	"testing"
)

// 控制台输出
//go:generate go test --cover						# 执行所有测试
//go:generate go test --cover -run='CoverTimely'	# 执行测试函数
func TestCoverTimely(t *testing.T) {
	type args struct{ a int }
	tests := []struct {
		name string
		args args
		want string
	}{
		{name: "优秀", args: args{a: 98}, want: "A"},
		{name: "良好", args: args{a: 88}, want: "B"},
		// {args: args{a: 80}, want: "B"},
		// {args: args{a: 70}, want: "C"},
		// {args: args{a: 55}, want: "D"},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := ScoreLevel(tt.args.a); got != tt.want {
				t.Errorf("结果： %v, 期望： %v", got, tt.want)
			}
		})
	}
}

// 输出测试报告文件
//go:generate go test -coverprofile=$TMPDIR/a.out ./
//go:generate go tool cover -func=$TMPDIR/a.out
//go:generate go tool cover -html=$TMPDIR/a.out
func TestScoreLevel(t *testing.T) {
	type args struct{ a int }
	tests := []struct {
		name string
		args args
		want string
	}{
		{name: "优秀", args: args{a: 98}, want: "A"},
		{name: "良好", args: args{a: 88}, want: "B"},
		{name: "无名", args: args{a: 80}, want: "B"},
		{args: args{a: 70}, want: "C"},
		// {args: args{a: 55}, want: "D"},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := ScoreLevel(tt.args.a); got != tt.want {
				t.Errorf("结果： %v, 期望： %v", got, tt.want)
			}
		})
	}
}
