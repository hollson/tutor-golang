// Copyright 2022 Hollson. All rights reserved.
// Use of this source code is governed by a MIT style
// license that can be found in the LICENSE file.

package cover

// ScoreLevel 成绩
func ScoreLevel(score int) string {
	switch {
	case score >= 90: // 优秀
		return "A"
	case score >= 80: // 良好
		return "B"
	case score >= 60: // 及格
		return "C"
	default: // 不及格
		return "D"
	}
}
