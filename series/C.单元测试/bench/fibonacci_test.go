package bench

import (
	"fmt"
	"testing"
	"time"
)

func TestName(t *testing.T) {
	fmt.Println(time.Duration(2040380537))
}

// 单次性能统计
//go:generate go test
func TestFibOnes(t *testing.T) {
	t.Run("Fib(05)", func(t *testing.T) {
		Fibonacci(5)
	})
	t.Run("Fib(20)", func(t *testing.T) {
		Fibonacci(20)
	})
	t.Run("Fib(30)", func(t *testing.T) {
		Fibonacci(30)
	})
	t.Run("Fib(40)", func(t *testing.T) {
		Fibonacci(40)
	})
	t.Run("Fib(42)", func(t *testing.T) {
		Fibonacci(42)
	})
	t.Run("Fib(45)", func(t *testing.T) {
		Fibonacci(45)
	})
}

// 观察单位时间(默认1S)内函数执行的「次数」和「平均耗时」
//go:generate go test -bench=BenchmarkFib* -run=none
func BenchmarkFibonacci(b *testing.B) {
	time.Sleep(1 * time.Second) // 模拟耗时准备工作
	b.ResetTimer()              // 重置计时器(忽略准备时间)

	b.Run("Fib(05)", func(b *testing.B) {
		for i := 0; i < b.N; i++ {
			Fibonacci(5)
		}
	})

	b.Run("Fib(20)", func(b *testing.B) {
		for i := 0; i < b.N; i++ {
			Fibonacci(20)
		}
	})

	b.Run("Fib(30)", func(b *testing.B) {
		for i := 0; i < b.N; i++ {
			Fibonacci(30)
		}
	})

	b.Run("Fib(40)", func(b *testing.B) {
		for i := 0; i < b.N; i++ {
			Fibonacci(40)
		}
	})

	b.Run("Fib(42)", func(b *testing.B) {
		for i := 0; i < b.N; i++ {
			Fibonacci(42)
		}
	})

	b.Run("Fib(45)", func(b *testing.B) {
		for i := 0; i < b.N; i++ {
			Fibonacci(43)
		}
	})
}

// 观察单位时间(默认1S)内函数执行的「次数」和「平均耗时」
//go:generate go test -bench=BenchmarkParallel -run=none
//go:generate go test -bench=BenchmarkParallel -run=none -parallel=1 -cpu=1
//go:generate go test -bench=BenchmarkParallel -run=none -parallel=2 -cpu=2
func BenchmarkParallel(b *testing.B) {
	b.ResetTimer()

	b.Run("Fib(05)", func(b *testing.B) {
		b.RunParallel(func(pb *testing.PB) {
			for pb.Next() {
				Fibonacci(5)
			}
		})
	})

	b.Run("Fib(20)", func(b *testing.B) {
		b.RunParallel(func(pb *testing.PB) {
			for pb.Next() {
				Fibonacci(20)
			}
		})
	})

	b.Run("Fib(30)", func(b *testing.B) {
		b.RunParallel(func(pb *testing.PB) {
			for pb.Next() {
				Fibonacci(30)
			}
		})
	})

	b.Run("Fib(40)", func(b *testing.B) {
		b.RunParallel(func(pb *testing.PB) {
			for pb.Next() {
				Fibonacci(40)
			}
		})
	})

	b.Run("Fib(42)", func(b *testing.B) {
		b.RunParallel(func(pb *testing.PB) {
			for pb.Next() {
				Fibonacci(42)
			}
		})
	})

	b.Run("Fib(45)", func(b *testing.B) {
		b.RunParallel(func(pb *testing.PB) {
			for pb.Next() {
				Fibonacci(45)
			}
		})
	})
}
