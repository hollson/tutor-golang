// Copyright 2022 Hollson. All rights reserved.
// Use of this source code is governed by a MIT style
// license that can be found in the LICENSE file.

package bench

import (
	"testing"

	"github.com/google/uuid"
)

// 基准测试 - 测试生成UUID
//go:generate go test -bench=UUID -run=none					# 执行1次，默认1秒
//go:generate go test -bench=UUID -run=none -count=3		# 执行3次
//go:generate go test -benchtime=3s -bench=UUID -run=none	# 执行3秒
func BenchmarkUUID(b *testing.B) {
	for i := 0; i < b.N; i++ {
		uuid.New()
	}
}
