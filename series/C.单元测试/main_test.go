// Copyright 2022 Hollson. All rights reserved.
// Use of this source code is governed by a MIT style
// license that can be found in the LICENSE file.

package main

import (
	"fmt"
	"testing"
	"time"
)

// 示例函数(在Goland等IDE中调用该目标函数时，会智能提示Example示例哦👍)。
func ExampleHello() {
	Hello("world")
	Hello("golang")

	// output:
	// hello world
	// hello golang
}

// 基本功能测试
//go:generate go test							# 测试当前项目
//go:generate go test main.go main_test.go		# 测试Go文件(须包含「源文件」和「测试文件」)
//go:generate go test -run='Add'				# 执行测试函数(TestAdd*)
//go:generate go test -v -run='Add'				# 始终显示结果
func TestAdd(t *testing.T) {
	actual, expect := Add(2, 1), 30
	if actual != expect {
		t.Errorf("测试失败：预期:%v，实际:%v", expect, actual)
		return
	}
}

// 基本功能「表格驱动」测试
//go:generate go test -v -run='AddTable'
func TestAddTable(t *testing.T) {
	type args struct {
		a int
		b int
	}
	tests := []struct {
		name string
		args args
		want int
	}{
		{args: args{a: 1, b: 2}, want: 3},
		{args: args{a: 1, b: 0}, want: 1},
		{args: args{a: -1, b: -2}, want: -3},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := Add(tt.args.a, tt.args.b); got != tt.want {
				t.Errorf("结果： %v, 期望： %v", got, tt.want)
			}
		})
	}
}

// 有些测试需要一定的启动和初始化时间，如果从 Benchmark() 函数开始计时会很大程度上影响测试结果的精准性
//go:generate go test -bench=TimerControl -run=none
func BenchmarkTimerControl(b *testing.B) {
	<-time.After(time.Second) // 睡1秒
	b.ResetTimer()            // 重置计时器
	// b.StopTimer()  // 停止计时器
	// b.StartTimer() // 开始计时器

	var n int
	for i := 0; i < b.N; i++ {
		n++
	}
}

// 基准测试 - 内存测试
//go:generate go test -benchmem -bench=Alloc -run=none
func BenchmarkAlloc(b *testing.B) {
	for i := 0; i < b.N; i++ {
		fmt.Sprintf("%d", i)
	}
}

// // 每次执行单元测试时，都会由TestMain发起，相当于测试函数的入口(main)函数。
// // m.Run()返回一个退出代码，以传递给os.Exit。
// func TestMain(m *testing.M) {
// 	var (
// 		start  = "安装/启动测试..."
// 		finish = "卸载/完成测试..."
// 	)
// 	fmt.Println(start)
// 	code := m.Run() // 执行单元测试，并返回Code
// 	fmt.Println(finish)
// 	os.Exit(code) // 退出测试
// }
