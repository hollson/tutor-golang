# 单元测试

## 功能测试

## 基准测试

## 性能测试

### 1. 单次性能

```shell
=== RUN   TestFibbb
--- PASS: TestFibbb (7.45s)
=== RUN   TestFibbb/Fibonacci(05)
    --- PASS: TestFibbb/Fibonacci(05) (0.00s)
=== RUN   TestFibbb/Fibonacci(20)
    --- PASS: TestFibbb/Fibonacci(20) (0.00s)
=== RUN   TestFibbb/Fibonacci(30)
    --- PASS: TestFibbb/Fibonacci(30) (0.00s)
=== RUN   TestFibbb/Fibonacci(40)
    --- PASS: TestFibbb/Fibonacci(40) (0.50s)
=== RUN   TestFibbb/Fibonacci(42)
    --- PASS: TestFibbb/Fibonacci(42) (1.30s)
=== RUN   TestFibbb/Fibonacci(45)
    --- PASS: TestFibbb/Fibonacci(45) (5.64s)
PASS
```

### 2. 基准性能

> 单位时间(默认1S)内函数执行的「次数」和「平均耗时」

```shell
$ go test -bench=BenchmarkFib* -run=none -cpu 1
goos: darwin
goarch: amd64
pkg: mytest
cpu: Intel(R) Core(TM) i5-7360U CPU @ 2.30GHz
BenchmarkFibonacci/Fib(05)              44265548                22.61 ns/op
BenchmarkFibonacci/Fib(20)                 37838             33033 ns/op
BenchmarkFibonacci/Fib(30)                   282           3966020 ns/op
BenchmarkFibonacci/Fib(40)                     3         483121734 ns/op
BenchmarkFibonacci/Fib(42)                     1        1253826891 ns/op
BenchmarkFibonacci/Fib(45)                     1        2040380537 ns/op
PASS
ok      mytest  11.384s
```

```shell
$ go test -bench=BenchmarkFib* -run=none
goos: darwin
goarch: amd64
pkg: mytest
cpu: Intel(R) Core(TM) i5-7360U CPU @ 2.30GHz
BenchmarkFibonacci/Fib(05)-4            56187138                22.20 ns/op
BenchmarkFibonacci/Fib(20)-4               35690             34809 ns/op
BenchmarkFibonacci/Fib(30)-4                 292           3957237 ns/op
BenchmarkFibonacci/Fib(40)-4                   3         504208213 ns/op
BenchmarkFibonacci/Fib(42)-4                   1        1283163992 ns/op
BenchmarkFibonacci/Fib(45)-4                   1        2072633363 ns/op
PASS
ok      mytest  12.731s
```

### 并发性能

> 单位时间(默认1S)内函数执行的「次数」和「平均耗时」

```shell
$ go test -bench=BenchmarkParallel -run=none -parallel=1 -cpu=1
goos: darwin
goarch: amd64
pkg: mytest
cpu: Intel(R) Core(TM) i5-7360U CPU @ 2.30GHz
BenchmarkParallel/Fib(05)               53339127                23.43 ns/op
BenchmarkParallel/Fib(20)                  34896             37607 ns/op
BenchmarkParallel/Fib(30)                    302           4425691 ns/op
BenchmarkParallel/Fib(40)                      3         509358184 ns/op
BenchmarkParallel/Fib(42)                      1        1516023200 ns/op
BenchmarkParallel/Fib(45)                      1        5819358413 ns/op
PASS
ok      mytest  15.883s
```

```shell
$ go test -bench=BenchmarkParallel -run=none
goos: darwin
goarch: amd64
pkg: mytest
cpu: Intel(R) Core(TM) i5-7360U CPU @ 2.30GHz
BenchmarkParallel/Fib(05)-4             100000000               10.27 ns/op
BenchmarkParallel/Fib(20)-4                85794             13907 ns/op
BenchmarkParallel/Fib(30)-4                  697           1701719 ns/op
BenchmarkParallel/Fib(40)-4                    5         262973028 ns/op
BenchmarkParallel/Fib(42)-4                    1        1248854241 ns/op
BenchmarkParallel/Fib(45)-4                    1        5318026134 ns/op
PASS
ok      mytest  13.464s
```

## 测试覆盖率

### 1. 执行测试

```shell
# 测试当前目录
$ go test --cover ./

# 执行测试源文件
$ go test --cover ./foo.go ./foo_test.go
```

### 2. 输出测试结果

```shell
$ go test -coverprofile=$TMPDIR/a.out ./
$ go test -coverprofile=$TMPDIR/a.out ./foo.go ./foo_test.go

# covermode有三种模式：
# 1. set: 语句是否执行，默认模式
# 2. count: 每条语句执行次数
# 3. atomic: 并发中的精确技术
$ go test -covermode=count -coverprofile=$TMPDIR/a.out ./
```

### 3. 查看覆盖率

```shell
$ go tool cover --help				# 查看帮助
$ go tool cover -func=$TMPDIR/a.out		# 文本方式查看
$ go tool cover -html=$TMPDIR/a.out		# 浏览器方式查看
$ go tool cover -html=$TMPDIR/a.out -o coverage.html
```

coverage.html示例： [coverage.html](./cover/coverage.html)

# testify库

testify核心有三部分内容：

1. assert：断言；
2. mock：测试替身；
3. suite：测试套件。

*安装testify：*

```shell
go get -u github.com/stretchr/testify
```

https://blog.csdn.net/darjun/article/details/119658145

## 示例

### 1. 测试UUID性能

### 2. 测试字符串JOIN

### 3. 测试服务

## 参考链接

http://turbock79.cn/?p=3713#41_-benchtime_t
https://studygolang.com/articles/11857