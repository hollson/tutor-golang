package main

import (
	"fmt"
)

// Add 加法运算
//go:generate go test -v -run='TestAdd'
func Add(a, b int) int {
	return a + b
}

func Hello(s string) {
	fmt.Printf("hello %s\n", s)
}

// // Fibonacci 斐波那契数「递归」，模拟耗时运算
// func Fibonacci(n int) int {
// 	if n < 2 {
// 		return n
// 	}
// 	return Fibonacci(n-1) + Fibonacci(n-2)
// }
