package main

import (
	"fmt"
	"time"
)

func Send(n ...int) chan int {
	c := make(chan int)
	go func() {
		defer close(c)
		for i := 0; i < len(n); i++ {
			time.Sleep(time.Second)
			fmt.Println("===")
			c <- n[i]
		}
	}()
	return c
}

func main() {
	r := Send(1, 2, 3, 4, 5)
	fmt.Println("===ddd")
	// out := make(chan int)
	for v := range r {
		fmt.Println(v)
	}
	// close(r)
}
