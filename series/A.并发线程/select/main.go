package main

import (
    "fmt"
	"time"
)

func main() {
	c1 := make(chan int, 1000)
	for i := 0; i < 100; i++ {
		select {
		case c1 <- i:
			break // no used
		default:
		}
	}
	fmt.Println(len(c1))

	c2 := make(chan int, 10)
	for i := 0; i < 100; i++ {
		select {
		case c2 <- i:
		default:
		}
	}
	close(c2)
	v2 := []int{}
	for v := range c2 {
		v2 = append(v2, v)
	}
	fmt.Println(len(v2), v2)

	c1 = make(chan int, 1000)
	c2 = make(chan int, 1000)

	for i := 0; i < 100; i++ {
		select {
		case c1 <- i:
		case c2 <- i:
		default:
		}
	}
	fmt.Println(len(c1))
	fmt.Println(len(c2))

	for i := 0; i < 50; i++ {
		select {
		case <-c1:
		case <-c2:
		default:
		}
	}
	fmt.Println(len(c1))
	fmt.Println(len(c2))

	go func() {
		fmt.Println("start")
		// wait forever
		select {}
		fmt.Println("forever")
	}()

	time.Sleep(time.Second)
}
