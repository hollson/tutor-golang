package main

import (
	"fmt"
)

// 生产者
func producer(c chan<- int) {
	for i := 0; i < 10; i++ {
		c <- i
		fmt.Println("send:", i)
	}
	close(c)
}

// 消费者
func consumer(c <-chan int) {
	for v := range c {
		fmt.Println("receive:", v)
	}
}

func main() {
	ch := make(chan int)    // 1.阻塞式
	ch = make(chan int, 5) // 2.非阻塞式
	go producer(ch)
	 go consumer(ch)

	fmt.Scanln()
}
