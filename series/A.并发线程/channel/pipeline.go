package main

import (
	"fmt"
	"time"
)

// 1.管道入口
func input(nums ...int) <-chan int {
	c := make(chan int)
	go func() {
		for _, num := range nums {
			time.Sleep(time.Second)
			c <- num
		}
		close(c)
	}()
	return c
}

// 2.传输管道 - 计算平方
func calculate(in <-chan int) <-chan int {
	c := make(chan int)
	go func() {
		for v := range in {
			c <- v*v
		}
		close(c)
	}()
	return c
}

// 3. 管道出口 - 打印数据
func out(c <-chan int) {
	for v := range c {
		fmt.Println(v)
	}
}

func main() {
	c := input(1, 3, 5, 7)
	c=calculate(c)
	out(c)
}
