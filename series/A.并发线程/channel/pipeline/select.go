// Copyright 2022 Hollson. All rights reserved.
// Use of this source code is governed by a MIT style
// license that can be found in the LICENSE file.

package main

func main() {
	// c1 := make(chan string)
	// c2 := make(chan string)
	//
	// go DoStuff(c1, 5)
	// go DoStuff(c2, 2)
	//
	// for   {
	// 	select {
	// 	case msg1 := <-c1:
	// 		fmt.Println("received ", msg1)
	// 		go DoStuff(c1, 1)
	// 	case msg2 := <-c2:
	// 		fmt.Println("received ", msg2)
	// 		go DoStuff(c2, 9)
	// 	}
	// }
}
