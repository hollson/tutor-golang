package main

import (
	"context"
	"fmt"
)

// https://studygolang.com/articles/21425
type User struct {
	Name string
	Age  int
}

type userKey struct{}

func NewUserContext(ctx context.Context, u *User) context.Context {
	return context.WithValue(ctx, userKey{}, u)
}

func FromUserContext(ctx context.Context) (*User, bool) {
	u, ok := ctx.Value(userKey{}).(*User)
	return u, ok
}

func main2() {
	u := &User{"Gricks", 18}
	ctx := NewUserContext(context.Background(), u)

	fmt.Println(FromUserContext(ctx))

	var cancel func()
	ctx, cancel = context.WithCancel(ctx)
	defer cancel()

	fmt.Println(FromUserContext(ctx))
}
