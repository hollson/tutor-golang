package main

import (
	"fmt"
	"time"
)

// 协程异常
//  结论：子协程的panic会引发程序exit，正确的做法是每个Goroutine都要有recover处理机制。
func exception() {
	defer func() {
		if err := recover(); err != nil {
			fmt.Printf("捕获的异： %v\n", err)
		}
	}()

	go func() {
		panic("子协程panic") // 引发程序exit
	}()

	<-time.After(time.Second * 3)
	fmt.Println("主协程还会执行吗？")
}

func main() {
	exception()
}
