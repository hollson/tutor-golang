package main

import (
	"fmt"
	"net/http"
	"os"
	"runtime"
	"syscall"
)

/*
https://tip.golang.org/pkg/runtime/#LockOSThread
*/

/*
如果想要使用 runtime.LockOSThread 实现优先级效果, 不能使用 select, time.sleep 之类的调用.
这些调用会依赖于 go 本身的 heap 定时器, 需要使用 syscall 来实现, 例如使用 epoll 配合 SYS_TIMER_SETTIME
*/

func main() {
	runtime.LockOSThread()
	defer runtime.UnlockOSThread()

	fmt.Println("main thread:", syscall.Getpid(), os.Getpid())

	http.HandleFunc("/ping", func(w http.ResponseWriter, r *http.Request) {
		// fmt.Println("working thread:", syscall.Gettid())
		fmt.Println("working thread:", syscall.Getpid())
	})

	http.ListenAndServe(":8080", nil)
}
