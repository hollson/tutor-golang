package main

import (
	"fmt"
	"runtime"
	"sync"
	"sync/atomic"
	"time"
)

func main() {
	UnsafeV1()
	UnsafeV2()
	Locker()
	Atomic()
}

// UnsafeV1 并发异常
func UnsafeV1() {
	sum := 0
	for i := 0; i < 1000; i++ {
		go func() {
			sum += 1
		}()
	}
	time.Sleep(time.Second)
	fmt.Println("UnsafeV1 =>", sum)
}

// UnsafeV2 设置CPU数为1
func UnsafeV2() {
	sum := 0
	runtime.GOMAXPROCS(1)
	for i := 0; i < 1000; i++ {
		go func() {
			sum += 1
		}()
	}
	time.Sleep(time.Second)
	fmt.Println("UnsafeV2 =>", sum)
}

func Locker() {
	var (
		sum  uint32
		lock sync.Mutex
	)

	for i := 0; i < 1000; i++ {
		go func() {
			lock.Lock()
			defer lock.Unlock()
			sum += 1
		}()
	}

	time.Sleep(time.Second)
	// fmt.Println(sum)
	fmt.Println("Locker =>", sum)
}

func Atomic() {
	var result int32 = 2
	var wg sync.WaitGroup

	// store
	atomic.StoreInt32(&result, 1)

	// load
	fmt.Println("Atomic =>")
	fmt.Println(atomic.LoadInt32(&result))

	// add
	for i := 0; i < 999; i++ {
		wg.Add(1)
		go func() {
			atomic.AddInt32(&result, 1)
			wg.Done()
		}()
	}
	wg.Wait()
	fmt.Println(result)

	// swap
	atomic.SwapInt32(&result, 999)
	fmt.Println("swap ", result)

	// cas
	atomic.CompareAndSwapInt32(&result, 999, 9999)
	fmt.Println("cas ", result)

	fmt.Println()
}
