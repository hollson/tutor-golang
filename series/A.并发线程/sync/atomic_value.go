package main

import (
	"fmt"
	"math/rand"
	"sync"
	"sync/atomic"
	"time"
)

// Config 模拟Atomic配置文件
type Config struct {
	Name string
	Host string
	Port int
}

func (c Config) String() string {
	return fmt.Sprintf("%s %v:%v", c.Name, c.Host, c.Port)
}

func DefaultConfig() Config {
	return Config{
		Name: "Example",
		Host: "127.0.0.1",
		Port: 10000,
	}
}

func main() {
	var broad = sync.NewCond(&sync.RWMutex{})
	var conf = DefaultConfig()

	// 将默认配置写入atomic.Value
	var global atomic.Value
	global.Store(conf)
	fmt.Println(global.Load().(Config))

	// 更新Config
	go func() {
		for {
			conf.Port = rand.Intn(65535)
			global.Store(conf)
			broad.Broadcast()
			time.Sleep(time.Second)
		}
	}()

	// 监听Config
	func() {
		for {
			broad.L.Lock()
			broad.Wait()
			fmt.Println(global.Load().(Config))
			broad.L.Unlock()
		}
	}()
}
