package main

import (
	"fmt"
	"time"
	_ "unsafe"
)

//go:linkname now time.now
func now() (sec int64, nsec int32, mono int64)

type StdClock struct{}

func (*StdClock) Nanosecond() int64 {
	sec, nsec, _ := now()
	return sec*1e9 + int64(nsec)
}

func (*StdClock) Monotonic() int64 {
	_, _, mono := now()
	return mono
}

func main() {
	clock := &StdClock{}
	fmt.Println(clock.Nanosecond())
	fmt.Println(time.Now().UnixNano())
}
