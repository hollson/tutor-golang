// Copyright 2021 Hollson. All rights reserved.
// Use of this source code is governed by a MIT style
// license that can be found in the LICENSE file.

package main

import (
	"fmt"
	"net/http"
	_ "net/http/pprof"

	"performance/pprof/util"
)

//go:generate curl -X GET "http://127.0.0.1:8080"
//go:generate open -a "Google Chrome" "http://127.0.0.1:8080/debug/pprof/"
func main() {
	http.HandleFunc("/cpu", FibonacciHandler)
	http.HandleFunc("/mem", HeapHandler)
	http.ListenAndServe("localhost:8080", nil)
}

func FibonacciHandler(w http.ResponseWriter, r *http.Request) {
	n := util.Fibonacci(43)
	fmt.Println("cpu ok")
	fmt.Fprintln(w, n)
}

func HeapHandler(w http.ResponseWriter, r *http.Request) {
	util.Malloc(2)
	fmt.Println("mem ok")
	fmt.Fprintln(w, "ok")
}
