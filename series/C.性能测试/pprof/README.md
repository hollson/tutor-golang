# Install
```
yum install -y graphviz
```

# CPU

CPU 采样
```
curl http://localhost/debug/pprof/profile?seconds=5 > request-5-sec.profile
```

CPU 采样分析
```
go tool pprof -http=":81" request-5-sec.profile
```

# Heap

内存分析
```
go tool pprof -http=":81" http://localhost/debug/pprof/heap
```

# Goroutine 
```
go tool pprof -http=":81" http://localhost/debug/pprof/goroutine
```

# Note

Flat 和 Cum 区别
```
func a() {
    b() // take 1s
    ... // do something directly (without any call) // take 2s
    c() // take 2s
}
```
flat 表示自身的花费(不包括调用其他函数), cum 表示整个函数的花费. eg. flat=2s, cum=5s;
