package main

import (
	"fmt"
	_ "net/http/pprof"
	"os"
	"runtime"
	"runtime/pprof"
	"time"

	"github.com/hollson/gdk/memory"
	"github.com/hollson/gdk/rat"
)


// CPU分析：
func CpuProfile() {
	var n = 50
	start := time.Now()
	f, _ := os.Create("./cpu.pprof")
	pprof.StartCPUProfile(f) // 开始分析
	result := rat.Fibonacci(n)   // 采样耗时须在30秒以上
	pprof.StopCPUProfile()   // 结束分析
	fmt.Printf("fibonacci(%d)=%d, cost=%v\n", n, result, time.Since(start))
	f.Close()
}

// 创建一个大大数组切片(模拟一个内存分配算法)
func malloc(mb int) {
	var c [][memory.MB]byte
	for i := 0; i < mb; i++ {
		c = append(c, [memory.MB]byte{})
	}
	fmt.Printf("malloc(%d) => %v\n", mb, memory.Size(len(c))*memory.MB)
}

// 内存分析:
func MemProfile() {
	runtime.GC()
	f, _ := os.Create("./mem.pprof")

	malloc(500)
	pprof.WriteHeapProfile(f) // 输出栈信息
	f.Close()
}

//go:generate go run main.go
//go:generate go tool pprof cpu.pprof
//go:generate go tool pprof mem.pprof
func main() {
	CpuProfile()
	MemProfile()
}
