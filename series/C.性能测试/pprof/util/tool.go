package util

import (
	"log"

	"github.com/hollson/goox/memory"
)

// 斐波那契数列(模拟一个耗时运算)
// Test Outs:
// fibonacci(42) => 981.471912ms
// fibonacci(43) => 1.569058476s
// fibonacci(49) => 28.107765764s
// fibonacci(50) => 45.106843729s
// fibonacci(51) => 73.621611718s
// fibonacci(52) => 118.23563034s
func Fibonacci(num int) int {
	if num < 1 {
		return 0
	}
	if num < 3 {
		return 1
	}
	return Fibonacci(num-1) + Fibonacci(num-2)
}


// 创建一个大数组切片(模拟一个内存分配算法)
func Malloc(mb int) {
	var c [][memory.MB]byte
	for i := 0; i < mb; i++ {
		c = append(c, [memory.MB]byte{})
	}
	log.Printf("malloc(%d) => %v\n", mb, memory.Size(len(c))*memory.MB)
}
