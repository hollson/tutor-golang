// Copyright 2021 Hollson. All rights reserved.
// Use of this source code is governed by a MIT style
// license that can be found in the LICENSE file.

package main

import (
    "fmt"
    "testing"
    "time"

    "hello/lib"
)

func TestMemory(t *testing.T) {
    s := time.Now()
    lib.fibonacci(51)
    fmt.Println(time.Since(s))
}
