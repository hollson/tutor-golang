package main

import (
    "fmt"
	"runtime"
	"runtime/debug"
	"sync"
)

type User struct {
	Name string
}
// 特点：
// Pool用于缓存未使用的对象，以减轻GC压力
// Pool有1个私有池和多个共享池
// 适合长生命周期的对象，如http.Context
// 并发安全(私有池不加锁,共享池加锁)
// 先访问私有池，再访问其他公共池
// Pool会被GC回收，不能用于数据库连接等
func main() {
	// close GC
	debug.SetGCPercent(-1)

	var u1, u2, u3 *User
	sp := &sync.Pool{New: func() interface{} { return &User{} }}
	u1 = sp.Get().(*User)
	u1.Name = "Jam"
	u2 = sp.Get().(*User)
	u2.Name = "Ray"
	u3 = sp.Get().(*User)
	u3.Name = "Tom"

	fmt.Println(u1, u2, u3)

	sp.Put(u1)
	sp.Put(u2)
	//	sp.Put(u3)
	u1, u2, u3 = nil, nil, nil
	u1 = sp.Get().(*User)
	u2 = sp.Get().(*User)
	u3 = sp.Get().(*User)
	fmt.Println(u1, u2, u3)

	sp.Put(u1)
	u1, u2, u3 = nil, nil, nil

	// run GC
	runtime.GC()

	u1 = sp.Get().(*User)
	u2 = sp.Get().(*User)
	u3 = sp.Get().(*User)
	fmt.Println(u1, u2, u3)
}
