# OPA
The Open Policy Agent (OPA) is an open source, general-purpose policy engine that enables unified, context-aware policy enforcement across the entire stack.
  
[https://www.openpolicyagent.org/docs/latest](https://www.openpolicyagent.org/docs/latest)
