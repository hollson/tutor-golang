package rbac

# By default, deny requests.
default allow = false

# Allow admins to do anything.
allow {
    some i
    data.user_roles[input.user][i] == "admin"
}

# Allow the action if the user is granted permission to perform the action.
allow {
    some i,j;
    role := data.user_roles[input.user][i]
    grant := data.role_grants[role][j]

    # Check if the grant permits the action.
    input.type == grant.type
    input.action == grant.action
}

# Print role info
role := data.user_roles[input.user]
