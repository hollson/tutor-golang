package adaptor

import "fmt"

// 我们的接口（新接口）——音乐播放
type MusicPlayer interface {
	Play(fileType string, fileName string)
}

// 在网上找的已实现好的库 音乐播放
// ( 旧接口）
type ExistPlayer struct {
}

func (*ExistPlayer) PlayMp3(fileName string) {
	fmt.Println("play mp3 :", fileName)
}
func (*ExistPlayer) PlayWma(fileName string) {
	fmt.Println("play wma :", fileName)
}

// 适配器
type PlayerAdaptor struct {
	// 持有一个旧接口
	ExistPlayer
}

// 实现新接口
func (player *PlayerAdaptor) Play(fileType string, fileName string) {
	switch fileType {
	case "mp3":
		player.PlayMp3(fileName)
	case "wma":
		player.PlayWma(fileName)
	default:
		fmt.Println("暂时不支持此类型文件播放")
	}
}

// adaptee： 被适配者
// USB cable： 主要有Micro USB接口、Type C接口、Lightning接口三种接口 ， 充电，传输
