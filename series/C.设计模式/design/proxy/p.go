package main

import "fmt"

// 主题接口
type IInternetAccess interface {
	GrantInternetAccess(website string)
}

// 真实主题类
type RealInternetAccess struct{}

func (r *RealInternetAccess) GrantInternetAccess(website string) {
	fmt.Printf("Internet access granted for %s\n", website)
}

// 代理类
type InternetAccessProxy struct {
	internetAccess     IInternetAccess
	restrictedWebsites []string
}

func NewInternetAccessProxy(internetAccess IInternetAccess) *InternetAccessProxy {
	return &InternetAccessProxy{
		internetAccess:     internetAccess,
		restrictedWebsites: []string{"facebook.com", "twitter.com", "instagram.com"},
	}
}

func (p *InternetAccessProxy) GrantInternetAccess(website string) {
	for _, restrictedWebsite := range p.restrictedWebsites {
		if website == restrictedWebsite {
			fmt.Println("Access to this website is restricted.")
			return
		}
	}

	p.internetAccess.GrantInternetAccess(website)
}

// 使用示例
func main() {
	internetAccess := &RealInternetAccess{}
	proxy := NewInternetAccessProxy(internetAccess)

	proxy.GrantInternetAccess("google.com")   // 输出：Internet access granted for google.com
	proxy.GrantInternetAccess("facebook.com") // 输出：Access to this website is restricted.
	proxy.GrantInternetAccess("twitter.com")  // 输出：Access to this website is restricted.
}
