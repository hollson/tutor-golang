package strategy

// IPayment 支付策略
type IPayment interface {
	Pay(float64) error
}

// Alipay 支付宝
type Alipay struct{}

func (a *Alipay) Pay(float64) error {
	return nil
}

// WechatPay 微信支付
type WechatPay struct{}

func (a *WechatPay) Pay(float64) error {
	return nil
}

// 策略调用
// ====================================

type Payment struct {
	context  *PayContext
	strategy IPayment
}

// PayContext 支付上下文
type PayContext struct {
	Name, CardID string
	Money        float64
}

func NewPayment(name, cardid string, money float64, strategy IPayment) *Payment {
	return &Payment{
		context: &PayContext{
			Name:   name,
			CardID: cardid,
			Money:  money,
		},
		strategy: strategy,
	}
}

// // TradeManager 交易管理
// type TradeManager struct {
// 	// 余额
// 	// receipt  收款
// 	// 支出
// 	// 充值
// }
