以下是Go语言实现订阅者模式的代码：

```go
package main

import "fmt"

// 抽象观察者接口
type Observer interface {
    update()
}

// 抽象主题接口
type Subject interface {
    addObserver(observer Observer)
    removeObserver(observer Observer)
    notifyObservers()
}

// 具体主题
type ConcreteSubject struct {
    observers []Observer
}

func (s *ConcreteSubject) addObserver(observer Observer) {
    s.observers = append(s.observers, observer)
}

func (s *ConcreteSubject) removeObserver(observer Observer) {
    index := -1
    for i, obs := range s.observers {
        if obs == observer {
            index = i
            break
        }
    }
    if index >= 0 {
        s.observers = append(s.observers[:index], s.observers[index+1:]...)
    }
}

func (s *ConcreteSubject) notifyObservers() {
    for _, observer := range s.observers {
        observer.update()
    }
}

// 具体观察者A
type ConcreteObserverA struct{}

func (a *ConcreteObserverA) update() {
    fmt.Println("ConcreteObserverA has received the news!")
}

// 具体观察者B
type ConcreteObserverB struct{}

func (b *ConcreteObserverB) update() {
    fmt.Println("ConcreteObserverB has received the news!")
}

func main() {
    subject := &ConcreteSubject{}
    observerA := &ConcreteObserverA{}
    observerB := &ConcreteObserverB{}

    subject.addObserver(observerA)
    subject.addObserver(observerB)

    subject.notifyObservers()

    subject.removeObserver(observerB)

    subject.notifyObservers()
}
```

以下是使用PlantUML画出的UML图：

```plantuml
@startuml

Interface Observer {
    +update()
}

Interface Subject {
    +addObserver(observer Observer)
    +removeObserver(observer Observer)
    +notifyObservers()
}

Class ConcreteSubject {
    -observers: []Observer
    +addObserver(observer Observer)
    +removeObserver(observer Observer)
    +notifyObservers()
}

Class ConcreteObserverA {
    +update()
}

Class ConcreteObserverB {
    +update()
}

Observer <|-- ConcreteObserverA
Observer <|-- ConcreteObserverB
Subject <|.. ConcreteSubject
Subject *-- Observer

@enduml
```

解释一下UML图：

- 抽象观察者接口Observer定义了update()方法，表示接收到主题的通知后的操作。
- 抽象主题接口Subject定义了添加、删除观察者和通知观察者的方法。
- 具体主题ConcreteSubject实现了Subject接口，同时维护了一个Observer类型的观察者列表。
- 具体观察者ConcreteObserverA和ConcreteObserverB实现了Observer接口，重写了update()方法，表示接收到通知后的具体操作。
- Observer接口是ConcreteObserverA和ConcreteObserverB的父类。
- Subject为ConcreteSubject提供了一种多态的方式。