# Proxy

```plantuml
@startuml

interface Subject {
    + operation(): void
}

class RealSubject {
    - importantData: string
    + operation(): void
}

class Proxy {
    - realSubject: RealSubject
    + operation(): void
}

Subject <|-- RealSubject
Subject <|-- Proxy
Proxy --> RealSubject

@enduml




```