// Copyright 2023 Hollson. All rights reserved.
// Use of this source code is governed by a MIT style
// license that can be found in the LICENSE file.

package main

import "fmt"

// Subject 接口定义了代理和真实对象的共同行为
type Subject interface {
	Operation()
}

// RealSubject 是真实对象，实现了 Subject 接口
type RealSubject struct {
	importantData string
}

func (rs *RealSubject) Operation() {
	fmt.Println("RealSubject operation")
}

// Proxy 代理，同时也实现了 Subject 接口
type Proxy struct {
	realSubject *RealSubject
}

func (p *Proxy) Operation() {
	fmt.Println("Proxy operation")
	if p.realSubject == nil {
		p.realSubject = &RealSubject{}
	}
	// 调用真实对象的操作
	p.realSubject.Operation()
}

func main() {
	// 创建代理对象
	proxy := &Proxy{}
	// 调用代理对象的操作
	proxy.Operation()
}

// 在上面的示例代码中，`Subject` 接口定义了代理和真实对象的共同行为，`RealSubject` 类型是真实对象，
// 实现了 `Subject` 接口，而 `Proxy` 类型则是代理，同时也实现了 `Subject` 接口。
// 在 `Proxy` 的 `Operation` 方法中，该类首先执行一些额外的功能，然后再调用真实对象的方法。
// 这样，客户端就可以通过代理来访问真实对象。
