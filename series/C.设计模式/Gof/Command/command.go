package main

type Command interface {
	Execute()
}

type LightOnCommand struct {
	Light *Light
}

func (c *LightOnCommand) Execute() {
	c.Light.TurnOn()
}

type LightOffCommand struct {
	Light *Light
}

func (c *LightOffCommand) Execute() {
	c.Light.TurnOff()
}

type RemoteControl struct {
	Command Command
}

func (r *RemoteControl) PressButton() {
	r.Command.Execute()
}

type Light struct {
	IsOn bool
}

func (l *Light) TurnOn() {
	l.IsOn = true
}

func (l *Light) TurnOff() {
	l.IsOn = false
}

func main() {
	light := &Light{}
	lightOnCommand := &LightOnCommand{Light: light}
	remoteControl := &RemoteControl{Command: lightOnCommand}
	remoteControl.PressButton() // turn on light
	lightOffCommand := &LightOffCommand{Light: light}
	remoteControl.Command = lightOffCommand
	remoteControl.PressButton() // turn off light
}
