# Title
以下是命令模式的UML图

```
@startuml
interface Command {
    + execute() : void
}

class ConcreteCommand {
    - receiver : Receiver
    + ConcreteCommand(Receiver)
    + execute() : void
}

class Receiver {
    + action() : void
}

class Invoker {
    - command : Command
    + setCommand(Command) : void
    + executeCommand() : void
}

class Client {
    + main() : void
}

Client -> Invoker : creates
Client -> ConcreteCommand : creates
ConcreteCommand -> Receiver
Invoker -> Command : uses
Command <|.. ConcreteCommand
Command <|-- ConcreteCommand : inheritance
@enduml
```

解释：

命令模式包括以下角色：

- Command（命令接口），定义一个execute()方法，该方法由ConcreteCommand类实现。
- ConcreteCommand（具体命令），实现Command接口，包含一个Receiver对象，可以调用Receiver的方法执行具体的操作。
- Receiver（接收者），包含实际执行动作的方法。
- Invoker（调用者），包含一个Command对象，调用Command对象的execute()方法。
- Client（客户端），创建Command、Receiver和Invoker对象，并将Command对象设置到Invoker对象中。

UML图中的箭头表示与关系，类名后的加号表示public方法，减号表示private属性。