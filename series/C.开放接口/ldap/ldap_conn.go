// Copyright 2022 Hollson. All rights reserved.
// Use of this source code is governed by a MIT style
// license that can be found in the LICENSE file.

package main

import (
	"github.com/go-ldap/ldap/v3"
)

type Option func(opt *ldap.Conn)

func WithTLS(s bool) Option {
	return func(opt *ldap.Conn) {
		// opt.StartTLS()
	}
}

func WithPort(s bool) Option {
	return func(opt *ldap.Conn) {

	}
}

// func (o Option) WithPort(s bool) {
//
// }

// NewDial 创建Ldap连接
func NewDial(option ...Option) (conn *ldap.Conn) {
	conn, _ = ldap.Dial("tcp", "localhost:389") // 普通连接
	return conn
}
