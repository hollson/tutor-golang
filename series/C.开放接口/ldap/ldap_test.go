// https://wyanlord.blog.csdn.net/article/details/99416987
// https://studygolang.com/articles/21479?fr=sidebar

package main_test

import (
	"crypto/tls"
	"fmt"
	"os"
	"testing"

	"github.com/go-ldap/ldap/v3"
)

const (
	baseDN   = "dc=example,dc=org"
	admin    = "cn=admin,dc=example,dc=org"
	password = "123456"
)

const (
	// 1: 普通	389
	// 2: TLS	636
	// 3: Ldap	389
	// 4: Ldaps	636
	// 5: Ldaps	1636
	ConnModel  = 4
	NormalAddr = "localhost:389"
	TlsAddr    = "localhost:636"
)

func newConn() (conn *ldap.Conn) {
	if ConnModel < 1 || ConnModel > 4 {
		fmt.Println(" ❌ ldap conn model error")
		os.Exit(1)
	}
	var err error
	switch ConnModel {
	case 1:
		conn, err = ldap.Dial("tcp", NormalAddr)
	case 2:
		conn, err = ldap.DialTLS("tcp", TlsAddr, &tls.Config{InsecureSkipVerify: true})
	case 3:
		conn, err = ldap.DialURL("ldap://" + NormalAddr)
	// conn.StartTLS(&tls.Config{InsecureSkipVerify: true})
	case 4:
		conn, err = ldap.DialURL("ldaps://"+TlsAddr, ldap.DialWithTLSConfig(&tls.Config{InsecureSkipVerify: true}))
	}

	// 其他连接方式：
	// ldap.NewConn()

	if err != nil {
		fmt.Printf(" ❌ ldap dial error,%v\n", err)
		os.Exit(1)
	}

	// conn.SetTimeout(10 * time.Second)
	if err = conn.Bind(admin, password); err != nil {
		fmt.Printf(" ❌ ldap bind admin error,%v\n", err)
		os.Exit(1)
	}
	return
}

func whoAmi(userName, password string) {
	cli := newConn()
	defer cli.Close()
	// conn, err = ldap.DialURL("ldaps://"+TlsAddr, ldap.DialWithTLSConfig(&tls.Config{InsecureSkipVerify: true}))
	// cli, err := ldap.DialURL("ldaps://"+TlsAddr, ldap.DialWithTLSConfig(&tls.Config{InsecureSkipVerify: true})) // Ldaps连接
	// defer cli.Close()

	if err := cli.Bind(userName, password); err != nil {
		fmt.Printf(" ❌ ldap bind admin error,%v\n", err)
		os.Exit(1)
	}

	result, err := cli.WhoAmI(nil)
	if err != nil {
		fmt.Println(" ❌ ldap WhoAmI Api error")
		os.Exit(1)
	}
	fmt.Println(" 😊 WhoAmI => ", result.AuthzID)
}

// 验证用户
func TestWhoAmI(t *testing.T) {
	whoAmi(admin, "123456")
	whoAmi("cn=zhangsan,ou=tech,dc=example,dc=org", "123456")
}

// 添加组织机构
//go:generate ldapsearch -x -H ldap://localhost -b dc=example,dc=org -D "cn=admin,dc=example,dc=org" -w 123456 "ou=go*"
func TestAddOrg(t *testing.T) {
	cli := newConn()
	defer cli.Close()

	req := ldap.NewAddRequest("ou=gopher,dc=example,dc=org", nil)
	req.Attribute("objectClass", []string{"top", "organizationalUnit"})
	req.Attribute("description", []string{"土拨鼠团队"})
	err := cli.Add(req)
	if err != nil {
		fmt.Printf(" ❌ ldap conn model error,%v\n", err)
		os.Exit(1)
	}

	req = ldap.NewAddRequest("ou=tech,dc=example,dc=org", nil)
	req.Attribute("objectClass", []string{"top", "organizationalUnit"})
	req.Attribute("description", []string{"技术部"})
	err = cli.Add(req)
	if err != nil {
		fmt.Printf(" ❌ ldap conn model error,%v\n", err)
		os.Exit(1)
	}
}

// 删除组织机构
//go:generate ldapdelete -x -H ldap://localhost -D "cn=admin,dc=example,dc=org" -w 123456 "ou=gopher,dc=example,dc=org"
func TestDelOrg(t *testing.T) {
	cli := newConn()
	defer cli.Close()

	req := ldap.NewDelRequest("ou=gopher,dc=example,dc=org", nil)
	err := cli.Del(req)
	if err != nil {
		fmt.Printf(" ❌ ldap conn model error,%v\n", err)
		os.Exit(1)
	}
}

// 自定义schema

// 添加用户
// fixme: 验证自定义属性
func TestAddUser(t *testing.T) {
	cli := newConn()
	defer cli.Close()

	// 张三
	req := ldap.NewAddRequest("cn=zhangsan,ou=tech,dc=example,dc=org", nil)
	req.Attribute("uidNumber", []string{"10001"})
	req.Attribute("gidNumber", []string{"101"})
	req.Attribute("userPassword", []string{"123456"})
	req.Attribute("homeDirectory", []string{"/home/zhangsan"})
	req.Attribute("cn", []string{"zhangsan"})
	req.Attribute("uid", []string{"zhangsan"})
	// req.Attribute("mail", []string{"zhangsan@qq.com"})
	// req.Attribute("age", []string{"22"})  //自定义属性
	// req.Attribute("addr", []string{"beijing"})
	// req.Attribute("title", []string{"张三"})
	req.Attribute("objectClass", []string{"shadowAccount", "posixAccount", "account"})
	err := cli.Add(req)
	if err != nil {
		fmt.Printf(" ❌ 张三：ldap conn model error,%v\n", err)
		os.Exit(1)
	}

	// 李四
	req = ldap.NewAddRequest("cn=lisi,ou=tech,dc=example,dc=org", nil)
	req.Attribute("uidNumber", []string{"10001"})
	req.Attribute("gidNumber", []string{"101"})
	req.Attribute("userPassword", []string{"123456"})
	req.Attribute("homeDirectory", []string{"/home/lisi"})
	req.Attribute("cn", []string{"lisi"})
	req.Attribute("uid", []string{"lisi"})
	req.Attribute("objectClass", []string{"shadowAccount", "posixAccount", "account"})
	err = cli.Add(req)
	if err != nil {
		fmt.Printf(" ❌ 李四：ldap conn model error,%v\n", err)
		os.Exit(1)
	}

	// 王五
	req = ldap.NewAddRequest("cn=wangwu,ou=tech,dc=example,dc=org", nil)
	req.Attribute("uidNumber", []string{"10001"})
	req.Attribute("gidNumber", []string{"101"})
	req.Attribute("userPassword", []string{"123456"})
	req.Attribute("homeDirectory", []string{"/home/wangwu"})
	req.Attribute("cn", []string{"wangwu"})
	req.Attribute("uid", []string{"wangwu"})
	req.Attribute("description", []string{"王五"})
	req.Attribute("objectClass", []string{"shadowAccount", "posixAccount", "account"})
	err = cli.Add(req)
	if err != nil {
		fmt.Printf(" ❌ 王五：ldap conn model error,%v\n", err)
		os.Exit(1)
	}
}

// 修改用户信息
func TestModifyUser(t *testing.T) {
	cli := newConn()
	defer cli.Close()

	// 1. 添加属性
	// 2. 替换属性
	modify := ldap.NewModifyRequest("cn=zhangsan,ou=tech,dc=example,dc=org", nil)
	modify.Add("description", []string{"这个人是张三"})
	modify.Replace("homeDirectory", []string{"/home/zs"})
	// modify.Add("addr", []string{"北京"})
	// modify.Replace("mail", []string{"zhangxiaosan@qq.com"})

	err := cli.Modify(modify)
	if err != nil {
		t.Errorf("❌ %v\n", err)
		return
	}

	// fixme：四类操作
	ops := []ldap.Change{
		{ // 增加性别
			Operation:    ldap.AddAttribute,
			Modification: ldap.PartialAttribute{Type: "sex", Vals: []string{"male"}}},
		{ // 删除地址
			Operation:    ldap.DeleteAttribute,
			Modification: ldap.PartialAttribute{Type: "addr"}},
		{ // 替换title
			Operation:    ldap.ReplaceAttribute,
			Modification: ldap.PartialAttribute{Type: "title", Vals: []string{"title.V2"}}},
		{ // 年龄增2
			Operation:    ldap.IncrementAttribute,
			Modification: ldap.PartialAttribute{Type: "age", Vals: []string{"2"}}},
	}
	err = cli.Modify(&ldap.ModifyRequest{
		DN:      "cn=zhangsan,ou=tech,dc=example,dc=org",
		Changes: ops,
	})
	if err != nil {
		t.Errorf("❌ %v\n", err)
		return
	}
}

// 移动
func TestRemove(t *testing.T) {
	cli := newConn()
	defer cli.Close()

	req := ldap.NewModifyDNRequest("cn=zhangsan,ou=tech,dc=example,dc=org", "cn=zhangsan",
		true, "ou=gopher,dc=example,dc=org")
	err := cli.ModifyDN(req)
	if err != nil {
		t.Errorf("❌ %v\n", err)
		return
	}
}

// 改名
func TestRename(t *testing.T) {
	cli := newConn()
	defer cli.Close()

	req := ldap.NewModifyDNRequest("cn=zhangsan,ou=gopher,dc=example,dc=org", "cn=zhangxiaosan", true, "")
	err := cli.ModifyDN(req)
	if err != nil {
		t.Errorf("❌ %v\n", err)
		return
	}
}

// 移动并改名
func TestRenameAndMove(t *testing.T) {
	cli := newConn()
	defer cli.Close()

	req := ldap.NewModifyDNRequest("cn=zhangxiaosan,ou=gopher,dc=example,dc=org",
		"cn=zhangsan", true, "ou=tech,dc=example,dc=org")
	err := cli.ModifyDN(req)
	if err != nil {
		t.Errorf("❌ %v\n", err)
		return
	}
}

// 修改用户密码
//  ldapwhoami -x -D "cn=admin,dc=example,dc=org" -w 123123
//  ldapwhoami -x -D "cn=zhangsan,ou=tech,dc=example,dc=org" -w 123456
func TestPasswordModify(t *testing.T) {
	cli := newConn()
	defer cli.Close()

	// // 修改管理员密码
	// req := ldap.NewPasswordModifyRequest("cn=admin,dc=example,dc=org", "123456", "123123")
	// modify, err := cli.PasswordModify(req)
	// if err != nil {
	// 	t.Error(err)
	// 	return
	// }
	// fmt.Println(modify.GeneratedPassword)

	passwordModifyRequest := ldap.NewPasswordModifyRequest("cn=zhangsan,ou=tech,dc=example,dc=org", "", "123456")
	passwordModifyResponse, err := cli.PasswordModify(passwordModifyRequest)
	if err != nil {
		t.Error(err)
		return
	}

	generatedPassword := passwordModifyResponse.GeneratedPassword
	fmt.Println(generatedPassword)

}

// 查询全部
func TestSearchAll(t *testing.T) {
	cli := newConn()
	defer cli.Close()

	// 查询全部
	req := ldap.NewSearchRequest(baseDN, ldap.ScopeWholeSubtree, ldap.NeverDerefAliases,
		0, 0, false, "(&(objectClass=account))",
		[]string{"cn", "uid", "description"}, nil)

	result, err := cli.Search(req)
	if err != nil {
		t.Fatal(err)
	}

	for _, v := range result.Entries {
		v.Print()
		fmt.Printf("=> %v\n\n", v.GetAttributeValue("cn"))
	}
}

// 条件查询
func TestSearchCond(t *testing.T) {
	cli := newConn()
	defer cli.Close()

	req := ldap.NewSearchRequest(baseDN, ldap.ScopeWholeSubtree, ldap.NeverDerefAliases,
		0, 0, false,  fmt.Sprintf("(&(cn=liu*))"),
		[]string{"cn", "uid", "description","entryDN","modifyTimestamp"}, nil)

	// 查询具体用户
	// req.Filter = fmt.Sprintf("(&(cn=shong*))")
	result, err := cli.Search(req)
	if err != nil {
		panic(err)
	}

	for _, v := range result.Entries {
		v.Print()
		fmt.Printf("Name=%v,Values=%v\n\n", v.Attributes[1].Name, v.Attributes[1].Values)
	}
}

// 删除用户
func TestDelUser(t *testing.T) {
	cli := newConn()
	defer cli.Close()

	// 删除李四
	err := cli.Del(&ldap.DelRequest{
		DN:       "cn=lisi,ou=tech,dc=example,dc=org",
		Controls: nil,
	})
	if err != nil {
		t.Fatal(err)
	}
}
