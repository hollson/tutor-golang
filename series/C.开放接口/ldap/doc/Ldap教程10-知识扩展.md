## 域认证中userPrincipalName 与SamAccountName 差别和使用场景



https://blog.csdn.net/oscar999/article/details/121216835

https://blog.csdn.net/oscar999/article/details/121216835






## 关于LDAP的objectClass及Attribute
https://blog.csdn.net/zhf_2016cs/article/details/77883881

https://www.cnblogs.com/kevingrace/p/5773974.html

https://www.cnblogs.com/saltish/p/16353370.html

https://zhuanlan.zhihu.com/p/532447126







##   OpenLDAP相关配置文件

>  /etc/openldap/slapd.conf：OpenLDAP的主配置文件，记录根域信息，管理员名称，密码，日志，权限等
>  /etc/openldap/slapd.d/*：这下面是/etc/openldap/slapd.conf配置信息生成的文件，每修改一次配置信息，这里的东西就要重新生成
>  /etc/openldap/schema/*：OpenLDAP的schema存放的地方
>  /var/lib/ldap/*：OpenLDAP的数据文件
>  /usr/share/openldap-servers/slapd.conf.obsolete 模板配置文件
>  /usr/share/openldap-servers/DB_CONFIG.example 模板数据库配置文件
>  OpenLDAP监听的端口：    默认监听端口：389（明文数据传输）    加密监听端口：636（密文数据传输）



