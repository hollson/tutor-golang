

# Ldap教程01-安装服务



## LDAP简介

[**OpenLDAP**](https://www.openldap.org) 是开源的[轻型目录访问协议](https://www.openldap.org/doc/admin24/intro.html#What%20is%20LDAP)(Lightweight Directory Access Protocol，LDAP) 。它本身**是一个小型文件数据库**。Ldap**是树形结构的**，能够通过server + client(服务端+客户端)的方式，进行**统一的用户(账号)管理**。



### LDAP目录树

**示例1:**  基于地理/组织边界的**传统LDAP目录树**

![https://www.openldap.org/doc/admin24/intro_tree.png](assets/intro_tree.png)



**示例2:**  基于互联网域名的**互联网LDAP目录树**

![https://www.openldap.org/doc/admin24/intro_dctree.png](./assets/intro_dctree.png)





### Ldap相关术语

| 标识 | 英文全称  | 含义  |
| :--- | :--- | :-- |
| dc     | Domain Component   | 域名的部分，如example.com,那么:dc=example,dc=com |
| dn     | Distinguished Name | 惟一辨别名(绝对路径)，如"uid=tom,ou=market,dc=example,dc=org"|
| rdn    | Relative dn        | 相对辨别名(相对路径)，与目录树结构无关，如 "uid=tom" |
| c      | Country            | 国家，如 "CN" 或"US"等|
| o      | Organization       | 组织名，如 "Example, Inc."|
| ou     | Organization Unit  | 组织单位，类似于Linux中的子目录，如 "market" |
| cn/uid | CommonName/UserId | 表示一条记录具体的名字 |
| sn     | Surname   | 姓，如 "Johansson" |
| olc | Online Configuration | 写入LDAP后不需要重启，立即生效 |



<br/>

## 安装服务

### 安装OpenLdap服务

> Docker仓库：https://hub.docker.com/r/osixia/openldap
```shell
# 下载镜像
docker pull osixia/openldap
docker images|grep ldap
```
**默认方式安装：**

```shell
# 默认配置
# dn          dc=example,dc=org
# admin       admin,dc=example,dc=org
# password    admin
docker rm -f openldap
docker run --name openldap --detach osixia/openldap
```
**自定义方式安装：**

```shell
# 参数说明：
#-p 389:389 					  		# LDAP端口
#-p 636:636 					 		# LDAP加密端口
#LDAP_TLS_VERIFY_CLIENT=“never”	  		# 客户端认证，never代表不需要认证
#LDAP_ORGANISATTON=“openldap”	  		# 组织名称
#LDAP_DOMAIN=“example.org” 		  		# 域名
#LDAP_APMIN_PASSWORD=“123456” 	  		# 超级管理员密码
#detach							 	 	# 分离模式
#-v /data/ldap/data:/var/lib/ldap 	    # 挂载数据目录
#-v /data/ldap/conf:/etc/ldap/slapd.d   # 挂载配置目录

docker rm -f openldap
docker run --name openldap \
    -p 389:389 -p 636:636 \
    --env LDAP_ORGANISATION="example" \
    --env LDAP_DOMAIN="example.org" \
    --env LDAP_BASE_DN="dc=example,dc=org" \
    --env LDAP_ADMIN_PASSWORD="123456" \
    --env LDAP_TLS_VERIFY_CLIENT="never" \
    --env TZ=Asia/Shanghai \
    --hostname openldap \
    --network bridge \
    --detach osixia/openldap
```



### 验证Openldap服务

>  Openldap主要有slap(服务管理)命令和ldap(客户端工具)命令


```shell
# 进入容器
docker exec -it openldap /bin/bash
```

**验证服务状态：**

```shell
slaptest			# 检查配置
slapcat -v  		# 查看Admin信息
ldapsearch -VV		# 查看Ldap版本信息

# 验证Admin
ldapwhoami -x -H ldap://localhost -D "cn=admin,dc=example,dc=org" -w 123456

# 基于example.org域,查看目录数据
ldapsearch -x -h localhost -b dc=example,dc=org -D "cn=admin,dc=example,dc=org" -w 123456
```

**Slap命令：** 服务命令

```shell
# 查看Slap命令
root@d3ce615f0c78:/# ls -l /usr/sbin/slap* --color=auto
-rwxr-xr-x 10 root root 1271400 Jan 30  2021 /usr/sbin/slapacl
-rwxr-xr-x 10 root root 1271400 Jan 30  2021 /usr/sbin/slapadd
-rwxr-xr-x 10 root root 1271400 Jan 30  2021 /usr/sbin/slapauth
-rwxr-xr-x 10 root root 1271400 Jan 30  2021 /usr/sbin/slapcat
-rwxr-xr-x 10 root root 1271400 Jan 30  2021 /usr/sbin/slapd
-rwxr-xr-x 10 root root 1271400 Jan 30  2021 /usr/sbin/slapdn
-rwxr-xr-x 10 root root 1271400 Jan 30  2021 /usr/sbin/slapindex
-rwxr-xr-x 10 root root 1271400 Jan 30  2021 /usr/sbin/slappasswd
-rwxr-xr-x 10 root root 1271400 Jan 30  2021 /usr/sbin/slapschema
-rwxr-xr-x 10 root root 1271400 Jan 30  2021 /usr/sbin/slaptest
```


**Ldap命令**： 客户端命令

```shell
# 查看Ldap命令
root@d3ce615f0c78:/# ls -l /usr/bin/ldap* --color=auto
lrwxrwxrwx 1 root root    10 Jan 30  2021 /usr/bin/ldapadd -> ldapmodify
-rwxr-xr-x 1 root root 63648 Jan 30  2021 /usr/bin/ldapcompare
-rwxr-xr-x 1 root root 67776 Jan 30  2021 /usr/bin/ldapdelete
-rwxr-xr-x 1 root root 67744 Jan 30  2021 /usr/bin/ldapexop
-rwxr-xr-x 1 root root 75936 Jan 30  2021 /usr/bin/ldapmodify
-rwxr-xr-x 1 root root 63648 Jan 30  2021 /usr/bin/ldapmodrdn
-rwxr-xr-x 1 root root 67744 Jan 30  2021 /usr/bin/ldappasswd
-rwxr-xr-x 1 root root 92352 Jan 30  2021 /usr/bin/ldapsearch
-rwxr-xr-x 1 root root 22528 Jan 30  2021 /usr/bin/ldapurl
-rwxr-xr-x 1 root root 63648 Jan 30  2021 /usr/bin/ldapwhoami
```

提示 ⏰：可在容器外添加相关命令连接，在容器外直接执行ldap命令：
> alias ldapadd='docker exec openldap ldapadd'
>
> alias ldapsearch='docker exec openldap ldapsearch'
>
> alias ldapmodify='docker exec openldap ldapmodify'
>
> alias slappasswd='docker exec openldap slappasswd'
>
> alias ldapwhoami='docker exec openldap ldapwhoami'





<br/>



## 安装客户端

### 安装phpldapadmin

```shell
# 下载镜像
docker pull docker.io/osixia/phpldapadmin
```

**以Http方式启动:**

```shell
# Openldap服务地址
OpenLdapIP=`docker inspect -f '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' openldap`

# 以Http方式启动
docker rm -f phpldapadmin
docker run \
	-p 1080:80 \
	--privileged \
	--name phpldapadmin \
	--env PHPLDAPADMIN_HTTPS=false \
	--env PHPLDAPADMIN_LDAP_HOSTS=${OpenLdapIP}  \
	--detach osixia/phpldapadmin
```
**以Https方式启动:**
```shell
# Openldap服务地址
OpenLdapIP=`docker inspect -f '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' openldap`

# 以Https方式启动
docker rm -f phpldapadmin
docker run -d --privileged \
	-p 1443:443 \
	--link openldap \
	--name phpldapadmin \
	--env PHPLDAPADMIN_LDAP_HOSTS=${OpenLdapIP} \
	--detach osixia/phpldapadmin
```
### 浏览器登录

> Http:    http://localhost:1080  
>
> Https:   https://localhost:1443 
>
> 帐号: `cn=admin,dc=example,dc=org`, 密码：`123456`

![](./assets/ldapadmin-1.png)
![](./assets/ldapadmin-2.png)

<br/>



## 参考链接

https://www.openldap.org

https://zhuanlan.zhihu.com/p/147768058

https://www.bbsmax.com/A/qVdeaYapdP/

https://www.lmlphp.com/user/16538/article/item/509896/

https://blog.csdn.net/liumiaocn/category_8355958.html

