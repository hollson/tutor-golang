默认情况下OpenLDAP是没有启用日志记录功能的，但是在实际使用过程中，我们为了定位问题需要使用到OpenLDAP日志。


```shell
cat > /root/loglevel.ldif << "EOF"
dn: cn=config
changetype: modify
replace: olcLogLevel
olcLogLevel: stats
EOF


# 导入到OpenLDAP中，并重启OpenLDAP服务，如下：
ldapmodify -Y EXTERNAL -H ldapi:/// -f /root/loglevel.ldif
systemctl restart slapd

```

```shell
修改rsyslog配置文件，并重启rsyslog服务，如下：

cat >> /etc/rsyslog.conf << "EOF"
local4.* /var/log/slapd.log
EOF

systemctl restart rsyslog
```

```shell
查看OpenLDAP日志，如下：

tail -f /var/log/slapd.log
```







ls /var/lib/ldap/





#  参考链接

https://blog.csdn.net/a137748099/article/details/115533006

https://blog.csdn.net/dockj/article/details/82392263

