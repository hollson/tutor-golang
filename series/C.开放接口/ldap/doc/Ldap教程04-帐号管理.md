# OpenLdap管理员密码

**第1步：查看管理员密码**

```shell
# 进入ldap容器
docker exec -ti openldap /bin/bash

# 查看管理员密码
ldapsearch -H ldapi:// -LLL -Q -Y EXTERNAL -b "cn=config" "(olcRootDN=*)" dn olcRootDN olcRootPW
# dn 		 数据库配置
# olcRootDN  连接LDAP的DN
# olcRootPW  连接LDAP的密码
```



**第2步：生成密码配置文件**

```shell
# 生成密码数据
slappasswd -s 666666
{SSHA}4h7RdWEFMBhBYmJqhN3Fyz9jic1grb3r
 
# 制作密码文件
cat > /root/newpasswd.ldif <<'EOF'
dn: olcDatabase={1}mdb,cn=config
changetype: modify
replace: olcRootPW
olcRootPW: {SSHA}4h7RdWEFMBhBYmJqhN3Fyz9jic1grb3r    # 跟上一步保持一致
EOF

# 说明: 
# olcDatabase={1}mdb,cn=config  # 更改mdb数据库配置
# changetype: modify 			# 类型为更改
# replace: olcRootPW 			# 更改的字段
```



**第3步：导入密码配置文件**

```shell
ldapmodify -H ldapi:// -Y EXTERNAL -f /root/newpasswd.ldif
```



**第4步：验证新密码**

```shell
ldapwhoami -x -D "cn=admin,dc=example,dc=org" -w 666666
ldapsearch -x -H ldap://localhost -b dc=example,dc=org -D "cn=admin,dc=example,dc=org" -w 666666
```



# 直接修改密码

```shell
slappasswd
```
