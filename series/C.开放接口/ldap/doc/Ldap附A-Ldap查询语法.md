# LDAP查询过滤语法（MS）

## LDAP Clauses

A filter specifies the conditions that must be met for a record to be included in the recordset (or collection) that results from a query. An LDAP filter has one or more clauses, each enclosed in parentheses. Each clause evaluates to either True or False. An LDAP syntax filter clause  is in the following form:

`(<AD Attribute><comparison operator><value>)`

The `<AD Attribute>` must the the [LDAP Display name](http://social.technet.microsoft.com/wiki/contents/articles/16757.active-directory-glossary.aspx#LDAPDisplayName) of an Active Directory [attribute](http://social.technet.microsoft.com/wiki/contents/articles/16757.active-directory-glossary.aspx#Attribute). The allowed comparison operators are as follows:

| **Operator** | **Meaning**    |
| :----- | :----- |
| =            | Equality                                   |
| >=           | Greater than or equal to (lexicographical) |
| <=           | Less than or equal to (lexicographical)    |

Note that the operators "<" and ">" are not supported. Another  operator, ~= (which means approximately equal to) is supported, but no  case has been found where this is useful in Active Directory.  The` <value>` in a clause will be the actual value of the Active  Directory attribute. The value is not case sensitive and should not be  quoted. The wildcard character"*" is allowed, except when the `<AD  Attribute>` is a DN attribute. Examples of DN attributes are [distinguishedName](http://social.technet.microsoft.com/wiki/contents/articles/16757.active-directory-glossary.aspx#Distinguished_Name), manager, directReports, member, and memberOf. If the attribute is DN,  then only the equality operator is allowed and you must specify the full distinguished name for the value (or the "*" character for all objects  with any value for the attribute). Do not enclose the DN value in  parentheses (as is done erroneously in some documentation). If the  attribute is multi-valued, then the condition is met if any of the  values in the attribute match the filter. An example LDAP syntax filter  clause is:

(cn=Jim Smith)

This filters on all objects where the value of the cn attribute (the [common name](http://social.technet.microsoft.com/wiki/contents/articles/16757.active-directory-glossary.aspx#Common_Name) of the object) is equal to the string "Jim Smith" (not case sensitive).  Filter clauses can be combined using the following operators:

| **Operator** | **Meaning**         |
| :------- | :-------- |
| &            | AND, all conditions must be met        |
| \|           | OR, any of the conditions must be met  |
| !            | NOT, the clause must evaluate to False |

For example, the following specifies that either the cn attribute  must be "Jim Smith", or the givenName attribute must be "Jim" and the sn attribute must be "Smith": 

(|(cn=Jim Smith)(&(givenName=Jim)(sn=Smith)))

Conditions can be nested with parentheses, but make sure the parentheses match up.

<br/>

------

## Special Characters

The LDAP filter specification assigns special meaning to the following characters:

`\* ( ) \ NUL`

The NUL character is ASCII 00. In LDAP filters these 5 characters  should be escaped with the backslash escape character, followed by the  two character ASCII hexadecimal representation of the character. The  following table documents this:

| **Character** | **Hex Representation** |
| :------------- | :---------------------- |
| *             | \2A                    |
| (             | \28                    |
| )             | \29                    |
| \             | \5C                    |
| Nul           | \00                    |

For example, to find all objects where the common name is "James Jim*) Smith", the LDAP filter would be:

(cn=James Jim\2A\29 Smith)

Actually, the parentheses only need to be escaped if they are  unmatched, as above. If instead the common name were"James (Jim) Smith", nothing would need to be escaped. However, any characters, including  non-display and foreign characters, can be escaped in a similar manner  in an LDAP filter. For example, here are a few foreign characters:

| **Character** | **Hex Representation** |
| :----------- | :------------------ |
| á             | \E1                    |
| é             | \E9                    |
| í             | \ED                    |
| ó             | \F3                    |
| ú             | \FA                    |
| ñ             | \F1                    |



<br/>

------

## Filter on objectCategory and objectClass

When your filter clause includes the objectCategory attribute, LDAP  does some magic to convert the values for your convenience. The  objectCategory attribute is a DN attribute. A typical value for an  object in Active Directory might  be"cn=person,cn=Schema,cn=Configuration,dc=MyDomain,dc=com". You can use a filter clause similar to the following:

(objectCategory=cn=person,cn=Schema,cn=Configuration,dc=MyDomain,dc=com)

However, Active Directory allows you to instead use the following shortcut:

(objectCategory=person)

The following table documents the result of various combinations of  clauses specifying values for objectCategory and objectClass:

| **objectCategory**   | **objectClass**      | **Result**                          |
| :--------- | :-------- | :------- |
| person               | user                 | user objects                        |
| person               |                      | user and contact objects            |
| person               | contact              | contact objects                     |
|                      | user                 | user and computer objects           |
| computer             |                      | computer objects                    |
| user                 |                      | user and contact objects            |
|                      | contact              | contact objects                     |
|                      | computer             | computer objects                    |
|                      | person               | user, computer, and contact objects |
| contact              |                      | user and contact objects            |
| group                |                      | group objects                       |
|                      | group                | group objects                       |
| person               | organizationalPerson | user and contact objects            |
|                      | organizationalPerson | user, computer, and contact objects |
| organizationalPerson |                      | user and contact objects            |

Use the filter that makes your intent most clear. Also, if you have a choice between using objectCategory and objectClass, it is recommended  that you use objectCategory. That is because objectCategory is both  single valued and indexed, while objectClass is multi-valued and not  indexed (except on Windows Server 2008 and above). A query using a  filter with objectCategory will be more efficient than a similar filter  with objectClass. Windows Server 2008 domain controllers (and above)  have a special behavior that indexes the objectClass attribute. You can  take advantage of this if all of your domain controllers are Windows  Server 2008, or if you specify a Windows Server 2008 domain controller  in your query.

<br/>

------

## Examples

The following table shows many example LDAP filters that can be useful when you query Active Directory:

| **Query**          | **LDAP Filter**        |
| :----------- | :--------- |
| All user objects                                             | (&(objectCategory=person)(objectClass=user))                 |
| All user objects (Note 1)                                    | (sAMAccountType=805306368)                                   |
| All computer objects                                         | (objectCategory=computer)                                    |
| All contact objects                                          | (objectClass=contact)                                        |
| All group objects                                            | (objectCategory=group)                                       |
| All organizational unit objects                              | (objectCategory=organizationalUnit)                          |
| All container objects                                        | (objectCategory=container)                                   |
| All builtin container objects                                | (objectCategory=builtinDomain)                               |
| All domain objects                                           | (objectCategory=domain)                                      |
| Computer objects with no description                         | (&(objectCategory=computer)(!(description=*)))               |
| Group objects with a description                             | (&(objectCategory=group)(description=*))                     |
| Users with cn starting with "Joe"                            | (&(objectCategory=person)(objectClass=user)  (cn=Joe*))      |
| Object with description "East\West Sales"  (Note 2)          | (description=East\5CWest Sales)                              |
| Phone numbers in form (xxx) xxx-xxx                          | (telephoneNumber=(*)*-*)                                     |
| Groups with cn starting with  "Test" or "Admin"              | (&(objectCategory=group)  (\|(cn=Test*)(cn=Admin*)))         |
| All users with both a first and last name.                   | (&(objectCategory=person)(objectClass=user)  (givenName=*)(sn=*)) |
| All users with direct reports but no  manager                | (&(objectCategory=person)(objectClass=user)  (directReports=*)(!(manager=*))) |
| All users with specified email address                       | (&(objectCategory=person)(objectClass=user)  (\|(proxyAddresses=*:jsmith@company.com)  (mail=jsmith@company.com))) |
| All users with Logon Script: field occupied                  | (&(objectCategory=person)(objectClass=user)(scriptPath=*))   |
| Object with Common Name "Jim * Smith"  (Notes 3, 19)         | (cn=Jim \2A Smith)                                           |
| Objects with sAMAccountName that begins  with "x", "y", or "z" | (sAMAccountName>=x)                                          |
| Objects with sAMAccountName that begins with "a" or any number or symbol except "$" | (&(sAMAccountName<=a)(!(sAMAccountName=$*)))                 |
| All users with "Password Never Expires" set  (Note 4)        | (&(objectCategory=person)(objectClass=user)  (userAccountControl:1.2.840.113556.1.4.803:=65536)) |
| All disabled user objects (Note 4)                           | (&(objectCategory=person)(objectClass=user)  (userAccountControl:1.2.840.113556.1.4.803:=2)) |
| All enabled user objects (Note 4)                            | (&(objectCategory=person)(objectClass=user)  (!(userAccountControl:1.2.840.113556.1.4.803:=2))) |
| All users not required to have a password  (Note 4)          | (&(objectCategory=person)(objectClass=user)  (userAccountControl:1.2.840.113556.1.4.803:=32)) |
| All users with "Do not require kerberos  preauthentication" enabled | (&(objectCategory=person)(objectClass=user)  (userAccountControl:1.2.840.113556.1.4.803:=4194304)) |
| Users with accounts that do not expire  (Note 5)             | (&(objectCategory=person)(objectClass=user)  (\|(accountExpires=0)  (accountExpires=9223372036854775807))) |
| Users with accounts that do expire (Note 5)                  | (&(objectCategory=person)(objectClass=user)  (accountExpires>=1)  (accountExpires<=9223372036854775806)) |
| Accounts trusted for delegation  (unconstrained delegation)  | (userAccountControl:1.2.840.113556.1.4.803:=524288)          |
| Accounts that are sensitive and not trusted  for delegation  | (userAccountControl:1.2.840.113556.1.4.803:=1048576)         |
| All distribution groups (Notes 4, 15)                        | (&(objectCategory=group)  (!(groupType:1.2.840.113556.1.4.803:=2147483648))) |
| All security groups (Notes 4, 19)                            | (groupType:1.2.840.113556.1.4.803:=2147483648)               |
| All built-in groups (Notes 4, 16, 19)                        | (groupType:1.2.840.113556.1.4.803:=1)                        |
| All global groups (Notes 4, 19)                              | (groupType:1.2.840.113556.1.4.803:=2)                        |
| All domain local groups (Notes 4, 19)                        | (groupType:1.2.840.113556.1.4.803:=4)                        |
| All universal groups (Notes 4, 19)                           | (groupType:1.2.840.113556.1.4.803:=8)                        |
| All global security groups (Notes 17, 19)                    | (groupType=-2147483646)                                      |
| All universal security groups (Notes 17, 19)                 | (groupType=-2147483640)                                      |
| All domain local security groups  (Notes 17, 19)             | (groupType=-2147483644)                                      |
| All global distribution groups (Note 19)                     | (groupType=2)                                                |
| All objects with service principal name                      | (servicePrincipalName=*)                                     |
| Users with "Allow Access" on "Dial-in"  tab of ADUC  (Note 6) | (&(objectCategory=person)(objectClass=user)  (msNPAllowDialin=TRUE)) |
| Users with "Control access though  NPS Network Policy" on "Dial-in" tab of ADUC | (&(objectCategory=person)(objectClass=user)  (!(msNPAllowDialin=*))) |
| All groups created after March 1, 2011                       | (&(objectCategory=group)  (whenCreated>=20110301000000.0Z))  |
| All users that must change their password  at next logon     | (&(objectCategory=person)(objectClass=user)  (pwdLastSet=0)) |
| All users that changed their password since  April 15, 2011 (CST) (Note 7) | (&(objectCategory=person)(objectClass=user)  (pwdLastSet>=129473172000000000)) |
| All users with "primary" group  other than "Domain Users"    | (&(objectCategory=person)(objectClass=user)  (!(primaryGroupID=513))) |
| All computers with "primary" group  "Domain Computers"       | (&(objectCategory=computer)  (primaryGroupID=515))           |
| Object with GUID  "90395F191AB51B4A9E9686C66CB18D11"  (Note 8) | (objectGUID=\90\39\5F\19\1A\B5\1B\4A\9E\96  \86\C6\6C\B1\8D\11) |
| Object beginning with GUID  "90395F191AB51B4A"  (Note 8)     | (objectGUID=\90\39\5F\19\1A\B5\1B\4A*)                       |
| Object with SID "S-1-5-21-73586283  -152049171-839522115-1111" (Note 9) | (objectSID=S-1-5-21-73586283-152049171  -839522115-1111)     |
| Object with SID "010500000000000515000  0006BD662041316100943170A3257040000"  (Note 9) | (objectSID=\01\05\00\00\00\00\00\05\15  \00\00\00\6B\D6\62\04\13\16\10\09\43\17\0A\32  \57\04\00\00) |
| All computers that are not  Domain Controllers (Note 4)      | (&(objectCategory=computer)  (!(userAccountControl:1.2.840.113556.1.4.803:=8192))) |
| All Domain Controllers (Note 4)                              | (&(objectCategory=computer)  (userAccountControl:1.2.840.113556.1.4.803:=8192)) |
| All Domain Controllers (Notes 14, 19)                        | (primaryGroupID=516)                                         |
| All servers                                                  | (&(objectCategory=computer)  (operatingSystem=*server*))     |
| All member servers (not DC's) (Note 4)                       | (&(objectCategory=computer)  (operatingSystem=*server*)  (!(userAccountControl:1.2.840.113556.1.4.803:=8192))) |
| All direct members of specified group                        | (memberOf=cn=Test,ou=East,dc=Domain,dc=com)                  |
| All users not direct members of  a specified group           | (&(objectCategory=person)(objectClass=user)  (!(memberOf=cn=Test,ou=East,dc=Domain,dc=com))) |
| All groups with specified direct member  (Note 19)           | (member=cn=Jim Smith,ou=West,  dc=Domain,dc=com)             |
| All members of specified group, including  due to group nesting (Note 10) | (memberOf:1.2.840.113556.1.4.1941:=  cn=Test,ou=East,dc=Domain,dc=com) |
| All groups specified user belongs to,  including due to group nesting (Notes 10, 19) | (member:1.2.840.113556.1.4.1941:=  cn=Jim Smith,ou=West,dc=Domain,dc=com) |
| Objects with givenName "Jim*" and sn  "Smith*", or with cn "Jim Smith*" (Note 11) | (anr=Jim Smith)                                              |
| All attributes in the Schema container  replicated to the GC (Notes 6, 12) | (&(objectCategory=attributeSchema)  (isMemberOfPartialAttributeSet=TRUE)) |
| All operational (constructed) attributes in  the Schema container (Notes 4, 12) | (&(objectCategory=attributeSchema)  (systemFlags:1.2.840.113556.1.4.803:=4)) |
| All attributes in the Schema container not  replicated to other Domain Controllers  (Notes 4, 12) | (&(objectCategory=attributeSchema)  (systemFlags:1.2.840.113556.1.4.803:=1)) |
| All objects where deletion is not allowed  (Notes 4)         | (systemFlags:1.2.840.113556.1.4.803:=2147483648)             |
| Attributes whose values are copied when  the object is copied (Notes 4, 12) | (searchFlags:1.2.840.113556.1.4.803:=16)                     |
| Attributes preserved in tombstone object  when object deleted (Notes 4, 12) | (searchFlags:1.2.840.113556.1.4.803:=8)                      |
| Attributes in the Ambiguous Name  Resolution (ANR) set (Notes 4, 12) | (searchFlags:1.2.840.113556.1.4.803:=4)                      |
| Attributes in the Schema that are  indexed (Notes 4, 12)     | (searchFlags:1.2.840.113556.1.4.803:=1)                      |
| Attributes marked confidential in  the schema (Notes 4, 12)  | (searchFlags:1.2.840.113556.1.4.803:=128)                    |
| Attributes in the RODC filtered attribute  set, or FAS (Notes 4, 12) | (searchFlags:1.2.840.113556.1.4.803:=512)                    |
| All site links in the Configuration  container (Note 13)     | (objectClass=siteLink)                                       |
| The nTDSDSA objects associated with  all Global Catalogs. This will identify all DC's  that are GC's. (Note 4) | (&(objectCategory=nTDSDSA)  (options:1.2.840.113556.1.4.803:=1)) |
| The nTDSDSA object associated with the  PDC Emulator. This will identify the DC  with the PDC Emulator FSMO role (Note 18). | (&(objectClass=domainDNS)(fSMORoleOwner=*))                  |
| The nTDSDSA object associated with the  RID Master. This will identify the DC  with the RID Master FSMO role (Note 18). | (&(objectClass=rIDManager)(fSMORoleOwner=*))                 |
| The nTDSDSA object associated with the  Infrastructure Master. This will identify the DC  with this FSMO role (Note 18). | (&(objectClass=infrastructureUpdate)  (fSMORoleOwner=*))     |
| The nTDSDSA object associated with the  Schema Master. This will identify the DC with  the Schema Master FSMO role (Note 18). | (&(objectClass=dMD)(fSMORoleOwner=*))                        |
| The nTDSDSA object associated with the  Domain Naming Master. This will identify the  DC with this FSMO role (Note 18). | (&(objectClass=crossRefContainer)  (fSMORoleOwner=*))        |
| All Exchange servers in the Configuration  container (Note 13) | (objectCategory=msExchExchangeServer)                        |
| All objects protected by AdminSDHolder                       | (adminCount=1)                                               |
| All trusts established with a domain                         | (objectClass=trustedDomain)                                  |
| All Group Policy objects                                     | (objectCategory=groupPolicyContainer)                        |
| All service connection point objects                         | (objectClass=serviceConnectionPoint)                         |
| All Read-Only Domain Controllers  (Notes 4, 19)              | (userAccountControl:1.2.840.113556.1.4.803:=67108864)        |

---



## 参考链接

https://social.technet.microsoft.com/wiki/contents/articles/5392.active-directory-ldap-syntax-filters.aspx 

