# Ldap教程02-基础操作



## 连接方式

> 常用的数据的查询，可以使用ldapsearch，常用的Option的信息和用途整理如下：

| Option | 说明                                                         |
| :----- | :----------------------------------------------------------- |
| -H     | ldapuri，格式为ldap://机器名或者IP:端口号，不能与-h和-p同时使用 |
| -h     | LDAP服务器IP或者可解析的hostname，与-p可结合使用，不能与-H同时使用 |
| -p     | LDAP服务器端口号，与-h可结合使用，不能与-H同时使用           |
| -x     | 使用简单认证方式                                             |
| -D     | 所绑定的服务器的DN                                           |
| -w     | 绑定DN的密码，与-W二者选一                                   |
| -W     | 不输入密码，会交互式的提示用户输入密码，与-w二者选一         |
| -f     | 指定输入条件，在RFC 4515中有更详细的说明                     |
| -c     | 出错后忽略当前错误继续执行，缺省情况下遇到错误即终止         |
| -n     | 模拟操作但并不实际执行，用于验证，常与-v一同使用进行问题定位 |
| -v     | 显示详细信息                                                 |
| -d     | 显示debug信息，可设定级别                                    |
| -s     | 指定搜索范围, 可选值：base                                   |

**Uri方式：**

```shell
ldapwhoami -x -H ldap://localhost:389 -D "cn=admin,dc=example,dc=org" -w 123456
ldapwhoami -x -H ldaps://localhost:636 -D "cn=admin,dc=example,dc=org" -w 123456
```
**Host方式：**

```shell
ldapwhoami -x -h localhost -p 389 -D "cn=admin,dc=example,dc=org" -w 123456
ldapwhoami -x -h localhost -p 636 -D "cn=admin,dc=example,dc=org" -w 123456
```

**缺省端口：** 采用默认的389和636端口安装服务后，可在命令执行过程中省略端口号

```shell
ldapwhoami -x -D "cn=admin,dc=example,dc=org" -w 123456
ldapwhoami -x -H ldap://localhost -D "cn=admin,dc=example,dc=org" -w 123456
ldapwhoami -x -H ldaps://localhost -D "cn=admin,dc=example,dc=org" -w 123456
```



<br/>



## 基础命令

### 认证：whoami

```shell
ldapwhoami -x -D "cn=admin,dc=example,dc=org" -w 123456
ldapwhoami -x -D "cn=hollson,ou=tech,dc=example,dc=org" -w 123456
```



### 添加：ldapadd

> 提示 ⏰:  将文件拷贝到容器内： `docker cp ./import openldap:/`

创建文件：`touch addorg.ldif`

```ldif
# Ldif文件：addorg.ldif

# dn必须和ou保持一致

# 技术部(dn和主体中的ou保持一致)
dn: ou=tech,dc=example,dc=org
changetype: add
objectclass: top
objectclass: organizationalUnit
ou: tech
description: Example科技-技术部


# 销售部
dn: ou=sales,dc=example,dc=org
changetype: add
objectclass: top
objectclass: organizationalUnit
ou: sales
description: Example科技-销售部


# 销售部(华北)
dn: ou=huabei,ou=sales,dc=example,dc=org
objectClass: organizationalUnit
objectClass: top
ou: huabei
description: Example科技-「华北」销售部


# 财务部
dn: ou=finance,dc=example,dc=org
changetype: add
objectclass: top
objectclass: organizationalUnit
ou: finance
description: Example科技-财务部
```
创建文件：`touch adduser.ldif`
```ldif
# Ldif文件：adduser.ldif

# cn代表公共名称；sn代表姓，cn和sn必须存在

# 刘总
dn: cn=liuzong,dc=example,dc=org
objectClass: inetOrgPerson
cn: liuzong
sn: lz
title: 刘总
mail: liuzong@qq.com
uid: 101
description: Boss

# 史布斯
dn: cn=shongsheng,dc=example,dc=org
objectClass: inetOrgPerson
cn: shongsheng
sn: sz
title: 史布斯
mail: shongsheng@qq.com
labeledURI: https://www.shongsheng.com
userPassword: 123456
uid: 102

# 张三
dn: cn=zhangsan,ou=tech,dc=example,dc=org
objectClass: inetOrgPerson
cn: Zhangsan
sn: zs
title: 张三
mail: zhangsan@qq.com
uid: 10001

# 李四
dn: cn=lisi,ou=tech,dc=example,dc=org
objectClass: inetOrgPerson
cn: lisi
sn: ls
title: 李四
mail: lisi@qq.com
uid: 10002

# 王五
dn: cn=wangwu,ou=tech,dc=example,dc=org
objectClass: inetOrgPerson
cn: wangwu
sn: ww
title: 王五
mail: wangwu@qq.com
uid: 10003
```

**执行命令：**

```bash
ldapadd -x -H ldap://localhost -D "cn=admin,dc=example,dc=org" -w 123456 -f ./import/addorg.ldif
ldapadd -x -H ldap://localhost -D "cn=admin,dc=example,dc=org" -w 123456 -f ./import/adduser.ldif
```



### 修改：ldapmodify

创建文件：`touch modify.ldif`

```ldif
# Ldif文件：modify.ldif

# 替换标题
dn: cn=shongsheng,dc=example,dc=org
changetype: modify
replace: title
title: 史宏生的Title.v2

# 添加描述
dn: cn=shongsheng,dc=example,dc=org
changetype: modify
add: description
description: 史宏生的Description.v2

# 修改密码
dn: cn=shongsheng,dc=example,dc=org
changetype: modify
replace: userPassword
userPassword: 666666

# 移动并重命名
# 1: 重命名CN: 将shongsheng重命名为hollson
# 2. 删除旧CN: 删除旧的shongsheng
# 3. 移动目录: 移动到tech组织机构下
dn: cn=shongsheng,dc=example,dc=org
changetype: modrdn
newrdn: cn=hollson
deleteoldrdn: 1
newsuperior: ou=tech,dc=example,dc=org
```

**执行命令：**

```shell
# 执行
ldapmodify -x -H ldap://localhost -D "cn=admin,dc=example,dc=org" -w 123456 -f ./import/modify.ldif

# 验证
ldapwhoami -x -D "cn=hollson,dc=example,dc=org" -w 666666
ldapwhoami -x -D "cn=hollson,ou=tech,dc=example,dc=org" -w 123456
ldapwhoami -x -D "cn=hollson,ou=tech,dc=example,dc=org" -w 666666
```



### 查询：ldapsearch

```js
-b <searchbase>：指定查找的节点，目录树的基准目录树信息。
-D <binddn>：指定查找的 DN，DN 是整个 OpenLDAP 树的唯一识别名称，类似于系统中根的概念。
-v：输出详细信息。
-x：使用简单的认证，不使用任何加密的算法，例如，TLS、SASL 等相关加密算法,默认使用SASL 认证方式。
-W：在查询时，会提示输入密码，如果不想输入密码，使用 -w password 即可。
-h（OpenLDAP 主机）：使用指定的 ldaphost，可以使用 FQDN 或 IP 地址。
-H（LDAP-URL）：使用 LDAP 服务器的 URL 地址进行操作。
-p（port）：指定 OpenLDAP 监听的端口（默认端口为 389，加速端口为 636）。
-LLL：禁止输出与过滤条件不匹配的信息。
```



```shell
# 添加命令别名，简化查询
alias ldapsearch='ldapsearch -x -H ldap://localhost -D "cn=admin,dc=example,dc=org" -w 123456'

# 查询
ldapsearch -b "dc=example,dc=org"
ldapsearch -b dc=example,dc=org "cn=shong*"
ldapsearch -b ou=tech,dc=example,dc=org "cn=shong*"

# 查询组织机构
ldapsearch -b dc=example,dc=org "ou=tec*"

# 指定查询字段
ldapsearch -b dc=example,dc=org "cn=zh*" cn title objectclass

# 组合过滤器
ldapsearch -b dc=example,dc=org "(&(objectclass=person)(cn=zha*))"
ldapsearch -b dc=example,dc=org "(&(objectclass=person)(cn=zha*))" +

# -x : 简单身份验证
# -LLL : 简化输出，不输出注释
# -b : 表示搜索的起始节点
# -s : 搜索范围，base, one, sub or children的一个
# -H : 用于指定链接的服务器，如果不指定默认为本机地址及ldap协议
ldapsearch -LLL -b 'ou=tech,dc=example,dc=org'
ldapsearch -LLL -b 'dc=example,dc=org' '(objectclass=*)'
ldapsearch -LLL -b 'dc=example,dc=org' '(cn=shong*)' +

# 查看当前 OpenLDAP 目录树关于 12345678 用户的信息（cn）
ldapsearch -x -LLL -b "dc=foobar,dc=com" uid=12345678 cn
```



### 删除：ldapdelete

```shell
# 删除张三
ldapdelete -x -D "cn=admin,dc=example,dc=org" -w 123456 "cn=zhangsan,dc=example,dc=org"
```



<br/>



## 用户密码

>  可以通过**ldappasswd**和**ldapmodify**两种方式修改用户密码

### ldappasswd方式

- 可以使用**随机密码**或**指定密码**。

```bash
# 管理员修改密码
ldappasswd -x -D "cn=admin,dc=example,dc=org" -w 123456 "uid=barbara,dc=example,dc=org"
ldappasswd -x -D "cn=admin,dc=example,dc=org" -w 123456 "uid=barbara,dc=example,dc=org" -s 111111

# 修改自己密码
ldappasswd -x -D "uid=barbara,dc=example,dc=org" -w 111111 -s 222222

# 验证密码
ldapwhoami -x -D "uid=barbara,dc=example,dc=org" -w 222222
```

### ldapmodify方式

> 参考 `ldapmodify`命令



<br/>

## 附：辅助操作

**1. cat创建ldif文件：** 

>  由于容器内一般无vi等编辑器，所以可以使用cat命令创建文本文件：
> 命令格式：`cat >x.ldif <<EOF... EOF`

```shell
cat > x.ldif << EOF
dn: dc=poke_domain,dc=com
o: poke_domain com
dc: poke_domain
objectClass: top
objectClass: dcObject
objectclass: organization
EOF
```



**2. 通道编辑ldif文件：** 

```shell
cat << EOF | ldapadd  -x -D "cn=admin,dc=example,dc=org" -w 123456
dn: uid=barbara,dc=example,dc=org
objectClass: account
objectClass: simpleSecurityObject
uid: barbara
userPassword: {SSHA}iES3qeH0nYUcwGtSQm1hIBCEsV+gBF3P
EOF
```
**3. 交互模式编辑ldif文件：** 

```shell
# 交互模式
ldapadd -x -D "cn=admin,dc=example,dc=org" -w 123456  <-回车进入交互模式

# 在交互模式下输入:
dn: cn=zhaoliu,dc=example,dc=org
objectClass: person
objectClass: organizationalPerson
objectClass: inetOrgPerson
cn: zhaoliu
sn: zhao
displayName: 赵六
mail: zhaoliu@example.com
userPassword: {SSHA}iES3qeH0nYUcwGtSQm1hIBCEsV+gBF3P
# 再次回车完成编辑
adding new entry "cn=zhaoliu,dc=example,dc=org"
```



<br/>



## 参考链接

https://liumiaocn.blog.csdn.net/article/details/83990918

https://blog.csdn.net/weixin_34061482/article/details/85025851
