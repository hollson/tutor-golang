package main

import (
	"crypto/tls"
	"fmt"
	"log"
	"os"
	"time"

	"github.com/go-ldap/ldap/v3"
)

// ;INI文件由节、键、值组成。
// [mysql]
// host = 192.168.xxx.xxx
// port = 3306
// charset  = "utf8"



//gopkg.in/ini
var (
	connModel = 0
	ldapAddr  = "localhost:389"
	baseDN    = "dc=example,dc=org"
	account   = "cn=admin,dc=example,dc=org"
	password  = "123456"
)

func newConn() (conn *ldap.Conn) {
	if connModel < 0 || connModel > 3 {
		fmt.Println(" ❌ ldap conn model error")
		os.Exit(1)
	}
	var err error
	switch connModel {
	case 0:
		conn, err = ldap.Dial("tcp", ldapAddr)
		log.Printf("connect mode : 【tcp normal】%v\n", ldapAddr)
	case 1:
		conn, err = ldap.DialTLS("tcp", ldapAddr, &tls.Config{InsecureSkipVerify: true})
		log.Printf("connect mode : 【tcp tls】%v\n", ldapAddr)
	case 2:
		conn, err = ldap.DialURL("ldap://" + ldapAddr)
		log.Printf("connect mode : 【ldap】%v\n", ldapAddr)
	// conn.StartTLS(&tls.Config{InsecureSkipVerify: true})
	case 3:
		conn, err = ldap.DialURL("ldaps://"+ldapAddr, ldap.DialWithTLSConfig(&tls.Config{InsecureSkipVerify: true}))
		log.Printf("connect mode : 【ldaps】%v\n", ldapAddr)
	}

	if err != nil {
		log.Printf(" ❌ ldap dial error,%v\n", err)
		os.Exit(1)
	}

	conn.SetTimeout(10 * time.Second)
	// if err = conn.Bind(account, password); err != nil {
	// 	log.Printf(" ❌ ldap bind admin error,%v\n", err)
	// 	os.Exit(1)
	// }
	return
}

func whoAmi(userName, password string) {
	cli := newConn()
	defer cli.Close()
	if err := cli.Bind(userName, password); err != nil {
		log.Printf(" ❌ ldap bind user error,%v\n", err)
		os.Exit(1)
	}

	result, err := cli.WhoAmI(nil)
	if err != nil {
		log.Printf("\n连接失败,err： \n%v\n", err)
		os.Exit(1)
	}
	fmt.Printf("\n连接成功,WhoAmi： \n%v\n\n", result.AuthzID)
}

// 客户提供信息如下：
// iphost : 172.16.95.31  iam-uat.fotc.com.cn
// 端口：1636
// 用户名：cn=uosadmin,dc=uos,dc=chinamoney,dc=com
// 密码：uosadmin
// 搜索入口：dc=uos,dc=chinamoney,dc=com
//go:generate GOOS=linux GOARCH=amd64 go build -o ldaper
//go:generate tar -zcvf ./ldap-tool.tar.gz ./ldap-tool
func main() {
	fmt.Println("请输入连接类型,0:Tcp,1:Tcp(TSL), 2:ldap,3: ldaps(TSL)")
	fmt.Scanln(&connModel)
	fmt.Println("请输入连接地址,如：localhost:389")
	fmt.Scanln(&ldapAddr)
	fmt.Println("请输入baseDN,如：dc=example,dc=org")
	fmt.Scanln(&baseDN)
	fmt.Println("请输入用户账号,如：cn=admin,dc=example,dc=org")
	fmt.Scanln(&account)
	fmt.Println("请输入用户密码")
	fmt.Scanln(&password)

	fmt.Println()
	fmt.Println("请确认连接信息 ：")
	fmt.Printf("conn model：%v\n", connModel)
	fmt.Printf("ldap host：%v\n", ldapAddr)
	fmt.Printf("baseDN：%v\n", baseDN)
	fmt.Printf("account：%v\n", account)
	fmt.Printf("password：%v\n", password)

	fmt.Print("\n回车键继续:")
	fmt.Scanln()

	whoAmi(account, password)
	whoAmi("cn=shongsheng,dc=example,dc=org", "123456")
}

// https://github.com/shanghai-edu/ldap-test-tool
