// Copyright 2022 Hollson. All rights reserved.
// Use of this source code is governed by a MIT style
// license that can be found in the LICENSE file.

package main

import (
	_ "embed"
	"fmt"
)

//go:embed config.ini
var example string

func main() {
	fmt.Println(example)
}
