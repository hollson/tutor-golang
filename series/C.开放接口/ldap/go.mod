module ldap_project

go 1.17

require (
	github.com/go-ldap/ldap/v3 v3.4.4
	github.com/liushuochen/gotable v0.0.0-20220831134725-cbcd6bb0a5f9
)

require (
	github.com/Azure/go-ntlmssp v0.0.0-20220621081337-cb9428e4ac1e // indirect
	github.com/go-asn1-ber/asn1-ber v1.5.4 // indirect
	github.com/mattn/go-runewidth v0.0.9 // indirect
	github.com/olekukonko/tablewriter v0.0.5 // indirect
	golang.org/x/crypto v0.0.0-20220622213112-05595931fe9d // indirect
)
