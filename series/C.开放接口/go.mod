module openapi

go 1.17

require (
	github.com/aws/aws-sdk-go v1.41.0
	github.com/go-ldap/ldap/v3 v3.4.3
)

require (
	github.com/Azure/go-ntlmssp v0.0.0-20211209120228-48547f28849e // indirect
	github.com/go-asn1-ber/asn1-ber v1.5.4 // indirect
	github.com/jmespath/go-jmespath v0.4.0 // indirect
	golang.org/x/crypto v0.0.0-20220331220935-ae2d96664a29 // indirect
)
