# aws客户端工具：
#
# 官方文档：https://docs.aws.amazon.com/zh_cn/cli/latest/userguide/cli-chap-welcome.html

#================================================安装aws命令后工具==========================================
curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscli.zip"
unzip awscli.zip
sudo ./aws/install
aws --version

# 配置aws
$ aws configure
AWS Access Key ID [None]: AKIAYXWTULWBIDOZTF45
AWS Secret Access Key [None]: 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
Default region name [None]: us-east-1
Default output format [None]: json

# 开启智能提示
export AWS_CLI_AUTO_PROMPT=on

#===================================================aws-s3命令=============================================
aws s3 help

# 创建桶
aws s3 mb s3://bucket-name

# 查看桶或对象
aws s3 ls
aws s3 ls s3://bucket-name
aws s3 ls s3://bucket-name/example/

# 删除桶
aws s3 rb s3://bucket-name
aws s3 rb s3://bucket-name --force

# 删除对象(recursive:递归)
aws s3 rm s3://bucket-name/example/test
aws s3 rm s3://bucket-name/example --recursive

# 移动对象
aws s3 mv s3://bucket-name/example s3://my-bucket/ # S3 -> S3
aws s3 mv test.zip s3://bucket-name                # 本地 -> S3
aws s3 mv s3://bucket-name/test.zip ./             # S3 -> 本地

# 复制(上传下载)对象
aws s3 cp s3://bucket-name/example s3://my-bucket/                                     # S3 -> S3
aws s3 cp test.zip s3://bucket-name                                                    # 本地 -> S3
aws s3 cp s3://bucket-name/test.zip ./                                                 # S3 -> 本地
echo "hello world" | aws s3 cp - s3://bucket-name/test.zip                             # 文本 -> S3
aws s3 cp s3://bucket-name/test.zip -                                                  # S3 -> 控制台
aws s3 cp s3://bucket-name/pre - | bzip2 --best | aws s3 cp - s3://bucket-name/key.bz2 # S3 -> (控制台)压缩 -> S3

# 差异同步
aws s3 sync . s3://my-bucket/path                                       # 本地 -> S3
aws s3 sync . s3://my-bucket/path --delete                              # 本地 -> S3 (删除S3多余对象)
aws s3 sync s3://my-bucket/path . --delete                              # S3 -> 本地 (删除本地多余对象)
aws s3 sync . s3://my-bucket/path --storage-class STANDARD_IA           # 与不频繁访问存储类同步
aws s3 sync . s3://my-bucket/path --delete --exclude "path/MyFile?.txt" # 排除文件

# 权限控制
aws s3 sync . s3://my-bucket/path --acl public-read

# S3Api接口
aws s3api list-objects --bucket text-content
aws s3api head-bucket --bucket my-bucket
aws s3api put-object --bucket text-content --key dir-1/my_images.tar.bz2 --body my_images.tar.bz2

#===================================================Reworld操作实例=============================================
aws s3 ls
aws s3 ls s3://sg-sourcefile.reworld.io/resource/
aws s3 sync ./resource s3://sg-sourcefile.reworld.io/resource --delete --acl public-read
http://sg-sourcefile.reworld.io.s3.ap-southeast-1.amazonaws.com/resource/hello.txt


http://sg-sourcefile.reworld.io.s3.ap-southeast-1.amazonaws.com/resource/T37HxTBvDT1RCvBVdK


aws s3 mb s3://airacer-cn-test
aws s3 mb s3://airacer-cn-dc
aws s3 mb s3://airacer-cn-taxi-stage
aws s3 mb s3://airacer-cn-dc-stage
aws s3 sync . s3://airacer-cn-test


