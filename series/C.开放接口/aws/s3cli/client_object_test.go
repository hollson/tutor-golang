// Copyright 2021 Hollson. All rights reserved.
// Use of this source code is governed by a MIT style
// license that can be found in the LICENSE file.

package s3cli

import (
	"fmt"
	"os"
	"strings"
	"testing"
	"time"
)

func TestClient_PutObject(t *testing.T) {
	cli := Default()

	fmt.Println(cli.PutObject("reworld-bucket-test-01", "test01", strings.NewReader("hello test01")))
	fmt.Println(cli.PutObject("reworld-bucket-test-01", "test01", strings.NewReader("hello 覆盖01")))
	fmt.Println(cli.PutObject("reworld-bucket-test-01", "test02", strings.NewReader("hello test02")))
	fmt.Println(cli.PutObject("reworld-bucket-test-01", "test01/aaa", strings.NewReader("hello aaa")))
	fmt.Println(cli.PutObject("reworld-bucket-test-01", "test01/bbb", strings.NewReader("hello_bbb")))

	f, _ := os.Open("/Users/sybs/Desktop/elephant.png")
	fmt.Println(cli.PutObject("reworld-bucket-test-01", "elephant.png", f))
	// https://reworld-bucket-test-01.s3.ap-southeast-1.amazonaws.com/elephant.png
}

func TestClient_Get(t *testing.T) {
	cli := Default()
	n, ret, err := cli.Get("reworld-bucket-test-01", "test01")
	fmt.Println(n, string(ret), err)
	n, ret, err = cli.Get("reworld-bucket-test-01", "test02")
	fmt.Println(n, string(ret), err)
	n, ret, err = cli.Get("reworld-bucket-test-01", "test01/aaa")
	fmt.Println(n, string(ret), err)
	n, ret, err = cli.Get("reworld-bucket-test-01", "test01/bbb")
	fmt.Println(n, string(ret), err)
}

func TestClient_ListObjects(t *testing.T) {
	cli := Default()
	fmt.Println(cli.ListObjects("reworld-bucket-test-01"))
	fmt.Println(cli.ListObjects("reworld-bucket-test-01", "test"))
	fmt.Println(cli.ListObjects("reworld-bucket-test-01", "test01"))
}

func TestClient_DeleteObject(t *testing.T) {
	cli := Default()
	fmt.Println(cli.DeleteObject("reworld-bucket-test-01", "elephant"))
}

func TestClient_DeleteObjects(t *testing.T) {
	cli := Default()
	fmt.Println(cli.DeleteObjects("reworld-bucket-test-01", "elephant2", "test02"))
}

func TestClient_Upload(t *testing.T) {
	cli := Default()
	fmt.Println(cli.Upload("reworld-bucket-test-01", "file01", strings.NewReader("hello01.jpg")))
	fmt.Println(cli.Upload("reworld-bucket-test-01", "file01", strings.NewReader("hello01.png")))
	fmt.Println(cli.Upload("reworld-bucket-test-01", "file02", strings.NewReader("hello02.txt")))
	fmt.Println(cli.Upload("reworld-bucket-test-01", "file01/aaa", strings.NewReader("hello_aaa.mp3")))
	fmt.Println(cli.Upload("reworld-bucket-test-01", "file01/bbb", strings.NewReader("hello_bbb.zip")))

	f, _ := os.Open("/Users/sybs/Desktop/elephant.png")
	fmt.Println(cli.Upload("reworld-bucket-test-01", "postgres.png", f))
}

func TestClient_UploadWithContext(t *testing.T) {
	cli := Default()
	fmt.Println(cli.UploadWithContext("reworld-bucket-test-01", "file01", strings.NewReader("hello01.jpg"), time.Second*5))
}

func TestClient_Download(t *testing.T) {
	cli := Default()
	fmt.Println(cli.Download("reworld-bucket-test-01", "file01"))
	fmt.Println(cli.Download("reworld-bucket-test-01", "file01/aaa"))
}

func TestClient_CopyObject(t *testing.T) {
	cli := Default()
	fmt.Println(cli.CopyObject("reworld-bucket-test-01", "file01/aaa", "reworld-bucket-test-02", "hello_copy.mp3"))
	n, ret, err := cli.Get("reworld-bucket-test-02", "hello_copy.mp3")
	fmt.Println(n, string(ret), err)
}
