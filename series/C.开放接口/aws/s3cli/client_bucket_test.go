// Copyright 2021 Hollson. All rights reserved.
// Use of this source code is governed by a MIT style
// license that can be found in the LICENSE file.

package s3cli

import (
	"fmt"
	"testing"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/s3"
)

func TestClient_CreateBucket(t *testing.T) {
	cli := Default()
	ret, err := cli.CreateBucket("reworld-bucket-test-01")
	// aerr, ok := err.(awserr.Error) //aerr.Code()获取错误码
	fmt.Println(ret, err)
}

func TestClient_ListBuckets(t *testing.T) {
	cli := Default()
	fmt.Println(cli.ListBuckets())
}

func TestClient_GetBucketAcl(t *testing.T) {
	cli := Default()
	fmt.Println(cli.GetBucketAcl("sg-xk-reworld-io"))
}

func TestClient_DeleteBucket(t *testing.T) {
	cli := Default()
	fmt.Println(cli.DeleteBucket("reworld-bucket-test-01"))
}

func TestClient_SetBucketAcl(t *testing.T) {
	cli := Default()
	fmt.Println(cli.SetBucketAcl("reworld-bucket-test-02", BucketAcl{
		ACL: aws.String(s3.BucketCannedACLPrivate),
	}))
}

func TestClient_GetBucketAcl1(t *testing.T) {
	cli := Default()
	fmt.Println(cli.GetBucketAcl("reworld-bucket-test-01"))
}
