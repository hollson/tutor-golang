# 证书与安全

https://blog.csdn.net/weiyuanke/article/details/87256937
https://gohalo.me/post/security-openssl-commands-usage-introduce.html

https://juejin.cn/post/6844903989469773832

```shell
$ openssl ras help
usage: rsa [options]

 -check             检测秘钥合法性
 -in file           输入的文件名
 -out file          输出的文件名
 -inform defaultFormat     输入文件的格式 (DER, NET or PEM (default))
 -outform defaultFormat    输出文件的格式(DER, NET or PEM (default PEM))
 -passin src        输入文件的密码
 -passout src       输出文件的密码
 -pubin             该指令说明输入的是公钥，默认为私钥
 -pubout            该指令说明需要输出公钥，默认输出私钥
 -sgckey            what it is？
 -text              打印信息
```

```shell
# 生成私钥, 同openssl genrsa -out rsa_private.key 2048)
openssl genpkey -algorithm rsa -out rsa_private.key

# 根据私钥生成公钥(pubout:shuchu)
openssl rsa -in rsa_private.key -pubout -out rsa_public.key

# 公钥加密，私钥解密
openssl rsautl -encrypt -inkey rsa_public.key -pubin -in a.txt -out a.txt.en
openssl rsautl -decrypt -inkey rsa_private.key -in a.txt.en -out b.txt

# 私钥签名，公钥验签
openssl rsautl -sign   -inkey rsa_private.key -in a.txt -out a.txt.sign
openssl rsautl -verify -inkey rsa_public.key  -in a.txt.sign -pubin
```

（1）证书：公钥信息 + 额外的其他信息（比如所属的实体，采用的加密解密算法等）= 证书。证书文件的扩展名一般为crt。

看该证书的相关信息

openssl x509 -in .minikube/apiserver.crt -noout -text

（2）CA：证书认证中心；拿到一个证书之后，得先去找CA验证下，拿到的证书是否是一个“真”的证书，而不是一个篡改后的证书。

如果确认证书没有问题，那么从证书中拿到公钥之后，就可以与对方进行安全的通信了，基于非对称加密机制。 CA自身的分发及安全保证，一般是通过一些权威的渠道进行的，比如操作系统会内置一些官方的CA、浏览器也会内置一些CA； 采用CA校验一个证书
openssl verify -CAfile xxxx.crt usercert.crt 例如： $openssl verify -CAfile ca.cert tls.cert tls.cert: OK

# ssh-keygen命令

https://blog.csdn.net/qq_40932679/article/details/117487540

# SSL和SSH和OpenSSH，OpenSSL有什么区别

- 1、SSL(Secure Sockets Layer 安全套接层),它提供使用 TCP/IP 的通信应用程序间的隐私与完整性。比如你访问https://servername
  就是用了ssl协议，地址栏会出现小锁，双击就能查看ssl服务器证书的详细信息。TCP端口：443
- 2、SSH(Secure Shell 远程登陆用)，安全可以和telnet比较一下，比如telnet传输用户密码是明文的，而SSH是加密的。明文的可以监听到。TCP端口22
- 3、OpenSSH是个SSH的软件，OpenSSH is the premier connectivity tool for remote login with the SSH protocol.
  linux/unix都用openssh软件提供SSH服务。简单来说，比如以前的Solaris系统默认不提供ssh服务，需要安装OpenSSH才行。