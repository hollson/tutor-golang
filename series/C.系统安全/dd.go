package main

import (
	"crypto/rand"
	"crypto/rsa"
	"crypto/x509"
	"encoding/base64"
	"encoding/pem"
	"errors"
	"fmt"
	"testing"
)

// 可通过openssl产生
// openssl genrsa -out rsa_private_key.pem 2048
var privateKey = []byte(`
-----BEGIN RSA PRIVATE KEY-----
MIIEpQIBAAKCAQEAqLVnGAeN8Ox8CG3xFW1EOX0tADMO07qFgA/11hMsPPlk95FK
w7MfioSUkoyU7b6TpShCaenjSpMUUA+dFqpFOQy9e1Nlq/XG38zjmxql6Y7hWhFe
O66unO44myqySvvXimlV+iKXux+C6l/fH02Uabx7N7jIPXrj3XIKxOt4wb6MQe5A
F5Qb+vOviBIKi33pxhgSs6jlTScjhp7z+qwQuzxFhtIpL9zJZyqzODxwsmWDZ2Kt
52IE/xvhwXf7KH5cuyGhCC9lo1AXazwgUSkBfPXOmOMF4Xd1u5xJ879fEaOrxjUz
OUyXzMQ4+ImvkEoHvgL6gS7vr2SjdJgomZ3NbQIDAQABAoIBAQCdGe+arM48n7sQ
jnCFp250qmARY3pXJ8XRbwtaAhib3xFqiF26xz6D2toPbeHxUhpjHY5a2nXRqAyu
URvTU1OKwJZiFVMEQqajGUSJ1tLRH0AygyHhKdVoRV1pAOVudf5M4bIyb6wjkEB0
yDoaCEBABkgnp2av+ZPfTqdAoJWes7MKAWtsp3SeheL9dXfYKLlcS9G+OWC0ZZjo
9hpGrtiy6RKcIpQUmFBy91xxNGIgjou+AItqwEMRjeRUXCs/LS9y6RjvmkO7Sr5b
IZM3kK848qkgVaKWf7zpRv1CTKLTCC76A29JXb9leHulpaonDCdJrJQYfIwYiDtu
kGM+yD4BAoGBANK0bWyUzj4V93c4ozj+hiznBvdmYbtpGYxsbVgW70bR9nCeKOlz
DQ2Jg9X9RVseTzC7dY+uI/D87vKlr2cVljIXT7GKyKyDLLkeSFpSA3aQT9t8Az/y
C7mwcZFDi6KnY1szLzoW7GazRv3XqopqSgcbOMhVGBjPyZW5Vetxs1BNAoGBAMz5
0+TaEBv8ZgGhMigGE9YnPvSiUY3wCmFDk0zws/tz6WfSgfODX+m58k5NiUi79+9A
rCjugmU2Te5G4xuiuC8RuK74kFabwb17JUdo47TMZAhGisHw9GuVSyUcJsLPLqNJ
VfiLLJe1IiYcp5LDi/+JnyIX4mbmxN4Vaf96qAGhAoGABZnCJta1vLS8xTB+Ni+d
xWfGqaDRDDg486CWL3c8mPdPw6KQl9P1Dt0O4gs/YrKljDgPYehREEDI4S5CQU0z
ltVDywZTWzKJZ8pERhJUATzIwp7DP1nhsefcvO28snAclfoAUVz2n6w4QNsQr2kv
2oyAr+KDJ3WueApbHE0DZSkCgYEAhGIqF1k7JEgMPas1cKIUN7DbOTaIg6pvmcgC
H6QqKhH/wj00Dulfjd25gBcVvnMgO1bCmiEI+INtK6J+1X9vg37nj6Ib3sP0CgO5
3PgCcOe9B5c/0OhlhAJFKS+YXSNgVLdtnfuNDmI/Luw80k4XYjApJeTVJW6P4Xtl
HUrDrWECgYEAzbm9FCdY92UmHsphiSt/NZ4H5G7PDIw58oOZMlVrXH9HmCy2d0HC
WH7ikkurjO00VdeDlM+Hqperp53zG9Px6O+bTYCMRP6NyEYslzTZ3/ymQipqJDlz
O29ClLXxNkYx3zkUEQxvWKpWkJTRM5myPB4weM0KETmjxRHoiOG02s0=
-----END RSA PRIVATE KEY-----
`)

// openssl
// openssl rsa -in rsa_private_key.pem -pubout -out rsa_public_key.pem
var publicKey = []byte(`
-----BEGIN PUBLIC KEY-----
MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAqLVnGAeN8Ox8CG3xFW1E
OX0tADMO07qFgA/11hMsPPlk95FKw7MfioSUkoyU7b6TpShCaenjSpMUUA+dFqpF
OQy9e1Nlq/XG38zjmxql6Y7hWhFeO66unO44myqySvvXimlV+iKXux+C6l/fH02U
abx7N7jIPXrj3XIKxOt4wb6MQe5AF5Qb+vOviBIKi33pxhgSs6jlTScjhp7z+qwQ
uzxFhtIpL9zJZyqzODxwsmWDZ2Kt52IE/xvhwXf7KH5cuyGhCC9lo1AXazwgUSkB
fPXOmOMF4Xd1u5xJ879fEaOrxjUzOUyXzMQ4+ImvkEoHvgL6gS7vr2SjdJgomZ3N
bQIDAQAB
-----END PUBLIC KEY-----
`)

// 加密
func RsaEncrypt(origData []byte) ([]byte, error) {
	// 解密pem格式的公钥
	block, _ := pem.Decode(publicKey)
	if block == nil {
		return nil, errors.New("public key error")
	}
	// 解析公钥
	pubInterface, err := x509.ParsePKIXPublicKey(block.Bytes)
	if err != nil {
		return nil, err
	}
	// 类型断言
	pub := pubInterface.(*rsa.PublicKey)
	// 加密
	return rsa.EncryptPKCS1v15(rand.Reader, pub, origData)
}

// 解密
func RsaDecrypt(ciphertext []byte) ([]byte, error) {
	// 解密
	block, _ := pem.Decode(privateKey)
	if block == nil {
		return nil, errors.New("private key error!")
	}
	// 解析PKCS1格式的私钥
	priv, err := x509.ParsePKCS1PrivateKey(block.Bytes)
	if err != nil {
		return nil, err
	}
	// 解密
	return rsa.DecryptPKCS1v15(rand.Reader, priv, ciphertext)
}

func TestSecurity(t *testing.T) {
	data, _ := RsaEncrypt([]byte("abc"))
	fmt.Printf("加密后的密文:\n%s\n", base64.StdEncoding.EncodeToString(data))
	origData, _ := RsaDecrypt(data)
	fmt.Printf("解密后的明文:%s\n", string(origData))
}
