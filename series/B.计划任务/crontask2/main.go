package main

import (
    "fmt"
	"os"
	"time"
)

func main() {
	os.Mkdir("script", 0766)

	job := &CronJob{}
	job.Spec = "@every 1s"
	job.Script = `
	set -e
	echo "working start"
	make
	echo "working end"
	`

	crontask := NewCronTask()
	crontask.Load(nil)
	crontask.Start()
	err := crontask.AddJob(job)
	if err != nil {
		panic(err)
	}
	time.Sleep(3 * time.Second)
	crontask.Stop()
	snap, err := crontask.Snapshot()
	if err != nil {
		panic(err)
	}
	fmt.Println(snap.Stringify())
}
