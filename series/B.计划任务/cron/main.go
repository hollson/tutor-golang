package main

import (
	"fmt"
	"time"

	"golang/计划任务/cron/src/cron"
)

func main() {
	c := cron.New()
	c.Start()
	fmt.Println(time.Now())
	c.Add("@every 1s", 10, func() { fmt.Println("Every 1 seconds", time.Now()) })
	c.Add("@every 5s", 10, func() { fmt.Println("Every 5 seconds", time.Now()) })
	c.Add("0 30 * * * *", 1, func() { fmt.Println("Every hour on the half hour", time.Now()) })
	c.Add("@hourly", 1, func() { fmt.Println("Every hour", time.Now()) })
	c.Add("@at 2017-08-17T12:10:00+08:00", 1, func() { fmt.Println("run at", time.Now()) })
	time.Sleep(time.Second * 10000)
}
