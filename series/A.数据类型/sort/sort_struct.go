package main

import (
	"fmt"
	"sort"
)

type Person struct {
	Name string
	Age  int
}
type Persons []Person

func (p Persons) Less(i, j int) bool { return p[i].Age < p[j].Age }
func (p Persons) Swap(i, j int)      { p[i], p[j] = p[j], p[i] }
func (p Persons) Len() int           { return len(p) }

func main() {
	ps:=Persons{{"Tom",26},{"Jerry",28},{"Lucy",17}}
	sort.Sort(ps)
	fmt.Println("正向排序：",ps)

	sort.Sort(sort.Reverse(ps))
	fmt.Println("反向排序：",ps)
}