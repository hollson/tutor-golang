/*****************************************************
 sort库提供类 IntSlice、Float64Slice、StringSlice三个
 基础类型切片的排序操作，如：
*******************************************************/

package main

import (
	"fmt"
	"sort"
)

//go:generate man ascii  #查看Ascii码
func main() {
	var (
		_ints     = []int{5, 2, 7, -7, 0, 0, 42, 999, -1024}
		// _float64s = []float64{math.Inf(1), -256, 1.20, math.NaN(), 2.2e3, math.Inf(-1), 7.8}
		// _strings  = []string{"@@@", "A", "a", "foo", "中", "foo", "123", "***", "$$$"}
	)

	// 正向排序：
	// 说明：将切片转换成Slice对象，并排序
	sort.IntSlice(_ints[0:]).Sort()
	fmt.Println(_ints)
	fmt.Println("是否已正向排序：", sort.IntsAreSorted(_ints))
	fmt.Println("第一个x所在位置：", sort.SearchInts(_ints, 0))

	// 反向排序：
	// 将Slice对象做反向预处理，并排序
	sort.Sort(sort.Reverse(sort.IntSlice(_ints)))
	fmt.Println(_ints)
}

// // ExampleReverse 标准示例：
// func ExampleReverse() {
// 	s := []int{5, 2, 6, 3, 1, 4} // unsorted
// 	sort.Sort(sort.Reverse(sort.IntSlice(s)))
// 	fmt.Println(s)
// 	// Output: [6 5 4 3 2 1]
// }
