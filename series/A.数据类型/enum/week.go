package enum

import (
	"encoding/json"
)

/***************************************************
Stringer工具：
  go install golang.org/x/tools/cmd/stringer

说明：仅适用于string类型的枚举常量
***************************************************/

//go:generate stringer -type=Week
type Week int

const (
	Sun Week = iota
	Mon
	Tue
	Wed
	Thu
	Fri
	Sat
)

func (i Week) Enum() *Week {
	p := new(Week)
	*p = i
	return p
}

func (g Week) MarshalBinary() ([]byte, error) {
	// return []byte(g.String()), nil
	// bytes.
	return json.Marshal(g)
}

func (g Week) UnmarshalBinary(data []byte) error {
	// g = Week(data)
	// return nil
	return json.Unmarshal(data, &g)
}
