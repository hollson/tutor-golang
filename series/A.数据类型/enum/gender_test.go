// Copyright 2022 Hollson. All rights reserved.
// Use of this source code is governed by a MIT style
// license that can be found in the LICENSE file.

package enum

import (
	"context"
	"fmt"
	"testing"

	"github.com/go-redis/redis/v8"
)

func TestMarshal(t *testing.T) {
	var ctx = context.Background()
	rdb := redis.NewClient(&redis.Options{Addr: "127.0.0.1:6379"})

	// fmt.Println(rdb.Set(ctx, "gender", Male, 0).Result())
	//
	// var g Gender
	// err := rdb.Get(ctx, "gender").Scan(&g)
	// fmt.Println(g.String(), err)

	fmt.Println(rdb.Set(ctx, "testweek", Thu, 0))

	var w Week
	// rdb.Get(ctx, "testweek").Result()
	// fmt.Println(w)
	// var ff []byte
	err := rdb.Get(ctx, "testweek").Scan(&w)
	fmt.Println(w, err)
}
