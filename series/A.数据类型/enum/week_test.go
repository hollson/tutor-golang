// Copyright 2022 Hollson. All rights reserved.
// Use of this source code is governed by a MIT style
// license that can be found in the LICENSE file.

package enum

import (
	"fmt"
	"testing"
)

func Foo(w *Week) {
	fmt.Println(w)
}

func TestEnum(t *testing.T) {
	var w = Fri
	Foo(w.Enum())
}
