package enum

import (
	"bytes"
	"encoding/binary"
	"time"
)

// Gender 性别
//  ❌ 原则上枚举类型都应该上数值类型
type Gender string

const (
	Male   Gender = "male"   // 男
	Female        = "female" // 女
)

func (g Gender) Enum() *Gender {
	p := new(Gender)
	*p = g
	return p
}

func (g Gender) String() string {
	return string(g)
}

// func (g Gender) Marshal() ([]byte, error) {
// 	return []byte(g), nil
// }
//
// func (g Gender) Unmarshal([]byte) error {
// 	return nil
// }

func (g Gender) MarshalBinary() ([]byte, error) {
	time.Now().MarshalBinary()

	var buf bytes.Buffer
	err := binary.Write(&buf, binary.BigEndian, g)
	return buf.Bytes(), err
}

func (g Gender) UnmarshalBinary(data []byte) error {
	return binary.Read(bytes.NewBuffer(data), binary.BigEndian, &g)
}
