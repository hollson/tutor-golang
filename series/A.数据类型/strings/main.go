package main

import (
    "fmt"
	"strings"
	"unicode"
)
//go test -bench=.* -benchmem
func main() {
	fmt.Println(strings.Contains("main", "in"))
	fmt.Println(strings.Contains("main", ""))
	fmt.Println(strings.Contains("", ""))

	fmt.Println(strings.ContainsAny("main", "ac"))
	fmt.Println(strings.ContainsAny("main", "c"))

	fmt.Println(strings.Count("cheese", "e"))
	fmt.Println(strings.Count("five", "")) // before & after each rune

	fmt.Println(strings.Replace("oink oink oink", "k", "ky", 2))
	fmt.Println(strings.Replace("oink oink oink", "oink", "moo", -1))

	fmt.Println(strings.ToUpper("oink"))
	fmt.Println(strings.ToLower("OinK"))

	fmt.Printf("%q\n", strings.Trim(" !!! Achtung ! Achtung ! !!! ", "! "))
	fmt.Printf("%q\n", strings.TrimSpace(" \n\t Achtung ! Achtung \r"))

	str := []string{"s", "t", "r"}
	fmt.Println(strings.Join(str, ", "))

	fmt.Printf("%q\n", strings.Split(",a,b,c,", ","))
	fmt.Printf("%q\n", strings.Split(" xyz ", ""))
	fmt.Printf("%q\n", strings.Split("", "Bernardo O'Higgins"))
	fmt.Printf("%q\n", strings.Fields("hello world \nuser\ntom"))
	fmt.Printf("%q\n", strings.FieldsFunc("hello,,,,,,world,\nuser\ntom", func(r rune) bool {
		if unicode.IsSpace(r) || r == rune(',') {
			return true
		}
		return false
	}))

	// interesting
	fmt.Println(string([]byte{'a', 'b', 'c'}))
	fmt.Println(string([]byte{'a', 'b', 'c', 0, 'd'}))
	fmt.Println([]byte(string([]byte{'a', 'b', 'c', 0, 'd'})))

	// index
	str2 := "string"
	idx := strings.Index(str2, "i")
	fmt.Println(idx, str2[:idx])
}
