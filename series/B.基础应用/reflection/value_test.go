// Copyright 2021 Hollson. All rights reserved.
// Use of this source code is governed by a MIT style
// license that can be found in the LICENSE file.

package main

import (
	"fmt"
	"math"
	"net/http"
	"reflect"
	"testing"

	"golang/基础应用/reflection/user"
)

// 简单类型
func TestSimpleValue(t *testing.T) {
	var a = math.E
	valeResolve(a)
	valeResolve(&a)
	valeResolve("abc")
}

// 结构体
func TestStructValue(t *testing.T) {
	u := user.Player{
		User: user.User{
			Indent: "622722199900111120",
			Name:   "张三",
		},
		Account: "xxxx-xxxx-xxxx",
		Age: struct {
			Year, Month, Day int
		}{1988, 12, 30},
		Skills: []string{"CF", "AF47"},
	}

	valeResolve(u)
	valeResolve(&u)
}

// 数组
func TestArrayValue(t *testing.T) {
	valeResolve([3]int{1, 2, 3})
}

// 切片
func TestSliceValue(t *testing.T) {
	valeResolve([]string{"a", "b"})
}

func TestMapValue(t *testing.T) {
	valeResolve(map[string]int{"a": 1, "b": 2, "c": 3})

	var r = Router(nil)
	valeResolve(r)
	valeResolve(&r)
}

// 函数
func TestFuncValue(t *testing.T) {
	valeResolve(func(a, b, c string, d ...int) (int, error) { return 0, nil })
	valeResolve(http.HandlerFunc(nil))
}

// Channel
func TestChannelValue(t *testing.T) {
	valeResolve(chan<- int(nil))
	valeResolve(make(<-chan bool))
	valeResolve(make(chan struct{}))
}

// 修改数据
func TestUpdateValue(t *testing.T) {
	u := user.Player{User: user.User{Name: "张三"}}
	err := updateValue(&u) // 必须是指针传递
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Println(u)
}

// 反射值数据
func valeResolve(i interface{}) {
	val := reflect.ValueOf(i)
	fmt.Println("=====================基础元数据=====================")
	fmt.Printf("归类: %v\n", val.Kind())
	fmt.Printf("数值: %v\n", val)
	fmt.Printf("描述: %v\n", val.String()) // 同val.Kind()==reflect.String

	// 简单类型
	if val.Kind() == reflect.Float64 {
		fmt.Printf("类型: %v\n", val.Float())
	}

	// 简单类型
	if val.Kind() == reflect.Struct {
		fmt.Printf("类型: %v\n", val.Float())
	}

	// fmt.Printf("Value类型: %v\n", val.CanAddr())
}

func updateValue(i interface{}) error {
	if reflect.TypeOf(i).Kind() == reflect.Ptr {
		reflect.ValueOf(i).Elem().FieldByName("Name").SetString("李四")
		return nil
	}
	return fmt.Errorf("i必须是一个指针类型")
}
