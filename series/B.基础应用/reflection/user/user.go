package user

import (
	"errors"
	"fmt"
	"time"
)

type Game interface {
	Play(game string) (string, error)
}

type User struct {
	Indent string `json:"indent"`                                          // 身份证
	Name   string `protobuf:"bytes,2,req,name=name" json:"name,omitempty"` // 标签
}

type Player struct {
	User    `json:"user,omitempty" form:"user"` // 继承(匿名类型)
	Account string
	Age     struct{ Year, Month, Day int } // 内嵌结构体
	vip     bool                           `json:"vip"`     // 私有字段
	Skills  []string                       `json:"skills"`  // 引用类型
	Friends map[string]string              `json:"friends"` // 字典类型
	RegTime time.Time                      `json:"reg_time"`
}

// 值方法
func (p Player) Play(game string) (string, error) {
	if len(game) == 0 {
		return "", errors.New("game name is empty")
	}
	return fmt.Sprintf("%s play %s", p.Name, game), nil
}

// 指针方法
func (p *Player) Stop(game string) (string, error) {
	if len(game) == 0 {
		return "", errors.New("game name is empty")
	}
	return fmt.Sprintf("%s play %s", p.Name, game), nil
}

// 实现了fmt.Stringer接口
func (p *Player) String() string {
	return fmt.Sprintf("%+v", *p)
}

// 私有方法
func (p Player) greet() {
	fmt.Println("hello")
}
