package main

import "reflect"

// 结构体的Tag
type TagFinder struct {
	name string
	path map[string][]string
}

func NewTagFinder(i interface{}, name string) *TagFinder {
	finder := &TagFinder{}
	finder.name = name
	finder.path = make(map[string][]string)
	finder.init(reflect.ValueOf(i).Elem(), []string{})
	return finder
}

func (f *TagFinder) init(val reflect.Value, way []string) {
	for i := 0; i < val.NumField(); i++ {
		switch val.Field(i).Kind() {
		case reflect.Struct:
			way = append(way, val.Type().Field(i).Name)
			f.init(val.Field(i), way)
			way = way[:len(way)-1]
		default:
			tag := val.Type().Field(i).Tag.Get(f.name)
			if tag != "" {
				way = append(way, val.Type().Field(i).Name)
				way_copy := make([]string, len(way))
				copy(way_copy, way)
				f.path[tag] = way_copy
				way = way[:len(way)-1]
			}
		}
	}
}

func (f *TagFinder) GetValue(i interface{}, name string) *reflect.Value {
	if way, ok := f.path[name]; ok {
		v := reflect.ValueOf(i).Elem()
		for _, w := range way {
			v = v.FieldByName(w)
		}
		return &v
	}
	return nil
}

func (f *TagFinder) GetInt(i interface{}, name string) int {
	return int(f.GetValue(i, name).Int())
}

func (f *TagFinder) GetString(i interface{}, name string) string {
	return f.GetValue(i, name).String()
}
