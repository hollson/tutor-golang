package main

import (
	"fmt"
	"net/http"
	"reflect"
	"testing"

	"golang/基础应用/reflection/user"
)

// 简单类型
func TestSimpleType(t *testing.T) {
	typeResolve(int(1))
	typeResolve((*float32)(nil))
}

// 结构体
func TestStructType(t *testing.T) {
	typeResolve(user.Player{})  // 值类型
	typeResolve(&user.Player{}) // 指针类型
}

// 数组
func TestArrayType(t *testing.T) {
	typeResolve([3]int{1, 2, 3})
}

// 切片
func TestSliceType(t *testing.T) {
	typeResolve([]string{"a", "b"})
}

// 字典
type Router map[string]string

// 任何类型都可以有方法,且值类型和指针类型的方法集不相同，值类型会包装一个指针类型的方法
func (Router) String() string { return "router" }

func TestMapType(t *testing.T) {
	typeResolve(map[string]int{"a": 1, "b": 2, "c": 3})

	var r = Router(nil)
	typeResolve(r)
	typeResolve(&r)
}

// 函数
func TestFuncType(t *testing.T) {
	typeResolve(func(a, b, c string, d ...int) (int, error) { return 0, nil })
	typeResolve(http.HandlerFunc(nil))
}

// Channel
func TestChannelType(t *testing.T) {
	typeResolve(chan<- int(nil))
	typeResolve(make(<-chan bool))
	typeResolve(make(chan struct{}))
}

// 反射元数据
func typeResolve(i interface{}) {
	typ := reflect.TypeOf(i)

	fmt.Println("=====================基础元数据=====================")
	fmt.Printf("类型归类: %v\n", typ.Kind())
	fmt.Printf("类型简称: %v\n", typ.Name())
	fmt.Printf("类型全名: %v\n", typ.String())
	fmt.Printf("语法类型: %T\n", typ)
	fmt.Printf("包名: %v\n", typ.PkgPath())
	fmt.Printf("对齐: %v\n", typ.Align())
	fmt.Printf("大小: %v\n", typ.Size()) // 字节
	fmt.Printf("可比性: %t\n", typ.Comparable())
	fmt.Printf("方法数量: %v\n", typ.NumMethod()) // 值对象和指针对象(不含私有方法)
	if typ.NumMethod() > 0 {                  // 方法信息
		fmt.Printf("方法信息: %+v\n", typ.Method(0))            // {Name:Play PkgPath: Type:func(user.Player, string) (string, error) Func:<func(user.Player, string) (string, error) Value> Index:0}
		fmt.Printf("方法签名: %v\n", typ.Method(0).Func.Type()) // func(user.Player, string) (string, error)
		if typ.Method(0).Type.NumIn() > 0 {
			fmt.Printf("方法入参: %d个, paran[0].Type = %v\n", typ.Method(0).Type.NumIn(), typ.Method(0).Type.In(0))
		}
		if typ.Method(0).Type.NumOut() > 0 {
			fmt.Printf("方法出参: %d个, paran[0].Type = %v\n", typ.Method(0).Type.NumOut(), typ.Method(0).Type.Out(0))
		}
	}

	fmt.Printf("实现接口(user.Game): %t\n", typ.Implements(reflect.TypeOf((*user.Game)(nil)).Elem()))
	fmt.Printf("实现接口(fmt.Stringer): %t\n", typ.Implements(reflect.TypeOf((*fmt.Stringer)(nil)).Elem()))

	if typ.Kind() == reflect.Struct {
		fmt.Println("=====================结构体类型=====================")
		fmt.Printf("字段数量: %v\n", typ.NumField()) // 包含未导出字段
		fmt.Printf("字典对齐: %v,%v\n", typ.PkgPath(), typ.FieldAlign())
		fmt.Printf("字段信息: %+v\n", typ.Field(0)) // {Name:User PkgPath: Type:user.User Tag: Offset:0 Index:[0] Anonymous:true}
		fmt.Printf("字段标签: %+v\n", typ.Field(0).Tag)
		fmt.Printf("字段标签: %+v\n", typ.Field(0).Tag.Get("json"))
	}

	if typ.Kind() == reflect.Ptr && typ.Elem().Kind() == reflect.Struct {
		fmt.Println("=====================结构体指针=====================")
		fmt.Printf("类型简称: %v,%v\n", typ.Name(), typ.Elem().Name()) // 解指针
		fmt.Printf("包名: %v,%v\n", typ.PkgPath(), typ.Elem().PkgPath())

		fmt.Printf("字典对齐: %v,%v\n", typ.PkgPath(), typ.Elem().FieldAlign())
		fmt.Printf("字段数量: %v\n", typ.Elem().NumField()) // 包含未导出字段
		fmt.Printf("字段信息: %+v\n", typ.Elem().Field(0))  // {Name:User PkgPath: Type:user.User Tag: Offset:0 Index:[0] Anonymous:true}
	}

	if typ.Kind() == reflect.Array {
		fmt.Println("=====================数组类型=====================")
		fmt.Printf("数组长度: %d\n", typ.Len())
		fmt.Printf("元素类型: %v\n", typ.Elem().String())
	}

	if typ.Kind() == reflect.Slice {
		fmt.Println("=====================切片类型=====================")
		fmt.Printf("元素类型: %v\n", typ.Elem().String())
		// fmt.Printf("数组长度: %d\n", typ.Len())
	}

	if typ.Kind() == reflect.Map {
		fmt.Println("=====================字典类型=====================")
		fmt.Printf("Key类型: %v\n", typ.Key())
		fmt.Printf("元素类型: %v\n", typ.Elem().String())
	}

	if typ.Kind() == reflect.Chan {
		fmt.Println("=====================Channel类型=====================")
		fmt.Printf("通道方向: %v\n", typ.ChanDir())
		fmt.Printf("元素类型: %v,%t\n", typ.Elem().String(), typ.Elem().String() == "struct {}")
	}

	if typ.Kind() == reflect.Func {
		fmt.Println("=====================函数类型=====================")
		fmt.Printf("参量参数: %v\n", typ.IsVariadic()) // 带...的参数
		if typ.NumIn() > 0 {
			fmt.Printf("入参: %d个, paran[0].Type = %v\n", typ.NumIn(), typ.In(0))
		}
		if typ.NumOut() > 0 {
			fmt.Printf("出参: %d个, paran[0].Type = %v\n", typ.NumOut(), typ.Out(0))
		}
	}
}
