module permission

go 1.15

require (
	github.com/casbin/casbin/v2 v2.28.3
	github.com/casbin/gorm-adapter/v3 v3.0.4
	github.com/casbin/xorm-adapter/v2 v2.5.1
	github.com/fsnotify/fsnotify v1.5.4 // indirect
	github.com/gin-gonic/gin v1.7.7
	github.com/go-playground/universal-translator v0.17.0
	github.com/go-playground/validator/v10 v10.4.1
	github.com/go-sql-driver/mysql v1.5.0
	github.com/mitchellh/mapstructure v1.5.0 // indirect
	github.com/modern-go/concurrent v0.0.0-20180306012644-bacd9c7ef1dd // indirect
	github.com/pelletier/go-toml v1.9.5 // indirect
	github.com/pelletier/go-toml/v2 v2.0.0 // indirect
	github.com/pkg/errors v0.9.1 // indirect
	github.com/spf13/viper v1.11.0 // indirect
	golang.org/x/sys v0.0.0-20220503163025-988cb79eb6c6 // indirect
	gorm.io/driver/mysql v1.0.1
	gorm.io/gorm v1.20.7
)
