package main

import (
	"errors"
	"fmt"
	"net/http"

	"github.com/gin-gonic/gin"
	"permission/casbin/api"
	"permission/casbin/api/middle"
	"permission/casbin/repo"
)

func init() {
	err := repo.InitDatabase()
	if err != nil {
		panic(err)
	}

	err = middle.InitCasbin()
	if err != nil {
		panic(err)
	}
}

func HelloHandler(c *gin.Context) {
	c.JSON(200, api.OK)
}

//go:generate curl http://127.0.0.1:8000/home
//go:generate curl http://127.0.0.1:8000/home/index
//go:generate curl http://127.0.0.1:8000/posts/create
//go:generate curl http://127.0.0.1:8000/posts/update
//go:generate curl http://127.0.0.1:8000/posts/detail/1
//go:generate curl http://127.0.0.1:8000/system/set
//go:generate curl http://127.0.0.1:8000/home/role/author  # 修改角色
func main() {
	router := gin.Default()
	router.Use(middle.Permission())

	// 访客
	router.GET("/home", HelloHandler)
	router.GET("/home/index", HelloHandler)
	router.GET("/home/role/:role", func(c *gin.Context) {
		var _role = c.Param("role")
		if _role != "guest" && _role != "author" && _role != "admin" {
			_ = c.AbortWithError(http.StatusBadRequest, errors.New("参数错误"))
			return
		}
		middle.CasbinRole = _role // 修改角色
		c.String(200, fmt.Sprintf("当前用户角色已切换为： %s\n", _role))
	})

	// 帖子(作者)
	router.GET("/posts/create", HelloHandler)     // 发表帖子
	router.GET("/posts/update", HelloHandler)     // 修改帖子
	router.GET("/posts/detail/:id", HelloHandler) // 浏览帖子

	// 管理员
	router.GET("/system/set", HelloHandler) // 系统设置
	_ = router.Run(":8000")
}
