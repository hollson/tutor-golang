package middle

import (
	"fmt"
	"os"

	"github.com/casbin/casbin/v2"
	adapter "github.com/casbin/gorm-adapter/v3"
	// adapter "github.com/casbin/xorm-adapter/v2"
	"permission/casbin/repo"
)

// Enforcer 执行器
var Enforcer *casbin.Enforcer

func InitCasbin() error {
	_adapter, err := adapter.NewAdapterByDB(repo.DB())
	if err != nil {
		return err
	}

	// Enforcer, err=casbin.NewEnforcer("model.conf", "policy.csv")
	Enforcer, err = casbin.NewEnforcer(fmt.Sprintf("%s//config/casbin_model.conf", wd()), _adapter)
	if err != nil {
		return err
	}
	Enforcer.EnableLog(true)
	err = Enforcer.LoadPolicy() // 加载策略
	return err
}

func wd() string {
	dir, err := os.Getwd()
	if err != nil {
		return "./"
	}
	return dir
}
