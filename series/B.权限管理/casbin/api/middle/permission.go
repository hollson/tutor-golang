package middle

import (
	"fmt"
	"net/http"

	"github.com/gin-gonic/gin"
	"permission/casbin/api"
)

var CasbinRole = "guest" // guest author admin

// Permission 验证权限
//  通过action决定subject是否可以访问object
func Permission() gin.HandlerFunc {
	return func(c *gin.Context) {
		sub := CasbinRole
		obj := c.Request.URL.Path
		act := c.Request.Method

		fmt.Println(Enforcer.GetRolesForUser("author"))
		
		passed, err := Enforcer.Enforce(sub, obj, act)
		if err != nil {
			_ = c.AbortWithError(500, err)
			return
		}

		if passed {
			c.Next()
			return
		}

		c.AbortWithStatusJSON(http.StatusForbidden, api.Result{
			Code: 403,
			Msg:  "无权访问",
		})
	}
}
