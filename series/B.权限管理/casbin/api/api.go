package api

type Result struct {
	Code int         `json:"code"`
	Msg  string      `json:"msg"`
	Data interface{} `json:"data,omitempty"`
}

var OK = Result{
	Code: 200,
	Msg:  "ok",
}
