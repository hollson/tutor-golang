-- 配置好数据库后，程序会自动创建casbin_rule表，需手动写入数据
INSERT INTO `casbin_rule` (`id`, `p_type`, `v0`, `v1`, `v2`, `v3`, `v4`, `v5`) VALUES
(1, 'p', '*', '/home*', '*', NULL, NULL, NULL),
(2, 'p', '*', '/posts/detail/*', '*', NULL, NULL, NULL),
(3, 'p', 'author', '/posts/*', '*', NULL, NULL, NULL),
(4, 'p', 'admin', '/posts/*', '*', NULL, NULL, NULL),
(5, 'p', 'admin', '/system/*', '*', NULL, NULL, NULL);
