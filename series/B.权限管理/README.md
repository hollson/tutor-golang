

## Casbin介绍

`casbin`是一个强大、高效的访问控制库。支持常用的多种访问控制模型，如`ACL/RBAC/ABAC`等



### 安装casbin

```shell
$ go get github.com/casbin/casbin/v2
```



### PERM模型

权限实际上就是控制**谁**能对**什么资源**进行什么操作, `casbin`将访问控制模型抽象到一个基于 PERM（Policy，Effect，Request，Matchers）

-   `policy`是策略或者说是规则的定义。它定义了具体的规则。

-   `request`是对访问请求的抽象，它与`e.Enforce()`函数的参数是一一对应的

-   `matcher`匹配器会将请求与定义的每个`policy`一一匹配，生成多个匹配结果。

-   `effect`根据对请求运用匹配器得出的所有结果进行汇总，来决定该请求是**允许**还是**拒绝**。



_模型文件：rbac.conf_

```toml
[request_definition]
r = sub, obj, act

[policy_definition]
p = sub, obj, act

[matchers]
m = r.sub == p.sub && r.obj == p.obj && r.act == p.act

[policy_effect]
e = some(where (p.eft == allow))
```

_策略文件：policy.csv_

```nginx
p, dajun, data1, read
p, lizi, data2, write
```

```go
package main

import (
  "fmt"
  "log"

  "github.com/casbin/casbin/v2"
)

func check(e *casbin.Enforcer, sub, obj, act string) {
  ok, _ := e.Enforce(sub, obj, act)
  if ok {
    fmt.Printf("%s CAN %s %s\n", sub, act, obj)
  } else {
    fmt.Printf("%s CANNOT %s %s\n", sub, act, obj)
  }
}

func main() {
  e, err := casbin.NewEnforcer("./model.conf", "./policy.csv")
  if err != nil {
    log.Fatalf("NewEnforecer failed:%v\n", err)
  }

  check(e, "dajun", "data1", "read")
  check(e, "lizi", "data2", "write")
  check(e, "dajun", "data1", "write")
  check(e, "dajun", "data2", "read")
}

```







## RBAC模型

**RBAC (`Role-Based Access Control`)： **基于角色的访问控制模型，



**RBAC的组成：** 用户(User)、角色(Role)和权限(Permission)。

RBAC通过定义角色的权限，并对用户授予某个角色从而来控制用户的权限，实现了用户和权限的逻辑分离（区别于ACL模型），极大地方便了权限的管理
：

    User（用户）：每个用户都有唯一的UID识别，并被授予不同的角色
    Role（角色）：不同角色具有不同的权限
    Permission（权限）：访问权限



https://casbin.org/docs/zh-CN/overview

https://www.topgoer.com/gin%E6%A1%86%E6%9E%B6/%E5%85%B6%E4%BB%96/%E6%9D%83%E9%99%90%E7%AE%A1%E7%90%86.html

https://studygolang.com/articles/30207
https://www.modb.pro/db/100594
https://www.kancloud.cn/oldlei/casbin/1289451
https://www.kancloud.cn/oldlei/casbin/1289454