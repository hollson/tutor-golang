package rbac

import (
	"fmt"
	"log"

	"github.com/casbin/casbin/v2"
)

func TestRBAC() {
	e, _ := casbin.NewEnforcer("rbac/rbac.conf", "rbac/rbac.csv")

	fmt.Printf("RBAC test start\n") // output for debug

	// superAdmin
	if ok, err := e.Enforce("superAdmin", "project", "read"); err == nil && ok {
		log.Println("superAdmin can read project")
	} else {
		log.Fatal("ERROR: superAdmin can not read project")
	}

	if ok, err := e.Enforce("superAdmin", "project", "write"); err == nil && ok {
		log.Println("superAdmin can write project")
	} else {
		log.Fatal("ERROR: superAdmin can not write project")
	}

	// admin
	if ok, err := e.Enforce("quyuan", "project", "read"); err == nil && ok {
		log.Println("quyuan can read project")
	} else {
		log.Fatal("ERROR: quyuan can not read project")
	}

	if ok, err := e.Enforce("quyuan", "project", "write"); err == nil && ok {
		log.Println("quyuan can write project")
	} else {
		log.Fatal("ERROR: quyuan can not write project")
	}

	if ok, err := e.Enforce("quyuan", "asse", "read"); err == nil && ok {
		log.Println("quyuan can read asse")
	} else {
		log.Fatal("ERROR: quyuan can not read asse")
	}

	if ok, err := e.Enforce("quyuan", "asse", "write"); err == nil && ok {
		log.Println("quyuan can write asse")
	} else {
		log.Fatal("ERROR: quyuan can not write asse")
	}

	// zhuangjia
	if ok, err := e.Enforce("wenyin", "project", "read"); err == nil && ok {
		log.Fatal("ERROR: wenyin can read project")
	} else {
		log.Println("wenyin can not read project")
	}

	if ok, err := e.Enforce("wenyin", "project", "write"); err == nil && ok {
		log.Println("wenyin can write project")
	} else {
		log.Fatal("ERROR: wenyin can not write project")
	}

	if ok, err := e.Enforce("wenyin", "asse", "read"); err == nil && ok {
		log.Fatal("ERROR: wenyin can read asse")
	} else {
		log.Println("wenyin can not read asse")
	}

	if ok, err := e.Enforce("wenyin", "asse", "write"); err == nil && ok {
		log.Println("wenyin can write asse")
	} else {
		log.Fatal("ERROR: wenyin can not write asse")
	}

	// shangshang
	if ok, err := e.Enforce("shangshang", "project", "read"); err == nil && ok {
		log.Println("shangshang can read project")
	} else {
		log.Fatal("ERROR: shangshang can not read project")
	}

	if ok, err := e.Enforce("shangshang", "project", "write"); err == nil && ok {
		log.Fatal("ERROR: shangshang can write project")
	} else {
		log.Println("shangshang can not write project")
	}

	if ok, err := e.Enforce("shangshang", "asse", "read"); err == nil && ok {
		log.Println("shangshang can read asse")
	} else {
		log.Fatal("ERROR: shangshang can not read asse")
	}

	if ok, err := e.Enforce("shangshang", "asse", "write"); err == nil && ok {
		log.Fatal("ERROR: shangshang can write asse")
	} else {
		log.Println("shangshang can not write asse")
	}
}
