package rbac

import (
	"fmt"
	"log"

	"github.com/casbin/casbin/v2"
)

// https://www.cnblogs.com/wang_yb/archive/2018/11/20/9987397.html

// TestTenants test tenants
func TestTenants() {
	e, _ := casbin.NewEnforcer("tenants/tenants.conf", "tenants/tenants.csv")

	fmt.Printf("RBAC TENANTS test start\n") // output for debug

	// superAdmin
	if ok, err := e.Enforce("superAdmin", "gy", "project", "read"); err == nil && ok {
		log.Println("superAdmin can read project in gy")
	} else {
		log.Fatal("ERROR: superAdmin can not read project in gy")
	}

	if ok, err := e.Enforce("superAdmin", "gy", "project", "write"); err == nil && ok {
		log.Println("superAdmin can write project in gy")
	} else {
		log.Fatal("ERROR: superAdmin can not write project in gy")
	}

	if ok, err := e.Enforce("superAdmin", "jn", "project", "read"); err == nil && ok {
		log.Println("superAdmin can read project in jn")
	} else {
		log.Fatal("ERROR: superAdmin can not read project in jn")
	}

	if ok, err := e.Enforce("superAdmin", "jn", "project", "write"); err == nil && ok {
		log.Println("superAdmin can write project in jn")
	} else {
		log.Fatal("ERROR: superAdmin can not write project in jn")
	}

	// admin
	if ok, err := e.Enforce("quyuan", "gy", "project", "read"); err == nil && ok {
		log.Println("quyuan can read project in gy")
	} else {
		log.Fatal("ERROR: quyuan can not read project in gy")
	}

	if ok, err := e.Enforce("quyuan", "gy", "project", "write"); err == nil && ok {
		log.Println("quyuan can write project in gy")
	} else {
		log.Fatal("ERROR: quyuan can not write project in gy")
	}

	if ok, err := e.Enforce("quyuan", "jn", "project", "read"); err == nil && ok {
		log.Fatal("ERROR: quyuan can read project in jn")
	} else {
		log.Println("quyuan can not read project in jn")
	}

	if ok, err := e.Enforce("quyuan", "jn", "project", "write"); err == nil && ok {
		log.Fatal("ERROR: quyuan can write project in jn")
	} else {
		log.Println("quyuan can not write project in jn")
	}

	if ok, err := e.Enforce("quyuan", "gy", "asse", "read"); err == nil && ok {
		log.Fatal("ERROR: quyuan can read asse in gy")
	} else {
		log.Println("quyuan can not read asse in gy")
	}

	if ok, err := e.Enforce("quyuan", "gy", "asse", "write"); err == nil && ok {
		log.Fatal("ERROR: quyuan can write asse in gy")
	} else {
		log.Println("quyuan can not write asse in gy")
	}

	if ok, err := e.Enforce("quyuan", "jn", "asse", "read"); err == nil && ok {
		log.Println("quyuan can read asse in jn")
	} else {
		log.Fatal("ERROR: quyuan can not read asse in jn")
	}

	if ok, err := e.Enforce("quyuan", "jn", "asse", "write"); err == nil && ok {
		log.Println("quyuan can write asse in jn")
	} else {
		log.Fatal("ERROR: quyuan can not write asse in jn")
	}

	// wenyin
	if ok, err := e.Enforce("wenyin", "gy", "asse", "write"); err == nil && ok {
		log.Println("wenyin can write asse in gy")
	} else {
		log.Fatal("ERROR: wenyin can not write asse in gy")
	}

	if ok, err := e.Enforce("wenyin", "jn", "asse", "write"); err == nil && ok {
		log.Fatal("ERROR: wenyin can write asse in jn")
	} else {
		log.Println("wenyin can not write asse in jn")
	}

	// shangshang
	if ok, err := e.Enforce("shangshang", "jn", "project", "write"); err == nil && ok {
		log.Println("shangshang can write project in jn")
	} else {
		log.Fatal("ERROR: shangshang can not write project in jn")
	}

	if ok, err := e.Enforce("shangshang", "gy", "project", "write"); err == nil && ok {
		log.Fatal("ERROR: shangshang can write project in gy")
	} else {
		log.Println("shangshang can not write project in gy")
	}
}
