package main

import (
	"fmt"

	"github.com/casbin/casbin/v2"
)

func check(e *casbin.Enforcer, sub, obj, act string) {
	ok, _ := e.Enforce(sub, obj, act)
	if ok {
		fmt.Printf("%s	%s	%s ✅ \n", sub, act, obj)
		return
	}
	fmt.Printf("%s	%s	%s ❌ \n", sub, act, obj)
}

func main() {
	enforcer, err := casbin.NewEnforcer("./model.conf", "./policy.csv")
	if err != nil {
		panic(err)
	}

	check(enforcer, "tom", "a.txt", "read")
	check(enforcer, "tom", "a.txt", "write")
	check(enforcer, "tom", "a.txt", "exec")
	check(enforcer, "tom", "b.txt", "read")
	fmt.Println()

	check(enforcer, "lucy", "b.txt", "read")
	check(enforcer, "lucy", "b.txt", "write")
	check(enforcer, "lucy", "b.txt", "exec")
	check(enforcer, "lucy", "a.txt", "read")
	fmt.Println()

	check(enforcer, "root", "a.txt", "read")
	check(enforcer, "root", "a.txt", "write")
	check(enforcer, "root", "a.txt", "exec")
	check(enforcer, "root", "b.txt", "read")
	check(enforcer, "root", "b.txt", "write")
	check(enforcer, "root", "b.txt", "exec")
}
