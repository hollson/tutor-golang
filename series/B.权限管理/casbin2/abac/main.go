package main

import (
	"fmt"

	"github.com/casbin/casbin/v2"
)

type Object struct {
	Name string
	Type string
}

type Subject struct {
	Name string
	Hour int
}

func check(e *casbin.Enforcer, sub Subject, obj Object, act string) {
	ok, _ := e.Enforce(sub, obj, act)
	if ok {
		fmt.Printf("%s	%s	%s	%s	%d:00 ✅ \n", sub.Name, obj.Name, obj.Type, act, sub.Hour)
		return
	}

	fmt.Printf("%s	%s	%s	%s	%d:00 ❌ \n", sub.Name, obj.Name, obj.Type, act, sub.Hour)
}

// 「平板电脑」家长模式：仅18点到20点可进行娱乐活动，教育类全天候可使用
func main() {
	enforcer, err := casbin.NewEnforcer("./model.conf", "./policy.csv")
	if err != nil {
		panic(err)
	}

	sub := Subject{"Tom", 10}
	obj := Object{"computer", "movie"}
	check(enforcer, sub, obj, "watch")

	sub = Subject{"Jim", 19}
	check(enforcer, sub, obj, "watch")

	sub = Subject{"Jim", 20}
	obj = Object{"computer", "game"}
	check(enforcer, sub, obj, "play")

	sub = Subject{"Jim", 22}
	check(enforcer, sub, obj, "play")

	sub = Subject{"Tom", 12}
	obj = Object{"computer", "educate"}
	check(enforcer, sub, obj, "study")

	sub = Subject{"Jim", 24}
	obj = Object{"computer", "music"}
	check(enforcer, sub, obj, "listen")
}
