package main

import (
	"fmt"
	"log"

	"github.com/casbin/casbin/v2"
	"github.com/casbin/casbin/v2/model"
	fileAdapter "github.com/casbin/casbin/v2/persist/file-adapter"
)

func check(e *casbin.Enforcer, sub, obj, act string) {
	ok, _ := e.Enforce(sub, obj, act)
	if ok {
		fmt.Printf("%s	%s	%s ✅ \n", sub, act, obj)
		return
	}
	fmt.Printf("%s	%s	%s ❌ \n", sub, act, obj)
}

func main() {
	_model := model.NewModel()
	_model.AddDef("r", "r", "sub, obj, act")
	_model.AddDef("p", "p", "sub, obj, act")
	_model.AddDef("enforcer", "enforcer", "some(where (p.eft == allow))")
	_model.AddDef("_model", "_model", "r.sub == g.sub && r.obj == p.obj && r.act == p.act")

	adapter := fileAdapter.NewAdapter("./policy.csv")
	enforcer, err := casbin.NewEnforcer(_model, adapter)
	if err != nil {
		log.Fatalf("NewEnforecer failed:%v\n", err)
	}

	check(enforcer, "dajun", "data1", "read")
	check(enforcer, "lizi", "data2", "write")
	check(enforcer, "dajun", "data1", "write")
	check(enforcer, "dajun", "data2", "read")
}
