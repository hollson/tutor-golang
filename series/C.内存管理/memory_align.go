package main

import (
	"fmt"
	"unsafe"
)

type B struct {
	x int32 // 4  -> 4+4i
	y int64 // 8  ->8 (8倍对齐)
	z bool  // 1  ->8 (向前对齐)
}

type T struct {
	a bool   // 1
	b int8   // 1
	c uint16 // 2
	d uint32 // 4  ->8
	e int64  // 8  ->8
	f bool   // 1  ->8
}

type R struct {
	a bool   // 1
	b int8   // 1
	d uint32 // 4  ->8
	c uint16 // 2  ->8
	e int64  // 8  ->8
	f bool   // 1  ->8
}

// 内存对齐示例
func main() {
	var b B
	fmt.Println("t占用的实际内存大小:", unsafe.Sizeof(b), "字节,结构体对齐保证:", unsafe.Alignof(b))
	fmt.Println()

	var t = T{}
	fmt.Println("t占用的实际内存大小:", unsafe.Sizeof(t), "字节,结构体对齐保证:", unsafe.Alignof(t))
	fmt.Println("a:", unsafe.Sizeof(t.a), "字节,字段对齐保证:", unsafe.Alignof(t.a), ",偏移地址:", unsafe.Offsetof(t.a))
	fmt.Println("b:", unsafe.Sizeof(t.b), "字节,字段对齐保证:", unsafe.Alignof(t.b), ",偏移地址:", unsafe.Offsetof(t.b))
	fmt.Println("c:", unsafe.Sizeof(t.c), "字节,字段对齐保证:", unsafe.Alignof(t.c), ",偏移地址:", unsafe.Offsetof(t.c))
	fmt.Println("d:", unsafe.Sizeof(t.d), "字节,字段对齐保证:", unsafe.Alignof(t.d), ",偏移地址:", unsafe.Offsetof(t.d))
	fmt.Println("e:", unsafe.Sizeof(t.e), "字节,字段对齐保证:", unsafe.Alignof(t.e), ",偏移地址:", unsafe.Offsetof(t.e))
	fmt.Println("f:", unsafe.Sizeof(t.f), "字节,字段对齐保证:", unsafe.Alignof(t.f), ",偏移地址:", unsafe.Offsetof(t.f))
	fmt.Println(uintptr(unsafe.Pointer(&t)))
	fmt.Println()

	var r = R{}
	fmt.Println("r占用的实际内存大小:", unsafe.Sizeof(r), "字节,结构体对齐保证:", unsafe.Alignof(r))
	fmt.Println("a:", unsafe.Sizeof(r.a), "字节,字段对齐保证:", unsafe.Alignof(r.a), ",偏移地址:", unsafe.Offsetof(r.a))
	fmt.Println("b:", unsafe.Sizeof(r.b), "字节,字段对齐保证:", unsafe.Alignof(r.b), ",偏移地址:", unsafe.Offsetof(r.b))
	fmt.Println("d:", unsafe.Sizeof(r.d), "字节,字段对齐保证:", unsafe.Alignof(r.d), ",偏移地址:", unsafe.Offsetof(r.d))
	fmt.Println("c:", unsafe.Sizeof(r.c), "字节,字段对齐保证:", unsafe.Alignof(r.c), ",偏移地址:", unsafe.Offsetof(r.c))
	fmt.Println("e:", unsafe.Sizeof(r.e), "字节,字段对齐保证:", unsafe.Alignof(r.e), ",偏移地址:", unsafe.Offsetof(r.e))
	fmt.Println("f:", unsafe.Sizeof(r.f), "字节,字段对齐保证:", unsafe.Alignof(r.f), ",偏移地址:", unsafe.Offsetof(r.f))
	fmt.Println(uintptr(unsafe.Pointer(&r)))
}
