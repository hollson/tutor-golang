package main

import (
	"fmt"
	"os"
	"time"

	"encoder/compressor/util"
	"github.com/golang/snappy"
	"github.com/hollson/gdk/memory"
)

// snappy 适用于压缩包含大量重复字符的文本数据，压缩率比gzip小，CPU占用小。snappy可被用于爬虫、博客数据处理等。

func snappySimpleDemo() {
	raw := []byte("hello world ! hello world ! hello world ! hello world ! hello world ! hello world ! ")

	enc := snappy.Encode(nil, raw)
	fmt.Printf("压缩：%d => %d\n", len(raw), len(enc))

	dec, _ := snappy.Decode(nil, enc)
	fmt.Printf("解压：%s\n", dec)
}

func snappyHttpDemo() {
	raw := util.HttpContent("https://www.cnblogs.com/sss4/p/12717453.html")

	enc := snappy.Encode(nil, raw)
	fmt.Printf("压缩：%v => %v\n", memory.Size(float64(len(raw))), memory.Size(float64(len(enc))))

	dc, _ := snappy.Decode(nil, enc)
	f, _ := os.CreateTemp("./", "*.html")
	f.Write(dc)
	defer f.Close()
	fmt.Printf("解压： %s\n", f.Name())

	// 删除临时文件
	<-time.After(time.Second * 10)
	os.RemoveAll(f.Name())
}

func main() {
	snappySimpleDemo()
	snappyHttpDemo()
}
