package main

import (
	"bytes"
	"fmt"
)

// Buffer
// Bytes
// String
// Len
// Cap
// Truncate
// Reset
// Grow
// Write
// WriteString
// ReadFrom
// WriteTo
// WriteByte
// WriteRune
// Read
// Next
// ReadByte
// ReadRune
// UnreadRune
// UnreadByte
// ReadBytes
// ReadString
// NewBuffer
// NewBufferString

func main() {
	NewBuffer()

}

// 构造Buffer
func NewBuffer() {
	buf := bytes.NewBuffer([]byte("hello"))
	// buf = bytes.NewBufferString("hello")

	fmt.Println(buf.Len(), buf.Cap(), buf.Bytes(), buf.String())
	buf.Write([]byte(" world"))
	fmt.Println(buf.Len(), buf.Cap(), buf.Bytes(), buf.String())
	buf.Reset()
	fmt.Println(buf.Len(), buf.Cap(), buf.Bytes(), buf.String())
}

// 函数

// IO
