package main

import (
	"encoding/hex"
	"fmt"
)

// 十六进制编码
func hexEncode(src []byte) string {
	return hex.EncodeToString(src)
}

// 十六进制解码
func hexDecode(s string) []byte {
	data, err := hex.DecodeString(s)
	fmt.Println(err)
	return data
}

func main() {
	hx := hexEncode([]byte("hello world"))
	fmt.Println(hx)
	fmt.Println(string(hexDecode(hx)))
}
