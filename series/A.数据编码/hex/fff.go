// Copyright 2022 Hollson. All rights reserved.
// Use of this source code is governed by a MIT style
// license that can be found in the LICENSE file.

package main

import (
	"encoding/hex"
	"fmt"
	"log"
)

func hexByte() {
	// 编码
	var src = []byte("hello world")
	var dst = make([]byte, hex.EncodedLen(len(src)))

	hex.Encode(dst, src)
	fmt.Printf("%s\n", dst) // 输出: 68656c6c6f20776f726c64

	// src := []byte("48656c6c6f20476f7068657221")

	// 解码
	src = make([]byte, hex.DecodedLen(len(dst)))
	n, err := hex.Decode(dst, src)
	if err != nil {
		log.Fatal(err)
	}

	fmt.Printf("%s\n", dst[:n])

	// dst2 := hex.Decode(src)
}

func hexString() {

}

func main() {
	hexByte()
	// src := []byte("hello")
	// maxEnLen := hex.EncodedLen(len(src)) // 源字节长度
	//
	// dst1 := make([]byte, maxEnLen)
	// n := hex.Encode(dst1, src)
	//
	// dst2 := hex.EncodeToString(src)
	// fmt.Println("编码后的结果:", string(dst1[:n]))
	// fmt.Println("编码后的结果:", dst2)
	//
	// // 解码
	// src = dst1
	// // maxDeLen := hex.DecodedLen(len(src))
	// // dst1 = make([]byte, maxDeLen)
	//
	// var dst1 []byte
	//
	// n, err := hex.Decode(dst1, src)
	// if err != nil {
	// 	fmt.Println(err)
	// } else {
	// 	fmt.Printf("%s解码后的数据为:%s\n", src, string(dst1[:n]))
	// }
	//
	// dst3, err := hex.DecodeString(string(src))
	// fmt.Printf("%s解码后的数据为:%s\n", src, string(dst3[:n]))
	// // dump
	// fmt.Printf(hex.Dump(src))
	//
	//
	// // dumper
	// stdoutDumper := hex.Dumper(os.Stdout)
	// defer stdoutDumper.Close()
	//
	// stdoutDumper.Write(src)
}

/*
输出内容：
编码后的结果: 68656c6c6f
编码后的结果: 68656c6c6f
68656c6c6f解码后的数据为:hello
68656c6c6f解码后的数据为:hello
00000000  36 38 36 35 36 63 36 63  36 66                    |68656c6c6f|
00000000  36 38 36 35 36 63 36 63  36 66
*/
