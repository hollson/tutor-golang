package main

import (
	"fmt"
	"time"

	"github.com/golang/protobuf/ptypes"

	"github.com/google/cel-go/cel"
	"github.com/google/cel-go/checker/decls"
)

// exercise
// https://codelabs.developers.google.com/codelabs/cel-go/index.html#0

// spec
// https://github.com/google/cel-spec/blob/master/doc/langdef.md
// 通用表达式语言——规范和二进制表示
func eval(expr string, value map[string]interface{}) {
	env, err := cel.NewEnv(cel.Declarations(
		decls.NewVar("object", decls.NewMapType(decls.String, decls.Dyn))))
	if err != nil {
		panic(err)
	}

	ast, iss := env.Compile(expr)
	// Check iss for compilation errors.
	if iss.Err() != nil {
		panic(iss.Err())
	}

	prg, err := env.Program(ast)
	if err != nil {
		panic(err)
	}

	fmt.Println(prg.Eval(map[string]interface{}{"object": value}))
}

func main() {
	eval(`object.date < duration("30s")`, map[string]interface{}{"date": ptypes.DurationProto(time.Second * 10)})
	eval(`object.date < duration("30s")`, map[string]interface{}{"date": ptypes.DurationProto(time.Second * 40)})
	eval(`object.date < duration("30s")`, map[string]interface{}{"date": time.Second * 10})
	eval(`object.date < duration("30s")`, map[string]interface{}{"date": time.Second * 40})
	eval(`duration(object.date) < duration("30s")`, map[string]interface{}{"date": "10s"})
	eval(`duration(object.date) < duration("30s")`, map[string]interface{}{"date": "40s"})

	eval(`object.num < 30`, map[string]interface{}{"num": 10})
	eval(`object.num < 30`, map[string]interface{}{"num": 40})
	eval(`int(object.num) < 30`, map[string]interface{}{"num": "10"})
	eval(`int(object.num) < 30`, map[string]interface{}{"num": "40"})
	eval(`int(object.num) < 30`, map[string]interface{}{"num": "a"})

	eval(`object.text.size() < 2`, map[string]interface{}{"text": "a"})
	eval(`object.text.size() < 2`, map[string]interface{}{"text": "ab"})
	eval(`object.text.size() < 2`, map[string]interface{}{"text": "abc"})
	eval(`object.text.startsWith("ab")`, map[string]interface{}{"text": "a"})
	eval(`object.text.startsWith("ab")`, map[string]interface{}{"text": "abc"})
}
