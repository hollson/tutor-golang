package main

import (
	"bytes"
	"encoding/gob"
	"encoding/json"
	"fmt"
	"testing"

	"encoder/copy/types"
	jsoniter "github.com/json-iterator/go"
	"github.com/mitchellh/copystructure"
	"github.com/ugorji/go/codec"
)

func init() {
	gob.Register(&types.Struct{})
	gob.Register(&types.SubStruct{})
	gob.Register(&types.Bits{})
}

func compare(a, b interface{}) bool {
	ba, err := json.Marshal(a)
	if err != nil {
		panic(err)
	}
	bb, err := json.Marshal(b)
	if err != nil {
		panic(err)
	}
	if string(ba) != string(bb) {
		fmt.Println(string(ba))
		fmt.Println(string(bb))
		return false
	}
	return true
}

func TestCopy(t *testing.T) {
	d0 := &types.Struct{}
	data.DeepCopyInto(d0)
	if !compare(d0, &data) {
		t.Fatal("unexpected")
	}

	d1, _ := copystructure.Copy(&data)
	if !compare(d1, &data) {
		t.Fatal("unexpected")
	}

	d2 := &types.Struct{}
	b2, _ := json.Marshal(data)
	json.Unmarshal(b2, d2)
	if !compare(d2, &data) {
		t.Fatal("unexpected")
	}

	d3 := &types.Struct{}
	b3, _ := jsoniter.Marshal(data)
	json.Unmarshal(b3, d3)
	if !compare(d3, &data) {
		t.Fatal("unexpected")
	}

	d4 := &types.Struct{}
	b4 := new(bytes.Buffer)
	enc := gob.NewEncoder(b4)
	enc.Encode(&data)
	dec := gob.NewDecoder(b4)
	dec.Decode(d4)
	if !compare(d4, &data) {
		t.Fatal("unexpected")
	}

	d5 := &types.Struct{}
	b, _ := msgpack.Marshal(&data)
	msgpack.Unmarshal(b, d5)
	if !compare(d5, &data) {
		t.Fatal("unexpected")
	}
}

func BenchmarkCopy(b *testing.B) {
	b.Run("Generate", func(tb *testing.B) {
		for i := 0; i < tb.N; i++ {
			d := &types.Struct{}
			data.DeepCopyInto(d)
		}
	})

	b.Run("Copy", func(tb *testing.B) {
		for i := 0; i < tb.N; i++ {
			copystructure.Copy(&data)
		}
	})

	b.Run("gob", func(tb *testing.B) {
		for i := 0; i < tb.N; i++ {
			b := new(bytes.Buffer)
			enc := gob.NewEncoder(b)
			enc.Encode(&data)
			d := &types.Struct{}
			dec := gob.NewDecoder(b)
			dec.Decode(d)
		}
	})

	b.Run("json", func(tb *testing.B) {
		for i := 0; i < tb.N; i++ {
			d := &types.Struct{}
			b, _ := json.Marshal(&data)
			json.Unmarshal(b, d)
		}
	})

	b.Run("jsoniter", func(tb *testing.B) {
		for i := 0; i < tb.N; i++ {
			d := &types.Struct{}
			b, _ := jsoniter.Marshal(&data)
			jsoniter.Unmarshal(b, d)
		}
	})

	b.Run("msgpack", func(tb *testing.B) {
		for i := 0; i < tb.N; i++ {
			d := &types.Struct{}
			b, _ := msgpack.Marshal(&data)
			msgpack.Unmarshal(b, d)
		}
	})

	b.Run("msgpack2", func(tb *testing.B) {
		h := &codec.MsgpackHandle{}
		for i := 0; i < tb.N; i++ {
			b := new(bytes.Buffer)
			enc := codec.NewEncoder(b, h)
			enc.Encode(&data)
			d := &types.Struct{}
			dec := codec.NewDecoder(b, h)
			dec.Decode(d)
		}
	})
}
