package main

import (
	"encoding/json"
	"fmt"

	"encoder/copy/types"
	"github.com/mitchellh/copystructure"
)

var data = types.Struct{
	V1: 1,
	V2: "2",
	V3: []int{1, 2, 3},
	V4: []types.SubStruct{
		types.SubStruct{1, []byte("SV2"), "SV3"},
		types.SubStruct{2, []byte("SV22"), "SV33"},
	},
	V5: []*types.SubStruct{
		&types.SubStruct{1, []byte("SV2"), "SV3"},
		&types.SubStruct{2, []byte("SV22"), "SV33"},
	},
	V6: map[string]types.SubStruct{
		"1": types.SubStruct{1, []byte("SV2"), "SV3"},
		"2": types.SubStruct{1, []byte("SV22"), "SV33"},
	},
	V7: map[string]*types.SubStruct{
		"1": &types.SubStruct{1, []byte("SV2"), "SV3"},
		"2": &types.SubStruct{1, []byte("SV22"), "SV33"},
	},
	V8: types.Bits([]int{1, 2, 3}),
}

func main() {
	d, err := copystructure.Copy(&data)
	if err != nil {
		panic(err)
	}

	data1, ok := d.(*types.Struct)
	if !ok {
		panic("Unexpected")
	}

	str, _ := json.Marshal(&data)
	str1, _ := json.Marshal(&data1)

	fmt.Println(string(str))
	fmt.Println(string(str1))

	if string(data1.V5[0].SV2) != string(data.V5[0].SV2) {
		panic("Unexpected")
	}

	if string(str) != string(str1) {
		panic(err)
	}
}
