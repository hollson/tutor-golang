package main

import (
    "encoding/json"
	"io/ioutil"
	"log"
	"testing"

	"github.com/json-iterator/go"
)

func BenchmarkJsonStdMarshal(b *testing.B) {
	b.StopTimer()

	var stBookMap BookMap
	strBookJson, err := ioutil.ReadFile("book.json")
	if err != nil {
		log.Fatalln(err)
	}
	if err := json.Unmarshal(strBookJson, &stBookMap); err != nil {
		log.Fatalln(err)
	}

	b.StartTimer()

	for i := 0; i < b.N; i++ {
		json.Marshal(stBookMap)
	}
}

func BenchmarkJsonStdUnmarshal(b *testing.B) {
	b.StopTimer()

	var stBookMap BookMap
	strBookJson, err := ioutil.ReadFile("book.json")
	if err != nil {
		log.Fatalln(err)
	}

	b.StartTimer()

	for i := 0; i < b.N; i++ {
		if err := json.Unmarshal(strBookJson, &stBookMap); err != nil {
			log.Fatalln(err)
		}
	}
}

func BenchmarkJsoniterMarshal(b *testing.B) {
	b.StopTimer()

	var stBookMap BookMap
	strBookJson, err := ioutil.ReadFile("book.json")
	if err != nil {
		log.Fatalln(err)
	}
	if err := jsoniter.Unmarshal(strBookJson, &stBookMap); err != nil {
		log.Fatalln(err)
	}

	b.StartTimer()

	for i := 0; i < b.N; i++ {
		jsoniter.Marshal(stBookMap)
	}
}

func BenchmarkJsoniterUnmarshal(b *testing.B) {
	b.StopTimer()

	var stBookMap BookMap
	strBookJson, err := ioutil.ReadFile("book.json")
	if err != nil {
		log.Fatalln(err)
	}

	b.StartTimer()

	for i := 0; i < b.N; i++ {
		if err := jsoniter.Unmarshal(strBookJson, &stBookMap); err != nil {
			log.Fatalln(err)
		}
	}
}
