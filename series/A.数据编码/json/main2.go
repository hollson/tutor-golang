package main

import (
	"encoding/json"
	"fmt"
	"strconv"
)

// Int64Array Int64数组,用于处理浏览器对int64的兼容问题
type Int64Array []int64

// MarshalJSON 将对象的Json编码,转换为string数组格式
func (arr Int64Array) MarshalJSON() ([]byte, error) {
	strArr := make([]string, len(arr))
	for i := 0; i < len(arr); i++ {
		strArr[i] = strconv.Itoa(int(arr[i]))
	}
	return json.Marshal(strArr)
}

// UnmarshalJSON 将string数组格式的Json，反解析为int数组类型
func (arr *Int64Array) UnmarshalJSON(data []byte) error {
	var strArr []string
	if err := json.Unmarshal(data, &strArr); err != nil {
		return err
	}

	for _, s := range strArr {
		n, err := strconv.Atoi(s)
		if err != nil {
			return err
		}
		*arr = append(*arr, int64(n))
	}
	return nil
}

// Foo Json字段类型转换
type Foo struct {
	Fa string     `json:"fa"`        // 字符串
	Fb int        `json:"fb"`        // 数字
	Fc int        `json:"fc,string"` // 数字转字符串
	Fd Int64Array `json:"fe"`        // 数字装字符串数组
}

func main() {
	f := Foo{
		Fa: "aa",
		Fb: 12345678,
		Fc: 20134541502808064,
		Fd: []int64{
			// 16158824669401088,
			// 16201161216495616,
			// 16212290206568448,
			// 17610461036449792,
			// 17622470180290560,
			// 17649398152990720,
			// 17926338342739968,
			// 18024789163175936,
			// 18319061912506368,
			// 18641625985699840,
			// 18732479078465536,
			// 19717995134668800,
			// 20129949381656576,
			20132770789294080,
			20134541502808064,
			20152585864183808,
			20154522378534912,
		},
	}

	data, err := json.Marshal(&f)
	if err != nil {
		panic(err)
	}
	fmt.Printf("%q",string(data))
	fmt.Println()

	var f2 Foo
	err = json.Unmarshal(data, &f2)
	if err != nil {
		panic(err)
		return
	}
	fmt.Println(f2)
}