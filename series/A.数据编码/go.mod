module encoder

go 1.17

require (
	github.com/cyberdelia/lzo v1.0.0
	github.com/golang/protobuf v1.5.2
	github.com/golang/snappy v0.0.4
	github.com/google/cel-go v0.9.0
	github.com/hollson/gdk v0.0.0-20220207065538-9d6ddc100e7c
	github.com/json-iterator/go v1.1.12
	github.com/mitchellh/copystructure v1.2.0
	github.com/ugorji/go/codec v1.2.6
)

require (
	github.com/antlr/antlr4/runtime/Go/antlr v0.0.0-20210826220005-b48c857c3a0e // indirect
	github.com/mitchellh/reflectwalk v1.0.2 // indirect
	github.com/modern-go/concurrent v0.0.0-20180228061459-e0a39a4cb421 // indirect
	github.com/modern-go/reflect2 v1.0.2 // indirect
	github.com/stoewer/go-strcase v1.2.0 // indirect
	golang.org/x/text v0.3.7 // indirect
	google.golang.org/genproto v0.0.0-20210831024726-fe130286e0e2 // indirect
	google.golang.org/protobuf v1.27.1 // indirect
)
