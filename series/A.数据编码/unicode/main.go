package main

import (
	"fmt"
	"unicode"
)

func ExistChinese(s string) bool {
	// unicode.In()
	for _, r := range s {
		if unicode.Is(unicode.Scripts["Han"], r) {
			return true
		}
	}
	return false
}

// 英文: ASCII_Hex_Digit (大小写+数字)
// 西文:
// 中文
// 日文：平假名(Hiragana),如：きっとの；片假名(Katakana),如：ジスットハピモュボ
// 韩文：Hangul
// 俄文：撕拉法文(Cyrillic)  Ideographic
// 法文
// 德文
// 罗马文
// 阿拉伯文
func main() {
	fmt.Println(unicode.Hangul)                                                                                   // 汉字编码范围( 低位,高位,步长)
	fmt.Println('中', unicode.Is(unicode.Han, '中'))                                                                         // 中的unicode码，是否在汉字编码范围
	fmt.Println('a', unicode.Is(unicode.Han, 'a'))                                                                         // a的unicode码，是否在汉字编码范围
	fmt.Println(unicode.In('に', unicode.ASCII_Hex_Digit, unicode.Han, unicode.Arabic, unicode.Hiragana, unicode.Katakana)) // 中的unicode码，是否在汉字编码范围
}
