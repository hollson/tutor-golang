package main

import (
	"fmt"

	"cmdline/myCobra/cmd"
)

// PersistentFlags

//go:generate go run main.go
//go:generate go run main.go -h
//go:generate go run main.go -h=true
//go:generate go run main.go -h=T
//go:generate go run main.go -h=t
//go:generate go run main.go -h=1
//go:generate go run main.go --host="192.168.1.10" --config="./my.ini"
//go:generate go run main.go --host "192.168.1.10" --config ./my.ini
//go:generate go run main.go run
//go:generate go run main.go run -h
//go:generate go run main.go run --config="./my.ini"
//go:generate go run main.go run --config="./my.ini" --foo="hello"
func main() {
	fmt.Printf("=== 🎬 演示一个github.com/spf13/cobra的标准用法===\n\n")
	cmd.Execute()
	fmt.Printf("\n 👄 读取配置：%+v\n",cmd.MyConfig)
}
