package cmd

import (
	"fmt"

	"github.com/spf13/cobra"
)

type myConfig struct {
	Host   string // 主机
	Port   int    // 端口
	Config string // 配置文件
}

var MyConfig myConfig

// cmdRoot 定义Root命令
var cmdRoot = &cobra.Command{
	Use:           "mysql",   // 应用名称
	Version:       "v8.0.26", // 应用版本
	SilenceErrors: true,      // 错误静默
	CompletionOptions: cobra.CompletionOptions{
		DisableDefaultCmd:   true,
		DisableNoDescFlag:   true,
		DisableDescriptions: true,
	},

	Short: "MySQL 一个免费开源的关系型数据库管理系统",
	// Long:  `      MySQL 是最流行的关系型数据库管理系统，在WEB应用方面MySQL是最好的
	// RDBMS(Relational Database Management System：关系数据库管理系统)应用
	// 软件之一。`,
}

func initConfig() {
	fmt.Println("读取配置文件和ENV变量(如果已设置)。")
}

func init() {
	cobra.OnInitialize(initConfig)

	// 全局配置
	cmdRoot.Flags().SortFlags = false
	cmdRoot.PersistentFlags().SortFlags = false
	cobra.EnableCommandSorting = false

	// 本地Flag参数，P表示带有速记字母
	cmdRoot.Flags().String("host", "localhost", "主机")
	cmdRoot.Flags().IntP("port", "p", 3306, "端口")
	cmdRoot.Flags().BoolP("help", "h", false, "查看帮助信息")
	// cmdRoot.MarkFlagRequired("host")

	// 全局Flag参数，Persistent:执着的
	cmdRoot.PersistentFlags().StringVar(&MyConfig.Config, "config", "/etc/mysql.ini", "配置文件")
	cmdRoot.PersistentFlags().StringP("model", "m", "prod", "运行模式")

	// 命令事件
	// 注意 ⚠️: 只有执行了Run,相关命令才会执行
	cmdRoot.PersistentPreRun = func(cmd *cobra.Command, args []string) {

		fmt.Println(" 😊 我是root command中的方法，子命令也会(继承)并执行我 - ", cmd.Name())
	}

	// Run前操作，如获取参数值
	cmdRoot.PreRun = func(cmd *cobra.Command, args []string) {
		MyConfig.Host = cmd.Flag("host").Value.String()
		fmt.Println(" 😜 我是root command中的方法，子命令不会继承我的操作")
	}

	cmdRoot.PostRun = func(cmd *cobra.Command, args []string) {
		fmt.Println(" 🔚 我在Run之后执行", args)
	}

	cmdRoot.Run = func(cmd *cobra.Command, args []string) {
		fmt.Printf(" 💬 %+v\n", MyConfig)
		fmt.Println(" 🚗 执行一个具体的命令喽～～")
	}
}

// Execute 执行命令行
func Execute() {
	cmdRoot.Execute()
}
