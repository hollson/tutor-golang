package cmd

import (
	"fmt"

	"github.com/spf13/cobra"
)

// cmdRun represents the create command
var cmdRun = &cobra.Command{
	Use:   "run",
	Short: "运行mysql服务",
	Long:  `这里演示了一个mysql启动命令`,
	Run: func(cmd *cobra.Command, args []string) {
		fmt.Println(" 🚀 mysql服务已启动了")
		fmt.Printf(" 🖨️ 读取配置：%+v\n", MyConfig)
		fmt.Printf(" 🖨️ 读取配置项目：%+v\n", cmd.Flag("foo").Value)
		fmt.Printf(" 🖨️ 读取全局配置：%+v\n", cmd.Flag("config").Value)
		fmt.Printf(" 🖨️ 参数是否已排序： %+v\n", cmd.Flags().SortFlags)
	},
}

func init() {
	// 命令设置
	cmdRun.Flags().SortFlags = false

	// 添加到根命令下
	cmdRoot.AddCommand(cmdRun)

	// 添加子命令参数
	cmdRun.PersistentFlags().String("foo", "foobar", "run的全局参数")
	cmdRun.Flags().BoolP("toggle", "t", false, "run的本地参数")
}
