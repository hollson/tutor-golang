package main

import (
    "flag"
	"fmt"
	"os/exec"
)

func init() {
    flag.Parse()
}
//exec用于执行外部命令，它将os.StartProcess进行包装使得它更容易映射到stdin和stdout，并且利用pipe连接i/o．
//go:generate go run main.go
func main() {
    // var b bytes.Buffer
    // exe := exec.Command("echo", "hello")
    // exe.Stdout = &b
    // exe.Stderr = &b
    // err:= exe.Run()
    // if err!=nil{
    // 	fmt.Println("xxx:",b.String())
    // 	fmt.Println(string(b.Bytes()))
    // 	fmt.Println()
    // }

    c := "echo hello world"
    cmd := exec.Command("sh", "-c", c)
    out, err := cmd.Output()
    // cmd.Run()

    fmt.Println(string(out), err)
    // if len(flag.Args()) == 0 {
    // 	a := []string{
    // 		"p1",
    // 		"p2",
    // 		"p3=0",
    // 	}
    // 	var b bytes.Buffer
    // 	exe := exec.Command("bash", "-c", "./exec "+strings.Join(a, " "))
    // 	exe.Stdout = &b
    // 	exe.Stderr = &b
    // 	err := exe.Run()
    //
    // 	if err != nil {
    // 		fmt.Println(string(b.Bytes()))
    // 		panic(err)
    // 	}
    // 	fmt.Println(string(b.Bytes()))
    // } else {
    // 	for _, v := range flag.Args() {
    // 		fmt.Printf("%s ", v)
    // 	}
    // 	fmt.Println()
    // }
}
