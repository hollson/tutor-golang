/***************************************
  自定义Flag参数类型,实现了flag.Value接口
***************************************/

package cmd

import (
	"strings"
)

// StrArray 从命令行参数接收一个以逗号分割的字符串，并转换为StrArray类型
type StrArray []string

// Set 将一个逗号分割的字符串写入StrArray
func (s *StrArray) Set(val string) error {
	*s = strings.Split(val, ",")
	return nil
}

// 以逗号分割的形式输出StrArray
func (s *StrArray) String() string {
	return strings.Join(*s, ",")
}
