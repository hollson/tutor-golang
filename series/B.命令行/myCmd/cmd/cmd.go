// Copyright 2021 Hollson. All rights reserved.
// Use of this source code is governed by a MIT style
// license that can be found in the LICENSE file.

package cmd

import (
	"flag"
	"fmt"
	"os"
	// "os"
	// "fmt"
	// "os"
)

// server命令行参数
var (
	// _plugs   StrArray
	_version bool
	_help    bool
)

// Execute 执行命令行程序
//go:generate go build -o server
//go:generate ./server --help
//go:generate ./server -host=localhost -port 8888 -port 9999 -daemon -tags="tag1,tag2"
func Execute() {
	// flag.Var(&_plugs, "plugs", "组件(以逗号分割)")
	flag.BoolVar(&_version, "version", false, "查看版本")
	flag.BoolVar(&_help, "help", false, "查看帮助")

	var usage = `%s Git 是一个免费的开源分布式版本控制系统。%s

Usage:
    git [<Command>] [Options]...

Command:
    clone	克隆仓库到一个新目录
    pull	获取并整合另外的仓库或一个本地分支
    push	更新远程引用和相关的对象

Options:
`

	flag.Usage = func() {
		fmt.Printf(usage, "\b\033[35m", "\u001B[0m")
		flag.PrintDefaults() // 默认的帮助说明
	}

	// 解析flag
	flag.Parse()

	if _help || len(os.Args) == 1 {
		flag.Usage()
	}

	if _version {
		fmt.Println("git version v1.0.0")
	}
	subRun()
}
