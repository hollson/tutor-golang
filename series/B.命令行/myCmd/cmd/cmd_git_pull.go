// Copyright 2021 Hollson. All rights reserved.
// Use of this source code is governed by a MIT style
// license that can be found in the LICENSE file.

package cmd

import (
	"flag"
	"fmt"
	"os"
)

// 二级命令集：Clone
func subRun() {
	_subRun := flag.NewFlagSet("run", flag.ExitOnError)
	var _help, _verbose bool

	var _brchs StrArray

	_subRun.Var(&_brchs, "b", "多个分支名称(数组类型)")
	_subRun.BoolVar(&_verbose, "v", false, "更加详细")
	_subRun.BoolVar(&_help, "h", false, "查看帮助")

	_subRun.Parse(os.Args[2:])
	if _help || len(os.Args) == 2 {
		_subRun.Usage()
	}

	if _verbose {
		fmt.Println("仅仅是个pull的演示而已")
	}
}

// // 二级命令集：Stop
// func subStop() {
// 	_subRun := flag.NewFlagSet("stop", flag.ExitOnError)
// 	var (
// 		_after time.Duration
// 		_force bool
// 	)
//
// 	_subRun.DurationVar(&_after, "after", time.Second*5, "N秒后停止服务")
// 	_subRun.BoolVar(&_force, "force", false, "强制停止服务")
// 	_subRun.Parse(os.Args[2:])
// }
