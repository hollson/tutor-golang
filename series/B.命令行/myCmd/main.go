// Copyright 2021 Hollson. All rights reserved.
// Use of this source code is governed by a MIT style
// license that can be found in the LICENSE file.

package main

import (
	"cmdline/myCmd/cmd"
)

// 以git为示例，演示一个原生的Flag命令行使用案例
func main() {
	cmd.Execute()
}
