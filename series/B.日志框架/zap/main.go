package main

import (
    "time"

	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
)

// https://blog.csdn.net/C_jian/article/details/111315457
// https://www.cnblogs.com/ExMan/p/12264685.html
// https://www.cnblogs.com/ExMan/p/12264925.html
// https://www.liwenzhou.com/posts/Go/use_zap_in_gin/

func main() {
	{
		logger, err := zap.NewProduction()
		if err != nil {
			panic(err)
		}

		sugar := logger.Sugar()
		sugar.Infow("asldkfjasldfj", "url", "go.uber.org", "attempt", 3, "backoff", time.Second, sugar)
		sugar.Infow("asldkfjasldfj", "url", "go.uber.org", "attempt", 3, "backoff", time.Second, "cc", sugar)
	}

	{
		cfg := zap.NewProductionConfig()
		cfg.EncoderConfig.EncodeTime = zapcore.ISO8601TimeEncoder
		cfg.OutputPaths = []string{"log"}
		logger, err := cfg.Build()
		if err != nil {
			panic(err)
		}

		sugar := logger.Sugar()
		sugar.Infow("asldkfjasldfj", "url", "go.uber.org", "attempt", 3, "backoff", time.Second, sugar)
		sugar.Infow("asldkfjasldfj", "url", "go.uber.org", "attempt", 3, "backoff", time.Second, "cc", sugar)
	}
}
