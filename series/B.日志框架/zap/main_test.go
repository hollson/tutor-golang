package main

import (
    "io/ioutil"
	"testing"

	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"

	"gitee.com/gricks/logrus"
)

type Val struct {
	v string
}

func (this *Val) String() string {
	return this.v
}

func newZapLogger(lvl zapcore.Level, f string) *zap.Logger {
	ec := zap.NewProductionEncoderConfig()
	ec.EncodeDuration = zapcore.NanosDurationEncoder
	ec.EncodeTime = zapcore.ISO8601TimeEncoder
	enc := zapcore.NewJSONEncoder(ec)
	var (
		err  error
		sink zapcore.WriteSyncer
	)
	if f == "" {
		sink, _, err = zap.Open()
		if err != nil {
			panic(err)
		}
	} else {
		sink, _, err = zap.Open(f)
		if err != nil {
			panic(err)
		}
	}
	return zap.New(zapcore.NewCore(
		enc,
		sink,
		lvl,
	), zap.AddCaller())
}

func Benchmark_Logrus(b *testing.B) {
	v := &Val{"asdlkfja"}
	logrus.SetOutput(ioutil.Discard)
	for i := 0; i < b.N; i++ {
		logrus.Info("a;sldfkjaldfkj:%s %d %s %s %s %s", "alsdfjaslkf", 1021301, "as;dklfalfxxx", "klasdjfa", "Val", v)
	}
}

func Benchmark_Logrus_File(b *testing.B) {
	v := &Val{"asdlkfja"}
	logger := logrus.NewWrapper("logrus", logrus.DEBUG).GetEntry()
	for i := 0; i < b.N; i++ {
		logger.Info("a;sldfkjaldfkj:%s %d %s %s %s %s", "alsdfjaslkf", 1021301, "as;dklfalfxxx", "klasdjfa", "Val", v)
	}
}

func Benchmark_ZapF(b *testing.B) {
	logger := newZapLogger(zapcore.DebugLevel, "")
	sugar := logger.Sugar()
	v := &Val{"asdlkfja"}
	for i := 0; i < b.N; i++ {
		sugar.Debugf("a;sldfkjaldfkj:%s %d %s %s %s %s", "alsdfjaslkf", 1021301, "as;dklfalfxxx", "klasdjfa", "Val", v)
	}
}

func Benchmark_ZapF_File(b *testing.B) {
	logger := newZapLogger(zapcore.DebugLevel, "zapF.log")
	sugar := logger.Sugar()
	v := &Val{"asdlkfja"}
	for i := 0; i < b.N; i++ {
		sugar.Debugf("a;sldfkjaldfkj:%s %d %s %s %s %s", "alsdfjaslkf", 1021301, "as;dklfalfxxx", "klasdjfa", "Val", v)
	}
}

func Benchmark_ZapW(b *testing.B) {
	logger := newZapLogger(zapcore.DebugLevel, "")
	sugar := logger.Sugar()
	v := &Val{"asdlkfja"}
	for i := 0; i < b.N; i++ {
		sugar.Debugw("a;sldfkjaldfkj", "alsdfjaslkf", 1021301, "as;dklfalfxxx", "klasdjfa", "Val", v)
	}
}

func Benchmark_ZapW_File(b *testing.B) {
	logger := newZapLogger(zapcore.DebugLevel, "zapW.log")
	sugar := logger.Sugar()
	v := &Val{"asdlkfja"}
	for i := 0; i < b.N; i++ {
		sugar.Debugw("a;sldfkjaldfkj", "alsdfjaslkf", 1021301, "as;dklfalfxxx", "klasdjfa", "Val", v)
	}
}
