package loger

import (
    "encoding/json"
	"fmt"
)

type JSONFormatter struct{}

func (this *JSONFormatter) Format(entry *Entry) ([]byte, error) {
	data := make(map[string]interface{}, len(entry.Data)+3)

	data["msg"] = entry.Message
	data["time"] = entry.Time.Format(timeformat)
	data["level"] = entry.Level.String()

	b, err := json.Marshal(data)
	if err != nil {
		return nil, fmt.Errorf("Failed to marshal fields to JSON, %v", err)
	}

	return append(b, '\n'), nil
}
