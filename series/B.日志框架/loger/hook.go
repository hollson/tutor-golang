package loger

type Hook interface {
	Levels() []Level
	Fire(*Entry) error
}

type Hooks map[Level][]Hook

func (this Hooks) Add(hook Hook) {
	for _, level := range hook.Levels() {
		this[level] = append(this[level], hook)
	}
}

func (this Hooks) Fire(level Level, entry *Entry) error {
	for _, hook := range this[level] {
		if err := hook.Fire(entry); err != nil {
			return err
		}
	}

	return nil
}
