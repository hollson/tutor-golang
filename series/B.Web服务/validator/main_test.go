package main

import (
    "context"
	"testing"
)

func BenchmarkDecode(b *testing.B) {
	b.RunParallel(func(pb *testing.PB) {
		values := parseForm()
		user := &User{}
		for pb.Next() {
			_ = decoder.Decode(user, values)
		}
	})
}

func BenchmarkConform(b *testing.B) {
	b.RunParallel(func(pb *testing.PB) {
		values := parseForm()
		user := &User{}
		decoder.Decode(user, values)
		for pb.Next() {
			_ = conform.Struct(context.Background(), user)
		}
	})
}

func BenchmarkValidate(b *testing.B) {
	b.RunParallel(func(pb *testing.PB) {
		values := parseForm()
		user := &User{}
		decoder.Decode(user, values)
		for pb.Next() {
			_ = validate.Struct(user)
		}
	})
}
