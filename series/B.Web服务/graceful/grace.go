// Copyright 2021 Hollson. All rights reserved.
// Use of this source code is governed by a MIT style
// license that can be found in the LICENSE file.

package main

import (
	"fmt"
	"os"
	"os/signal"
	"syscall"
	"time"
)

// 参考：https://colobu.com/2015/10/09/Linux-Signals/
func main() {
	// simple()
	grace()
	// stop()
}

// 简单示例：
//  1.在终端1执行 go run main.go
//  2.在终端2执行 kill -USR2 <进程号>
func simple() {
	// 创建一个接收信号的管道
	c := make(chan os.Signal)

	// 监听信号(可以指定监听的信号,如：syscall.SIGHUP, syscall.SIGUSR2)
	signal.Notify(c)

	fmt.Println("程序已启动，当前进程号：", os.Getpid())

	// 阻塞等待信号输入
	s := <-c
	fmt.Println("接收到信号:", s)
}

// 优雅退出
func grace() {
	quit := make(chan int)

	var graceHandler = func() {
		c := make(chan os.Signal)
		signal.Notify(c,
			syscall.SIGHUP,  // 挂起(hangup)			kill -HUP 6053
			syscall.SIGINT,  // 打断(interrupt)		ctl+c
			syscall.SIGTERM, // 终止(terminated)		kill 6113
			syscall.SIGQUIT, // 退出					kill -QUIT 6053
			syscall.SIGUSR1, // 自定义信号			kill -USR1 6053
			syscall.SIGUSR2) // 自定义信号			kill -USR2 6053

		for s := range c {
			switch s {
			case syscall.SIGHUP, syscall.SIGINT, syscall.SIGTERM, syscall.SIGQUIT:
				fmt.Println("启动优雅退出：", s)
				fmt.Println("执行退出1 ：发出通知")
				time.Sleep(time.Second)
				fmt.Println("执行退出2 ：保存数据")
				time.Sleep(time.Second)
				fmt.Println("执行退出3 ：清理垃圾")
				time.Sleep(time.Second * 3)
				quit <- 1
			case syscall.SIGUSR1:
				fmt.Println("usr1 signal", s)
			case syscall.SIGUSR2:
				fmt.Println("usr2 signal", s)
			default:
				fmt.Println("other signal", s)
			}
		}
	}
	go graceHandler()

	fmt.Println("程序已启动，当前进程号：", os.Getpid())
	if <-quit == 1 {
		fmt.Println("主程序已退出～～～")
	}
}

// 停止接收信号：
//  1.在终端1执行 go run main.gos
//  2.在终端2执行 kill -USR1 <进程号>
//  3.在终端2执行 kill -USR2 <进程号>,停止接收信号
//  4.在终端2执行 kill -USR1 <进程号>,即收不到任何信号了
func stop() {
	c := make(chan os.Signal)
	signal.Notify(c)

	fmt.Println("程序已启动，当前进程号：", os.Getpid())
	for {
		s := <-c
		fmt.Println("接收到信号:", s)

		if s == syscall.SIGUSR2 {
			fmt.Println("停止接收信号.")
			signal.Stop(c)
		}
	}
}
