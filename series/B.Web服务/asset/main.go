// Copyright 2021 Hollson. All rights reserved.
// Use of this source code is governed by a MIT style
// license that can be found in the LICENSE file.

package main

import (
	"fmt"
	"net/http"
)

// FileServer示例：
//	http://localhost:8080/
//	http://localhost:8080/asset/"
//	http://localhost:8080/static/
// http.FileServer和http.ServeFile底层调用http.ServeContent()

func main() {
	// 方式一：默认路径
	http.Handle("/", http.FileServer(http.Dir("static")))

	// 方式二：自定义URL ( StripPrefix：修剪url中的特定前缀，即删除虚拟路径 )
	http.Handle("/asset/", http.StripPrefix("/asset/", http.FileServer(http.Dir("static"))))

	// _handler := http.StripPrefix("/asset/", http.FileServer(http.Dir("static")))
	// http.HandleFunc("/asset/", _handler.ServeHTTP)

	// 方式三(推荐)：自定义ServeFile,可添加中间件等
	http.HandleFunc("/static/", func(w http.ResponseWriter, r *http.Request) {
		fmt.Println("==> ", r.URL.Path)
		http.ServeFile(w, r, r.URL.Path[1:])
	})

	// http.ServeFile()
	// http.ServeContent()
	// http.Redirect()
	// 如何添加文件元数据

	// 自定义简码
	// 本地文件数据库

	http.ListenAndServe(":8080", nil)
}
