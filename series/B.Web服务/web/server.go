package main

import (
	"fmt"
	"net/http"
)

func myHandler(w http.ResponseWriter, r *http.Request) {
	fmt.Printf("==> %s\n", r.RequestURI)
	w.Write([]byte("hello world\n"))
}

// DefaultMux 系统默认路由(DefaultServeMux)
func DefaultMux() {
	http.Handle("/default/foo", http.HandlerFunc(myHandler))
	http.HandleFunc("/default/bar", myHandler)

	http.ListenAndServe(":8080", nil)
}

// DefineMux 自定义Mux路由
func DefineMux() {
	mux := http.NewServeMux()
	mux.HandleFunc("/define/foo", myHandler)
	http.ListenAndServe(":8080", mux)
}

// 默认路由
func main() {
	DefaultMux()
	// DefineMux()
}
