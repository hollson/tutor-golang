package main
//
// import (
//     "context"
// 	"log"
// 	"net/http"
// 	"os"
// 	"os/signal"
// 	"syscall"
// 	"time"
//
// 	"github.com/gin-gonic/gin"
// )
//
// func main() {
// 	r := gin.Default()
// 	r.GET("/", func(c *gin.Context) {
// 		time.Sleep(5 * time.Second)
// 		c.String(http.StatusOK, "gin Welcome\n")
// 	})
//
// 	srv := &http.Server{
// 		Addr:         ":19881",
// 		Handler:      r,
// 		ReadTimeout:  30 * time.Second,
// 		WriteTimeout: 30 * time.Second,
// 	}
//
// 	go srv.ListenAndServe()
//
// 	// Handle SIGINT and SIGTERM.
// 	ch := make(chan os.Signal)
// 	signal.Notify(ch, syscall.SIGINT, syscall.SIGTERM)
// 	log.Println(<-ch)
//
// 	// Stop the service gracefully.
// 	ctx, cancel := context.WithTimeout(context.Background(), 1*time.Second)
// 	defer cancel()
// 	log.Println("graceful shutdown:", srv.Shutdown(ctx))
// }
