package proxy
//
// import (
// 	"fmt"
// 	"locojoy.com/app/api"
// 	"math/rand"
// 	"net/http"
// 	"time"
//
// 	"github.com/BurntSushi/toml"
// 	"github.com/gin-gonic/gin"
//
// 	"locojoy.com/common/logger"
// 	"locojoy.com/system/global"
// 	"locojoy.com/system/serverlist"
// 	"locojoy.com/system/zkm"
// )
//
// func Router() *gin.Engine {
// 	router := gin.New()
// 	router.Use(logger.Recovery(), logger.StatisticApi())
// 	router.Use(proxyMiddle)
//
// 	regRouter(router)
// 	regAdmin(router)
//
// 	return router
// }
//
// // 注册路由
// func regRouter(router *gin.Engine) {
//
// 	etc := router.Group("/proxy")
// 	// etc.Use(middle.Auth())
// 	{
// 		etc.GET("/profile", ProfileHandler) // [开放] 服务器环境全局配置表
// 		etc.GET("/servers", ServersHandler) // [开放] 向客户端下发服务列表
// 	}
//
// 	router.GET("/server/list", ServerListHandler) // Deprecated:不规范的API,废弃
// }
//
// // 获取服务列表
// //go:generate curl -X GET "http://127.0.0.1:18889/server/list"
// //go:generate curl -X GET "http://127.0.0.1:18889/gate/server/list"
// // Deprecated: 废弃
// func ServerListHandler(c *gin.Context) {
// 	servers := serverlist.ServerList.Servers()
//
// 	type Info struct {
// 		Type     int    `json:"type"`
// 		Name     string `json:"name"`
// 		IP       string `json:"ip"`
// 		Port     int    `json:"port"`
// 		ConnType int    `json:"conn_type"`
// 	}
//
// 	ret := make([]Info, 0)
// 	r := rand.New(rand.NewSource(time.Now().UnixNano()))
// 	if _, ok := servers[global.ServerPush]; ok {
// 		index := r.Intn(len(servers[global.ServerPush]))
// 		ret = append(ret, Info{
// 			Type:     global.SERVER_TYPE_PUSH,
// 			Name:     global.ServerPush.String(),
// 			IP:       servers[global.ServerPush][index].OuterIP,
// 			Port:     servers[global.ServerPush][index].Port,
// 			ConnType: 1,
// 		})
// 	}
//
// 	if _, ok := servers[global.ServerTeamwork]; ok {
// 		index := r.Intn(len(servers[global.ServerTeamwork]))
// 		ret = append(ret, Info{
// 			Type:     global.SERVER_TYPE_Teamwork,
// 			Name:     global.ServerTeamwork.String(),
// 			IP:       servers[global.ServerTeamwork][index].OuterIP,
// 			Port:     servers[global.ServerTeamwork][index].Port,
// 			ConnType: 0,
// 		})
// 	}
//
// 	if _, ok := servers[global.ServerFile]; ok {
// 		index := r.Intn(len(servers[global.ServerFile]))
// 		ret = append(ret, Info{
// 			Type:     global.SERVER_TYPE_FILE,
// 			Name:     global.ServerFile.String(),
// 			IP:       servers[global.ServerFile][index].OuterIP,
// 			Port:     servers[global.ServerFile][index].Port,
// 			ConnType: 0,
// 		})
// 	}
//
// 	c.JSON(http.StatusOK, ret)
// }
//
// // 向客户端下发服务列表
// // 例: http://10.0.0.20:18889/proxy/servers
// func ServersHandler(c *gin.Context) {
// 	servers := serverlist.ServerList.Servers()
//
// 	// 下发的服务信息
// 	dump := make(map[global.Server][]zkm.ServerInfo)
// 	dump[global.ServerFile] = servers[global.ServerFile]
// 	dump[global.ServerTeamwork] = servers[global.ServerTeamwork]
// 	dump[global.ServerPush] = servers[global.ServerPush]
//
// 	c.JSON(http.StatusOK, dump)
// }
//
// type Profile struct {
// 	Base      map[string]interface{} `toml:"base"`
// 	Thirdpart map[string]interface{} `toml:"thirdpart"`
// }
//
// // 用于向客户端提供全局服务配置信息
// // 例: http://10.0.0.20:18889/proxy/profile
// //    http://10.0.0.20:18889/proxy/profile?format=json
// func ProfileHandler(c *gin.Context) {
// 	format := c.DefaultQuery("format", "toml") // 默认原生显示
// 	if format == "json" {
// 		var g Profile
// 		if _, err := toml.DecodeFile("./conf/profile.toml", &g); err != nil {
// 			c.JSON(http.StatusInternalServerError, "profile[json]文件加载失败")
// 			return
// 		}
//
// 		c.JSON(http.StatusOK, g)
// 		return
// 	}
// 	c.Header("Content-Type", "application/toml;charset=UTF-8")
// 	c.File("./conf/profile.toml")
//
// 	// data, err := ioutil.ReadFile("./conf/profile.toml")
// 	// if err != nil {
// 	// 	c.JSON(http.StatusInternalServerError, "profile[raw]文件加载失败")
// 	// 	return
// 	// }
// 	// c.Header("Content-Type", "text/plain;charset=UTF-8")
// 	// c.String(http.StatusOK, fmt.Sprintf("%s", string(data)))
// }
//
// func regAdmin(router *gin.Engine) {
// 	admin := router.Group(fmt.Sprintf("/%s", global.ServerProxy.String()))
// 	{
// 		admin.GET("/ping", PingHandler)
// 	}
// }
//
// func PingHandler(c *gin.Context) {
// 	var pong = api.DefaultResult()
// 	if err := zkm.Ping(); err != nil {
// 		pong.Msg = fmt.Sprintf("ZK检查失败,%v", err.Error())
// 		pong.ErrorWithPing(c)
// 		return
// 	}
// 	pong.Response(c)
// }
