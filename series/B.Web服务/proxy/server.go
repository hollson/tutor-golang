package proxy
//
// import (
// 	"time"
//
// 	"github.com/gin-gonic/gin"
// )
//
// type server struct {
// 	frame.BaseServer
// }
//
// // 构造函数
// func NewServer() *server {
// 	s := &server{}
// 	s.BeforeRun = beforeRun
// 	s.DisableDB = true
// 	s.DisableTable = true
// 	s.DisableSF = true
// 	return s
// }
//
// func (s *server) Name() global.Server {
// 	return global.ServerProxy
// }
//
// func (s *server) Port() int {
// 	return global.ServerProxyPort
// }
//
// func (s *server) Route() *gin.Engine {
// 	gin.SetMode(gin.ReleaseMode)
// 	return Router()
// }
//
// func (s *server) ZkHandler() {
// 	// 设置事件
// 	zkm.Register(&serverlist.ServerList)
//
// 	// 添加关注
// 	serverlist.ServerList.AddMonitor(global.NameSpace, global.ServerOAuth)
// 	serverlist.ServerList.AddMonitor(global.NameSpace, global.ServerHall)
// 	serverlist.ServerList.AddMonitor(global.NameSpace, global.ServerPush)
// 	serverlist.ServerList.AddMonitor(global.NameSpace, global.ServerFile)
// 	serverlist.ServerList.AddMonitor(global.NameSpace, global.ServerMatch)
// 	serverlist.ServerList.AddMonitor(global.NameSpace, global.ServerForum)
// 	serverlist.ServerList.AddMonitor(global.NameSpace, global.ServerTeamwork)
// 	serverlist.ServerList.AddMonitor(global.NameSpace, global.ServerPlay)
// 	serverlist.ServerList.AddMonitor(global.NameSpace, global.ServerNotify)
// 	serverlist.ServerList.AddMonitor(global.NameSpace, global.ServerFavorite)
// 	serverlist.ServerList.AddMonitor(global.NameSpace, global.ServerDiy)
// 	serverlist.ServerList.AddMonitor(global.NameSpace, global.ServerPlat)
// 	serverlist.ServerList.AddMonitor(global.NameSpace, global.ServerProxy)
// 	serverlist.ServerList.AddMonitor(global.NameSpace, global.ServerAdmin)
// 	serverlist.ServerList.AddMonitor(global.NameSpace, global.ServerPhysics)
// 	serverlist.ServerList.AddMonitor(global.NameSpace, global.ServerSync)
// 	serverlist.ServerList.AddMonitor(global.NameSpace, global.ServerMgee)
//
// 	// 设置事件
// 	zkm.Register(&serverlist.ServerList)
//
// 	zkm.Start()
//
// 	frame.RegToZkWithLoopAsync(s, time.Second*15)
// }
//
// func beforeRun(s *frame.BaseServer) error {
// 	regProxyMapping()
// 	return nil
// }
