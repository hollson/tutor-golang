package proxy
//
// import (
// 	"fmt"
// 	"net/http"
// 	"net/http/httputil"
// 	"net/url"
// 	"strings"
//
// 	"github.com/ulule/limiter/v3"
//
// 	"github.com/gin-gonic/gin"
// 	"github.com/sirupsen/logrus"
//
// 	"locojoy.com/app/api"
// 	"locojoy.com/system/global"
// 	"locojoy.com/system/serverlist"
// )
//
// // 网关代理映射
// var ProxyMapping map[string]global.Server
//
// // 注册网关代理映射关系(不需要代理的服务,则进行直连)
// func regProxyMapping() {
// 	ProxyMapping = make(map[string]global.Server)
// 	ProxyMapping["oauth"] = global.ServerOAuth
// 	ProxyMapping["hall"] = global.ServerHall
// 	ProxyMapping["play"] = global.ServerPlay
// 	ProxyMapping["diy"] = global.ServerDiy
// 	ProxyMapping["favorite"] = global.ServerFavorite
// 	ProxyMapping["room"] = global.ServerMatch
// 	ProxyMapping["feed"] = global.ServerForum
// 	ProxyMapping["store"] = global.ServerForum
// 	ProxyMapping["team"] = global.ServerTeamwork
// 	ProxyMapping["notify"] = global.ServerNotify
// 	ProxyMapping["plat"] = global.ServerPlat
// 	ProxyMapping["mgee"] = global.ServerMgee
// 	ProxyMapping["sync"] = global.ServerSync
// 	ProxyMapping["asset"] = global.ServerFile
// 	ProxyMapping["admin"] = global.ServerAdmin
// }
//
// // Http代理中间件: 约定使用服务名称前缀作为代理依据
// //go:generate  curl -v -X GET  "http://127.0.0.1:18889/server_name/param1/param2?param=xxx"
// func proxyMiddle(c *gin.Context) {
// 	var remote = c.Request.URL
// 	pathInfo := strings.SplitN(remote.Path, "/", 3) // {"","server_name","param1/param2?param=xxx"}
//
// 	// 请求地址错误
// 	if len(pathInfo) < 2 {
// 		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"code": api.CODE_1000, "msg": "request url error"})
// 		return
// 	}
// 	serverName := pathInfo[1]
//
// 	// 忽略对GateServer的代理
// 	if serverName == "gate" || serverName == "proxy" || serverName == "server" {
// 		return
// 	}
//
// 	// 根据Path中的server_name,获取代理转向URL
// 	if serverType, has := ProxyMapping[serverName]; has {
// 		ip, port, err := serverlist.ServerList.RandServerInfo(serverType)
// 		if err != nil {
// 			// API网关错误
// 			logrus.Warnf("proxy [%s] type error,%v", serverName, err)
// 			c.AbortWithStatusJSON(http.StatusBadGateway, gin.H{"code": api.CODE_1000, "msg": "api gate error"})
// 			return
// 		}
// 		remote, _ = url.Parse(fmt.Sprintf("http://%s:%d", ip, port))
// 	}
//
// 	// 创建一个反向代理
// 	// ReverseProxy 是一个 HTTP 处理程序，它接受传入的请求并将其发送到另一台服务器，将响应代理回客户端。
// 	// ReverseProxy 默认将客户端 IP 设置为 X-Forwarded-For 标头的值。 如果 X-Forwarded-For 标头已存在，
// 	// 则客户端 IP 将附加到现有值。 作为一种特殊情况，如果标头存在于 Request.Header 映射中但具有 nil 值
// 	// （例如由 Director func 设置时），则不会修改 X-Forwarded-For 标头。
// 	//
// 	// 为防止 IP 欺骗，请务必删除来自客户端或不受信任的代理的任何预先存在的 X-Forwarded-For 标头。
// 	proxy := httputil.NewSingleHostReverseProxy(remote)
// 	proxy.ErrorHandler = func(writer http.ResponseWriter, request *http.Request, err error) {
// 		logrus.Errorf("proxy handler error:=%v, remote=%+v", err, c.Request)
// 	}
//
// 	proxy.ServeHTTP(c.Writer, c.Request)
// }
//
// func rateMiddle(lim *limiter.Limiter, c *gin.Context) {
// 	var (
// 		limiterCtx limiter.Context
// 		err        error
// 		req        = c.Request
// 	)
//
// 	opt := limiter.Options{
// 		IPv4Mask:           limiter.DefaultIPv4Mask,
// 		IPv6Mask:           limiter.DefaultIPv6Mask,
// 		TrustForwardHeader: false,
// 	}
//
// 	ip := limiter.GetIP(req, opt)
//
// 	if strings.HasPrefix(c.Request.URL.String(), "/") {
// 		limiterCtx, err = lim.Get(req.Context(), ip.String())
// 		if err != nil {
// 			c.AbortWithStatusJSON(http.StatusInternalServerError, err.Error())
// 			return
// 		}
// 	} else {
// 		logrus.Infof("The api request is not track")
// 	}
//
// 	if limiterCtx.Reached {
// 		logrus.Warnf("Too Many Request from %s on %s", ip, c.Request.URL.String())
// 		c.AbortWithStatusJSON(http.StatusTooManyRequests, fmt.Errorf("429"))
// 		return
// 	}
// }
