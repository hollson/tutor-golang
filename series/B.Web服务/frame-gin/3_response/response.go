package main

import (
	"errors"
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/gin-gonic/gin/testdata/protoexample"
)

var router *gin.Engine

func main() {
	router = gin.Default()

	NormalRaw()
	StdFormat()
	HtmlRaw()
	// StaticFile()
	router.Run()
}

// 常用响应
// text
// json
// header
// data
// file
// status
// cookie

// 重定向
// redirect
// set
// abort

// 其他格式
// proto
// xml
// yaml
// jsonp
// ascii

// 标准格式，包括Json、XML、Yaml、Protobuf
func StdFormat() {
	// Json - 哈希
	// curl http://127.0.0.1:8080/json/hash
	router.GET("/json/hash", func(c *gin.Context) {
		c.JSON(http.StatusOK, gin.H{"message": "Json Hash"})
	})

	// Json - 对象
	// curl http://127.0.0.1:8080/json/obj
	router.GET("/json/obj", func(c *gin.Context) {
		c.IndentedJSON(http.StatusOK, struct {
			Name string `json:"name"`
		}{"Json Object"})
	})

	// Xml - 哈希
	// curl http://127.0.0.1:8080/xml/hash
	router.GET("/xml", func(c *gin.Context) {
		c.XML(http.StatusOK, gin.H{"name": "Xml Hash"})
	})

	// Ymal
	// curl http://127.0.0.1:8080/yml
	router.GET("/yml", func(c *gin.Context) {
		c.YAML(http.StatusOK, gin.H{"name": "YAML", "time": time.Now().Format("2006")})
	})

	// ProtoBuf
	router.GET("/pb", func(c *gin.Context) {
		label := "hello world"
		data := &protoexample.Test{
			Label: &label,
			Reps:  []int64{int64(1), int64(2)},
		}
		c.ProtoBuf(http.StatusOK, data) // 二进制数据
	})

}

func NormalRaw() {
	// 头部输出
	router.GET("/head", func(c *gin.Context) { c.Header("session_id", "123") })

	// 字符串
	router.GET("/str", func(c *gin.Context) {
		c.String(200, "我是一段字符串")
	})

	// 二进制
	router.GET("/data", func(c *gin.Context) {
		c.Data(200, "text/html;charset=utf-8", []byte("<h1>我来自context.Data</h1>"))
	})

	// 美化Json
	// curl http://127.0.0.1:8080/indent
	router.GET("/indent", func(c *gin.Context) {
		c.IndentedJSON(200, gin.H{"name": "<b>Thompson</b>", "email": "ken@gmail.com"})
	})

	// 重定向
	// curl -i http://127.0.0.1:8080/redirect
	router.GET("/redirect", func(c *gin.Context) {
		c.Redirect(302, "/str")
	})

	// // html渲染
	// // http://127.0.0.1:8080/html
	// router.GET("/html", func(c *gin.Context) {
	// https://blog.csdn.net/qq_34125999/article/details/127259015
	// 	// https://blog.csdn.net/a976134036/article/details/78867297
	// 	c.HTML(http.StatusOK, "<h1>Hello World<h1/>", "")
	// })

	// 跨域 (如果查询参数存在回调，则将回调添加到响应体中)
	// curl -i http://127.0.0.1:8080/jsonp?callback=www.abc.com
	router.GET("/jsonp", func(c *gin.Context) {
		data := map[string]interface{}{"foo": "bar"}
		c.JSONP(http.StatusOK, data)
	})

	// 纯Json
	// curl http://127.0.0.1:8080/pure
	router.GET("/pure", func(c *gin.Context) {
		c.PureJSON(200, gin.H{"msg": "<h1>我不会对html特殊字符进行unicode编码</h1>"})
	})

	// 阿斯卡码
	router.GET("/ascii", func(c *gin.Context) {
		c.AsciiJSON(200, "abc")
	})

	// Cookie
	// Cookie:account=1001;role=1
	// Cookie:token=%E4%B8%80%E6%AE%B5%E5%8A%A0%E5%AF%86%E6%96%87%E6%9C%AC
	// curl -i http://127.0.0.1:8080/cookie -H 'Cookie:account=1001;role=1' -H 'Cookie:token=%E4%B8%80%E6%AE%B5%E5%8A%A0%E5%AF%86%E6%96%87%E6%9C%AC'
	router.GET("/cookie", func(c *gin.Context) {
		// 获取Cookie
		token, _ := c.Cookie("token")
		account, _ := c.Cookie("account")
		group, err := c.Cookie("group")
		if err != nil {
			group = err.Error()
		}

		// cookie := &http.Cookie{
		// 	Name:     "session_id",
		// 	Value:    "123",
		// 	Path:     "/",
		// 	HttpOnly: true,
		// }
		// // 向响应的头部写入：Set-Cookie: session_id=123; Path=/; HttpOnly
		// http.SetCookie(c.Writer, cookie)

		// 设置Cookie
		c.SetCookie("gin_cookie", "test", 3600, "/", "localhost", false, true)
		c.JSONP(200, gin.H{"account": account, "group": group, "token": token})
	})

	// 中断 - 异常
	// curl -i http://127.0.0.1:8080/abort/err
	router.GET("/abort/err", func(c *gin.Context) {
		c.AbortWithError(500, errors.New("发生错误")).SetType(gin.ErrorTypeBind)
	})

	// 中断 - 状态值
	// curl -i http://127.0.0.1:8080/abort/code
	router.GET("/abort/code", func(c *gin.Context) {
		c.AbortWithStatus(800)
	})

	// 中断- Json
	// curl -i http://127.0.0.1:8080/abort/json
	router.GET("/abort/json", func(c *gin.Context) {
		c.AbortWithStatusJSON(200, gin.H{"msg": "hello"})
	})

}

// curl -i http://127.0.0.1:8080/dddd
func HtmlRaw() {
	router.GET("/dddd", func(c *gin.Context) {
		c.Set("WHAT", "buzhidao")
		c.String(200, "dddd\n")
	})
}
