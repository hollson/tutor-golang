// Copyright 2021 Hollson. All rights reserved.
// Use of this source code is governed by a MIT style
// license that can be found in the LICENSE file.

package frame_gin

import (
	"strings"
	"time"

	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
)

// https://www.cnblogs.com/-xuzhankun/p/11145772.html
// https://my.oschina.net/zyndev/blog/4524818
// www.ruanyifeng.com/blog/2016/04/cors.html


func main() {
	router := gin.Default()
	router.Use(cors.New(cors.Config{
		AllowOrigins:     []string{"https://foo.com"},
		AllowMethods:     []string{"PUT", "PATCH"},
		AllowHeaders:     []string{"Origin"},
		ExposeHeaders:    []string{"Content-Length"},
		AllowCredentials: true,
		AllowOriginFunc: func(origin string) bool {
			return strings.Contains(origin,"bar.com")
		},
		MaxAge: 12 * time.Hour,
	}))
	router.Run()
}
