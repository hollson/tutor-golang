package main

import (
	"fmt"
	"html/template"
	"io"
	"net/http"
	"os"
	"time"
)

// https://www.cnblogs.com/yh2924/p/12377309.html
// https://blog.csdn.net/kuangshp128/article/details/109598786

func Index(w http.ResponseWriter, r *http.Request) {
	t, err := template.ParseFiles("view/index.html")
	if err != nil {
		fmt.Fprintf(w, "parse template error: %s", err.Error())
		return
	}
	t.Execute(w, nil)
	return
}

func Upload(w http.ResponseWriter, r *http.Request) {
	file, head, err := r.FormFile("file")
	if err != nil {
		fmt.Fprintf(w, "parse template error: %s", err.Error())
		return
	}
	fmt.Println(head.Size,head.Filename,head.Header.Values(""))
// head.Size
	defer file.Close()

	f, err := os.OpenFile("./temp/"+head.Filename, os.O_WRONLY|os.O_CREATE|os.O_TRUNC, 0666)
	if err != nil {
		fmt.Fprintf(w, "parse template error: %s", err.Error())
		return
	}
	defer f.Close()

	io.Copy(f, file)

	fmt.Fprintf(w, "OK")

	time.Sleep(time.Second * 3)
	// Index(w, r)
}

func main() {
	os.Mkdir("./temp",0755)

	http.HandleFunc("/", Index)
	http.HandleFunc("/upload", Upload)

	http.ListenAndServe(":8080", nil)
}
