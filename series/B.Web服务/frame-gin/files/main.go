package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"net/http"

	"github.com/gin-gonic/gin"
)

// curl --location '127.0.0.1:8080/upload' --form 'file=@"/C:/Users/EDY/Desktop/a.txt"' --form 'type="product"'
func Upload(c *gin.Context) {
	fmt.Println(c.ContentType())
	file, _ := c.FormFile("file")
	uploadType := c.PostForm("type")

	log.Println(file.Filename, uploadType)
	c.SaveUploadedFile(file, "tmp/"+file.Filename)
	c.String(http.StatusOK, fmt.Sprintf("'%s' uploaded!", file.Filename))
}

// curl --location '127.0.0.1:8080/upload' --header 'Content-Type: text/plain' --data '@/C:/Users/EDY/Desktop/a.txt'
func uploadBinaryHandler(c *gin.Context) {
	data, err := c.GetRawData()
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	// 保存数据到文件
	err = ioutil.WriteFile("/path/to/destination", data, 0644)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		return
	}

	c.JSON(http.StatusOK, gin.H{
		"message": "数据上传成功",
	})
}

func main() {
	router := gin.Default()
	// 为 multipart forms 设置较低的内存限制 (默认是 32 MiB)
	router.MaxMultipartMemory = 8 << 20 // 8 MiB
	//router.LoadHTMLGlob("templates/*")

	router.GET("/upload", func(c *gin.Context) {
		c.HTML(200, "upload.html", nil)
	})
	router.POST("/upload", Upload)
	router.Run(":8080")
}
