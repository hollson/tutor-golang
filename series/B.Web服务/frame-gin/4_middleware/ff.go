package main

import (
    "net/http"

	"github.com/gin-gonic/gin"
)

func main() {
	r := gin.Default()
	r.GET("/", func(c *gin.Context) {
		c.JSON(http.StatusOK, gin.H{
			"message": "Hello World!",
		})
	})

	// WWW-Authenticate验证，适合于debug等简单验证，不安全。
	secret := r.Group("/", gin.BasicAuth(gin.Accounts{"admin": "123456", "guest": "111111"}))
	{
		secret.GET("/secret", func(c *gin.Context) {
			c.String(200, `www-authenticate是早期的一种简单的用户身份认证技术,在嵌入式领域中使用较多。由于采用用户名密码
加密方式为BASE-64，明码传输，其解码过程非常简单，是一种不安全等认证手段。`)
		})
	}

	r.Run() // 监听服务在 0.0.0.0:8080
}
