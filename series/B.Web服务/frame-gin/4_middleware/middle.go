// Copyright 2021 Hollson. All rights reserved.
// Use of this source code is governed by a MIT style
// license that can be found in the LICENSE file.

package main

import (
    "fmt"

    "github.com/gin-gonic/gin"
)

func main() {
    r := gin.Default()
    r.Use(middle1)
    r.GET("/home", homeHandler)
    r.Run()
}


func middle1(c *gin.Context) {
    fmt.Println("middle1 =====> 1")
    c.Next()  // 执行下一个中间件或子handler，相当于母版的{body}
    fmt.Println("middle1 =====> 2")
    fmt.Println()
}

func middle2(c *gin.Context) {
    fmt.Println("middle2 =====> 1")
    c.Abort() // 中止执行后续中间件或Handler，并执行当前中间件的剩余部分
    fmt.Println("middle2 =====> 2")
    fmt.Println()
}

func middle3(c *gin.Context) {
    fmt.Println("middle3 =====> 1")
    return   // 中止当前中间件，但会执行子handler
    fmt.Println("middle3 =====> 2")
}

//go:generate curl -X GET "http://127.0.0.1:8080/home"
func homeHandler(c *gin.Context) {
    fmt.Println("handler =====> ok")
    c.String(200, "OK")
}
