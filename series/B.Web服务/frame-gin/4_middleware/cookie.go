package main

import "github.com/gin-gonic/gin"

func Handler(c *gin.Context) {
	// 获得cookie
	s, err := c.Cookie("username")
	if err != nil {
		s = "ghz"
		// 设置cookie
		c.SetCookie("username", s, 60*60, "/", "localhost", false, true)
	}

	c.String(200, "测试cookie")
}

//
// 基于安全的考虑，需要给cookie加上Secure和HttpOnly属性，HttpOnly比较好理解，设置HttpOnly=true的cookie不能被js获取到，无法用document.cookie打出cookie的内容。
//
// Secure属性是说如果一个cookie被设置了Secure=true，那么这个cookie只能用https协议发送给服务器，用http协议是不发送的。

func main() {
	e := gin.Default()
	e.GET("/test", Handler)
	e.Run()
}
