package main

import (
    "fmt"
	"net/http"

	"github.com/gin-gonic/gin"
)

var router *gin.Engine

// 全局中间件
// 单次中间件
// 群组中间件
func main() {
	router = gin.Default()

	// 局部中间件
	router.GET("/flow", Flow(), func(c *gin.Context) {
		fmt.Println("handler - start")
		c.String(http.StatusOK,"hello")
		fmt.Println("handler - end")
	})

	router.GET("/counter", Counter(), func(c *gin.Context) {
		c.String(http.StatusOK,fmt.Sprintf("访问次数：%d",c.GetInt("counter")))
	})

	// 全局中间件 (按顺序注册路由，所以index不受影响)
	router.Use(BackList())
	router.GET("/counter", BackList(), func(c *gin.Context) {
		c.String(http.StatusOK,"ok")
	})

	router.GET("/flow", Flow(), func(c *gin.Context) {
		fmt.Println("handler - start")
		c.JSON(http.StatusOK, gin.H{
			"code": 200,
			"data": c.GetString("name"),
		})
		fmt.Println("handler - end")
	})


	// router.Run()
}

// 中间件 - 流程
// 		c.Next():  类似于模版中插入body，即插入子handler
// 		c.Abort(): 中止后续的所有子handler
func Flow() gin.HandlerFunc {
	return func(c *gin.Context) {
		fmt.Println("middleware - start")
		// c.Abort()// 中止
		c.Next() // 插入
		fmt.Println("middleware - end")
	}
}


// 中间件 - 计数器 (闭包)
func Counter() gin.HandlerFunc {
	count := 0
	return func(c *gin.Context) {
		count++
		c.Set("counter", count) // 向下传值
	}
}

// 中间件 - 黑名单
func BackList() gin.HandlerFunc {
	var backList = map[string]struct{}{"127.0.0.1": {}}
	return func(c *gin.Context) {
		if _, ok := backList[c.ClientIP()]; ok {
			c.String(http.StatusForbidden, "Current IP access is disabled\n")
			c.Abort()
		}
	}
}

// 中间件 - 授权验证
func AuthMiddleWare() gin.HandlerFunc {
	return func(c *gin.Context) {
		if cookie, err := c.Request.Cookie("session_id"); err == nil {
			value := cookie.Value
			fmt.Println(value)
			if value == "123" {
				c.Next()
				return
			}
		}
		c.JSON(http.StatusUnauthorized, gin.H{
			"error": "Unauthorized",
		})
		c.Abort()
		return
	}
}

func foo(){
	middleware := router.Group("", func(context *gin.Context) {
		name := context.Query("name")
		if(name=="admin"){
			context.Redirect(302,"/home")
		}
		context.String(403,"Unauthorized")
	})
	_=middleware
}