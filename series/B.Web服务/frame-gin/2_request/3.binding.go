package main

import (
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
)

/*****************************************************************************
  官方文档：https://pkg.go.dev/github.com/go-playground/validator/v10
  中文参考：https://www.cnblogs.com/zj420255586/p/13542395.html
  		https://www.cnblogs.com/jiujuan/p/13823864.html
  更多细节，可参考系统测试源码(github.com/gin-gonic/gin/binding/binding_test.go)
*****************************************************************************/

// Account 项目实例
//
//  测试：Form
//      curl -X POST 'http://localhost:8080/acc' \
//      --data-urlencode 'name=Jack' \
//      --data-urlencode 'password=123' \
//      --data-urlencode 'confirm=123' \
//      --data-urlencode 'email=x@qq.com' \
//      --data-urlencode 'phone=+8618201188666' \
//      --data-urlencode 'age=28' \
//      --data-urlencode 'height=182' \
//      --data-urlencode 'birth=1988-01-01'
//
//  测试：Json
//       curl -X POST 'http://localhost:8080/acc' \
//       -H 'Content-Type: application/json' \
//       --data-raw '{
//           "name": "Jack",
//           "password": "123",
//           "confirm": "123",
//           "email": "x@qq.com",
//           "age": 28,
//           "phone": "+8618201188666",
//           "height": 182,
//           "birth": "1988-01-01T00:00:00+08:00"
//       }'
type Account struct {
	Name     string    `json:"name" form:"name" binding:"required"`                        // 必填项
	Password string    `json:"password" form:"password" binding:"required,min=3,max=12"`   // 长度范围
	Confirm  string    `json:"confirm" form:"confirm" binding:"required,eqfield=Password"` // 比较相等(⚠️ eqfield指向Go结构体字段)
	Email    string    `json:"email" form:"email" binding:"required,email"`                // 电子邮件
	Age      int       `json:"age" form:"age" binding:"gte=1,lte=100"`                     // 数值范围
	Phone    string    `json:"phone" form:"phone" binding:"omitempty,e164"`                // 手机号码(⚠️ omitempty:忽略空值)
	Height   float32   `json:"height" form:"height" binding:"number,gte=50,lte=220"`       // 身高(实数)
	Birth    time.Time `json:"birth" form:"birth" time_format:"2006-01-02"`                // 时间格式(之对form有效?)
}


// binding是gin提供的字段绑定与验证标签
func main() {
	router := gin.New()
	router.POST("/binding", func(c *gin.Context) {
		var account Account
		if err := c.ShouldBind(&account); err != nil {
			c.String(http.StatusBadRequest, err.Error())
			return
		}
		c.JSON(200, account)
	})

	router.Run()
}
