package main

import (
	"fmt"
)

// https://blog.csdn.net/qq_35709559/article/details/109481269

// Users validator验证器 && 自定义验证方法
type Users struct {
	Phone    string `form:"phone" json:"phone" validate:"required"`
	Passwd   string `form:"passwd" json:"passwd" validate:"required,max=12,min=6"`
	Code     string `form:"code" json:"code" validate:"len=6"`
	Identity string `form:"identity" json:"identity" validate:"ident"` // 自定义验证标签
}

// IdentCheck 「自定义验证函数」验证18位身份证号
func IdentCheck(v validator.FieldLevel) bool {
	return v.Field().Len() == 18
}

// validator验证器
func main() {
	users := &Users{
		Phone:    "18200101811",
		Passwd:   "123456",
		Code:     "666666",
		Identity: "123456789123456789",
	}

	validate := validator.New()
	validate.RegisterValidation("ident", IdentCheck) // 注册自定义函数
	if err := validate.Struct(users); err != nil {
		for _, err = range err.(validator.ValidationErrors) {
			fmt.Println(err)
			return
		}
	}
	fmt.Println("OK")
}

// https://blog.csdn.net/guyan0319/article/details/105918559
