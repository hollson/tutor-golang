package main

import (
    "fmt"
    "net/http"

    "github.com/gin-gonic/gin"
)

var router *gin.Engine

func init() {
    router = gin.Default()
}

func main() {
    router = gin.Default()
    RouterParam()
    QueryParam()
    FormParam()
    BindParam()

    router.Run()
}

// 路由参数
// curl http://127.0.0.1:8080/user/hollson
// curl http://127.0.0.1:8080/user/hollson/
// curl http://127.0.0.1:8080/user/hollson/profile?part=3
func RouterParam() {
    // 冒号参数(严格模式)
    router.GET("/user/:name", func(c *gin.Context) {
        c.String(http.StatusOK, "R1: name=%s\n", c.Param("name"))
    })

    // 星号参数(宽泛模式)
    router.GET("/user/:name/*action", func(c *gin.Context) {
        name := c.Param("name")
        action := c.Param("action")
        c.String(http.StatusOK, "R2: name=%s;action=%s\n", name, action)
    })

}

// Query参数
func QueryParam() {
    // 单值：不传x参数时自动赋值
    // curl -X GET http://127.0.0.1:8080/query?name=tom
    router.GET("/query", func(c *gin.Context) {
        name := c.Query("name")              // c.Request.URL.Query().Get("name") 的快捷写法
        fmt.Println(c.GetQuery("name"))      // 带有has判断
        addr := c.DefaultQuery("addr", "北京") // 不传该字段
        c.JSON(200, gin.H{"name": name, "addr": addr})
    })

    // 数组
    // curl -X GET http://localhost:8080/arr?media=blog&media=wechat
    router.GET("/arr", func(c *gin.Context) {
        c.JSON(200, c.QueryArray("media"))
    })

    // 字典
    // curl -X GET http://localhost:8080/map?ids[a]=123&ids[b]=456
    router.GET("/map", func(c *gin.Context) {
        c.JSON(200, c.QueryMap("ids"))
    })

    // 简化 Set和get用于跨中间件传值
    // curl -X  GET http://localhost:8080/short?id=1001&vip=true&score[math]=99&score[history]=87&media=blog&media=wechat
    router.GET("/short", func(c *gin.Context) {
        c.Set("id", 100001)
        fmt.Println("=============")
        id := c.GetInt64("id") // ???????????????
        vip := c.GetBool("vip")
        score, has := c.GetQueryMap("score")
        media, exists := c.GetQueryArray("media")
        fmt.Println("=========", id, vip, has, exists)

        c.JSON(200, gin.H{"id": id, "vip": vip, "score": score, "media": media})
    })
}

// curl --location --request POST 'http://localhost:8080/form' \
// --header 'Content-Type: application/x-www-form-urlencoded' \
// --data-urlencode 'name=jack' \
// --data-urlencode 'tel=182' \
// --data-urlencode 'tag=student,boy' \
// --data-urlencode 'blog[github]=http://hollson.github.com' \
// --data-urlencode 'blog[twiter]=http://hollson.twiter.com'

// name:jack
// tel:182
// tag:student,boy
// blog[github]:http://hollson.github.com
// blog[twiter]:http://hollson.twiter.com
func FormParam() {
    router.POST("/form", func(c *gin.Context) {
        name := c.PostForm("name")
        addr := c.DefaultPostForm("addr", "北京")
        tel := c.PostForm("tel")
        fmt.Println(c.GetPostFormArray("tag"))
        fmt.Println(c.GetPostFormMap("blog"))
        c.JSON(http.StatusOK, gin.H{
            "name": name,
            "addr": addr,
            "tel":  tel,
        })
    })
}

// // 绑定类型
// type Login struct {
// 	User     string `form:"user" json:"user" xml:"user" uri:"user"  binding:"required"`
// 	Password string `form:"password" json:"password" xml:"password" uri:"password" binding:"required"`
// }

// 2.绑定对象，json
func BindParam() {
    type User struct {
        Name string `json:"name" form:"name"`
        Age  int    `json:"age" form:"age"`
    }
    u := &User{}

    // Must绑定
    // curl -v -X POST "http://127.0.0.1:8080/bind/must" -d 'name=Bob&age=20'  正常
    // curl -v -X POST "http://127.0.0.1:8080/bind/must" -d 'name=Bob&age=aa'  数据错误，中断并返回400
    // It writes a 400 error and sets Content-Type header "text/plain" in the response if input is not valid.
    router.POST("/bind/must", func(c *gin.Context) {
        c.Bind(u) // 调用：MustBindWith
        c.JSON(200, u)
    })

    // Should绑定
    // curl -v -X POST "http://127.0.0.1:8080/bind/should" -d 'name=Bob&age=20'
    // curl -v -X POST "http://127.0.0.1:8080/bind/should" -d 'name=Bob&age=aa'
    // Like c.Bind() but this method does not set the response status code to 400 and abort if the json is not valid.
    router.POST("/bind/should", func(c *gin.Context) {
        c.ShouldBind(u)
        c.JSON(200, u)
    })

    // 定向绑定，同 c.ShouldBindWith(obj, binding.JSON)
    // curl -H 'Content-Type:application/json' "http://localhost:8080/bind/json" -d '{"name":"Robert","age":22}'
    router.POST("/bind/json", func(c *gin.Context) {
        c.ShouldBindJSON(u)
        c.JSON(200, u)
    })

    // 绑定头部
    // curl -H 'pagenum:1' -H 'pagesize:10' http://127.0.0.1:8080/bind/head
    router.GET("/bind/head", func(c *gin.Context) {
        page := &struct {
            PageNum  int `json:"page_num"`
            PageSize int `json:"page_size"`
        }{}
        c.ShouldBindHeader(page)
        c.JSON(200, page)
    })

    // 绑定Query
    // curl -X GET http://127.0.0.1:8080/bind/query?name=Thompson&age=88
    router.GET("/bind/query", func(c *gin.Context) {
        c.ShouldBindQuery(u)
        c.JSON(200, u)
    })

    // 绑定Uri
    // curl -X GET http://127.0.0.1:8080/bind/uri/987fbc97-4bed-5078-9f07-9141ba07c9f3/Jackson
    router.GET("/bind/uri/:id/:name", func(c *gin.Context) {
        type Person struct {
            ID   string `uri:"id" binding:"required,uuid"`
            Name string `uri:"name" binding:"required"`
        }
        var person Person
        if err := c.ShouldBindUri(&person); err != nil {
            c.JSON(400, gin.H{"msg": err})
            return
        }
        c.JSON(200, person)
    })
}
