package main

import (
    "fmt"
	"io"
	"log"
	"net/http"
	"os"

	"github.com/gin-gonic/gin"

)

func main() {
	router := gin.Default()

	// 单文件上传(文件字段名不同，如可同时上传身份证正反面照)
	// curl -X POST http://127.0.0.1:8000/upload -H "Content-Type: multipart/form-data"
	// -F "upload=@~/pic.jpg"
	router.POST("/upload", func(c *gin.Context) {
		// 接收FormFile请求参数
		partFile, fileHeader, err := c.Request.FormFile("upload") // 字段：front 正面照
		// partFile, fileHeader, err := c.Request.FormFile("upload")  // 字段：reverse 背面照
		if err != nil {
			c.String(http.StatusBadRequest, "上传失败")
			return
		}

		// 创建Dump文件
		dump, err := os.Create(fileHeader.Filename)
		if err != nil {
			log.Fatal(err)
		}
		defer dump.Close()

		// 拷贝文件
		_, err = io.Copy(dump, partFile)
		if err != nil {
			log.Fatal(err)
		}
		c.String(http.StatusCreated, "上传成功")
	})

	// 多文件上传(文件名相同)
	// curl -X POST http://localhost:8000/multi/upload -H "Content-Type: multipart/form-data"
	// -F "upload=@~/a.jpg" -F "upload=@~/b.png"
	router.POST("/multi/upload", func(c *gin.Context) {
		// maxMemory字节的总和为它的文件部分存储在内存中，其余部分存储在磁盘中的临时文件。
		if err := c.Request.ParseMultipartForm(int64(memory.MB)); err != nil {
			c.String(http.StatusBadRequest, "ParseMultipartForm Err")
			return
		}

		// 读取多个同名Key文件值
		var size memory.Size = 0
		parts := c.Request.MultipartForm
		files := parts.File["upload"]
		for i, _ := range files {
			// 获取文件对象
			file, err := files[i].Open()
			if err != nil {
				log.Fatal(err)
			}

			// 创建dump文件
			dump, err := os.Create(files[i].Filename)
			if err != nil {
				log.Fatal(err)
			}

			// 拷贝文件
			_, err = io.Copy(dump, file)
			if err != nil {
				log.Fatal(err)
			}
			dump.Close()
			file.Close()
			size += memory.Size(files[i].Size)
		}
		c.String(http.StatusCreated, "上传成功,文件总大小：%s", size.String())
	})

	router.Run()
}

func FileDownload(c *gin.Context){
	c.Writer.Header().Add("Content-Disposition", fmt.Sprintf("attachment; filename=%s", filename))//fmt.Sprintf("attachment; filename=%s", filename)对下载的文件重命名
	c.Writer.Header().Add("Content-Type", "application/octet-stream")
	c.File("./file/a.txt")
}