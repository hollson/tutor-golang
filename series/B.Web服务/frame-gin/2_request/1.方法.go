package main

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

func main() {
	router := gin.Default()

	// 1.请求Head
	// curl --head http://127.0.0.1:8080/head
	router.HEAD("/head", func(c *gin.Context) {
		c.Header("Foo", "返回头部信息")
		c.String(200, "Head\n")
	})

	// 2.固定方法
	// curl -X GET http://127.0.0.1:8080/get
	router.GET("/get", func(c *gin.Context) { c.String(200, "Normal\n") })

	// 3.指定方法
	// curl -X POST http://127.0.0.1:8080/handle?token=123
	router.Handle(http.MethodPost, "handle", func(c *gin.Context) { c.PureJSON(200, c.Request.RequestURI) })

	// 4.动态方法
	// curl -X TRACE http://127.0.0.1:8080/any
	// curl -X DELETE http://127.0.0.1:8080/any
	// curl -X OPTIONS http://127.0.0.1:8080/any
	router.Any("/any", func(c *gin.Context) { c.String(200, c.Request.Method) })

	// 5.未找到路由
	// curl -X GET http://127.0.0.1:8080/404
	router.NoRoute(func(c *gin.Context) { c.String(404, "NotFound\n") })

	// 6.方法不匹配
	// curl -X POST http://127.0.0.1:8080/405  --header 'Content-Type: application/json'
	router.NoMethod(func(c *gin.Context) { c.String(405, "MethodNotAllowed\n") })

	// 静态资源
	router.StaticFile("favicon.ico", "./favicon.ico")
	// 静态资源目录
	router.Static("/css", "./asset/css")
	// 静态文件系统
	router.StaticFS("/image", http.Dir("./img"))

	router.Run() // 默认端口8080
}
