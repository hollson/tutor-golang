package main

import (
	"log"
	"time"

	"github.com/gin-gonic/gin"
)

func delayHandler(c *gin.Context) {
	<-time.After(5 * time.Second)
	log.Println("异步结果：" + c.Request.URL.Path)
}

// 注意 ⚠️ ：以Goroutine传递一个gin.Context时，务必要Copy上下文
func main() {
	router := gin.Default()
	router.GET("/async", func(c *gin.Context) {
		go delayHandler(c.Copy())
		c.String(200, "OK")
	})
	router.Run()
}

// curl -X GET http://localhost:8080/async