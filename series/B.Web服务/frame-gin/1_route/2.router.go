package main

import (
	"github.com/gin-gonic/gin"
)

// curl -X GET http://localhost:8080/index
// curl -X GET http://localhost:8080/v1/user
// curl -X POST http://localhost:8080/v1/user
// curl -X GET http://localhost:8080/v2/user
// curl -X POST http://localhost:8080/v2/user
func main() {
	router := gin.New()

	// 默认路由
	router.GET("/index", func(c *gin.Context) { c.String(200, "ok") })

	// 分组路由
	groupV1 := router.Group("/v1")
	groupV1.GET("/user", func(c *gin.Context) { c.String(200, "Get1") })
	groupV1.POST("/user", func(c *gin.Context) { c.String(200, "Post1") })

	groupV2 := router.Group("/v2")
	groupV2.GET("/user", func(c *gin.Context) { c.String(200, "Get2") })
	groupV2.POST("/user", func(c *gin.Context) { c.String(200, "Post2") })

	router.Run()
}
