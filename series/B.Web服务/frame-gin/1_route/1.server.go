package main

import (
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
	"golang.org/x/sync/errgroup"
)

// gin.Engine实现了http.Handler接口
var router = gin.Default()

// 服务托管一：gin.Run方式
//  gin.Run底层封装了http.ListenAndServe
func ginRunListen() {
	router.GET("/", func(c *gin.Context) { c.String(200, "ok") })
	router.Run()
}

// 服务托管二：http.ListenAndServe方式
//  底层构造了默认的http.Server
func httpListen() {
	router.GET("/", func(c *gin.Context) { c.String(200, "ok") })
	http.ListenAndServe(":8080", router)
}

// 服务托管三：自定义Server方式
//  Server完全自定义，灵活性最大
func defineListen() {
	router.GET("/", func(c *gin.Context) { c.String(200, "ok") })

	myServer := &http.Server{
		Addr:           ":8000",
		Handler:        router,
		ReadTimeout:    10 * time.Second,
		WriteTimeout:   10 * time.Second,
		MaxHeaderBytes: 1 << 20,
	}
	myServer.ListenAndServe()
}

// 多服务托管
//  curl http://localhost:8081
//  curl http://localhost:8082
//  curl http://localhost:8083
func multiServer() {
	server01 := gin.New()
	server01.GET("/", func(c *gin.Context) { c.String(200, "server 01") })

	server02 := gin.New()
	server02.GET("/", func(c *gin.Context) { c.String(200, "server 02") })

	server03 := gin.New()
	server03.GET("/", func(c *gin.Context) { c.String(200, "server 03") })

	var eg errgroup.Group
	eg.Go(func() error { return server01.Run(":8081") })
	eg.Go(func() error { return server02.Run(":8082") })
	eg.Go(func() error { return server03.Run(":8083") })
	if err := eg.Wait(); err != nil {
		panic(err)
	}
}

// gin使用了 https://github.com/julienschmidt/httprouter路由器，源码注释有说明
func main() {
	// ginRunListen()
	// httpListen()
	// defineListen()
	multiServer()
}
