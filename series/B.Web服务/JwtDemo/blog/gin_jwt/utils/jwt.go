package utils

import (
	"gin_jwt/model"
	"github.com/golang-jwt/jwt/v5"
	"github.com/google/uuid"
	"strings"
	"time"
)

var (
	Expires = time.Hour * 24 * 7                             //过期时间
	jwtKey  = []byte("L4D1S9E4T3W3T9Z7R4R2Y5Y9R0K9V6E2E9W7") //安全密钥
)

type Claims struct {
	*model.UserInfo
	*jwt.RegisteredClaims
}

func GenerateToken(u *model.UserInfo) (string, error) {
	claims := &Claims{
		UserInfo: u,
		RegisteredClaims: &jwt.RegisteredClaims{
			ID:        uuid.New().String(),                                  // JTI
			Issuer:    "SSO_ISS",                                            // 签发者
			Subject:   "SSO_SUB",                                            // 签发对象（用户ID或唯一标识）
			Audience:  jwt.ClaimStrings{"PC", "Android", "IOS"},             // 签发受众
			ExpiresAt: jwt.NewNumericDate(time.Now().Add(Expires)),          // 过期时间
			NotBefore: jwt.NewNumericDate(time.Now().Add(time.Second * 10)), // 生效时间
			IssuedAt:  jwt.NewNumericDate(time.Now()),                       // 签发时间
		},
	}

	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	tokenString, err := token.SignedString(jwtKey)
	if err != nil {
		return "", err
	}
	return tokenString, nil
}

func MustGenerateToken(u *model.UserInfo) string {
	t, _ := GenerateToken(u)
	return t
}

func ParseToken(token string) (*Claims, error) {
	token = strings.TrimPrefix(token, "Bearer ")
	claims := &Claims{}
	_, err := jwt.ParseWithClaims(token, claims, func(t *jwt.Token) (any, error) {
		return jwtKey, nil
	})
	return claims, err
}
