package main

import (
	"errors"
	"gin_jwt/middleware"
	"gin_jwt/model"
	"gin_jwt/utils"
	"github.com/gin-gonic/gin"
	"net/http"
	"time"
)

func setupRouter() *gin.Engine {
	router := gin.Default()

	// 匿名
	router.POST("login", func(c *gin.Context) {
		var user model.LoginModel
		if c.ShouldBindJSON(&user) != nil {
			c.AbortWithError(http.StatusBadRequest, errors.New("parameter error"))
			return
		}
		token := utils.MustGenerateToken(&model.UserInfo{
			Id:       1001,
			UserName: "admin",
			Roles:    []string{"Administrator", "Developer"},
			Email:    "abc@gmail.com"})

		// 模拟数据
		if user.UserName == "admin" && user.Password == "123456" {

			//可选： 添加Cookie
			cookie := &http.Cookie{
				Name:     "token",                       // Cookie名称
				Value:    token,                         // Cookie值
				Path:     "/",                           // 设置路径
				Expires:  time.Now().Add(utils.Expires), // 过期时间
				HttpOnly: true,                          // 设置Cookie只能通过HTTP访问，JavaScript无法访问
			}
			http.SetCookie(c.Writer, cookie)

			c.JSON(http.StatusOK, gin.H{
				"code":  1,
				"msg":   "ok",
				"token": token,
			})
			return
		}

		c.JSON(http.StatusOK, gin.H{
			"code": -1,
			"msg":  "account or password error",
		})
		return
	})

	// 鉴权
	authorized := router.Group("/", middleware.JWTAuth())

	authorized.GET("/info", func(c *gin.Context) {
		u := c.MustGet("UserInfo").(*model.UserInfo)
		c.JSON(200, u)
	})

	authorized.GET("/refresh", func(c *gin.Context) {
		u := c.MustGet("UserInfo").(*model.UserInfo)
		c.JSON(200, u)
	})

	// TODO: 退出(清理Cookie)
	authorized.GET("/logout", func(c *gin.Context) {
		cookie := &http.Cookie{
			Name:    "token",
			Expires: time.Now(),
		}
		http.SetCookie(c.Writer, cookie)

		c.JSON(200, gin.H{
			"code": 1,
			"msg":  "ok",
		})
	})

	return router
}

func main() {
	r := setupRouter()
	r.Run(":8080")
}
