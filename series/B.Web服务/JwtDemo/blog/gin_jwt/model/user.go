package model

type LoginModel struct {
	UserName string `json:"username"`
	Password string `json:"password"`
}

type UserInfo struct {
	Id       int      `json:"id"`
	UserName string   `json:"username"`
	Roles    []string `json:"roles"`
	Email    string   `json:"email"`
}
