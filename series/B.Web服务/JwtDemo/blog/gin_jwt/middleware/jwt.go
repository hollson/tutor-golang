package middleware

import (
	"errors"
	"gin_jwt/utils"
	"github.com/gin-gonic/gin"
	"github.com/golang-jwt/jwt/v5"
	"net/http"
	"time"
)

func JWTAuth() gin.HandlerFunc {
	return func(c *gin.Context) {
		auth := c.Request.Header.Get("Authorization")
		if len(auth) == 0 {
			c.AbortWithStatusJSON(http.StatusUnauthorized, gin.H{
				"code": -1,
				"msg":  "Unauthorized",
			})
			return
		}

		claims, err := utils.ParseToken(auth)
		if err != nil {
			if errors.Is(err, jwt.ErrTokenExpired) {
				// Token过期不超过10分钟则给它续签
				if time.Now().Sub(claims.ExpiresAt.Time) < time.Minute*10 {
					newToken, err := utils.GenerateToken(claims.UserInfo)
					if err == nil && newToken != "" {
						c.Header("NewToken", newToken)
						goto NEXT
					}
				}
			}

			// 鉴权失败
			c.Header("Location", "/login")
			c.AbortWithStatusJSON(http.StatusUnauthorized, gin.H{
				"code": -1,
				"msg":  err.Error(),
			})
			return
		}

	NEXT:
		c.Set("UserInfo", claims.UserInfo)
		c.Next()
	}
}
