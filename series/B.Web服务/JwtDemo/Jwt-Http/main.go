package main

import (
	"github.com/sohamkamani/jwt-go-example/handler"
	"log"
	"net/http"
)

func main() {
	http.HandleFunc("/signin", handler.Signin)   //登录
	http.HandleFunc("/welcome", handler.Welcome) //验证
	http.HandleFunc("/refresh", handler.Refresh) //刷新
	http.HandleFunc("/logout", handler.Logout)   //登出

	log.Fatal(http.ListenAndServe(":8000", nil))
}
