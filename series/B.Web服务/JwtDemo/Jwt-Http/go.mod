module github.com/sohamkamani/jwt-go-example

go 1.20

require (
	github.com/golang-jwt/jwt/v5 v5.0.0
	github.com/google/uuid v1.5.0
)
