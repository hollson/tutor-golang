package main

import (
	"fmt"
	"html/template"
	"net/http"
)

func login(w http.ResponseWriter, r *http.Request) {
	// GET
	if r.Method == "GET" {
		t, err := template.ParseFiles("view/login.html")
		if err != nil {
			fmt.Fprintf(w, "parse template error: %s", err.Error())
			return
		}
		t.Execute(w, nil)
		return
	}

	// POST
	r.ParseForm()
	username := r.Form["username"]
	password := r.Form["password"]
	fmt.Fprintf(w, "username = %s, password = %s", username, password)

}

func main() {
	http.HandleFunc("/login", login)
	http.ListenAndServe(":8080", nil)
}
