import (
	"encoding/json"
	"fmt"
	"net/http"

	_ "github.com/go-sql-driver/mysql"
	"github.com/jmoiron/sqlx"
)

var db *sqlx.DB

type Student struct {
	ID   int64
	Name string
	Sex  string
	Age  int64
}

// 连接数据库
func init() {
	dns := "user:pwd@tcp(localhost:3306)/db_name?charset=utf8&parseTime=True&loc=Local"
	db, err = sqlx.Open("mysql", dns)
	if err != nil {
		fmt.Println(err)
		return
	}
	db.SetMaxOpenConns(2000)
	db.SetMaxIdleConns(1000)
}

// 获取所有学生信息(数据自己事先插入)
func GetAllStudent() []Student {
	sqlStr := "SELECT * FROM student"
	students := make([]Student, 0)
	rows, _ := db.Query(sqlStr)
	student := Student{}
	for rows.Next() {
		rows.Scan(&student.ID, &student.Name, &student.Sex, &student.Age)
		students = append(students, student)
	}
	defer rows.Close()
	return students
}

func GetAllStudentInfo(w http.ResponseWriter, r *http.Request) {
	students := GetAllStudent()
	resp := make(map[string]interface{})
	resp["msg"] = "成功"
	resp["code"] = "200"
	resp["data"] = students
	jsonResp, err := json.Marshal(resp)
	if err != nil {
		fmt.Println(err)
		return
	}
	w.Write(jsonResp)
}

func main() {
	http.HandleFunc("/api/students", GetAllStudentInfo)
	http.ListenAndServe(":8080", nil)
}

//https://cloud.tencent.com/developer/article/2266216
