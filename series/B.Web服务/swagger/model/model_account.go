// Copyright 2021 Hollson. All rights reserved.
// Use of this source code is governed by a MIT style
// license that can be found in the LICENSE file.

package model

import (
	"time"

	"github.com/shopspring/decimal"
)

// AccountRequest 账号请求参数
type AccountRequest struct {
	Phone  string `form:"phone" binding:"required,phoneValid"`     // 手机号码
	Status string `form:"status" binding:"required,oneof='0' '1'"` // 用户状态
}

// AccountDetail 账号响应参数
type AccountDetail struct {
	Id      int64           `json:"id"`       // 编号
	Name    string          `json:"name"`     // 姓名
	Email   string          `json:"email"`    // 邮件
	Coin    decimal.Decimal `json:"coin"`     // 游戏币
	RegTime time.Time       `json:"reg_time"` // 注册时间
}
