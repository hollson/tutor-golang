// Copyright 2021 Hollson. All rights reserved.
// Use of this source code is governed by a MIT style
// license that can be found in the LICENSE file.

package model

import (
	"web/swagger/api"
)

type ProductRequest struct {
	api.RequestPaged
	Name string `json:"name" form:"name"` // 产品名称
}

type ProductDetail struct {
	Id     int64   `json:"id"`     // 产品编号
	Name   string  `json:"name"`   // 产品名称
	Price  float64 `json:"price"`  // 产品价格
	Origin string  `json:"origin"` // 产品产地
}
