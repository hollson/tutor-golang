// Copyright 2021 Hollson. All rights reserved.
// Use of this source code is governed by a MIT style
// license that can be found in the LICENSE file.

package controller

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

// IndexHandler 请求处理
func IndexHandler(c *gin.Context) {
	context := `
查看swagger文档： http://localhost:8080/swagger/index.html
`
	c.String(http.StatusOK, context)
}

