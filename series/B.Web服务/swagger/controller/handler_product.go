// Copyright 2021 Hollson. All rights reserved.
// Use of this source code is governed by a MIT style
// license that can be found in the LICENSE file.

package controller

import (
	"github.com/gin-gonic/gin"
)

// ProductListHandler
// @Summary 获取产品列表接口
// @Description 可按产地按时间或分数排序查询产品列表接口
// @Tags 二. 产品相关接口
// @Accept application/json
// @Produce application/json
// @Param Authorization header string false "Bearer 用户令牌"
// @Param object query model.ProductRequest false "查询参数"
// @Security ApiKeyAuth
// @Success 200 {object} []model.ProductDetail
// @Router /product/list [post]
func ProductListHandler(c *gin.Context) {
	// var (
	// 	ping model.ProductRequest
	// 	pong []model.ProductDetail
	// )
	// if err := c.Bind(&ping); err != nil {
	// 	c.JSON(http.StatusOK, api.ResponseError{
	// 		Code: 0,
	// 		Msg:  err.Error(),
	// 	})
	// 	return
	// }
	// pong = []model.ProductDetail{{
	// 	Id:     100012,
	// 	Name:   "苹果🍎",
	// 	Price:  6600.00,
	// 	Origin: "美国",
	// },
	// 	{
	// 		Id:     100013,
	// 		Name:   "大疆",
	// 		Price:  8888.20,
	// 		Origin: "中国",
	// 	},
	// }
	//
	// c.JSON(http.StatusOK, api.Response{
	// 	Code: 1,
	// 	Msg:  "OK",
	// 	Data: pong,
	// })
}
