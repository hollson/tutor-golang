// Copyright 2021 Hollson. All rights reserved.
// Use of this source code is governed by a MIT style
// license that can be found in the LICENSE file.

package controller

import (
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/shopspring/decimal"
	"web/swagger/api"
	"web/swagger/model"
)

// AccountGetHandler
// @Summary 「账号操作」 - 🚄 get请求
// @Description 调用 「AccountGetHandler」 控制器
// Schemes http, https, ws, wss
// @Tags 一. 账号相关接口
// @Accept  json
// @Produce json
// @param Authorization header string true "验证参数Bearer和token空格拼接"
// @Param accId query int true "用户ID,URL中的必要参数"
// @Param status query bool false "用户状态,URL中的非必要参数"
// @Success 200 {object} api.Response{data=model.AccountDetail}
// @Failure 500 {object} api.ResponseError
// @Router /account/get [get]
func AccountGetHandler(c *gin.Context) {
	var (
		pong model.AccountDetail
	)

	pong = model.AccountDetail{
		Id:      1001,
		Name:    "张三",
		Email:   "zhangsan@qq.com",
		Coin:    decimal.New(18, 2),
		RegTime: time.Now(),
	}

	c.JSON(http.StatusOK, api.Response{
		Code: 1,
		Msg:  "OK",
		Data: pong,
	})
}

// AccountPostHandler
// @Summary 「账号操作」 - ✈️ post请求
// @Description 调用 「AccountPostHandler」 控制器
// @Tags 一. 账号相关接口
// @Accept  json
// @Produce json
// @param Authorization header string true "验证参数Bearer和token空格拼接"
// @Param Phone body model.AccountRequest true "手机号码,这是body中的必要参数"
// @Success 200 {object} api.Response{data=model.AccountDetail}
// @Failure 500 {object} api.ResponseError
// @Router /account/post [post]
func AccountPostHandler(c *gin.Context) {
	var (
		ping model.AccountRequest
		pong model.AccountDetail
	)
	if err := c.Bind(&ping); err != nil {
		c.JSON(http.StatusOK, api.ResponseError{
			Code: 0,
			Msg:  err.Error(),
		})
		return
	}

	pong = model.AccountDetail{
		Id:      1001,
		Name:    "张三",
		Email:   "zhangsan@qq.com",
		Coin:    decimal.New(18, 2),
		RegTime: time.Now(),
	}

	c.JSON(http.StatusOK, api.Response{
		Code: 1,
		Msg:  "OK",
		Data: pong,
	})
}

// AccountHeadHandler
// @Summary 「账号操作」 - 🍉 head请求
// @Description 调用 「AccountHeadHandler」 控制器
// @Tags 一. 账号相关接口
// @Accept  json
// @Produce json
// @param Authorization header string true "验证参数Bearer和token空格拼接"
// @Success 200 {object} api.Response{data=model.AccountDetail}
// @Failure 500 {object} api.ResponseError
// @Router /account/head [head]
func AccountHeadHandler(c *gin.Context) {}

// AccountPutHandler
// @Summary 「账号操作」 - 👋 put请求
// @Description 调用 「AccountPutHandler」 控制器
// @Tags 一. 账号相关接口
// @Accept  json
// @Produce json
// @param Authorization header string true "验证参数Bearer和token空格拼接"
// @Success 200 {object} api.Response{data=model.AccountDetail}
// @Failure 500 {object} api.ResponseError
// @Router /account/put [put]
func AccountPutHandler(c *gin.Context) {}

// AccountDelHandler
// @Summary 「账号操作」 - ❌ delete请求
// @Description 调用 「AccountDelHandler」 控制器
// @Tags 一. 账号相关接口
// @Accept  json
// @Produce json
// @param Authorization header string true "验证参数Bearer和token空格拼接"
// @Success 200 {object} api.Response{data=model.AccountDetail}
// @Failure 500 {object} api.ResponseError
// @Router /account/del [delete]
func AccountDelHandler(c *gin.Context) {}

// AccountUploadHandler
// @Summary 「账号操作」 - 👮 头像上传
// @Description 调用 「AccountUploadHandler」 控制器
// @Tags 一. 账号相关接口
// @ID file.upload
// @Accept  multipart/form-data
// @Produce  json
// @Param   file formData file true  "this is a test file"
// @Success 200 {string} string "ok"
// @Failure 400 {object} api.ResponseError "We need ID!!"
// @Failure 404 {object} api.ResponseError "Can not find ID"
// @Router /account/file [post]
func AccountUploadHandler(c *gin.Context) {
	// write your code
}

// AccountDropHandler
// @Summary 「账号操作」 - 🌍 接口废弃
// @Description 调用 「AccountDropHandler」 控制器
// @Tags 一. 账号相关接口
// @Accept  json
// @Produce json
// @Deprecated true
// @param Authorization header string true "验证参数Bearer和token空格拼接"
// @Success 200 {object} api.Response{data=model.AccountDetail}
// @Failure 500 {object} api.ResponseError
// @Router /account/drop [get]
func AccountDropHandler(c *gin.Context) {}
