/*********************************************************************************
一. 安装swag：
	go get -u github.com/swaggo/swag/cmd/swag

二. 使用过程：
	1. 执行"swag init -g ./main.go"提取生成docs文档
	2. 引入依赖包
		"github.com/swaggo/gin-swagger"
		"github.com/swaggo/gin-swagger/swaggerFiles"
	3. 引入生成的docs包
		_ "${AppRoot}/docs"

三. 体验VS插件
	https://baijiahao.baidu.com/s?id=1627071794164885954&wfr=spider&for=pc

四. 参考链接：
	官网文档：https://goswagger.io/install.html
	官网示例：https://petstore.swagger.io/


TODO:
1. 添加Action授权验证
2. 添加文档查看登录
*********************************************************************************/

package main

import (
	"fmt"

	"web/swagger/controller"
	_ "web/swagger/docs"

	"github.com/gin-gonic/gin"
	ginSwagger "github.com/swaggo/gin-swagger"
	"github.com/swaggo/gin-swagger/swaggerFiles"
)

// @title Swagger使用示例
// @version v2.0.0
// @description [`Swagger`](https://goswagger.io/install.html)是一个规范和完整的框架，用于**生成、描述、调用**和可视化RESTful风格的Web服务。<br/> Explore中可以输入`https://petstore.swagger.io/v2/swagger.json`可浏览[官方示例](https://petstore.swagger.io/)。<br/>
// @termsOfService [http://swagger.io/terms/](http://swagger.io/terms/)
// @contact.name Hollson
// @contact.url [联系地址](http://www.swagger.io/support)
// @contact.email support@swagger.io
// @license.name ️保留版权@Swagger-Apache 2.0
// @license.url [http://www.apache.org/licenses/LICENSE-2.0.html](http://www.apache.org/licenses/LICENSE-2.0.html)
// @host http://localhost:8080
// @BasePath /api/v2
// @securityDefinitions.apikey Bearer
// @in header
// @name Authorization
func main() {
	router := gin.Default()
	router.GET("/", controller.IndexHandler)
	router.GET("/api/v2/account/get", controller.AccountGetHandler)     // get
	router.GET("/api/v2/account/post", controller.AccountPostHandler)   // post
	router.GET("/api/v2/account/head", controller.AccountHeadHandler)   // head
	router.GET("/api/v2/account/put", controller.AccountPutHandler)     // put
	router.GET("/api/v2/account/del", controller.AccountDelHandler)     // delete
	router.GET("/api/v1/account/file", controller.AccountUploadHandler) // upload
	router.GET("/api/v1/account/drop", controller.AccountDropHandler)   // deprecated
	router.GET("/api/v2/product/list", controller.ProductListHandler)

	// 添加或禁用swagger路由
	router.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler))
	// router.GET("/swagger/*any", ginSwagger.DisablingWrapHandler(swaggerFiles.Handler, "NAME_OF_ENV_VARIABLE"))

	fmt.Println(" 🥮 访问主页：http://localhost:8080")
	fmt.Println(" 🥮 查看文档：http://localhost:8080/swagger/index.html")
	router.Run(":8080")
}

// 开放示例：http://httpbin.org/#/

// https://www.coder.work/article/1022706
// https://juejin.cn/post/7126802030944878600
//https://zhuanlan.zhihu.com/p/176986546
