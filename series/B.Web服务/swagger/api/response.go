// Copyright 2021 Hollson. All rights reserved.
// Use of this source code is governed by a MIT style
// license that can be found in the LICENSE file.

package api

type RequestPaged struct {
	Page  int64  `json:"page" form:"page" example:"1"`       // 页码
	Size  int64  `json:"size" form:"size" example:"10"`      // 每页数据量
	Order string `json:"order" form:"order" example:"score"` // 排序依据
}
type Response struct {
	// 状态码
	// 说明: 这里可以写上多行状态码，例如：
	//   400	错误的请求
	//   401	未经授权
	//   403	无访问权限
	//   404	无法找到请求资源
	//   500	服务器内部错误
	//   502	网关错误
	Code int         `json:"code"`
	Msg  string      `json:"msg"`  // 响应消息
	Data interface{} `json:"data"` // 响应数据
}

type ResponseError struct {
	Code int    `json:"code"` // 错误码
	Msg  string `json:"msg"`  // 错误消息
}
