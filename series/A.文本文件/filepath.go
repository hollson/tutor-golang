package main

import (
	"fmt"
	"os"
	"path/filepath"
	"strings"
)

func main() {
	fileWalk()
}

func pathInfo() {
	pathname := "~/go/src"
	fmt.Println(filepath.Dir(pathname))       // 相对路径或绝对路径
	fmt.Println(filepath.Base(pathname))      // 目录名称
	fmt.Println(filepath.Abs(pathname))       // 绝对路径
	fmt.Println(filepath.Split(pathname))     // 切分路径和目录
	fmt.Println(filepath.Clean("..//./src/")) // 清理无效的符号
	fmt.Println(filepath.Glob("../s*"))       // 通配符查找(文件或目录)
}

//  遍历文件树
func fileWalk() {
	suffix := ".go"
	path := filepath.Join(os.Getenv("GOROOT"), "/src/log")

	filepath.Walk(path, func(f string, fi os.FileInfo, err error) error {
		// 非常规文件(即目录)时，跳过当前遍历
		if fi == nil || !fi.Mode().IsRegular() {
			// fmt.Println(fi.Name(), fi.Mode().Type())
			return err
		}
		if suffix == "" || strings.HasSuffix(fi.Name(), suffix) {
			fmt.Println(fi.Name())
		}
		return nil
	})
}
