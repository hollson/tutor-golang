package main

import (
	"fmt"
	"regexp"
)

func main() {
	txt := "abc azc a7c aac 888 a9c  tac"
	reg := regexp.MustCompile(`a.c`)

	result1 := reg.FindAllStringSubmatch(txt, 4)
	fmt.Println("result1 = ", result1)
}
