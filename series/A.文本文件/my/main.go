package main

import (
	"log"
	"os"
)

func main() {
	f, err := os.Open("stream")
	if err != nil {
		panic(err)
	}
	defer f.Close()

	// copy twice, but call once sys read
	packet := NewPacket(f, nil)
	if err := packet.Read(); err != nil {
		panic(err)
	}
	// log.Println("length:", packet.Length, "message:", string(packet.Msg))

	if err := packet.Read(); err != nil {
		panic(err)
	}
	// log.Println("length:", packet.Length, "message:", string(packet.Msg))

	// only copy from f.Read once, but call sys read twice
	f, err = os.Open("stream")
	if err != nil {
		panic(err)
	}
	defer f.Close()

	rd := NewStreamReader(f)

	content, err := rd.Read()
	if err != nil {
		panic(err)
	}
	// log.Println("length:", len(content), "message:", string(content))

	content, err = rd.Read()
	if err != nil {
		panic(err)
	}
	log.Println("length:", len(content), "message:", string(content))
}
