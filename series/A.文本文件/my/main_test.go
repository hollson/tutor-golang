package main

import (
    "os"
	"syscall"
	"testing"
)

func BenchmarkRead_Bufio(b *testing.B) {
	b.StopTimer()

	f, err := os.Open("stream")
	if err != nil {
		panic(err)
	}
	defer f.Close()

	packet := NewPacket(f, nil)

	b.StartTimer()

	for i := 0; i < b.N; i++ {
		if err := packet.Read(); err != nil {
			panic(err)
		}
		if err := packet.Read(); err != nil {
			panic(err)
		}
		syscall.Seek(int(f.Fd()), 0, 0)
	}
}

func BenchmarkRead_ReadAtLeast(b *testing.B) {
	b.StopTimer()

	f, err := os.Open("stream")
	if err != nil {
		panic(err)
	}
	defer f.Close()

	rd := NewStreamReader(f)

	b.StartTimer()

	for i := 0; i < b.N; i++ {
		if _, err := rd.Read(); err != nil {
			panic(err)
		}
		if _, err := rd.Read(); err != nil {
			panic(err)
		}
		syscall.Seek(int(f.Fd()), 0, 0)
	}
}
