// Copyright 2021 Hollson. All rights reserved.
// Use of this source code is governed by a MIT style
// license that can be found in the LICENSE file.

package main

import (
	"fmt"
	"io"
	"io/ioutil"
	"os"
	"path/filepath"
	"strings"
	"time"
)

// https://blog.csdn.net/chenbaoke/article/details/40317941?utm_campaign=studygolang.com&utm_medium=studygolang.com&utm_source=studygolang.com
func main() {
	Discard()
}
func ReadAll() {
	s := strings.NewReader("Hello World!")
	ra, _ := ioutil.ReadAll(s)
	fmt.Printf("%s", ra)
	// Hello World!
}

func ReadFile() {
	ra, _ := ioutil.ReadFile("C:\\Windows\\win.ini")
	fmt.Printf("%s", ra)
}

func WriteFile() {
	fn := "C:\\Test.txt"
	s := []byte("Hello World!")
	ioutil.WriteFile(fn, s, os.ModeAppend)
	rf, _ := ioutil.ReadFile(fn)
	fmt.Printf("%s", rf)
	// Hello World!
}

func ReadDir() {
	rd, err := ioutil.ReadDir("C:\\Windows")
	for _, fi := range rd {
		fmt.Println("")
		fmt.Println(fi.Name())
		fmt.Println(fi.IsDir())
		fmt.Println(fi.Size())
		fmt.Println(fi.ModTime())
		fmt.Println(fi.Mode())
	}
	fmt.Println("")
	fmt.Println(err)
}

// Discard (丢弃)实现了io.ReaderFrom接口
func Discard() {
	a := strings.NewReader("hi")
	io.Copy(ioutil.Discard, a)

	p := make([]byte, 5)
	ioutil.Discard.Write(p)
	fmt.Println(p)
}

func TempFile() {
	f, _ := ioutil.TempFile("./", "*.log") // 创建临时文件000000.log
	fmt.Printf("%s\n", f.Name())

	defer func() {
		<-time.After(time.Second * 5)
		os.Remove(f.Name()) // 删除临时文件
	}()

	f.WriteString("hello world")
	f.Close()
}

func TempDir() {
	ioutil.TempDir("./", "Tmp")   // 创建目录Tmp000000
	ioutil.TempDir("./", "Tmp_*") // 创建目录Tmp_000000

	matches, _ := filepath.Glob("Tmp*") // 通配符匹配
	fmt.Println(matches)

	defer func() {
		time.Sleep(time.Second * 5)
		for _, d := range matches {
			os.RemoveAll(d) // 删除临时目录
		}
	}()
}

// io.NopCloser实现了ReadCloser,即无需Close的IO
//  见源码：https://cs.opensource.google/go/go/+/refs/tags/go1.17.6:src/io/io.go;l=620
func nopCloser() {
	s := strings.NewReader("hello world")
	r := io.NopCloser(s)
	p := make([]byte, 5)
	r.Read(p)              // 读取并放入p
	fmt.Println(string(p)) // hello
}
