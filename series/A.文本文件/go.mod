module filetext

go 1.17

require (
	github.com/gocarina/gocsv v0.0.0-20211203214250-4735fba0c1d9
	github.com/golang/snappy v0.0.4
	github.com/json-iterator/go v1.1.12
)

require (
	github.com/modern-go/concurrent v0.0.0-20180228061459-e0a39a4cb421 // indirect
	github.com/modern-go/reflect2 v1.0.2 // indirect
)
