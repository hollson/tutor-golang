package main

import (
	"bufio"
	"fmt"
	"strings"
)

func readLine() {
	s := strings.NewReader("123456789abcdefg123456789ABCDEFGhello \r\nworld\nonly")
	buf := bufio.NewReaderSize(s, 0)
	for {
		// 超出Buf，则返回prefix,即defaultBufSize；
		// 未超Buf，则返回以换行符分割的内容；
		// 如果内容读取完毕，则返回EOF错误。
		line, isPrefix, err := buf.ReadLine()
		if err != nil {
			fmt.Println(err)
			break
		}
		fmt.Printf("%q %v\n", line, isPrefix)
	}

	// buf = strings.NewReader(s)
	scan := bufio.NewScanner(s)
	scan.Scan()
}

func scan(split bufio.SplitFunc) {
	s := strings.NewReader("I live\n中国")

	scanner := bufio.NewScanner(s)
	scanner.Split(split)
	for i := 0; scanner.Scan(); i++ {
		data := scanner.Bytes()
		// 或 data := scanner.Text()
		if len(data) == 0 {
			break
		}
		fmt.Printf("[%s]", data)
	}
	fmt.Println()
	fmt.Println()
}

// 😊😊😊 最好的参考资料就是官方文档
// https://pkg.go.dev/bufio#pkg-overview
func main() {
	scan(bufio.ScanBytes)
	scan(bufio.ScanRunes)
	scan(bufio.ScanWords)
	scan(bufio.ScanLines)
}
