package main

import (
	"flag"
	"fmt"
	"os"

	generate2 "golang/代码模板/generate/src/generate"
)

var (
	file = flag.String("file", "", "file to parse")
)

func main() {
	flag.Parse()
	if *file == "" {
		*file = os.Getenv("GOFILE")
		if *file == "" {
			fmt.Println("No file to parse")
			os.Exit(1)
		}
	}

	if err := Generate(*file); err != nil {
		fmt.Println(" \x1b[91mFailed\x1b[0m, error:", err)
		os.Exit(1)
	}

	fmt.Println(" \x1b[92mSuccess\x1b[0m")
}

func Generate(file string) error {
	fmt.Print(">>> Input: ", file)

	fset, err := generate2.Parse(file)
	if err != nil {
		return err
	}

	/* Using Custom Template */
	var tpl generate2.FileTemplate

	if err := generate2.PrintFile(fset, &tpl); err != nil {
		return err
	}

	return nil
}
