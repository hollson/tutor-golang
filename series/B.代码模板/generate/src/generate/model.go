package generate

const strTpl = `package {{.Package}}
// NOTE: THIS FILE WAS PRODUCED BY
// CODE GENERATION TOOL
// DO NOT EDIT
{{$length:= len .Imports}}{{if ne $length 0}}
import (
	{{range .Imports}} "{{.}}" {{end}}	
)
{{end}}

{{range $key, $val := .Structs}}
func (self *{{$key}}) Info() string {
	var info string {{range $name, $elem := $val.Elems}}
	info += "name:{{$name}}, tag:{{$elem.Tag}}, type:{{$elem.Type}}"{{end}}
	return info
}
{{end}}
`

type FileModel struct {
	Package string
	Imports []string
	Structs map[string]StructModel
}

type StructModel struct {
	Name  string
	Elems map[string]ElemModel
}

type ElemModel struct {
	Name string
	Tag  string
	Type string
}

type FileTemplate struct {
	Name  string
	Model *FileModel
}

func (self *FileTemplate) GetName() string {
	return self.Name
}

func (self *FileTemplate) GetModel() interface{} {
	return self.Model
}

func (self *FileTemplate) Generate(fset *FileSet) {
	self.Name = strTpl
	self.Model = &FileModel{
		Package: fset.Package,
		Imports: make([]string, 0),
		Structs: make(map[string]StructModel, 0),
	}

	for _, i := range fset.Imports {
		self.Model.Imports = append(self.Model.Imports, i.Path.Value)
	}

	for name, struc := range fset.Structs {
		model := StructModel{
			Name:  name,
			Elems: make(map[string]ElemModel, 0),
		}
		for name, elem := range struc.Elems {
			model.Elems[name] = ElemModel{
				Name: elem.Name,
				Tag:  elem.Tag,
				Type: elem.Type,
			}
		}
		self.Model.Structs[name] = model
	}
}
