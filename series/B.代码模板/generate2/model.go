package main

type Source struct {
	Filename string
	Filedir  string
	Package  string
	Suffix   string
	Template string
	Imports  []string
	Structs  []*Struct
}

type Struct struct {
	Name   string
	Field  map[string]*StructField
	Fields []*StructField
}

type StructField struct {
	Name   string
	Type   string
	TagRaw string
	Tags   []*StructTag
}

type StructTag struct {
	Key     string
	Name    string
	Options []string
}
