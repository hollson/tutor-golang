package main

// NOTE: THIS FILE WAS PRODUCED BY
// CODE GENERATION TOOL
// DO NOT EDIT

func init() {
    RegisterHandle(CC_Calculate1, Calculate1Invoke, func() interface{} { return new(Calculate1Request) })
    RegisterHandle(CC_Calculate2, Calculate2Invoke, func() interface{} { return new(Calculate2Request) })
}

func Calculate1Invoke(r interface{}) (int, interface{}) {
    return Calculate1Handle(r.(*Calculate1Request))
}

func Calculate2Invoke(r interface{}) (int, interface{}) {
    return Calculate2Handle(r.(*Calculate2Request))
}
