package main

import "fmt"

//go:generate generate2 -suffix="_gen.go"

type Calculate1 struct{}

func Calculate1Handle(req *Calculate1Request) (int, *Calculate1Response) {
	fmt.Println("Request:", req)
	rsp := &Calculate1Response{}
	switch req.Expr {
	case "+":
		rsp.Value = req.Value1 + req.Value2
	case "-":
		rsp.Value = req.Value1 - req.Value2
	default:
		return -1, nil
	}
	return 0, rsp
}

type Calculate2 struct{}

func Calculate2Handle(req *Calculate2Request) (int, *Calculate2Response) {
	fmt.Println("Request:", req)
	rsp := &Calculate2Response{}
	switch req.Expr {
	case "+":
		rsp.Value = req.Value1 + req.Value2
	case "-":
		rsp.Value = req.Value1 - req.Value2
	default:
		return -1, nil
	}
	return 0, rsp
}
