package main

import "fmt"

func init() {
	RegisterHandle(CC_Register, Register, func() interface{} { return new(RegisterRequest) })
}

func Register(r interface{}) (int, interface{}) {
	req := r.(*RegisterRequest)
	fmt.Println("Request:", req)
	rsp := &RegisterResponse{
		Name: req.Name,
		Age:  req.Age,
	}
	return 0, rsp
}
