package main

import (
	"fmt"
	"reflect"
	"strings"
)

// INT 整型约束
type INT interface {
	~int | ~int8 | ~int16 | ~int32 | ~int64 | ~byte
}

// UINT 整型约束
type UINT interface {
	~uint | ~uint8 | ~uint16 | ~uint32 | ~uint64 | ~rune | ~uintptr
}

// FLOAT 浮点型约束
type FLOAT interface {
	~float32 | ~float64
}

// STRING 字符串约束
type STRING interface {
	~string
}

// // CHAR 字符串约束
// type CHAR interface {
// 	~uint8｜ru string
// }

// ALL 字符串约束
type ALL interface {
	INT | UINT | FLOAT | string
}

// Sum 泛型求和
func Sum[T INT | UINT | FLOAT](arr ...T) T {
	var result T = 0
	for _, item := range arr {
		result += item
	}
	return result
}

// SQLIn 泛型拼接SQL数据
func SQLIn[T INT | UINT | FLOAT | STRING](arr ...T) string {
	if len(arr) == 0 {
		return "\"\""
	}
	var sb strings.Builder
	v := reflect.TypeOf(arr[0])
	if v.Kind() == reflect.String {
		for i, val := range arr {
			sb.WriteString(fmt.Sprintf(`"%v"`, val))
			if i < len(arr)-1 {
				sb.WriteString(",")
			}
		}
	} else {
		for i, val := range arr {
			sb.WriteString(fmt.Sprintf(`%v`, val))
			if i < len(arr)-1 {
				sb.WriteString(",")
			}
		}
	}
	return sb.String()
}

// Dump 泛型打印数据
func Dump[T ALL](v T) {
	fmt.Printf("类型:%s \t 值:%v\n", reflect.TypeOf(v), v)
}

// Coord 坐标信息
type Coord[T float32 | float64] [2]T

// type ReadWriter interface {
// 	~string | ~[]rune
// 	Read(p []byte) (n int, err error)
// 	Write(p []byte) (n int, err error)
// }

func main() {
	fmt.Println(SQLIn[int]([]int{-1, -2, -3}...))
	fmt.Println(SQLIn[uint](1, 2, 3))
	fmt.Println(SQLIn([]float32{1.2, 2.3, 3.4}...))
	fmt.Println(SQLIn("a", "b", "c")) // 推断类型
	fmt.Println(SQLIn(-1, -2, -3))
	fmt.Println(SQLIn[string]()) // 空数组

	fmt.Println("==============================")
	fmt.Println(Sum[int](1, 2, 3, 4, 5))
	fmt.Println(Sum[uint](1, 2, 3, 4, 5))
	fmt.Println(Sum(1, 2, 3, 4, 5))
	fmt.Println(Sum[float32](1, 2, 3, 4, 5))
	fmt.Println(Sum(1.0, 2.0, 3.0, 4.0, 5.0))

	fmt.Println("==============================")
	var location = make(map[string]Coord[float32])
	location["BeiJing"] = Coord[float32]{116.4074, 9.9042}
	location["ShangHai"] = Coord[float32]{121.4737, 31.2304}
	fmt.Println(location)

	fmt.Println("==============================")
	Dump(uint8(1))
	Dump(-2)
	Dump(3.0)
	Dump(-4.0)
	Dump(int32(5))
	Dump(byte('a'))
	Dump(rune('中'))
	Dump("Hello")
}
