package main

import (
	"fmt"
	"reflect"
)

func main() {
	pointerAndElement()
	// structField()
	// structValues()
	// modifyValue()
}

type Person struct {
	ID   int
	Name string `protobuf:"string,1,opt,name=Msg" json:"Msg,omitempty"`
}

// 1. 指针与指针元素
func pointerAndElement() {
	type Person struct{}
	person := &Person{}

	// 获取类型信息
	p := reflect.TypeOf(person)
	fmt.Println(p.Name()) // 类型名(字符串)，""
	fmt.Println(p.Kind()) // 类型分类(枚举)，ptr

	// 解引用(创建Type副本并返回)，等同于*操作
	pp := p.Elem()
	fmt.Println(pp.Name()) // Person
	fmt.Println(pp.Kind()) // struct
}

// type StructField struct {
// 	Name      string    // 字段名
// 	PkgPath   string    // 字段路径
// 	Type      Type      // 字段反射类型对象
// 	Tag       StructTag // 字段的结构体标签
// 	Offset    uintptr   // 字段在结构体中的相对偏移
// 	Index     []int     // Type.FieldByIndex中的返回的索引值
// 	Anonymous bool      // 是否为匿名字段
// }

// 2. 结构图字段
func structField() {
	person := Person{Name: "mimi", ID: 1}
	objType := reflect.TypeOf(person)

	// 获取字段名称和标签等
	for i := 0; i < objType.NumField(); i++ {
		f := objType.Field(i)
		fmt.Printf("name:%v tag:'%v'\n", f.Name, f.Tag)
	}

	// 进一步解析标签信息
	if f, ok := objType.FieldByName("Name"); ok {
		fmt.Println(f.Tag.Get("protobuf"), f.Tag.Get("json"))
	}
}

// 3. 结构体值
func structValues() {
	type T struct {
		Fa int
		Fb string
	}
	t := T{123, "hello"}

	// 解引用
	val := reflect.ValueOf(&t).Elem()
	vt := val.Type()

	for i := 0; i < val.NumField(); i++ {
		field := val.Field(i)
		fmt.Printf("[%d]  %s  %s  %v\n", i, vt.Field(i).Name, field.Type(), field.Interface())
	}
}

// 4. 修改字段值
func modifyValue() {
	p := &Person{}
	vp := reflect.ValueOf(p)
	vp = vp.Elem() // 解引用

	// 获取legCount字段的值
	fID := vp.FieldByName("ID")
	fID.SetInt(1001)

	fName := vp.FieldByName("Name")
	fName.SetString("Jack")

	fmt.Println(p)
}
