// Copyright 2023 Hollson. All rights reserved.
// Use of this source code is governed by a MIT style
// license that can be found in the LICENSE file.

package main

import (
	"fmt"

	"github.com/codegangsta/inject"
)

type SpecialString interface{}
type TestStruct struct {
	Name   string `inject`
	Nick   []byte
	Gender SpecialString `inject`
	uid    int           `inject`
	Age    int           `inject`
}

// http://c.biancheng.net/view/5132.html
func main() {
	s := TestStruct{}
	inj := inject.New()
	inj.Map("张三") // 注入参数
	inj.MapTo("男", (*SpecialString)(nil))

	inj2 := inject.New()
	inj2.Map(26)
	inj.SetParent(inj2) // 指定父Injector

	inj.Apply(&s) // 对struct 的字段进行注入
	fmt.Println("s.Name =", s.Name)
	fmt.Println("s.Gender =", s.Gender)
	fmt.Println("s.Age =", s.Age)
}
