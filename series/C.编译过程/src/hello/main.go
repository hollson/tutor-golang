package main

import (
	"hello/v2/lib"
)

// https://blog.csdn.net/huha666/article/details/121305696
// https://blog.csdn.net/jqqjj/article/details/118192004
// https://blog.schwarzeni.com/2020/01/07/go-generate-%E7%AE%80%E5%8D%95%E5%AE%9E%E7%94%A8/

// go tool compile -o main.o lib/lib.a main.go
func main() {
	lib.Test()
}
