package main

import (
	"fmt"
)

/*
逃逸分析：
	逃逸分析(Escape Analysis)是指编译器在执行静态代码分析后，对内存管理进行的优化和简化，然后决定一个变量是分配到堆还栈上。

查看逃逸：
命令：go tool compile，其中-l表示禁用内连；-m表示打印优化决策(majorization)，如：
	go build -gcflags '-m -l' main.go
	go tool compile -l -m main.go
	go tool compile -o /dev/null -l -m main.go
	go tool compile -S main.go | grep escape
*/

// 1. 指针逃逸
func pointer() *int {
	var x = 1 // moved to heap: x
	return &x
}

// 2. 栈空间不足逃逸
func space() {
	_ = make([]int, 10000) // escapes to heap
	_ = make([]int, 2000)  // does not escape
}

// 3. 闭包逃逸
func closure() func() int {
	var count = 0       // moved to heap: count
	return func() int { // func literal escapes to heap
		count++
		return count
	}
}

// 4. 动态类型逃逸（不确定长度大小）
// 很多函数参数为interface类型，比如fmt.Println(a …interface{})，编译期间很难确定其参数的具体类型，也能产生逃逸。
func dynamic() {
	var a = 1
	fmt.Println(a)
}

func main() {
}
