[TOC]

# 一个简单的trace示例
```go
import (
    "os"
    "runtime/trace"
)

func main() {
    trace.Start(os.Stderr)
    defer trace.Stop()

    ch := make(chan string)
    go func() {
        ch <- "HELLO"
    }()
    <-ch
}
```
**生成跟踪文件:**

```sh
$ go run main.go 2> trace.out
```
_对于包含PProf的Web应用，可从 `curl http://localhost/debug/pprof/trace?seconds=10 > ~/trace.out`下载跟踪文件。_

**启动可视化界面: **
```sh
$ go tool trace -http=":8000" trace.out
2021/05/07 15:08:19 Parsing trace...
2021/05/07 15:08:19 Splitting trace...
2021/05/07 15:08:19 Opening browser. Trace viewer is listening on http://[::]:8000
```
**跟踪内容概要：**
- View trace：查看跟踪
- Goroutine analysis：Goroutine 分析
- Network blocking profile：网络阻塞概况
- Synchronization blocking profile：同步阻塞概况
- Syscall blocking profile：系统调用阻塞概况
- Scheduler latency profile：调度延迟概况
- User defined tasks：用户自定义任务
- User defined regions：用户自定义区域
- Minimum mutator utilization：最低 Mutator 利用率

<br/>

# Scheduler latency profile
在刚开始时，一般先查看 “Scheduler latency profile”，我们能通过Graph看到`整体的调用开销情况`，如下：

![55447393.png](Trace性能跟踪_files/55447393.png)

<br/>
# Goroutine analysis
第二步看 “Goroutine analysis”，我们能通过这个功能看到整个运行过程中，每个函数块有多少个有 Goroutine 在跑，并且观察每个的 Goroutine 的运行开销都花费在哪个阶段。如下

![56185912.png](Trace性能跟踪_files/56185912.png)
- 通过上图我们可以看到共有 3 个 goroutine，分别是 runtime.main、 runtime/trace.Start.func1、 main.main.func1，那么它都做了些什么事呢，接下来我们可以通过点击具体细项去观察。如下：

![56639721.png](Trace性能跟踪_files/56639721.png)
同时也可以看到当前 Goroutine 在整个调用耗时中的占比，以及GC清扫和GC暂停等待的一些开销。如果你觉得还不够，可以把图表下载下来分析，相当于把整个 Goroutine 运行时掰开来看了，这块能够很好的帮助我们**对 Goroutine 运行阶段做一个的剖析，可以得知到底慢哪，然后再决定下一步的排查方向**。如下：
| 名称                  | 含义         | 耗时   |
| :-------------------- | :----------- | :----- |
| Execution Time        | 执行时间     | 3140ns |
| Network Wait Time     | 网络等待时间 | 0ns    |
| Sync Block Time       | 同步阻塞时间 | 0ns    |
| Blocking Syscall Time | 调用阻塞时间 | 0ns    |
| Scheduler Wait Time   | 调度等待时间 | 14ns   |
| GC Sweeping           | GC 清扫      | 0ns    |
| GC Pause              | GC 暂停      | 0ns    |

<br/>
# View trace
在对当前程序的 Goroutine 运行分布有了初步了解后，我们再通过 “查看跟踪” 看看之间的关联性，如下：
![58142325.png](Trace性能跟踪_files/58142325.png)
> 这个跟踪图粗略一看，相信有的小伙伴会比较懵逼，我们可以依据注解一块块查看，如下：
1. 时间线：显示执行的时间单元，根据时间维度的不同可以调整区间，具体可执行 shift
 + ?
 查看帮助手册。
2. 堆：显示执行期间的内存分配和释放情况。
3. 协程：显示在执行期间的每个 Goroutine 运行阶段有多少个协程在运行，其包含 GC 等待（GCWaiting）、可运行（Runnable）、运行中（Running）这三种状态。
4. OS 线程：显示在执行期间有多少个线程在运行，其包含正在调用 Syscall（InSyscall）、运行中（Running）这两种状态。
5. 虚拟处理器：每个虚拟处理器显示一行，虚拟处理器的数量一般默认为系统内核数。
6. 协程和事件：显示在每个虚拟处理器上有什么 Goroutine 正在运行，而连线行为代表事件关联。

点击具体的行为后可以看到其相关联的详细信息，如：
> - Start：开始时间
> - Wall Duration：持续时间
> - Self Time：执行时间
> - Start Stack Trace：开始时的堆栈信息
> - End Stack Trace：结束时的堆栈信息
> - Incoming flow：输入流
> - Outgoing flow：输出流
> - Preceding events：之前的事件
> - Following events：之后的事件
> - All connected：所有连接的事件

<br/>

# View Events
我们可以通过点击 View Options-Flow events、Following events 等方式，查看我们应用运行中的事件流情况。如下：
![60587981.png](Trace性能跟踪_files/60587981.png)
通过分析图上的事件流，我们可得知这程序从 `G1 runtime.main` 开始运行，在运行时创建了2个 Goroutine，先是创建 `G18 runtime/trace.Start.func1`，然后再是`G19 main.main.func1` 。而同时我们可以通过其`Goroutine Name`去了解它的调用类型，如：`runtime/trace.Start.func1` 就是程序中在 main.main 
调用了` runtime/trace.Start` 方法，然后该方法又利用协程创建了一个闭包 func1 去进行调用。


[https://www.cnblogs.com/-lee/p/12718025.html](https://www.cnblogs.com/-lee/p/12718025.html)
[https://www.jianshu.com/p/9a66fcae3a4a](https://www.jianshu.com/p/9a66fcae3a4a)
[https://www.cnblogs.com/-lee/p/12724436.html](https://www.cnblogs.com/-lee/p/12724436.html)
[https://studygolang.com/articles/9693](https://studygolang.com/articles/9693)

https://blog.csdn.net/lengyue1084/article/details/108252799


https://mp.weixin.qq.com/s?__biz=MzAxMTA4Njc0OQ==&mid=2651441259&idx=2&sn=b57987e22dcb7014173c1151c523eed2&chksm=80bb1699b7cc9f8f5cb30a15eaafdef845835dece6bb20649a05483eabf32e31dd3ff6374e32&scene=21#wechat_redirect

https://mp.weixin.qq.com/s/iXkbF018fxgTWtMqxZfo6g

https://www.cnblogs.com/double12gzh/p/13654879.html





