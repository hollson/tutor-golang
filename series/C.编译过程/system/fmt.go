// Copyright 2022 Hollson. All rights reserved.
// Use of this source code is governed by a MIT style
// license that can be found in the LICENSE file.

package main

import (
	"fmt"
	"os"
	"syscall"
)

func sysCall() {
	// syscall - stdout
	fmt.Println("hello")             // 底层调用了syscall，即先调用fmt.Fprintln
	fmt.Fprintln(os.Stdout, "hello") // os.Stdout调用os.NewFile,并指向syscall.Stdout
	fmt.Fprintln(os.NewFile(uintptr(syscall.Stdout), "/dev/stdout"), "hello")

	// syscall - file
	syscall.Mkdir("./tmp", 0755) // 644
	syscall.Rename("./tmp", "./temp")

	// syscall - env
	fmt.Println(syscall.Getenv("USER"))

	fmt.Scanln()
	syscall.Rmdir("./temp")
}

// 文件句柄
// fd，即file descriptor，文件描述符。linux下，所有的操作都是对文件进行操作，而对文件的操作是利用文件描述符(file descriptor)来实现的。
// 每个文件进程控制块中都有一份文件描述符表（可以把它看成是一个数组，里面的元素是指向file结构体指针类型），这个数组的下标就是文件描述符。
// 在源代码中，一般用fd作为文件描述符的标识。
// https://cs.opensource.google/go/go/+/refs/tags/go1.18beta1:src/os/file_unix.go;l=122;bpv=1;bpt=0
func fd() {
	fd, err := syscall.Open("./a.txt", syscall.O_RDONLY, 0)
	fmt.Println(fd, err)

	f, err := os.Open("./a.txt")
	fmt.Println(f.Fd(), err)

	f, err = os.Open("./a.txt")
	fmt.Println(f.Fd(), err)
}

// 构造一个虚拟file
func sysfile() {
	fd, err := syscall.Open("./a.txt", syscall.O_RDONLY, 0)
	// fd, err := syscall.Open("./b.txt", syscall.O_CREAT|syscall.O_RDWR, 0666)
	if err != nil {
		panic(err)
	}

	f := os.NewFile(uintptr(fd), "虚拟文件名")
	f.WriteString("hello world")
	defer f.Close()
	fmt.Println(fd, f.Fd(), f.Name())

	buf := make([]byte, 1024)
	n, err := f.Read(buf)
	if err != nil {
		panic(err)
	}
	fmt.Println(string(buf[:n]))
}
func main() {
	sysCall()
	// fd()
	// sysfile()
}
