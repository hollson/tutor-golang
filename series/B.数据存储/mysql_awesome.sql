CREATE database IF NOT EXISTS awesome DEFAULT charset utf8 collate utf8_general_ci;
# CREATE database IF NOT EXISTS awesome DEFAULT charset utf8mb4 collate utf8mb4_general_ci;
USE awesome;

drop table if exists table_demo;
CREATE TABLE table_demo
(
    id          int(0) not null auto_increment comment '主键',
    b_bit       bit(8)                           default 255 comment '位类型',
    b_bool      bit(1)                           default true comment '默认1位,表示布尔类型',
    n_tinyint   tinyint                          default 127 comment '1字节',
    n_smallint  smallint                         default 0 comment '2字节',
    n_mediumint mediumint                        default 0 comment '3字节',
    n_int       int                              default 0 comment '4字节',
    n_bigint    bigint                           default 0 comment '8字节',
    n_unsigned  tinyint unsigned                 default 255 comment '无符号',
    f_float     float(4, 2)                      default 99.99 comment '单精度(百分比)',
    f_double    double(5, 2)                     default 123.45 comment '双精度',
    f_decimal   decimal(18, 2)                   default 1.01 comment '定点数',
    s_char      char(5)                          default 'a' comment '固定长度存储,不大于255',
    s_varchar   varchar(5)                       default '12345' comment '按实际存储，且不大于设定长度',
    t_date      date                             default '2020:01:02' comment '日期',
    t_time      time                             default '15:04:05' comment '时间',
    t_datetime  datetime                         default current_timestamp comment '日期',
    t_timestamp timestamp                        default current_timestamp comment '时间戳(自动填充)',
    t_year      year                             default 2022 comment '年',
    b_binary    binary(20)                       default 'hello' comment '二进制字符串，最大255个字节',
    b_varbinary varbinary(20)                    default 'hello' comment '二进制字符串，max(65535)',
    b_tityblob  tinyint comment '最大255个字节',
    b_blob      blob comment '二进制长文本',
    o_enum      ENUM ('female','male','unknown') default 'unknown' comment '枚举(可插入下标或文本)',
    PRIMARY KEY (id)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;
