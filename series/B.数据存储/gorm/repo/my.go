// Copyright 2021 Hollson. All rights reserved.
// Use of this source code is governed by a MIT style
// license that can be found in the LICENSE file.

package main

import (
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
)

// column: 数据库列名
// PRIMARY_KEY: 主键定义
// AUTO_INCREMENT: 自增定义
// index: 普通索引，可以不加名字，gorm会自动生成
// UNIQUE: 唯一索引
// NOT_NULL：非空约束


func main() {
	db, err := gorm.Open("mysql", "user:password@(localhost)/dbname?charset=utf8mb4&parseTime=True&loc=Local")
	defer db.Close()
}
