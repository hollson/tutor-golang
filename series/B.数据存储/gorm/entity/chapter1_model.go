package entity

import (
	"database/sql"
	"time"

	"gorm.io/gorm"
)

/*================================模型==================================*/

// 约定使用ID、CreatedAt、UpdatedAt，DeletedAt为默认字段，并自动填充
type Model struct {
	ID        uint `gorm:"primaryKey"`
	CreatedAt time.Time
	UpdatedAt time.Time
	DeletedAt gorm.DeletedAt `gorm:"index"`
}

/*================================类型==================================*/

// 模型由基本数据类型和自定义类型(实现了Scanner和Valuer接口)组成
type Structure struct {
	ID           uint
	Name         string
	Email        *string
	Age          uint8
	Birthday     *time.Time
	MemberNumber sql.NullString // 自定义类型，实现了Scanner和Valuer接口

	ActivatedAt sql.NullTime
	CreatedAt   time.Time
	UpdatedAt   time.Time
}

/*================================时间==================================*/

type Time struct {
	CreatedAt time.Time // 在创建时，如果该字段值为零值，则使用当前时间填充
	UpdatedAt int       // 在创建时该字段值为零值或者在更新时，使用当前时间戳秒数填充
	Updated   int64     `gorm:"autoUpdateTime:nano"`  // 使用时间戳填纳秒数充更新时间
	Updated2  int64     `gorm:"autoUpdateTime:milli"` // 使用时间戳毫秒数填充更新时间
	Created   int64     `gorm:"autoCreateTime"`       // 使用时间戳秒数填充创建时间
}

/*================================约束==================================*/

type Bound struct {
	Field1 string `gorm:"<-:create"`          // 允许读取和创建
	Field2 string `gorm:"<-:update"`          // 允许读取和更新
	Field3 string `gorm:"<-"`                 // 允许读写（创建和更新）
	Field4 string `gorm:"<-:false"`           // 允许读取，禁用写入权限
	Field5 string `gorm:"->"`                 // 只读（除非已配置，否则禁用写权限）
	Field6 string `gorm:"->;<-:create"`       // 允许读取和创建
	Field7 string `gorm:"->:false;<-:create"` // createonly（禁用从数据库读取）
	Field8 string `gorm:"-"`                  // 使用 struct 读写时忽略此字段
	Field9 string `gorm:"-:all"`              // 使用 struct 写入、读取和迁移时忽略此字段
	Field0 string `gorm:"-:migration"`        // 使用 struct 迁移时忽略此字段
}

/*================================嵌套==================================*/

// 嵌套模型
type MyModel struct {
	gorm.Model
	Name string
}

type Author struct {
	Name  string
	Email string
}

// 普通嵌套
type Blog struct {
	ID      int
	Author  Author `gorm:"embedded"`
	Author2 Author `gorm:"embedded;embeddedPrefix:author_"` // 添加前缀
	Upvotes int32
}

/*================================标签==================================*/
