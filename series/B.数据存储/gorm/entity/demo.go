package entity

import (
	"errors"
	"fmt"
	"log"
	"time"

	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
)

func TestORM() {

	db, err := ConnectDB()
	HandlerError(err)
	// 数据插入
	// InsertData(db)
	// 数据查询
	// SelectData(db)
	// UpdateDate(db)
	// DeleteData(db)
	CreateTable(db)
}

// 连接数据库
func ConnectDB() (*gorm.DB, error) {
	// 配置MySQL连接参数，一般放在配置文件
	username := "root"  // 账号
	password := "123"   // 密码
	host := "127.0.0.1" // 数据库地址，可以是Ip或者域名
	port := 3306        // 数据库端口
	DbName := "test"    // 数据库名
	timeout := "10s"    // 连接超时，10秒
	// 配置dsn
	dsn := fmt.Sprintf("%s:%s@tcp(%s:%d)/%s?charset=utf8&parseTime=True&loc=Local&timeout=%s", username, password, host, port, DbName, timeout)
	db, err := gorm.Open("mysql", dsn)
	if err != nil {
		return nil, err
	}
	return db, nil
}

// 数据插入
func InsertData(db *gorm.DB) {
	// 构造数据模型
	stu := Student{
		StuName:    "test_stu",
		Email:      "test@qq.com",
		Sex:        1,
		CreateTime: time.Now().Unix(),
		UpdateTime: time.Now().Unix(),
	}
	// 新增数据库
	// 插入数据时，自增主键一般不需要在声明对象时赋值，在gorm插入时需要传递地址
	// 解决：https://blog.csdn.net/u013536232/article/details/105513072
	if err := db.Create(&stu).Error; err != nil {
		HandlerError(err)
	} else {
		log.Print("插入成功")
	}
	ids := []int64{}
	db.Raw("SELECT LAST_INSERT_ID() as id").Pluck("id", &ids)
	fmt.Println("自增主键值：", ids[0])
}

// 查询数据
func SelectData(db *gorm.DB) {
	stu := []Student{}
	// 查询多列数据，查询不到数据也会抛出panic
	if err := db.Where("name = ?", "test_stu").Find(&stu).Error; err != nil {
		HandlerError(errors.New("查询多列数据失败"))
	} else {
		fmt.Println("查询多列数据：", stu)
	}
	// 查询单列数据，根据主键排序，取第一
	if err := db.Where("sex = ?", 1).First(&stu).Error; err != nil {
		HandlerError(errors.New("查询单列数据失败"))
	} else {
		fmt.Println("查询单列数据：", stu)
	}
	// 查询单列数据，根据主键排序，取最后
	if err := db.Where("sex = ?", 1).Last(&stu).Error; err != nil {
		HandlerError(errors.New("查询单列数据失败"))
	} else {
		fmt.Println("查询单列数据：", stu)
	}
	// 查一列数据
	if err := db.Where("id = ?", 6).Take(&stu).Error; err != nil {
		HandlerError(errors.New("查询单列数据失败"))
	} else {
		fmt.Println("查询单列数据：", stu)
	}
	if flag := db.Where("id = ?", 6).Take(&stu).RecordNotFound(); flag {
		fmt.Println("找不到相关数据")
	}

	var stuName []string
	// 查询某个字段数据，接收应该使用切片
	if err := db.Model(Student{}).Where("email = ?", "test@qq.com").Pluck("name", &stuName).Error; err != nil {
		fmt.Println(err)
	}
	fmt.Println("student_name: ", stuName)
	// 如果查询不到也可以使用链式函数调用的方式
	rowNotFound := db.Model(Student{}).Where("id = ?", 6).Pluck("name", &stuName).RecordNotFound()
	if rowNotFound {
		fmt.Println("没有找到对应记录")
	}
	fmt.Println("student_name: ", stuName)
	var id []string
	// select也需要绑定表，查询多个可以直接pluck多次
	if err := db.Model(Student{}).Where("id = ?", 6).Pluck("name", &stuName).Pluck("id", &id).Error; err != nil {
		fmt.Println(err)
	}
	fmt.Println(id)
	fmt.Println(stuName)
	stu1 := []Student{}
	// order + 分页，find返回的是一个切片
	db.Where("sex in (?)", []int{1, 0}).Order("id desc").Limit(10).Offset(0).Find(&stu1)
	fmt.Println(stu1)
	// count，也需要绑定数据表
	count := 0
	db.Model(Student{}).Where("sex in (?)", []int{1, 0}).Order("id desc").Limit(10).Offset(0).Count(&count)
	fmt.Println(count)
}

// 删除数据
func DeleteData(db *gorm.DB) {
	// 绑定模型
	db.Where("id = ?", 7).Delete(&Student{})
}

// 更新数据
func UpdateDate(db *gorm.DB) {
	// 第一种更新方法：save
	stu := Student{}
	db.Where("id = ?", 6).First(&stu)
	直接修改想要更新的字段
	// stu.Sex = 2
	// stu.UpdateTime = 1111
	// stu.CreateTime = 111
	// db.Save(stu)

	// 第二种：update
	// 单个字段更新，需要绑定模型
	db.Model(&stu).Update("updated_at", 1111111)

	// 第三种：updates，多参数更新，支持struct和map[string]interface{}
	fmt.Println(db.Model(&stu).Updates(map[string]interface{}{"name": "updates", "sex": 1}).Error.Error())
}

type User struct {
	ID      int64  `gorm:"Column:id;type:bigint;PRIMARY_KEY;AUTO_INCREMENT"`
	StuName string `gorm:"Column:name;type:varchar(255);INDEX:idx_name;NOT NULL"`
}

// 快速建表
func CreateTable(db *gorm.DB) {
	db.AutoMigrate(&User{})
}

// 异常处理
func HandlerError(err error) {
	if err != nil {
		panic(err)
	}
}
