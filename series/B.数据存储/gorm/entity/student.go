// Copyright 2022 Hollson. All rights reserved.
// Use of this source code is governed by a MIT style
// license that can be found in the LICENSE file.

package entity

type Student struct {
	ID         int64  `gorm:"Column:id;type:bigint;PRIMARY_KEY;AUTO_INCREMENT"`
	StuName    string `gorm:"Column:name;type:varchar(255);INDEX:idx_name;NOT NULL"`
	Email      string `gorm:"Column:email;type:varchar(255);UNIQUE:uniq_email;NOT NULL"`
	Sex        int64  `gorm:"Column:sex;type:bigint;NOT NULL"`
	CreateTime int64  `gorm:"Column:created_at;type:bigint;NOT NULL"`
	UpdateTime int64  `gorm:"Column:updated_at;type:bigint;NOT NULL"`
	Sum        int    `gorm:"-"` // -：不参数gorm数据读写
}

func (s Student) TableName() string {
	return "tb_student"
}
