CREATE TABLE `tb_student` (
    `id` bigint(20) NOT NULL AUTO_INCREMENT,
    `name` varchar(255) NOT NULL,
    `sex` bigint(20) NOT NULL,
    `email` varchar(255) NOT NULL,
    `created_at` bigint(20) NOT NULL,
    `updated_at` bigint(20) NOT NULL,
    PRIMARY KEY (`id`),
    UNIQUE KEY `uniq_email` (`email`),
    KEY `idx_name` (`name`)
    ) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
