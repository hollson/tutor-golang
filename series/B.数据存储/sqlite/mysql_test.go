/*
一. 安装驱动：
    go get github.com/mattn/go-sqlite3

二. 创建数据库表：
    create table users (id integer not null primary key, name text,addr text);

三. 驱动操作注意项：
    1. 当前驱动支持「$」或「?」参数占位符
*/
package main

import (
	"database/sql"
	"fmt"
	"testing"

	_ "github.com/mattn/go-sqlite3"
)

type User struct {
	Id   int64  // 编号
	Name string // 姓名
	Addr string // 地址
}

// 连接数据库
func OpenDB() *sql.DB {
	_db, err := sql.Open("sqlite3", "/tmp/foo.db")
	if err != nil {
		panic(err)
	}
	_db.SetMaxOpenConns(50)
	_db.SetMaxIdleConns(10)
	return _db
}

// 1.测试数据库连接
func TestConnect(t *testing.T) {
	_db := OpenDB()
	defer _db.Close()

	if _db.Ping() == nil {
		fmt.Printf("数据库连接成功:%+v\n", _db.Stats())
	}
}

// 2.插入数据
func TestInsert(t *testing.T) {
	_db := OpenDB()
	defer _db.Close()

	// 隐式的Prepare操作
	res, _ := _db.Exec("insert into users(name,addr) values (?,?)", "张三", "北京")
	fmt.Println(res.LastInsertId())
	fmt.Println(res.RowsAffected()) // 影响行数
}

// 3.修改数据
func TestUpdate(t *testing.T) {
	_db := OpenDB()
	defer _db.Close()

	res, _ := _db.Exec("update users set addr=? where name=?", "上海", "张三")
	fmt.Println(res.RowsAffected()) // 影响行数
}

// 4.删除数据
func TestDelete(t *testing.T) {
	_db := OpenDB()
	defer _db.Close()

	res, _ := _db.Exec("delete from users where id=$1", 1)
	fmt.Println(res.RowsAffected()) // 影响行数
}

// 5.查询单值(总数)
func TestSingle(t *testing.T) {
	_db := OpenDB()
	defer _db.Close()

	var count int64
	_db.QueryRow("select count(*) from users where id>$1", 0).Scan(&count)
	fmt.Println("总数：", count)
}

// 6.查询单行
func TestQueryFirst(t *testing.T) {
	_db := OpenDB()
	defer _db.Close()

	var u User
	_row := _db.QueryRow("select id,name,addr from users where id=$1", 2)
	err := _row.Scan(&u.Id, &u.Name, &u.Addr)
	fmt.Println(u, err)
}

// 7.查询列表
func TestQueryList(t *testing.T) {
	_db := OpenDB()
	defer _db.Close()

	_rows, _ := _db.Query("select * from users where 1=1 limit $1 offset $2;", 10, 0)
	defer _rows.Close() // 务必要关闭Rows

	var u User
	for _rows.Next() {
		_rows.Scan(&u.Id, &u.Name, &u.Addr)
		fmt.Println(u)
	}
}

// 8.执行事务
func TestTrans(t *testing.T) {
	_db := OpenDB()
	defer _db.Close()

	var err error
	tx, _ := _db.Begin()
	defer func(tx *sql.Tx) {
		if err != nil {
			fmt.Println("回滚事务：", tx.Rollback(), err.Error())
			return
		}
		fmt.Println("提交事务：", tx.Commit())
	}(tx)

	if _, err = tx.Exec(`insert into users(name,addr) values (?,?)`, "事务1", "北京"); err != nil {
		err = fmt.Errorf("事务1执行失败：%v", err)
		return
	}
	if _, err = tx.Exec(`insert into users(name,addr) values (?,?)`, "事务2", "上海"); err != nil {
		err = fmt.Errorf("事务2执行失败：%v", err)
		return
	}
}

// 9.(显式)自定义Prepare
func TestPrepare(t *testing.T) {
	_db := OpenDB()
	defer _db.Close()

	stmt, _ := _db.Prepare("insert into users(name,addr) values ($1,$2)")
	defer stmt.Close() // 务必关闭

	res, _ := stmt.Exec("Prepare示例", "火星")
	fmt.Println(res.RowsAffected())
}
