// +build !redis_cluster

/*
	Redis集群说明:
		1. 解决了单机的网络,CPU和内存的瓶颈,实现了服务的水平扩展和负载均衡;
		2. 集群最小配置建议:三主三从,从节点只作为故障转移使用(冷备);
		3. 数据通过异步复制,不保证数据的强一致性(即从库不提供数据读取)
		4. 集群不支持批量操作(不建议用哈希Tag),也不支持多节点的事务操作;
		5. 集群的使用会侵入客户端代码,即需使用集群体系命令,
		6. 集群节点的最大容量不要超过20G;
		7. 不建议使用pipeline和muti-keys操作,减少max redirect的发生
		8. 暂无官方的GUI监控和管理工具,维护成本和风险较高;
		9. 推荐第三方Codis或Redis-manager管理平台;
*/

package rdb

import (
	"context"

	"locojoy.com/system/config"
)

var (
	Rdb *redis.ClusterClient
	ctx = context.Background()
)

func Init() (model string, err error) {
	Rdb = redis.NewClusterClient(&redis.ClusterOptions{
		Addrs: config.Redis.Hosts,
		Password: config.Redis.Password,
		PoolSize: 200,
	})
	return "Cluster",Rdb.Ping(ctx).Err()
}
