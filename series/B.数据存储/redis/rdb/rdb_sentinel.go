//go:build redis_cluster
// +build redis_cluster

/*
	Redis哨兵说明:
		1. 哨兵模式解决了单点故障的问题,使Redis服务变得高可用;
		2. 数据量过小(如小于20G),建议使用哨兵模式,如1主2从3哨兵;
		3. 哨兵仅是个监控服务,不会侵入Redis实例和客户端(go-redis);

	go-redis客户端:
		1. 具有更多的Star和源码贡献者,被应用次数更高;
		2. 源码持续升级维护中,且有更全面的官方文档;
		3. sentinel,cluster集成度更高,代码封装更人性化;
		4. 具有较高的生态集成度,如RedisMock,分布式锁,缓存,限速等;
		5. 社区活跃度高,且有更全面的官方文档(redis.uptrace.dev);
*/

package rdb

import (
	"context"
	"time"

	"github.com/go-redis/redis/v8"
)

var (
	Rdb *redis.Client // Redis-Client
	ctx = context.Background()
)

// Model Redis部署模式
type Model int

const (
	Single   Model = iota // 单机
	Sentinel              // 哨兵
	Cluster               // 集群
)

type RedisConfig struct {
	Model string
	Hosts string
}

// DefaultSingleOption 单机版默认配置项
var DefaultSingleOption = &redis.Options{
	Addr:        "localhost:6379",
	Password:    "",
	DB:          0,
	PoolSize:    200,
	PoolTimeout: time.Second * 3,
}

// DefaultFailoverOptions 哨兵版默认配置项
var DefaultFailoverOptions = &redis.FailoverOptions{
	MasterName:    "mymaster",
	SentinelAddrs: []string{"127.0.0.1:16379;"},
	Password:      "",
}

// Init 单实例或哨兵模式
func Init(m ...Model) (err error) {
	// 单实例
	if len(m) > 0 && m[0] == Single {
		Rdb = redis.NewClient(DefaultSingleOption)
		return Rdb.Ping(ctx).Err()
	}

	// 哨兵模式
	Rdb = redis.NewFailoverClient(DefaultFailoverOptions)
	return Rdb.Ping(ctx).Err()
}
