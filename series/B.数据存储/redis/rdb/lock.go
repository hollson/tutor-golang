/*
分布式锁的选择：

	https://github.com/bsm/redislock
	原子性：利用 Lua 脚本实现原子性语义；
	阻塞唤醒：利用 Redis 的发布订阅来实现锁的阻塞唤醒；
	锁自动过期：避免因为宕机导致的死锁问题；
	锁的自动续期：利用 Go 协程实现锁资源的自动续期，避免出现业务时间>锁超时时间导致并发安全问题
	TryLock：尝试获取一次锁，获取失败后阻塞
	自旋锁：提供自旋锁 API 来实现分布式锁的自旋获取

	1、setnx
	①：有多个协程同时抢到锁，但只有一个协程拿到锁
	②：有过期时间，超过过期时间锁自动释放

	2、redislock
	①：有多个协程同时抢到锁，但只有一个协程拿到锁
	②：有过期时间，超过过期时间锁自动释放
	③：通过Lua脚本判断准备释放的锁中的值是否为自己设置的

	通过以上对比可以发现单纯的使用redis自带的setNx方法在遇到多个server使用一个redis时，
	因没有校验机制会出现serverA加的锁被serverB释放的情况，所以setNx多用于校验单个server
	是否重复提交请求，而分布式锁需要参考github.com/bsm/redislock的实现

	https://blog.csdn.net/ambzheng/article/details/111400170
*/

package rdb

import (
	"fmt"

	"github.com/bsm/redislock"
	"github.com/go-redis/redis/v8"
)

var redisClient = redis.NewClient(&redis.Options{
	Addr:     "localhost:6379",
	Password: "", // no password set
	DB:       0,  // use default DB
})

func main() {
	key := "reids-lock-key"
	value := "redis-lock-value"
	lock := redislock.NewRedisLock(redisClient, key, value)

	err := lock.Lock()
	if err != nil {
		fmt.Println(err.Error())
		return
	}
	fmt.Println("get redis lock success")
	defer func() {
		err = lock.Unlock()
		if err != nil {
			fmt.Println(err.Error())
			return
		}
		fmt.Println("release redis lock success")
	}()
}
