// Copyright 2021 Hollson. All rights reserved.
// Use of this source code is governed by a MIT style
// license that can be found in the LICENSE file.
// 更多内容，请访问：https://github.com/hollson

// Package rdb
// Redis命令与底层Package的适配层,封装了绝大多数的常用的业务命令
package rdb

import (
	"strconv"
	"time"

	"github.com/go-redis/redis/v8"
)

func Send(args ...interface{}) error {
	return Rdb.Do(ctx, args...).Err()
}

func Do(args ...interface{}) (interface{}, error) {
	return Rdb.Do(ctx, args...).Result()
}

func Ping() error {
	return Rdb.Ping(ctx).Err()
}

// Get 不存在/类型错误/格式错误时,返回空
func Get(key string) string {
	return Rdb.Get(ctx, key).Val()
}

// Set expiration: 过期时间(默认不过期)
func Set(key string, v interface{}, expiration ...time.Duration) error {
	var t time.Duration = 0
	if len(expiration) > 0 {
		t = expiration[0]
	}
	return Rdb.Set(ctx, key, v, t).Err()
}

// Exists 当所以Key都存在时返回true
func Exists(key ...string) bool {
	return Rdb.Exists(ctx, key...).Val() == int64(len(key))
}

// Del 删除指定key
func Del(key ...string) error {
	return Rdb.Del(ctx, key...).Err()
}

// Expire 延迟过期时间,如: Expire("test_key",time.time.Second*3)
func Expire(key string, expiration time.Duration) bool {
	return Rdb.Expire(ctx, key, expiration).Val()
}

// Persist 移除过期时间,使永不过期
func Persist(key string) error {
	return Rdb.Persist(ctx, key).Err()
}

// Ttl 获取过期剩余时长(秒)
func Ttl(key string) float64 {
	return Rdb.TTL(ctx, key).Val().Seconds()
}

// Pttl 获取过期剩余时长(毫秒)
func Pttl(key string) int64 {
	return Rdb.PTTL(ctx, key).Val().Milliseconds()
}

// Append 字符串追加,返回结果长度
func Append(key, v string) int64 {
	return Rdb.Append(ctx, key, v).Val()
}

// StrLen 取指定key的value值的长度
func StrLen(key string) int64 {
	return Rdb.StrLen(ctx, key).Val()
}

// SetRange 设置指定 key 的 value 值的子字符串
func SetRange(key string, offset int64, value string) (int64, error) {
	return Rdb.SetRange(ctx, key, offset, value).Result()
}

// GetRange 获取指定 key 的 value 值的子字符串
func GetRange(key string, start, end int64) (string, error) {
	return Rdb.GetRange(ctx, key, start, end).Result()
}

// GetSet 设置 key 的值,并返回 key 的旧值
func GetSet(key, v string) (string, error) {
	return Rdb.GetSet(ctx, key, v).Result()
}

// MGet 一次获取多个 key 的值,如果对应 key 不存在,则对应返回 nil
func MGet(key []string) ([]interface{}, error) {
	return Rdb.MGet(ctx, key...).Result()
}

// MSet 支持三种格式:
//   - MSet("key1", "value1", "key2", "value2")
//   - MSet([]string{"key1", "value1", "key2", "value2"})
//   - MSet(map[string]interface{}{"key1": "value1", "key2": "value2"})
func MSet(mk ...interface{}) (string, error) {
	return Rdb.MSet(ctx, mk...).Result()
}

// MSetNX 支持三种格式:
//   - MSetNX("key1", "value1", "key2", "value2")
//   - MSetNX([]string{"key1", "value1", "key2", "value2"})
//   - MSetNX(map[string]interface{}{"key1": "value1", "key2": "value2"})
func MSetNX(mk ...interface{}) (bool, error) {
	return Rdb.MSetNX(ctx, mk...).Result()
}

// Deprecated: 集群环境谨用
func Publish(channel string, message interface{}) (int64, error) {
	return Rdb.Publish(ctx, channel, message).Result()
}

// Deprecated: 集群环境谨用
func SubScribe(channel ...string) *redis.PubSub {
	return Rdb.Subscribe(ctx, channel...)
}

// SetNX 不存在时Set(默认不过期)
func SetNX(key string, v interface{}, expiration ...time.Duration) (bool, error) {
	var t time.Duration = 0
	if len(expiration) > 0 {
		t = expiration[0]
	}
	return Rdb.SetNX(ctx, key, v, t).Result()
}

// SetEX 存在时Set(默认不过期)
func SetEX(key string, v interface{}, expiration ...time.Duration) error {
	var t time.Duration = 0
	if len(expiration) > 0 {
		t = expiration[0]
	}
	return Rdb.SetEX(ctx, key, v, t).Err()
}

func Eval(script string, key []string, args ...interface{}) *redis.Cmd {
	return Rdb.Eval(ctx, script, key, args...)
}

// IncrN String:自增或自减,并返回结果值, span为跨度,默认为+1
func IncrN(key string, span ...int64) int64 {
	var v int64 = 1
	if len(span) > 0 {
		v = span[0]
	}

	return Rdb.IncrBy(ctx, key, v).Val()
}

// IncrF String:自增或自减,并返回结果值,span为跨度,默认为+1
func IncrF(key string, span ...float64) float64 {
	var v float64 = 1
	if len(span) > 0 {
		v = span[0]
	}

	return Rdb.IncrByFloat(ctx, key, v).Val()
}

// HIncrN Hash:自增或自减,并返回结果值,span为跨度,默认为+1
func HIncrN(key, filed string, span ...int64) int64 {
	var v int64 = 1
	if len(span) > 0 {
		v = span[0]
	}
	return Rdb.HIncrBy(ctx, key, filed, v).Val()
}

// HIncrF Hash:自增或自减,并返回结果值,span为跨度,默认为+1
func HIncrF(key, filed string, span ...float64) float64 {
	var v float64 = 1
	if len(span) > 0 {
		v = span[0]
	}

	return Rdb.HIncrByFloat(ctx, key, filed, v).Val()
}

// Deprecated: 推荐使用HSet (HMset在 Redis4.0中已被弃用)
func HMSet(key string, kv ...interface{}) error {
	return HSet(key, kv...)
}

// HExists 测试指定 field 是否存在
func HExists(key, fieldName string) (bool, error) {
	return Rdb.HExists(ctx, key, fieldName).Result()
}

// HLen 返回指定 hash 的 field 数量
func HLen(key string) int64 {
	return Rdb.HLen(ctx, key).Val()
}

// HKeys 返回hash的所有field
func HKeys(key string) ([]string, error) {
	return Rdb.HKeys(ctx, key).Result()
}

// HVals 返回 hash 的所有 value
func HVals(key string) ([]string, error) {
	return Rdb.HVals(ctx, key).Result()
}

// HDel 删除指定 hash 的 field 数量
func HDel(key, fieldName string) error {
	return Rdb.HDel(ctx, key, fieldName).Err()
}

// HSet 可接收KV,Slice和Map三种格式参数, 如:
//   - HSet("hash_key", "key1", "value1", "key2", "value2")
//   - HSet("hash_key", []string{"key1", "value1", "key2", "value2"})
//   - HSet("hash_key", map[string]interface{}{"key1": "value1", "key2": "value2"})
func HSet(key string, values ...interface{}) error {
	return Rdb.HSet(ctx, key, values...).Err()
}

// HSetNX 设置 hash field 为指定值,如果 key 不存在,则先创建如果 field 已经存在,返回 0,nx 是 not exist 的意思
func HSetNX(key, fieldName string, value interface{}) error {
	return Rdb.HSetNX(ctx, key, fieldName, value).Err()
}

// HGet 获取指定的 hash field
func HGet(key, fieldName string) (string, error) {
	return Rdb.HGet(ctx, key, fieldName).Result()
}

func HGetInt(key, fieldName string) (int, error) {
	return Rdb.HGet(ctx, key, fieldName).Int()
}

func HGetInt64(key, fieldName string) (int64, error) {
	return Rdb.HGet(ctx, key, fieldName).Int64()
}

func HGetUint64(key, fieldName string) (uint64, error) {
	return Rdb.HGet(ctx, key, fieldName).Uint64()
}

// HMGet 获取全部指定的 hash filed
func HMGet(key string, fieldName ...string) ([]interface{}, error) {
	return Rdb.HMGet(ctx, key, fieldName...).Result()
}

// HGetAll 获取全部指定的 hash filed
func HGetAll(key string) (map[string]string, error) {
	return Rdb.HGetAll(ctx, key).Result()
}

// HGetAllIntoModel 获取制定的hash filed 到对象中
// model:需要是指针类型
func HGetAllIntoModel(key string, model interface{}) error {
	return Rdb.HGetAll(ctx, key).Scan(model)
}

// SAdd 向名称为 key 的 set 中添加元素
func SAdd(key string, item ...interface{}) (int64, error) {
	return Rdb.SAdd(ctx, key, item...).Result()
}

// SRem 删除名称为 key 的 set 中的元素 member
func SRem(key string, members ...interface{}) (int64, error) {
	return Rdb.SRem(ctx, key, members...).Result()
}

// SPop 随机返回并删除名称为 key 的 set 中一个元素
func SPop(key string) (string, error) {
	return Rdb.SPop(ctx, key).Result()
}

// SDiff 返回所有给定 key 与第一个 key 的差集
func SDiff(key ...string) ([]string, error) {
	return Rdb.SDiff(ctx, key...).Result()
}

// SDiffStore 返回所有给定 key 与第一个 key 的差集,并将结果存为另一个 key,并返回个数
func SDiffStore(destination string, key ...string) (int64, error) {
	return Rdb.SDiffStore(ctx, destination, key...).Result()
}

// SInter 返回所有给定 key 的交集
func SInter(key ...string) ([]string, error) {
	return Rdb.SInter(ctx, key...).Result()
}

// SInterStore 返回所有给定 key 的交集,并将结果存为另一个 key,并返回个数
func SInterStore(destination string, key ...string) (int64, error) {
	return Rdb.SInterStore(ctx, destination, key...).Result()
}

// SUnion 返回所有给定 key 的并集
func SUnion(key ...string) ([]string, error) {
	return Rdb.SUnion(ctx, key...).Result()
}

// SUnionStore 返回所有给定 key 的并集,并将结果存为另一个 key,并返回个数
func SUnionStore(destination string, key ...string) (int64, error) {
	return Rdb.SUnionStore(ctx, destination, key...).Result()
}

// SMove 从第一个 key 对应的 set 中移除 member 并添加到第二个对应 set 中
func SMove(src, des string, member interface{}) (bool, error) {
	return Rdb.SMove(ctx, src, des, member).Result()
}

// SCard 返回名称为 key 的 set 的元素个数
func SCard(key string) (int64, error) {
	return Rdb.SCard(ctx, key).Result()
}

// SIsMember 测试 member 是否是名称为 key 的 set 的元素
func SIsMember(key string, item interface{}) (bool, error) {
	return Rdb.SIsMember(ctx, key, item).Result()
}

// SMembers 返回给定key的所有元素
func SMembers(key string) ([]string, error) {
	return Rdb.SMembers(ctx, key).Result()
}

// SRandMember 随机返回名称为 key 的 set 的一个元素,但是不删除元素
func SRandMember(key string) (string, error) {
	return Rdb.SRandMember(ctx, key).Result()
}

// LPush 在 key 对应 list 的头部添加字符串元素
func LPush(key string, item ...string) error {
	return Rdb.LPush(ctx, key, item).Err()
}

// 在 key 对应 list 的尾部添加字符串元素
func RPush(key, item string) error {
	return Rdb.RPush(ctx, key, item).Err()
}

// LInsert 在 key 对应 list 的特定位置之前或之后添加字符串元素
func LInsert(key, op, member, item string) error {
	return Rdb.LInsert(ctx, key, op, member, item).Err()
}

// LSet 设置 list 中指定下标的元素值(下标从 0 开始)
func LSet(key string, index int64, member string) error {
	return Rdb.LSet(ctx, key, index, member).Err()
}

// LRem 从 key 对应 list 中删除 count 个和 value 相同的元素
func LRem(key string, count int64, value string) error {
	return Rdb.LRem(ctx, key, count, value).Err()
}

// LTrim 保留指定 key 的值范围内的数据
func LTrim(key string, begin, end int64) (string, error) {
	return Rdb.LTrim(ctx, key, begin, end).Result()
}

// LPop 从 list 的头部删除元素,并返回删除元素
func LPop(key string) (string, error) {
	return Rdb.LPop(ctx, key).Result()
}

// RPop 从 list 的尾部删除元素,并返回删除元素
func RPop(key string) (string, error) {
	return Rdb.RPop(ctx, key).Result()
}

func BRPop(key ...string) ([]string, error) {
	return Rdb.BRPop(ctx, 0, key...).Result()
}

func RPopLPush(src, des string) (string, error) {
	return Rdb.RPopLPush(ctx, src, des).Result()
}

// LIndex 返回名称为 key 的 list 中 index 位置的元素
func LIndex(key string, index int64) (string, error) {
	return Rdb.LIndex(ctx, key, index).Result()
}

// LLen 返回 key 对应 list 的长度
func LLen(key string) (int64, error) {
	return Rdb.LLen(ctx, key).Result()
}

func LRange(key string, begin, end int64) ([]int64, error) {
	reply, err := Rdb.LRange(ctx, key, begin, end).Result()
	if err != nil {
		return nil, err
	}

	var list = make([]int64, len(reply))
	for i, v := range reply {
		s, err := strconv.ParseInt(v, 10, 64)
		if err != nil {
			return nil, err
		}
		list[i] = s
	}
	return list, err
}

func LRangeString(key string, begin, end int64) ([]string, error) {
	return Rdb.LRange(ctx, key, begin, end).Result()
}

func ZAdd(key string, members ...*redis.Z) error {
	return Rdb.ZAdd(ctx, key, members...).Err()
}

func ZAddInt64(key string, score int64, member string) error {
	return ZAddFloat64(key, float64(score), member)
}

func ZAddFloat64(key string, score float64, member interface{}) error {
	return Rdb.ZAdd(ctx, key, &redis.Z{
		Score:  score,
		Member: member,
	}).Err()
}

// ZRem 删除名称为 key 的 zset 中的元素 member
func ZRem(key, member string) error {
	return Rdb.ZRem(ctx, key, member).Err()
}

// ZRemove 删除名称为 key 的 zset 中的元素 member 类型为interface
func ZRemove(key string, member interface{}) error {
	return Rdb.ZRem(ctx, key, member).Err()
}

func ZIncrBy(key, member string, incr float64) float64 {
	return Rdb.ZIncrBy(ctx, key, incr, member).Val()
}

// ZCard 返回集合中元素个数
func ZCard(key string) (int64, error) {
	return Rdb.ZCard(ctx, key).Result()
}

// ZCount 返回集合中 score 在给定区间的数量
func ZCount(key, min, max string) (int64, error) {
	return Rdb.ZCount(ctx, key, min, max).Result()
}

func ZScore(key, member string) (float64, error) {
	return Rdb.ZScore(ctx, key, member).Result()
}

func ZScoreInt64(key, member string) (int64, error) {
	f, err := ZScore(key, member)
	return int64(f), err
}

// ZRank 返回名称为 key 的 zset 中 member 元素的排名(按 score 从小到大排序)即下标
func ZRank(key, member string) (int64, error) {
	return Rdb.ZRank(ctx, key, member).Result()
}

// ZRevRank 返回名称为 key 的 zset 中 member 元素的排名(按 score 从大到小排序)即下标
func ZRevRank(key, member string) (int64, error) {
	return Rdb.ZRevRank(ctx, key, member).Result()
}

// ZRange 返回名称为 key 的 zset(按 score 从小到大排序)中的 index 从 start 到 end 的所有元素
func ZRange(key string, start, end int64) ([]string, error) {
	return Rdb.ZRange(ctx, key, start, end).Result()
}

// ZRangeWithScores 返回名称为 key 的 zset(按 score 从小到大排序)中的 index 从 start 到 end 的所有元素,结果包含 score 信息
func ZRangeWithScores(key string, start, end int64) ([]redis.Z, error) {
	return Rdb.ZRangeWithScores(ctx, key, start, end).Result()
}

// ZRevRange 返回名称为 key 的 zset(按 score 从大到小排序)中的 index 从 start 到 end 的所有元素
func ZRevRange(key string, start, end int64) ([]string, error) {
	return Rdb.ZRevRange(ctx, key, start, end).Result()
}
