[TOC]

# 一. Mysql编码与字符

在 mysql 中，一个中文汉字所占的字节数与编码格式有关：

- 如果是`GBK`编码，则一个中文汉字占2个字节，英文占1个字节；
- 如果是`UTF8`编码，则一个中文汉字占3个字节，而英文字母占1字节。

比如`varchar(32)`，表示这个可以存储32个字符, 也就是该字段可以存储32个中文, 或32 个英文, 或32个中英文的混合字符。但如果字符数超过 32 个的话就会报错。



# 二. MySQL数据类型

主要包括以下五大类：

- **整数：** BIT、~~BOOL~~、TINY INT、SMALL INT、MEDIUM INT、 INT、 BIG INT；

- **浮点数：** FLOAT、DOUBLE、DECIMAL；

- **字符串：** CHAR、VARCHAR、TINY TEXT、TEXT、MEDIUM TEXT、LONGTEXT、TINY BLOB、BLOB、MEDIUM BLOB、LONG BLOB；

- **日期/时间：** Date、Time、DateTime、TimeStamp、Year；

- **其他：** BINARY、VARBINARY、ENUM、SET、Geometry、Point、MultiPoint、LineString、MultiLineString、Polygon、GeometryCollection等。



## 2.1 整型

| MySQL数据类型 | 含义（有符号）                        |
| ------------- | ------------------------------------- |
| tinyint(m)    | 1个字节  范围(-128~127)               |
| smallint(m)   | 2个字节  范围(-32768~32767)           |
| mediumint(m)  | 3个字节  范围(-8388608~8388607)       |
| int(m)        | 4个字节  范围(-2147483648~2147483647) |
| bigint(m)     | 8个字节  范围(+-9.22*10的18次方)   |

**说明：**

- **bit**表示位类型，常用**bit(1)**表示布尔类型；
-  **bit**在查询字段时，可以使用bin()或hex()函数进行读取；
- **bool/boolean**等同于**tinyint**，一般不推荐使用;
- 加了**unsigned**表示符号位，如： **tinyint unsigned**的取值范围为`0~256`；
-  整型里面的程度**m**只会影响显示的宽度，并不影响实际的取值范围；



## 2.2 浮点型

| MySQL数据类型 | 含义                                      |
| ------------- | ----------------------------------------- |
| float(m,d)    | 单精度,  8位精度(4字节) , m总个数,d小数位 |
| double(m,d)   | 双精度, 16位精度(8字节) , m总个数,d小数位 |
| decimal(m,d)  | 定点数，m<65，m总个数,d小数位             |

**说明：**

- 浮点型在数据库中存放的是近似值，而定点类型在数据库中存放的是精确值；

-  `DECIMAL(5,1)` 的取值范围是 `-9999.9至9999.9` ；`DECIMAL(4,2)` 的取值范围是` -99.99至99.99`；

- 对货币等对精度敏感的数据，应该用定点数表示或存储。



## 2.3 字符串

| MySQL数据类型 | 含义                            |
| ------------- | ------------------------------- |
| char(n)       | 固定长度，最多255个字符         |
| varchar(n)    | 固定长度，最多65535个字符       |
| tinytext      | 可变长度，最多255个字符         |
| text          | 可变长度，最多65535个字符       |
| mediumtext    | 可变长度，最多2的24次方-1个字符 |
| longtext      | 可变长度，最多2的32次方-1个字符 |

**说明：**

- char(n)按照长度n存储，字符数小于n，则以空格补于其后，且n最大为255;

- varchar(n)是存入的实际字符数+1个字节（n<=255）或2个字节(n>255);

- text类型不能有默认值。 



## 2.4 日期/时间

| MySQL数据类型 | 含义                          |
| ------------- | ----------------------------- |
| date          | 日期 '2008-12-2'              |
| time          | 时间 '12:25:36'               |
| datetime      | 日期时间 '2008-12-2 22:06:44' |
| timestamp     | 自动存储记录修改时间          |

**说明：**

- 若定义一个字段为timestamp，这个字段里的时间数据会随其他字段修改的时候自动刷新




## 2.5 二进制

| MySQL数据类型 | 含义                            |
| ------------- | ------------------------------- |
| tityblob      | 不超过 255 个字符的二进制字符串 |
| blob          | 二进制形式的长文本数据          |
| mediumblob    | 二进制形式的中等长度文本数据    |
| longblob      | 二进制形式的极大文本数据        |

**说明：**

- _BLOB和_text存储方式不同，_TEXT以文本方式存储，英文存储区分大小写，而_Blob是以二进制方式存储，不分大小写。

- BLOB存储的数据只能整体读出。 

- _TEXT可以指定字符集，_BLO不用指定字符集。



# 三. MYSQL类型长度

各数据类型及字节长度一览表：

| 数据类型  |  字节长度     | 范围或用法 |
| ------------------ | -------- | ------------------------------------------------------------ |
| Bit                | 1        | 无符号[0,255]，有符号[-128,127]，天缘博客备注：BIT和BOOL布尔型都占用1字节 |
| TinyInt            | 1        | 整数[0,255]                                                  |
| SmallInt           | 2        | 无符号[0,65535]，有符号[-32768,32767]                        |
| MediumInt          | 3        | 无符号[0,2^24-1]，有符号[-2^23,2^23-1]]                      |
| Int                | 4        | 无符号[0,2^32-1]，有符号[-2^31,2^31-1]                       |
| BigInt             | 8        | 无符号[0,2^64-1]，有符号[-2^63 ,2^63 -1]                     |
| Float(M,D)         | 4        | 单精度浮点数。天缘博客提醒这里的D是精度，如果D<=24则为默认的FLOAT，如果D>24则会自动被转换为DOUBLE型。 |
| Double(M,D)        | 8        | 双精度浮点。                                                 |
| Decimal(M,D)       | M+1或M+2 | 未打包的浮点数，用法类似于FLOAT和DOUBLE，天缘博客提醒您如果在ASP中使用到Decimal数据类型，直接从数据库读出来的Decimal可能需要先转换成Float或Double类型后再进行运算。 |
| Date               | 3        | 以YYYY-MM-DD的格式显示，比如：2009-07-19                     |
| Date Time          | 8        | 以YYYY-MM-DD HH:MM:SS的格式显示，比如：2009-07-19 11：22：30 |
| TimeStamp          | 4        | 以YYYY-MM-DD的格式显示，比如：2009-07-19                     |
| Time               | 3        | 以HH:MM:SS的格式显示。比如：11：22：30                       |
| Year               | 1        | 以YYYY的格式显示。比如：2009                                 |
| Char(M)            | M        | 定长字符串。                                                 |
| VarChar(M)         | M        | 变长字符串，要求M<=255                                       |
| Binary(M)          | M        | 类似Char的二进制存储，特点是插入定长不足补0                  |
| VarBinary(M)       | M        | 类似VarChar的变长二进制存储，特点是定长不补0                 |
| Tiny Text          | Max:255  | 大小写不敏感                                                 |
| Text               | Max:64K  | 大小写不敏感                                                 |
| Medium Text        | Max:16M  | 大小写不敏感                                                 |
| Long Text          | Max:4G   | 大小写不敏感                                                 |
| TinyBlob           | Max:255  | 大小写敏感                                                   |
| Blob               | Max:64K  | 大小写敏感                                                   |
| MediumBlob         | Max:16M  | 大小写敏感                                                   |
| LongBlob           | Max:4G   | 大小写敏感                                                   |
| Enum               | 1或2     | 最大可达65535个不同的枚举值                                  |
| Set                | 可达8    | 最大可达64个不同的值                                         |
| Geometry           |          |                                                              |
| Point              |          |                                                              |
| LineString         |          |                                                              |
| Polygon            |          |                                                              |
| MultiPoint         |          |                                                              |
| MultiLineString    |          |                                                              |
| MultiPolygon       |          |                                                              |
| GeometryCollection |          |                                                              |


# 四. Mysql建表示例
```sql
CREATE database IF NOT EXISTS awesome DEFAULT charset utf8 collate utf8_general_ci;
# CREATE database IF NOT EXISTS awesome DEFAULT charset utf8mb4 collate utf8mb4_general_ci;
USE awesome;

drop table if exists table_demo;
CREATE TABLE table_demo
(
    id          int(0) not null auto_increment comment '主键',
    b_bit       bit(8)                           default 255 comment '位类型',
    b_bool      bit(1)                           default true comment '默认1位,表示布尔类型',
    n_tinyint   tinyint                          default 127 comment '1字节',
    n_smallint  smallint                         default 0 comment '2字节',
    n_mediumint mediumint                        default 0 comment '3字节',
    n_int       int                              default 0 comment '4字节',
    n_bigint    bigint                           default 0 comment '8字节',
    n_unsigned  tinyint unsigned                 default 255 comment '无符号',
    f_float     float(4, 2)                      default 99.99 comment '单精度(百分比)',
    f_double    double(5, 2)                     default 123.45 comment '双精度',
    f_decimal   decimal(18, 2)                   default 1.01 comment '定点数',
    s_char      char(5)                          default 'a' comment '固定长度存储,不大于255',
    s_varchar   varchar(5)                       default '12345' comment '按实际存储，且不大于设定长度',
    t_date      date                             default '2020:01:02' comment '日期',
    t_time      time                             default '15:04:05' comment '时间',
    t_datetime  datetime                         default current_timestamp comment '日期',
    t_timestamp timestamp                        default current_timestamp comment '时间戳(自动填充)',
    t_year      year                             default 2022 comment '年',
    b_binary    binary(20)                       default 'hello' comment '二进制字符串，最大255个字节',
    b_varbinary varbinary(20)                    default 'hello' comment '二进制字符串，max(65535)',
    b_tityblob  tinyint comment '最大255个字节',
    b_blob      blob comment '二进制长文本',
    o_enum      ENUM ('female','male','unknown') default 'unknown' comment '枚举(可插入下标或文本)',
    PRIMARY KEY (id)
) ENGINE = InnoDB DEFAULT CHARSET = utf8;
```

