/******************************************************************************************
 数据库操作知识：
一. CURD主要有Query和Exec两种操作

二. PlianText和Prepare操作方式
   prepared即带有占位符的sql语句)，客户端将该语句和参数发给mysql服务器,服务器编译成一个prepared语句，
   这个语句可以根据不同的参数多次调用。prepared语句执行的方式如下：
      1.准备prepare语句
      2.执行prepared语句和参数
      3.关闭prepared语句
   使用prepare语句方式，主要有两个好处：
      1. 避免拼接sql语句,规避了sql注入风险；
      2. 可以多次执行的sql语句；
   示例：
       db.Query("SELECT * FROM user WHERE gid = 1")     PlianText方式
       db.Query("SELECT * FROM user WHERE gid = ?", 1)  Prepare方式
   参考连接： https://www.jianshu.com/p/ee0d2e7bef54

三. 关于连接池的设置
    参考: https://www.cnblogs.com/rickiyang/p/12239907.html

四. 数据库操作中的3个Close
   1. db.Close()
   2. stmt.Close()
   3. rows.Close()
*****************************************************************************************/

/*
一. 安装驱动：
    go get -u github.com/lib/pq

二. 创建数据库表：
    CREATE database IF NOT EXISTS db_test DEFAULT charset utf8 collate utf8_general_ci;
    USE db_test;
    CREATE TABLE `users` (
        `id` int(11) NOT NULL AUTO_INCREMENT,
        `name` varchar(50) DEFAULT NULL,
        `addr` varchar(50) DEFAULT NULL,
        PRIMARY KEY (`id`)
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
    INSERT INTO `db_test`.`users` (`name`, `addr`) VALUES ('Tom','Newyork');
    SELECT * from `db_test`.`users`;
    -- truncate table `db_test`.`users`;

三. 驱动操作注意项：
    1. 参数占位符仅支持「?」符号
    2. 插入操作支持自增主键覆盖
    3. Scan是以内存地址的方式赋值(即对象字段赋值须使用地址符)
*/

package main

import (
	"database/sql"
	"fmt"
	"testing"

	_ "github.com/go-sql-driver/mysql"
)

type User struct {
	Id   int64  // 编号
	Name string // 姓名
	Addr string // 地址
}

// 数据源
const source = `root:123456@tcp(localhost:3306)/db_test?charset=utf8`

// 连接数据库
func OpenDB() *sql.DB {
	// Open函数并没有创建连接，它只是验证参数是否合法
	_db, err := sql.Open("mysql", source)
	if err != nil {
		panic(err)
	}
	_db.SetMaxOpenConns(20)
	_db.SetMaxIdleConns(10)
	return _db
}

// 1.测试数据库连接
func TestConnect(t *testing.T) {
	_db := OpenDB()
	defer _db.Close()

	if _db.Ping() == nil {
		fmt.Printf("数据库连接成功:%+v\n", _db.Stats())
	}
}

// 2.插入数据
func TestInsert(t *testing.T) {
	_db := OpenDB()
	defer _db.Close()

	// 隐式的Prepare操作
	res, _ := _db.Exec("insert into users values (?,?,?)", 0, "张三", "北京")
	fmt.Println(res.LastInsertId()) // 插入的ID
	fmt.Println(res.RowsAffected()) // 影响行数
}

// 3.修改数据
func TestUpdate(t *testing.T) {
	_db := OpenDB()
	defer _db.Close()

	res, _ := _db.Exec("update users set addr=? where name=?", "上海", "张三")
	fmt.Println(res.RowsAffected()) // 影响行数
}

// 4.删除数据
func TestDelete(t *testing.T) {
	_db := OpenDB()
	defer _db.Close()

	res, _ := _db.Exec("delete from users where id=?", 1)
	fmt.Println(res.RowsAffected()) // 影响行数
}

// 5.查询单值(总数)
func TestSingle(t *testing.T) {
	_db := OpenDB()
	defer _db.Close()

	var count int64
	_db.QueryRow("select count(*) from users where id>?", 0).Scan(&count)
	fmt.Println("总数：", count)
}

// 6.查询单行
func TestQueryFirst(t *testing.T) {
	_db := OpenDB()
	defer _db.Close()

	var u User
	_row := _db.QueryRow("select id,name,addr from users where id=?", 2)
	err := _row.Scan(&u.Id, &u.Name, &u.Addr) // 按内存地址赋值
	fmt.Println(u, err)
}

// 7.查询列表
func TestQueryList(t *testing.T) {
	_db := OpenDB()
	defer _db.Close()

	_rows, _ := _db.Query("select * from users where 1=1 limit ? offset ?;", 10, 0)
	defer _rows.Close() // 务必要关闭Rows

	var u User
	for _rows.Next() {
		_rows.Scan(&u.Id, &u.Name, &u.Addr)
		fmt.Println(u)
	}
}

// 8.执行事务
func TestTrans(t *testing.T) {
	_db := OpenDB()
	defer _db.Close()

	var err error
	tx, _ := _db.Begin()
	defer func(tx *sql.Tx) {
		if err != nil {
			fmt.Println("回滚事务：", tx.Rollback(), err.Error())
			return
		}
		fmt.Println("提交事务：", tx.Commit())
	}(tx)

	if _, err = tx.Exec(`insert into users(name,addr) values (?,?)`, "事务1", "北京"); err != nil {
		err = fmt.Errorf("事务1执行失败：%v", err)
		return
	}
	if _, err = tx.Exec(`insert into users(name,addr) values (?,?)`, "事务2", "上海"); err != nil {
		err = fmt.Errorf("事务2执行失败：%v", err)
		return
	}
}

// 9.(显式)自定义Prepare
func TestPrepare(t *testing.T) {
	_db := OpenDB()
	defer _db.Close()

	stmt, _ := _db.Prepare("insert into users values (?,?,?)")
	defer stmt.Close() // 务必关闭

	res, _ := stmt.Exec(0, "Prepare示例", "火星")
	fmt.Println(res.LastInsertId())
	fmt.Println(res.RowsAffected())
}
