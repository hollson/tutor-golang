

[TOC]

## 概要

[BoltDB](https://github.com/boltdb/bolt)是一个`嵌入式KV`持久化解决方案。它甚至不属于任何数据库，而仅相当于一个(Golang)map的扩展。

## 安装

```shell
go get github.com/boltdb/bolt/...
```

```shell
$ bolt help
Bolt is a tool for inspecting bolt databases.

Usage:
	bolt command [arguments]
The commands are:
    bench       run synthetic benchmark against bolt
    check       verifies integrity of bolt database
    compact     copies a bolt database, compacting it in the process
    info        print basic info
    help        print this screen
    pages       print list of pages with their types
    stats       iterate over all pages and generate usage stats

Use "bolt [command] -h" for more information about a command.
```



## 打开数据库

>   Bolt会获取文件锁，因此多个进程无法同时打开同一个数据库

```go
db, err := bolt.Open("bolt.db", 0600, &bolt.Options{Timeout: 1 * time.Second})
  if err != nil {
    log.Fatal(err)
}
defer db.Close()
```


>   设置`Options.ReadOnly`标记可以`共享/只读`方式打开数据库。只读模式使用`共享锁`来允许多个进程读取数据，并阻止任何进程以读写模式打开数据库。 

```go
db, err := bolt.Open("bolt.db", 0666, &bolt.Options{ReadOnly: true})
```

## 事务

单个事务和从它们创建的所有对象（例如存储桶、密钥）都不是线程安全的。  要处理多个goroutine 中的数据，您必须为每个goroutine 启动一个事务，或者使用锁定来确保一次只有一个 goroutine 访问一个事务。  从创建事务  `DB` 是线程安全的。 

只读事务和读写事务不应相互依赖，通常不应在同一个 goroutine 中同时打开。  这可能会导致死锁，因为读写事务需要定期重新映射数据文件，但在只读事务打开时无法这样做。 

### 读写事务

>   要启动读写事务，您可以使用  `DB.Update()` 功能： 

```go
err := db.Update(func(tx *bolt.Tx) error {
	return nil
})
```

在闭包内部，您拥有数据库的一致视图。  你通过返回来提交事务  `nil`在最后。  您还可以随时通过返回错误来回滚事务。  所有数据库操作都允许在读写事务中进行。 

始终检查返回错误，因为它会报告任何可能导致您的事务无法完成的磁盘故障。  如果您在闭包中返回错误，它将被传递。 

### 只读事务

>   要启动只读事务，您可以使用  `DB.View()` 功能： 

```go
err := db.View(func(tx *bolt.Tx) error {
	return nil
})
```

您还可以在此闭包中获得一致的数据库视图，但是，在只读事务中不允许进行任何更改操作。  您只能在只读事务中检索存储桶、检索值和复制数据库。 

### 批量读写

>   每个  `DB.Update()`等待磁盘提交写入。  通过将多个更新与  `DB.Batch()` 功能： 

```
err := db.Batch(func(tx *bolt.Tx) error {
	...
	return nil
})
```

并发批处理调用机会性地组合成更大的事务。  Batch 仅在有多个 goroutine 调用它时才有用。 

权衡是  `Batch`如果事务的某些部分失败，可以多次调用给定的函数。  函数必须是幂等的，副作用必须在成功返回后才生效  `DB.Batch()`. 

例如：不要从函数内部显示消息，而是在封闭范围内设置变量： 

```go
var id uint64
err := db.Batch(func(tx *bolt.Tx) error {
	// Find last key in bucket, decode as bigendian uint64, increment
	// by one, encode back to []byte, and add new key.
	...
	id = newValue
	return nil
})
if err != nil {
	return ...
}
fmt.Println("Allocated ID %d", id)
```

### 自定义事务

```go
// 参数bool表示事务是否应该是可写的
tx, err := db.Begin(true)
if err != nil {
    return err
}
defer tx.Rollback()

// Use the transaction...
_, err := tx.CreateBucket([]byte("MyBucket"))
if err != nil {
    return err
}

// Commit the transaction and check for error.
if err := tx.Commit(); err != nil {
    return err
}
```

## 存储桶

>    存储桶是数据库中键/值对的集合，存储桶中的所有键都必须是唯一的。

```go
db.Update(func(tx *bolt.Tx) error {
	b, err := tx.CreateBucket([]byte("MyBucket"))
	if err != nil {
		return fmt.Errorf("create bucket: %s", err)
	}
	return nil
})
```

您也可以仅在存储桶不存在时使用 `Tx.CreateBucketIfNotExists()`功能；要删除存储桶，只需调用  `Tx.DeleteBucket()` 功能。 

## 键值对

>   要将键/值对保存到存储桶，请使用  `Bucket.Put()` 功能： 

```go
db.Update(func(tx *bolt.Tx) error {
	b := tx.Bucket([]byte("MyBucket"))
	err := b.Put([]byte("answer"), []byte("42"))
	return err
})
```

这将设置  `"answer"` 关键  `"42"` 在里面  `MyBucket`桶。  要检索此值，我们可以使用  `Bucket.Get()` 功能： 

```go
db.View(func(tx *bolt.Tx) error {
	b := tx.Bucket([]byte("MyBucket"))
	v := b.Get([]byte("answer"))
	fmt.Printf("The answer is: %s\n", v)
	return nil
})
```

这  `Get()`函数不会返回错误，因为它的操作可以保证正常工作（除非出现某种系统故障）。  如果键存在，那么它将返回它的字节切片值。  如果它不存在，那么它将返回  `nil`.  请务必注意，您可以将零长度值设置为与不存在的键不同的键。 

使用  `Bucket.Delete()` 函数从桶中删除一个键。 

请注意，从返回的值  `Get()`仅在事务打开时有效。  如果您需要在事务之外使用一个值，那么您必须使用  `copy()` 将其复制到另一个字节切片。 

## 桶自增整数

通过使用  `NextSequence()`函数，您可以让 Bolt 确定一个序列，该序列可用作您的键/值对的唯一标识符。  请参阅下面的示例。 

```go
// CreateUser saves u to the store. The new user ID is set on u once the data is persisted.
func (s *Store) CreateUser(u *User) error {
    return s.db.Update(func(tx *bolt.Tx) error {
        // Retrieve the users bucket.
        // This should be created when the DB is first opened.
        b := tx.Bucket([]byte("users"))

        // Generate ID for the user.
        // This returns an error only if the Tx is closed or not writeable.
        // That can't happen in an Update() call so I ignore the error check.
        id, _ := b.NextSequence()
        u.ID = int(id)

        // Marshal user data into bytes.
        buf, err := json.Marshal(u)
        if err != nil {
            return err
        }

        // Persist bytes to users bucket.
        return b.Put(itob(u.ID), buf)
    })
}

// itob returns an 8-byte big endian representation of v.
func itob(v int) []byte {
    b := make([]byte, 8)
    binary.BigEndian.PutUint64(b, uint64(v))
    return b
}

type User struct {
    ID int
    ...
}
```


## 迭代键 

Bolt 将其键以字节排序的顺序存储在一个桶中。  这使得对这些键的顺序迭代非常快。  要迭代键，我们将使用 `Cursor`: 

```go
db.View(func(tx *bolt.Tx) error {
	// Assume bucket exists and has keys
	b := tx.Bucket([]byte("MyBucket"))

	c := b.Cursor()

	for k, v := c.First(); k != nil; k, v = c.Next() {
		fmt.Printf("key=%s, value=%s\n", k, v)
	}

	return nil
})
```

光标允许您移动到键列表中的特定点，并一次向前或向后移动一个键。 

光标上可以使用以下功能： 

```go
First()  Move to the first key.
Last()   Move to the last key.
Seek()   Move to a specific key.
Next()   Move to the next key.
Prev()   Move to the previous key.
```

这些函数中的每一个都有一个返回签名  `(key []byte, value []byte)`.  当您迭代到光标的末尾时  `Next()` 将返回一个 `nil`钥匙。  您必须使用  `First()`,  `Last()`， 或者  `Seek()` 打电话之前  `Next()` 或者  `Prev()`.  如果您不寻求位置，那么这些函数将返回一个  `nil` 钥匙。 

在迭代过程中，如果key是非 `nil` 但价值是  `nil`，这意味着键是指一个桶而不是一个值。  利用  `Bucket.Bucket()` 访问子存储桶。 



### Prefix scans

要迭代键前缀，您可以组合  `Seek()` 和  `bytes.HasPrefix()`: 

```go
db.View(func(tx *bolt.Tx) error {
	// Assume bucket exists and has keys
	c := tx.Bucket([]byte("MyBucket")).Cursor()

	prefix := []byte("1234")
	for k, v := c.Seek(prefix); k != nil && bytes.HasPrefix(k, prefix); k, v = c.Next() {
		fmt.Printf("key=%s, value=%s\n", k, v)
	}

	return nil
})
```


### 范围扫描 

另一个常见用例是扫描某个范围，例如时间范围。  如果您使用诸如 RFC3339 之类的可排序时间编码，那么您可以像这样查询特定的日期范围： 

```go
db.View(func(tx *bolt.Tx) error {
	// Assume our events bucket exists and has RFC3339 encoded time keys.
	c := tx.Bucket([]byte("Events")).Cursor()

	// Our time range spans the 90's decade.
	min := []byte("1990-01-01T00:00:00Z")
	max := []byte("2000-01-01T00:00:00Z")

	// Iterate over the 90's.
	for k, v := c.Seek(min); k != nil && bytes.Compare(k, max) <= 0; k, v = c.Next() {
		fmt.Printf("%s: %s\n", k, v)
	}

	return nil
})
```

请注意，虽然 RFC3339 是可排序的，但 RFC3339Nano 的 Golang 实现不使用小数点后的固定位数，因此不可排序。 


### ForEach() 

您还可以使用该功能  `ForEach()` 如果您知道您将遍历存储桶中的所有键： 

```go
db.View(func(tx *bolt.Tx) error {
	// Assume bucket exists and has keys
	b := tx.Bucket([]byte("MyBucket"))

	b.ForEach(func(k, v []byte) error {
		fmt.Printf("key=%s, value=%s\n", k, v)
		return nil
	})
	return nil
})
```

请注意中的键和值  `ForEach()`仅在事务打开时有效。  如果需要在事务之外使用键或值，则必须使用  `copy()` 将其复制到另一个字节切片。 


## 嵌套桶 

您还可以将存储桶存储在键中以创建嵌套存储桶。  该 API 与上的存储桶管理 API 相同  `DB` 目的： 

```go
func (*Bucket) CreateBucket(key []byte) (*Bucket, error)
func (*Bucket) CreateBucketIfNotExists(key []byte) (*Bucket, error)
func (*Bucket) DeleteBucket(key []byte) error
```

假设您有一个多租户应用程序，其中根级别存储桶是帐户存储桶。  在这个桶里面是一系列帐户，它们本身就是桶。  在序列存储区中，您可以有许多属于帐户本身（用户、注释等）的存储区，将信息隔离为逻辑分组。 

```go
// createUser creates a new user in the given account.
func createUser(accountID int, u *User) error {
    // Start the transaction.
    tx, err := db.Begin(true)
    if err != nil {
        return err
    }
    defer tx.Rollback()

    // Retrieve the root bucket for the account.
    // Assume this has already been created when the account was set up.
    root := tx.Bucket([]byte(strconv.FormatUint(accountID, 10)))

    // Setup the users bucket.
    bkt, err := root.CreateBucketIfNotExists([]byte("USERS"))
    if err != nil {
        return err
    }

    // Generate an ID for the new user.
    userID, err := bkt.NextSequence()
    if err != nil {
        return err
    }
    u.ID = userID

    // Marshal and save the encoded user.
    if buf, err := json.Marshal(u); err != nil {
        return err
    } else if err := bkt.Put([]byte(strconv.FormatUint(u.ID, 10)), buf); err != nil {
        return err
    }

    // Commit the transaction.
    if err := tx.Commit(); err != nil {
        return err
    }

    return nil
}
```

## 数据备份

Bolt 是单个文件，因此很容易备份。  您可以使用  `Tx.WriteTo()`函数将数据库的一致视图写入编写器。  如果您从只读事务调用它，它将执行热备份，而不会阻止您的其他数据库读取和写入。 

默认情况下，它将使用将利用操作系统的页面缓存的常规文件句柄。  见  [`Tx`](https://godoc.org/github.com/boltdb/bolt#Tx) 有关优化大于 RAM 数据集的信息的文档。 

一个常见的用例是通过 HTTP 进行备份，以便您可以使用诸如  `cURL` 做数据库备份： 

```go
func BackupHandleFunc(w http.ResponseWriter, req *http.Request) {
	err := db.View(func(tx *bolt.Tx) error {
		w.Header().Set("Content-Type", "application/octet-stream")
		w.Header().Set("Content-Disposition", `attachment; filename="bolt.db"`)
		w.Header().Set("Content-Length", strconv.Itoa(int(tx.Size())))
		_, err := tx.WriteTo(w)
		return err
	})
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}
```

然后您可以使用以下命令进行备份： 

```shell
$ curl http://localhost/backup > bolt.db
```

或者你可以打开浏览器  `http://localhost/backup` 它会自动下载。 

如果你想备份到另一个文件，你可以使用  `Tx.CopyFile()` 辅助功能。 



## 统计数据 

数据库会对其执行的许多内部操作进行运行计数，以便您可以更好地了解正在发生的事情。  通过在两个时间点抓取这些统计数据的快照，我们可以看到在该时间范围内执行了哪些操作。 

例如，我们可以启动一个 goroutine 每 10 秒记录一次统计信息： 

```go
go func() {
	// Grab the initial stats.
	prev := db.Stats()

	for {
		// Wait for 10s.
		time.Sleep(10 * time.Second)

		// Grab the current stats and diff them.
		stats := db.Stats()
		diff := stats.Sub(&prev)

		// Encode stats to JSON and print to STDERR.
		json.NewEncoder(os.Stderr).Encode(diff)

		// Save stats for the next loop.
		prev = stats
	}
}()
```

将这些统计信息通过管道传输到诸如 statsd 之类的服务以进行监控或提供将执行固定长度样本的 HTTP 端点也很有用。 



## 参考链接

https://www.cnblogs.com/X-knight/p/10753833.html




