package main

import (
    "fmt"
	"math"
	"strings"
	"time"

	_ "unsafe"
)

//go:noescape
//go:linkname nanotime runtime.nanotime
func nanotime() int64

const (
	format          = "2006-01-02 15:04:05"
	format_timezone = "2006-01-02 15:04:05 -0700"
)

func main() {
	{
		var t time.Time = time.Now()
		fmt.Println(t.Unix())
		fmt.Println(t.Date())
		fmt.Println(t.Clock())
		fmt.Println(t.Format(time.RFC822Z))
		fmt.Println(t.Format(time.RFC1123Z))
	}

	{
		var timestamp uint32
		timestamp = uint32(time.Now().Unix())
		t := time.Unix(int64(timestamp), 0)
		fmt.Println(t.Format(time.RFC1123Z))
	}

	{
		var t time.Time = time.Now()
		strDate := t.Format(format)
		fmt.Println(strDate)
		fmt.Println(time.Parse(format, strDate))
		fmt.Println(t, "NOTICE: timezone is LOCAL")

		strDate = t.Format(format_timezone)
		fmt.Println(strDate)
		fmt.Println(time.Parse(format_timezone, strDate))
		fmt.Println(t)
	}

	{
		fmt.Println(time.ParseDuration("1s"))
		fmt.Println(time.ParseDuration("1 s"))
		fmt.Println(time.ParseDuration(strings.Replace("1 s", " ", "", -1)))
		fmt.Println(time.ParseDuration(" 1  s "))
	}

	{
		fmt.Println(int64(time.Duration(nanotime())))
		time.Sleep(time.Second)
		fmt.Println(int64(time.Duration(nanotime())))
	}

	{
		// Max Int64 Timestamp
		timestamp := int64(math.MaxInt64)
		t := time.Unix(timestamp, 0)
		fmt.Println("MaxInt64 timestamp:", t.Format(time.RFC1123Z))
	}

	{
		fmt.Println("timezone")
		now := time.Now().Truncate(time.Second)
		japan, _ := time.LoadLocation("Japan")
		tokyo, _ := time.LoadLocation("Asia/Tokyo")
		loc9 := time.FixedZone("+09:00", 9*3600)
		fmt.Println("Local:\t", now, now.Unix())
		fmt.Println("Tokyo:\t", now.In(tokyo), now.In(tokyo).Unix())
		fmt.Println("Japan:\t", now.In(japan), now.In(japan).Unix())
		fmt.Println("09:00:\t", now.In(loc9), now.In(loc9).Unix())
	}
}
