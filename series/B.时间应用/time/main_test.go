package main

import (
    "math"
	"testing"
	"time"
)

func BenchmarkUnix(b *testing.B) {
	timestamp := int64(math.MaxInt64)
	for i := 0; i < b.N; i++ {
		time.Unix(timestamp, 0)
	}
}

func BenchmarkFormatRFC1123Z(b *testing.B) {
	timestamp := int64(math.MaxInt64)
	t := time.Unix(timestamp, 0)
	for i := 0; i < b.N; i++ {
		t.Format(time.RFC1123Z)
	}
}

func BenchmarkFormat(b *testing.B) {
	timestamp := int64(math.MaxInt64)
	t := time.Unix(timestamp, 0)
	for i := 0; i < b.N; i++ {
		t.Format(format)
	}
}

func BenchmarkFormatNow(b *testing.B) {
	t := time.Unix(time.Now().Unix(), 0)
	for i := 0; i < b.N; i++ {
		t.Format(format)
	}
}

func BenchmarkParse(b *testing.B) {
	strDate := "9999-01-01 00:00:00"
	for i := 0; i < b.N; i++ {
		_, err := time.Parse("2006-01-02 15:04:05", strDate)
		if err != nil {
			b.Fatal(err)
		}
	}
}
