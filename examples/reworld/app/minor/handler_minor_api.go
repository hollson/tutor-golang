package minor

import (
	"fmt"
	"strconv"
	"time"

	"github.com/sirupsen/logrus"

	"locojoy.com/packet/proto/go/pb"
	"locojoy.com/repo/repo_redis"
	"locojoy.com/system/config"
)

var (
	// MinorGameLimit = true                                           // 配置文件: 是否开启未成年游戏限定
	// OpenWeekDay    = []int{0, 5, 6}                                 // 配置文件: 开放星期几
	// OpenTimePeriod = []time.Duration{time.Hour * 20, time.Hour * 1} // 配置文件: 按日偏移量+时长
	OpenDayCache = make(map[int]*DayInfo) // 自定义特定开放日,格式如:20060102
	UpdateYear   = time.Now().Year()
)

// 日期类型
type HolidayType int

const (
	Normal  HolidayType = iota // 0: 工作日
	Weekend                    // 1: 周末
	Holiday                    // 2: 节假日
	Rest                       // 3: (节假日被)调休
	Specify                    // 4: 特殊指定节假日
)

func (h HolidayType) Open() bool {
	return h == Holiday || h == Specify
}

func (h HolidayType) String() string {
	return []string{"normal", "weekend", "holiday", "rest", "specify"}[h]
}

// 官方法定节假日(第三方接口)
type CalendarApi interface {
	Availability() bool                                // Api是否可用
	FlushCache(cache map[int]*DayInfo, year int) error // 将日历信息写入cache
}

// 未成年人游戏开放日
type DayInfo struct {
	Dot     int          `json:"dot"`     // 日期,如:20060102
	WeekDay time.Weekday `json:"weekday"` // 星期几
	Type    HolidayType  `json:"type"`    // 日期类型
	Mark    string       `json:"mark"`    // 备注
}

// 检查当前是否为开放时段
func IsOpenTimePeriod() bool {
	now := time.Now()
	zero := time.Date(now.Year(), now.Month(), now.Day(), 0, 0, 0, 0, time.Local)

	// 多个时段
	pair := len(config.Game.MinorOpenTimePeriod) / 2
	for i := 0; i < pair; i++ {
		start := zero.Add(time.Duration(config.Game.MinorOpenTimePeriod[2*i]) * time.Minute)
		end := start.Add(time.Duration(config.Game.MinorOpenTimePeriod[2*i+1]) * time.Minute)
		logrus.Infoln("open_time:", start.String(), end.String())
		if now.Unix() >= start.Unix() && now.Unix() < end.Unix() {
			return true
		}
	}
	return false
}

// 检查是否在开放星期段
func IsOpenWeekDay() bool {
	now := time.Now().Weekday()
	for _, w := range config.Game.MinorOpenWeekDay {
		if int(now) == w {
			return true
		}
	}
	return false
}

// 检查是否为法定节假日或特殊指定日
func HolidayInfo() (*DayInfo, error) {
	n, _ := strconv.Atoi(time.Now().Format("20060102"))
	v, ok := OpenDayCache[n]
	if ok {
		return v, nil
	}

	if len(OpenDayCache) > 0 && UpdateYear == time.Now().Year() {
		return nil, nil
	}

	var flush CalendarApi = new(Timor)
	if !flush.Availability() {
		return nil, fmt.Errorf("th3 holiday api is unvailable")
	}

	if err := flush.FlushCache(OpenDayCache, time.Now().Year()); err != nil {
		return nil, fmt.Errorf("holiday info get error %v", err)
	}

	UpdateYear = time.Now().Year()
	v, ok = OpenDayCache[n]
	if ok {
		return v, nil
	}
	return nil, fmt.Errorf("holiday data null finally")
}

// 未成年游戏开放时段检查(Step优先级逐渐)
func CanPlaying(acc string) (bool, string) {
	var msg string

	if !config.Game.MinorGameLimit {
		return true, "all time allow"
	}

	// 未成年实名校验
	nh := time.Now().Hour()
	if nh > 20 {
		if IsMinor(acc) {
			return false, ""
		}
	}
	return true, ""
	// if IsMinor(acc) {
	// 	if time.Now().Hour() > 20 {
	// 		return false, ""
	// 	}
	// 	return true, "non minor" //
	// 	// return false, "minor verify limited" // 非实名或年龄限制
	// }

	// Step1: 是否开启了未成年游戏限定
	if !config.Game.MinorGameLimit {
		return true, "all time allow"
	}

	// Step2: 法定节假日或特殊指定日
	h, err := HolidayInfo()
	if err != nil {
		logrus.Errorln(err)
	} else {
		if h != nil && h.Type.Open() {
			msg = h.Type.String()
			goto CHECKTIMEPERIOD
		}

		if h != nil && h.Type == Rest {
			return false, "holiday rest limited" // 非开放日被限制
		}
	}

	// Step3: 周内开放日
	if IsOpenWeekDay() {
		msg = "open week day"
		goto CHECKTIMEPERIOD
	}
	return false, "non open day limited" // 非开放日被限制

	// 检查时段
CHECKTIMEPERIOD:
	if !IsOpenTimePeriod() {
		return false, "time period limited" // 非开放时段被限制
	}
	return true, msg
}

// 监测未成年游戏开放时段检查,并处理玩家状态(踢出账号)
func MinorCheckProcess(acc string) {
	can, msg := CanPlaying(acc)
	logrus.Debugln(can, acc, msg)
	if can {
		return
	}

	battleInfo, err := repo_redis.RoomRedis.GetPlayerBattleInfo(acc)
	if err != nil || len(acc) == 0 {
		logrus.Errorf("%v,%v", acc, err)
		return
	}

	repo_redis.NotifyPhysicsKickPlayerDirected(acc, battleInfo, pb.KickType_MinorLimit)
	logrus.Infof("Minior_Kickout : %v,%v,%v,%v", acc, msg, battleInfo, err)
}
