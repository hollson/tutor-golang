package minor

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	
	"github.com/sirupsen/logrus"
	
	"locojoy.com/system/config"
)

type VerifyResponse struct {
	Code    int    `json:"code"` // 1:成功
	Message string `json:"message"`
	Data    struct {
		Verified int `json:"idCardVerify"`
		Age      int `json:"age"`
	} `json:"data"`
}

// 是否未成年人
func IsMinor(acc string) bool {
	var _url = fmt.Sprintf("http://%s/%s", config.System.PlatUrl, "acc/rpc/studio/v1/check/canPlay")
	data := make(url.Values)
	data.Set("openId", acc)
	
	pong, err := http.PostForm(_url, data)
	if err != nil || pong.StatusCode != 200 {
		logrus.Errorf("Verified 失败: %v,%v,%v", acc, _url, err)
		return false
	}
	defer pong.Body.Close()
	
	body, err := ioutil.ReadAll(pong.Body)
	if err != nil {
		logrus.Errorf("Verified ReadAll error : %v,%v,%v", acc, _url, err)
	}
	
	p := new(VerifyResponse)
	if err = json.Unmarshal(body, p); err != nil {
		logrus.Errorf("Unmarshal error : %v,%v,%v", acc, _url, err)
	}
	logrus.Debugf("%+v", *p)
	// 已实名认证且未成年
	if p.Code == 1 && p.Data.Verified == 1 && p.Data.Age < 18 {
		return true
	}
	return false
}
