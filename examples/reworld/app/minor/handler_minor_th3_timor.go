package minor

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"strconv"
	"strings"
	"time"
)

// https://www.jianshu.com/p/1e05e72b41fd
// https://www.jianshu.com/p/99931a7a4f0d
// http://tool.bitefu.net/jiari/?d=2020

// http://timor.tech/api/holiday/
// http://timor.tech/api/holiday/year
// http://timor.tech/api/holiday/year/2021/
// http://timor.tech/api/holiday/year/2020-02
// http://timor.tech/api/holiday/info/2021-9-26

// 如果第三方接口失效,则禁用该API
var _availibility = true

type TimorType struct {
	Type int    `json:"type"` // 节假日类型(0:工作日、1:周末、2:节日、3:调休)
	Name string `json:"name"` // 节假日类型中文名,可能值为 周一至周日、假期的名字、某某调休,
	Wage int    `json:"wage"` // 薪资倍数,1表示是1倍工资
	Week int    `json:"week"` // 一周中的第几天,值为1-7,分别表示周一至周日
}

type TimorHoliday struct {
	Holiday bool   `json:"holiday"` // true表示是节假日,false表示是调休
	Name    string `json:"name"`    // 节假日的中文名,如果是调休,则是调休的中文名,例如'国庆前调休'
	After   bool   `json:"after"`   // 只在调休下有该字段,true表示放完假后调休,false表示先调休再放假
	Target  string `json:"target"`  // 只在调休下有该字段,表示调休的节假日
}

type Timor struct {
	Code    int                     `json:"code"`    // 0服务正常;-1服务出错
	Type    map[string]TimorType    `json:"type"`    // 类型信息
	Holiday map[string]TimorHoliday `json:"holiday"` // 节假日信息
}

func (r *Timor) Availability() bool {
	return _availibility
}

func (r *Timor) FlushCache(cache map[int]*DayInfo, year int) error {
	_url := fmt.Sprintf("http://timor.tech/api/holiday/year/%d?type=Y", year)
	pong, err := http.Get(_url)
	if err != nil {
		_availibility = false
		return err
	}
	defer pong.Body.Close()
	
	body, err := ioutil.ReadAll(pong.Body)
	if err != nil {
		_availibility = false
		return err
	}
	t := new(Timor)
	json.Unmarshal(body, t)
	
	if t.Code > 0 {
		return fmt.Errorf("timor api error,%v", _url)
	}
	
	for k, ty := range t.Type {
		k = strings.Replace(k, "-", "", -1)
		key, err := strconv.Atoi(fmt.Sprintf("%d%s", year, k))
		if err != nil {
			return err
		}
		cache[key] = &DayInfo{
			Dot:     key,
			WeekDay: time.Weekday(ty.Week % 7),
			Type:    HolidayType(ty.Type),
			Mark:    "timor",
		}
	}
	return nil
}
