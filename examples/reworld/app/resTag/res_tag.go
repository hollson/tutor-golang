// 标签解析与应用

package resTag

import (
	"locojoy.com/common"
	"locojoy.com/packet/json_packet"
	"locojoy.com/system/table"
	"strings"
)

//fixme:此方法为平台所有商品标签的方法,适用于针对于平台的所有标签

//langArea 区域地址代码
//如果使用c=×gin.context 则可以用 api.GetLang(c) 获取langArea
func NewResTagPlat(langArea string) *ResTagPlat {
	return &ResTagPlat{
		LangArea: langArea,
	}
}

type ResTagPlat struct {
	LangArea string
}

func (r *ResTagPlat) LangAreaIsNotNil() bool {
	return r.LangArea != ""
}
func (r *ResTagPlat) GetLangArea() string {
	return r.LangArea
}

//通过标签id获取平台tag信息
func (r *ResTagPlat) GetPlatTagInfo(tag int32) json_packet.TagsInfo {
	// chain := &TagChain{}

	all := r.GetPlatAllTags()
	if all == nil {
		return json_packet.TagsInfo{}
	}

	// 三级
	for _, v := range all.Level3 {
		if tag == v.ID {
			return v
		}
	}

	// 二级
	if tag > 0 {
		for _, v := range all.Level2 {
			if tag == v.ID {
				return v
			}
		}
	}

	// 一级
	if tag > 0 {
		for _, v := range all.Level1 {
			if tag == v.ID {
				return v
			}
		}
	}
	return json_packet.TagsInfo{}
}

//通过标签tagId获取平台级联
func (r *ResTagPlat) GetPlatTagChain(tag int32) *TagChain {
	chain := &TagChain{}

	all := r.GetPlatAllTags()
	if all == nil {
		return nil
	}

	// 三级
	for _, v := range all.Level3 {
		if tag == v.ID {
			chain.Tags = append(chain.Tags, tag)
			chain.TagsTitles = append(chain.TagsTitles, v.Name)
			tag = v.FK
			break
		}
	}

	// 二级
	if tag > 0 {
		for _, v := range all.Level2 {
			if tag == v.ID {
				chain.Tags = append(chain.Tags, tag)
				chain.TagsTitles = append(chain.TagsTitles, v.Name)
				tag = v.FK
				break
			}
		}
	}

	// 一级
	if tag > 0 {
		for _, v := range all.Level1 {
			if tag == v.ID {
				chain.Tags = append(chain.Tags, tag)
				chain.TagsTitles = append(chain.TagsTitles, v.Name)
				tag = v.FK
				break
			}
		}
	}
	common.ReverseInt32(chain.Tags)
	common.ReverseStrArray(chain.TagsTitles)
	return chain
}

// 根据标签ID获取级联标签列表
func (r *ResTagPlat) ResTagsTier(tag int32, splitter string) (titles string) {
	chain := r.GetPlatTagChain(tag)
	if chain != nil {
		return strings.Join(chain.TagsTitles, splitter)
	}
	return ""
}

var cacheAllPlatTags = make(map[string]*RegsLevels, 0)

// 将DIY标签写入内存缓存
func (r *ResTagPlat) GetPlatAllTags() *RegsLevels {

	if r.LangAreaIsNotNil() {
		levels, ok := cacheAllPlatTags[r.LangArea]
		if ok {
			return levels
		}
	}

	data := new(RegsLevels)
	var lv1 []json_packet.TagsInfo
	slv1 := table.TblDataMgr.Get1PlatTagItem()
	for _, v := range slv1 {
		v.Name = i8in(r.LangArea, v.LID, v.Name)
		lv1 = append(lv1, v)
	}
	data.Level1 = lv1

	var lv2 []json_packet.TagsInfo
	slv2 := table.TblDataMgr.Get2PlatTagItem()
	for _, v := range slv2 {
		v.Name = i8in(r.LangArea, v.LID, v.Name)
		lv2 = append(lv2, v)
	}
	data.Level2 = lv2

	var lv3 []json_packet.TagsInfo
	slv3 := table.TblDataMgr.Get3PlatTagItem()
	for _, v := range slv3 {
		v.Name = i8in(r.LangArea, v.LID, v.Name)
		lv3 = append(lv3, v)
	}
	data.Level3 = lv3
	if r.LangAreaIsNotNil() {
		cacheAllPlatTags[r.GetLangArea()] = data
	}
	return data
}

// 根据标签ID获取Diy级联标签列表
func (r *ResTagPlat) PlatResTagsTier(tag int32) (nums, titles string) {
	chain := r.GetPlatTagChain(tag)
	if chain != nil {
		return common.JoinInt32(chain.Tags, "-"), strings.Join(chain.TagsTitles, "-")
	}
	return "", ""
}

//通过标签等级获取Diy标签信息
func (r *ResTagPlat) GetPlatTagsByLevel(level int32) []json_packet.TagsInfo {
	all := r.GetPlatAllTags()
	switch level {
	case 1:
		return all.Level1
	case 2:
		return all.Level2
	case 3:
		return all.Level3
	}
	return []json_packet.TagsInfo{}
}
