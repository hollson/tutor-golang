package resTag

import (
	"locojoy.com/packet/json_packet"
	"locojoy.com/system/plat"
)

type RegsLevels struct {
	Level1 []json_packet.TagsInfo `json:"level1" form:"level1"`
	Level2 []json_packet.TagsInfo `json:"level2" form:"level2"`
	Level3 []json_packet.TagsInfo `json:"level3" form:"level3"`
}

// 标签级联信息,如:{ tags:[12,77,238],TagsTitles:[服装,上衣,体恤]}
type TagChain struct {
	Tags       []int32  `json:"tags"`
	TagsTitles []string `json:"tags_titles"`
}

// 多语言转换器
// config.SERVER_AREA_ID // 1 中文 2 英文
func i8in(langArea string, langId int32, _default string) string {
	lan, err := plat.GetLangByLangId(langId, langArea)
	if err != nil || lan == "" {
		return _default
	}
	return lan
}
