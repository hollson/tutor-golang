/*******************************************************
  多语言翻译词汇使用规范:
     1. 消息对象的ID必须使用String类型
     2. 通用词汇可以覆盖状态码词汇
     3. 添加完词汇后需要提取并人工翻译
     4. 提取的语言表只需要翻译面向客户的词汇(小于1W)
*******************************************************/

package api

import (
	"github.com/hollson/i18n"
)

func Trans(code int) string {
	return ""
}

// 通用翻译词汇
var (
	Lang_Success = &i18n.Message{ID: "SUCCESS", Other: "成功"}
	Lang_Fail    = &i18n.Message{ID: "FAIL", Other: "失败"}
	Lang_Pass    = &i18n.Message{ID: "PASS", Other: "通过"}
	Lang_Reject  = &i18n.Message{ID: "REJECT", Other: "拒绝"}
)

// 与状态码映射的翻译词汇
var LangMap map[int]*i18n.Message

// 注意:
//  MessageID必须使用原生String类型,不能是如下形式:
//  e.g. &i18n.Message{ID:striconv.Itoa("1001") , Other: "hello"}
func init() {
	LangMap = make(map[int]*i18n.Message)

	// 基础状态码
	LangMap[CODE_UNKNOWN] = &i18n.Message{ID: "CODE_UNKNOWN", Other: "未知错误"}
	LangMap[CODE_OK] = &i18n.Message{ID: "CODE_OK", Other: "成功"}
	LangMap[CODE_NO] = &i18n.Message{ID: "CODE_NO", Other: "失败"}

	LangMap[CODE_1000] = &i18n.Message{ID: "CODE_1000", Other: "访问异常"}
	LangMap[CODE_1001] = &i18n.Message{ID: "CODE_1001", Other: "访问异常"}
	LangMap[CODE_1002] = &i18n.Message{ID: "CODE_1002", Other: "访问异常"}
	LangMap[CODE_1003] = &i18n.Message{ID: "CODE_1003", Other: "访问异常"}
	LangMap[CODE_1004] = &i18n.Message{ID: "CODE_1004", Other: "访问异常"}
	LangMap[CODE_1005] = &i18n.Message{ID: "CODE_1005", Other: "访问异常"}
	LangMap[CODE_1006] = &i18n.Message{ID: "CODE_1006", Other: "访问异常"}
	LangMap[CODE_1007] = &i18n.Message{ID: "CODE_1007", Other: "访问异常"}
	LangMap[CODE_1008] = &i18n.Message{ID: "CODE_1008", Other: "访问异常"}
	LangMap[CODE_1009] = &i18n.Message{ID: "CODE_1009", Other: "访问异常"}
	LangMap[CODE_1010] = &i18n.Message{ID: "CODE_1010", Other: "访问受限"}
	LangMap[CODE_1011] = &i18n.Message{ID: "CODE_1011", Other: "无效请求"}

	// 服务器信息
	LangMap[CODE_1100] = &i18n.Message{ID: "CODE_1100", Other: "服务端错误"}
	LangMap[CODE_1101] = &i18n.Message{ID: "CODE_1101", Other: "服务端错误"}
	LangMap[CODE_1102] = &i18n.Message{ID: "CODE_1102", Other: "服务端错误"}
	LangMap[CODE_1103] = &i18n.Message{ID: "CODE_1103", Other: "服务端错误"}

	// 文件处理
	LangMap[CODE_1200] = &i18n.Message{ID: "CODE_1200", Other: "请求错误"}
	LangMap[CODE_1201] = &i18n.Message{ID: "CODE_1201", Other: "请求错误"}
	LangMap[CODE_1202] = &i18n.Message{ID: "CODE_1202", Other: "请求错误"}
	LangMap[CODE_1203] = &i18n.Message{ID: "CODE_1203", Other: "请求错误"}
	LangMap[CODE_1204] = &i18n.Message{ID: "CODE_1204", Other: "请求错误"}
	LangMap[CODE_1205] = &i18n.Message{ID: "CODE_1205", Other: "请求错误"}
	LangMap[CODE_1206] = &i18n.Message{ID: "CODE_1206", Other: "文件上传失败"}
	LangMap[CODE_1207] = &i18n.Message{ID: "CODE_1207", Other: "文件下载失败"}
	LangMap[CODE_1208] = &i18n.Message{ID: "CODE_1208", Other: "文件不存在"}

	//交易信息
	LangMap[CODE_1300] = &i18n.Message{ID: "CODE_1300", Other: "余额不足"}
	LangMap[CODE_1301] = &i18n.Message{ID: "CODE_1301", Other: "金币不足"}
	LangMap[CODE_1302] = &i18n.Message{ID: "CODE_1302", Other: "积分不足"}
	LangMap[CODE_1303] = &i18n.Message{ID: "CODE_1303", Other: "账户冻结"}
	LangMap[CODE_1304] = &i18n.Message{ID: "CODE_1304", Other: "支付失败"}
	LangMap[CODE_1305] = &i18n.Message{ID: "CODE_1305", Other: "购买失败"}

	//素材信息
	LangMap[CODE_2000] = &i18n.Message{ID: "CODE_2000", Other: "素材不存在"}
	LangMap[CODE_2001] = &i18n.Message{ID: "CODE_2001", Other: "素材未通过审核"}

	//商品信息
	LangMap[CODE_2100] = &i18n.Message{ID: "CODE_2100", Other: "商品不存在"}
	LangMap[CODE_2101] = &i18n.Message{ID: "CODE_2101", Other: "商品已下架"}
	LangMap[CODE_2102] = &i18n.Message{ID: "CODE_2102", Other: "请勿重复购买"}
	LangMap[CODE_2103] = &i18n.Message{ID: "CODE_2103", Other: "店铺信息无效"}

	// 房间相关
	LangMap[CODE_2200] = &i18n.Message{ID: "CODE_2200", Other: "未找到房间"}
	LangMap[CODE_2201] = &i18n.Message{ID: "CODE_2201", Other: "房间已关闭"}
	LangMap[CODE_2202] = &i18n.Message{ID: "CODE_2202", Other: "房间已满"}
	LangMap[CODE_2203] = &i18n.Message{ID: "CODE_2203", Other: "玩家匹配失败"}
	LangMap[CODE_2204] = &i18n.Message{ID: "CODE_2204", Other: "单人房间禁入"}
	LangMap[CODE_2205] = &i18n.Message{ID: "CODE_2205", Other: "地图已经下架"}
	LangMap[CODE_2206] = &i18n.Message{ID: "CODE_2206", Other: "服务器错误"}

	// 换装相关
	LangMap[CODE_2300] = &i18n.Message{ID: "CODE_2300", Other: "商品兑换失败"}
	LangMap[CODE_2301] = &i18n.Message{ID: "CODE_2301", Other: "非抽奖商品"}
	LangMap[CODE_2302] = &i18n.Message{ID: "CODE_2302", Other: "您已经抽奖"}

	//多人编辑相关
	LangMap[CODE_2400] = &i18n.Message{ID: "CODE_2400", Other: "多人项目数量已达上限"}
	LangMap[CODE_2401] = &i18n.Message{ID: "CODE_2401", Other: "项目成员已满，无法邀请"}
	LangMap[CODE_2402] = &i18n.Message{ID: "CODE_2402", Other: "被邀请的成员已经存在于该项目中"}
	LangMap[CODE_2403] = &i18n.Message{ID: "CODE_2403", Other: "该项目不存在"}
	LangMap[CODE_2404] = &i18n.Message{ID: "CODE_2404", Other: "创建多人项目失败"}
	LangMap[CODE_2405] = &i18n.Message{ID: "CODE_2405", Other: "设置项目信息失败"}
	LangMap[CODE_2406] = &i18n.Message{ID: "CODE_2406", Other: "设置版本号失败"}
	LangMap[CODE_2407] = &i18n.Message{ID: "CODE_2407", Other: "上传图片失败"}
	LangMap[CODE_2408] = &i18n.Message{ID: "CODE_2408", Other: "邀请成员失败"}
	LangMap[CODE_2409] = &i18n.Message{ID: "CODE_2409", Other: "获取用户列表失败"}
	LangMap[CODE_2410] = &i18n.Message{ID: "CODE_2410", Other: "该项目不存在此成员"}
	LangMap[CODE_2411] = &i18n.Message{ID: "CODE_2411", Other: "将成员从多人项目移除失败"}
	LangMap[CODE_2412] = &i18n.Message{ID: "CODE_2412", Other: "修改成员权限失败"}
	LangMap[CODE_2413] = &i18n.Message{ID: "CODE_2413", Other: "退出多人编辑项目失败"}
	LangMap[CODE_2414] = &i18n.Message{ID: "CODE_2414", Other: "获取版本列表失败"}
	LangMap[CODE_2415] = &i18n.Message{ID: "CODE_2415", Other: "保存版本失败"}
	LangMap[CODE_2416] = &i18n.Message{ID: "CODE_2416", Other: "设置自动保存时间失败"}
	LangMap[CODE_2417] = &i18n.Message{ID: "CODE_2417", Other: "多人项目处于未开放状态"}
	LangMap[CODE_2418] = &i18n.Message{ID: "CODE_2418", Other: "已有其他成员打开项目的另外一个版本"}
	LangMap[CODE_2419] = &i18n.Message{ID: "CODE_2419", Other: "不能重复打开该房间"}
	LangMap[CODE_2420] = &i18n.Message{ID: "CODE_2420", Other: "多人项目正在编辑中，无法修改版本号"}
	LangMap[CODE_2421] = &i18n.Message{ID: "CODE_2421", Other: "创建合作项目失败，地图文件不完整"}
}
