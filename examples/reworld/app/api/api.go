package api

import (
	"bytes"
	"fmt"
	"locojoy.com/system/config"
	"net/http"
	"os"

	"github.com/gin-gonic/gin"
	"github.com/hollson/i18n"

	"locojoy.com/system/lang"
)

// 标准: Api必要的头部信息,用于不同版本,终端,环境的数据处理和统计.
type ApiHeader struct {
	ContentType string `json:"Content-Type" form:"Content-Type"`   // "application/x-protobuf"或其他
	Accept      string `json:"Accept" form:"Accept"`               // 可接受的数据类型,如"application/json"
	Auth        string `json:"Authorization" form:"Authorization"` // 认证方式: "Basic xxx","Jwt xxx","Sign xxx","Token xxx"
	Lang        string `json:"X-Lang" form:"X-Lang"`               // 语言环境(zh,en)
	Driver      string `json:"driver" form:"driver"`               // 编译器,安卓,ios,浏览器,未知
	Account     string `json:"account"`                            // 用户编号(唯一标识)
}

// API请求参数
type BaseRequest struct {
	Account string `json:"account" form:"account"` // 账号
}

// API请求参数
type PagedRequest struct {
	BaseRequest
	PageNum  int32 `json:"pageNum" form:"pageNum"`   // 分页编号(从1开始)
	PageSize int32 `json:"pageSize" form:"pageSize"` // 分页尺寸
}

// 默认OK
func DefaultResult() *ApiResult {
	obj := &ApiResult{
		Code: CODE_OK,
		Msg:  "",
	}
	return obj
}

// 嵌套指针对象的输出(避免只输出指针)
func (p *ApiResult) String() string {
	return fmt.Sprintf("&%+v", *p)
}

// Response元数据
//  描述业务数据的数据,即数据说明书,如:照片是业务数据,EXIF(设备型号,光圈系数,感光度,曝光度,时间等)是元数据
func (p *ApiResult) Response(c *gin.Context) {
	// 从系统或环境变量读取服务器元数据,如:当前服务器是[东8区],[国内生产环境]
	c.Header("Meta-Env", "CST +0800;zh-prod;")

	if len(p.Msg) == 0 {
		langCode := ""
		switch {
		case len(c.PostForm("lang")) > 0:
			langCode = c.PostForm("lang")
		case len(c.Query("lang")) > 0:
			langCode = c.Query("lang")
		case len(c.GetHeader("lang")) > 0:
			langCode = c.GetHeader("lang")
		case len(c.GetHeader("Accept-Language")) > 0:
			langCode = c.GetHeader("Accept-Language")
		default:
			langCode = os.Getenv("REBUILD_LANG")
		}

		msg, ok := LangMap[p.Code]
		if ok {
			p.Msg = lang.Localizer(lang.LangCode(langCode)).MustLocalize(&i18n.LocalizeConfig{DefaultMessage: msg})
		} else {
			p.Msg = fmt.Sprintf("err code %d", p.Code)
		}
	}

	c.JSON(http.StatusOK, p)
}

func (p *ApiResult) ErrorWithPing(c *gin.Context) {
	p.Code = CODE_1102
	if len(p.Msg) == 0 {
		p.Msg = "ping error"
	}
	c.AbortWithStatusJSON(http.StatusInternalServerError, p)
}

func PostHandler(url string, data []byte) (r *http.Response, e error) {
	body := bytes.NewReader(data)
	request, err := http.NewRequest("POST", url, body)

	if err != nil {
		return nil, err
	}

	resp, err := http.DefaultClient.Do(request)
	return resp, err
}

//获取请求的语言
func GetLang(c *gin.Context) string {
	//如果找不到默认返回中文
	langCode := lang.ZH.String()
	switch {
	case len(c.PostForm("lang")) > 0:
		langCode = c.PostForm("lang")
	case len(c.Query("lang")) > 0:
		langCode = c.Query("lang")
	case len(c.GetHeader("lang")) > 0:
		langCode = c.GetHeader("lang")
	case len(c.GetHeader("Accept-Language")) > 0:
		langCode = c.GetHeader("Accept-Language")
	default:
		langCode = os.Getenv("REBUILD_LANG")
	}
	if langCode == "" {
		langCode = lang.ZH.String()
	}
	return langCode
}

//通过请求地区和code码获取message
func GetLangCodeMessage(c *gin.Context, code int64) string {
	langCode := GetLang(c)
	msg, ok := LangMap[int(code)]
	if ok {
		return lang.Localizer(lang.LangCode(langCode)).MustLocalize(&i18n.LocalizeConfig{DefaultMessage: msg})
	}
	return fmt.Sprintf("err code %d", code)
}

//判断地区需要分配的agones，如果没有的话默认分配给defaule
func GetAreaAgones(area string) string {
	a, ok := config.System.AreaAgonesMap[area]
	if ok {
		return a
	}
	return area
}
