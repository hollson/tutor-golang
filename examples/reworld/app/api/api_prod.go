// +build !pprof

//go:generate go build --tags=prod
package api

// Api响应
type ApiResult struct {
	Code  int         `json:"code"`           // 错误码
	Msg   string      `json:"message"`        // 错误消息(面向客户端的消息,可隐藏服务端错误细节)
	Debug string      `json:"-"`              // 开发环境中输出err信息,生产环境中会忽略该字段
	Data  interface{} `json:"data,omitempty"` // 数据
}
