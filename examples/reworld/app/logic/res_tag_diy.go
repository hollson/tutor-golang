// Diy相关的标签解析与应用

package logic

import (
	"strconv"
	"strings"
	
	"locojoy.com/common"
	"locojoy.com/packet/json_packet"
	"locojoy.com/system/table"
)

// DIY所包含的标签(上装110、下装111、鞋112、表情109、舞蹈127、表情泡129)
var (
	DiyTags        = []string{"110", "111", /*"112",*/ "109", "127", "129"}
	DiyDance int32 = 127
)

var cacheDiyTags *RegsLevels

// 将DIY标签写入内存缓存
func GetAllDiyTags() *RegsLevels {
	if cacheDiyTags != nil {
		return cacheDiyTags
	}
	
	data := new(RegsLevels)
	data.Level1 = table.TblDataMgr.Get1TagItem()
	for k, v := range data.Level1 {
		data.Level1[k].Name = i8in(v.LID, v.Name)
	}
	
	var lv2 []json_packet.TagsInfo
	slv2 := table.TblDataMgr.Get2TagItem()
	for _, v := range slv2 {
		if common.ExistOfstring(strconv.Itoa(int(v.ID)), DiyTags...) {
			v.Name = i8in(v.LID, v.Name)
			lv2 = append(lv2, v)
		}
	}
	data.Level2 = lv2
	
	var lv3 []json_packet.TagsInfo
	slv3 := table.TblDataMgr.Get3TagItem()
	for _, v := range slv3 {
		if common.ExistOfstring(strconv.Itoa(int(v.FK)), DiyTags...) {
			v.Name = i8in(v.LID, v.Name)
			lv3 = append(lv3, v)
		}
	}
	data.Level3 = lv3
	cacheDiyTags = data
	return data
}

func GetDiyTagChain(tag int32) *TagChain {
	chain := &TagChain{}
	
	all := GetAllDiyTags()
	if all == nil {
		return nil
	}
	
	// 三级
	for _, v := range all.Level3 {
		if tag == v.ID {
			chain.Tags = append(chain.Tags, tag)
			chain.TagsTitles = append(chain.TagsTitles, v.Name)
			tag = v.FK
			break
		}
	}
	
	// 二级
	if tag > 0 {
		for _, v := range all.Level2 {
			if tag == v.ID {
				chain.Tags = append(chain.Tags, tag)
				chain.TagsTitles = append(chain.TagsTitles, v.Name)
				tag = v.FK
				break
			}
		}
	}
	
	// 一级
	if tag > 0 {
		for _, v := range all.Level1 {
			if tag == v.ID {
				chain.Tags = append(chain.Tags, tag)
				chain.TagsTitles = append(chain.TagsTitles, v.Name)
				tag = v.FK
				break
			}
		}
	}
	common.ReverseInt32(chain.Tags)
	common.ReverseStrArray(chain.TagsTitles)
	return chain
}

// 根据标签ID获取级联标签列表
func DiyResTagsTier(tag int32) (nums, titles string) {
	chain := GetDiyTagChain(tag)
	if chain != nil {
		return common.JoinInt32(chain.Tags, "-"), strings.Join(chain.TagsTitles, "-")
	}
	return "", ""
}
