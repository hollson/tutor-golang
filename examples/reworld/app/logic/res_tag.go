// 标签解析与应用

package logic

import (
	"strings"

	"locojoy.com/common"
	"locojoy.com/packet/json_packet"
	"locojoy.com/system/config"
	"locojoy.com/system/table"
)

var cacheAllTags *RegsLevels

// fixme: 使用策略模式
var En = map[int32]string{
	3213: "Facility",
	3214: "Props",
	3353: "Hoodie",
	3215: "Creatures",
	3216: "Make up",
	3217: "Action",
	3218: "Effects",
	3219: "2D image",
	3220: "Audios",
	3431: "MeshPart",
	3448: "Bubble",
	3208: "TextureId",
	3230: "Tops",
	3231: "Bottoms",
	3232: "Shoes",
	200:  "Dance",
	3349: "Bubbles",
	3294: "T-shirt",
	3295: "Shirts",
	3296: "Sweaters",
	3297: "Outwear",
	3298: "Suits",
	3299: "Undergarment",
	3357: "Cropped Trousers",
	3360: "Midi Skirt",
	3359: "Mini Skirts",
	3300: "Skirts",
	3301: "Shorts",
	3302: "Pants",
	3303: "Dress Pants",
	3304: "Underwear",
	3305: "Leather Shoes",
	3306: "Sandals",
	3307: "Sneakers",
	3308: "Functional Shoes",
	3343: "Mood",
	3344: "Adorable",
	3345: "Funny",
	3346: "Celebrities",
	3347: "Blessing",
	3348: "Greet",
	3362: "Full Dress",
	3354: "Vest",
	3355: "Camisole",
	3351: "Waistcoat",
	3361: "Maxiskit",
	3363: "Dresses",
	3365: "Long Gown",
	3366: "Flat Shoes",
	3367: "Ankle Boot",
	3368: "Kinky Boots",
	3421: "Other",
}

// 多语言转换器
// config.SERVER_AREA_ID // 1 中文 2 英文
func i8in(langCode int32, _default string) string {
	if config.System.AreaId == 2 {
		return En[langCode]
	}
	return _default
}

type RegsLevels struct {
	Level1 []json_packet.TagsInfo `json:"level1" form:"level1"`
	Level2 []json_packet.TagsInfo `json:"level2" form:"level2"`
	Level3 []json_packet.TagsInfo `json:"level3" form:"level3"`
}

// 标签级联信息,如:{ tags:[12,77,238],TagsTitles:[服装,上衣,体恤]}
type TagChain struct {
	Tags       []int32  `json:"tags"`
	TagsTitles []string `json:"tags_titles"`
}

// 解析策划表
func GetAllTags() *RegsLevels {
	if cacheAllTags != nil {
		return cacheAllTags
	}

	data := new(RegsLevels)
	data.Level1 = table.TblDataMgr.Get1TagItem()
	for k, v := range data.Level1 {
		data.Level1[k].Name = i8in(v.LID, v.Name)
	}

	var lv2 []json_packet.TagsInfo
	slv2 := table.TblDataMgr.Get2TagItem()
	for _, v := range slv2 {
		v.Name = i8in(v.LID, v.Name)
		lv2 = append(lv2, v)

	}
	data.Level2 = lv2

	var lv3 []json_packet.TagsInfo
	slv3 := table.TblDataMgr.Get3TagItem()
	for _, v := range slv3 {
		v.Name = i8in(v.LID, v.Name)
		lv3 = append(lv3, v)
	}
	data.Level3 = lv3
	cacheAllTags = data
	return data
}
func GetTagInfo(tag int32) json_packet.TagsInfo {
	// chain := &TagChain{}

	all := GetAllTags()
	if all == nil {
		return json_packet.TagsInfo{}
	}

	// 三级
	for _, v := range all.Level3 {
		if tag == v.ID {
			return v
		}
	}

	// 二级
	if tag > 0 {
		for _, v := range all.Level2 {
			if tag == v.ID {
				return v
			}
		}
	}

	// 一级
	if tag > 0 {
		for _, v := range all.Level1 {
			if tag == v.ID {
				return v
			}
		}
	}
	return json_packet.TagsInfo{}
}

func GetTagChain(tag int32) *TagChain {
	chain := &TagChain{}

	all := GetAllTags()
	if all == nil {
		return nil
	}

	// 三级
	for _, v := range all.Level3 {
		if tag == v.ID {
			chain.Tags = append(chain.Tags, tag)
			chain.TagsTitles = append(chain.TagsTitles, v.Name)
			tag = v.FK
			break
		}
	}

	// 二级
	if tag > 0 {
		for _, v := range all.Level2 {
			if tag == v.ID {
				chain.Tags = append(chain.Tags, tag)
				chain.TagsTitles = append(chain.TagsTitles, v.Name)
				tag = v.FK
				break
			}
		}
	}

	// 一级
	if tag > 0 {
		for _, v := range all.Level1 {
			if tag == v.ID {
				chain.Tags = append(chain.Tags, tag)
				chain.TagsTitles = append(chain.TagsTitles, v.Name)
				tag = v.FK
				break
			}
		}
	}
	common.ReverseInt32(chain.Tags)
	common.ReverseStrArray(chain.TagsTitles)
	return chain
}

// 根据标签ID获取级联标签列表
func ResTagsTier(tag int32, splitter string) (titles string) {
	chain := GetTagChain(tag)
	if chain != nil {
		return strings.Join(chain.TagsTitles, splitter)
	}
	return ""
}
