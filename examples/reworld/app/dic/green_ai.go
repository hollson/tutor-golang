package dic

// Deprecated,Plat定义的审核类型,统一(平铺)为CheckAspect
//  审核类型: 0:文本;1:图片
type CheckType int

// Deprecated: Plat定义的文本审核枚举类型
//  (优化)推荐使用统一化的 CheckAspect
type PlatAspect int

const (
	Sign        PlatAspect = 1 + iota // 个性签名
	Post                              // 帖子
	PostDiscuss                       // 帖子评论
	GameDiscuss                       // 游戏评论
	UserNick                          // 用户昵称
	DiyText                           // Diy文本
	Studio                            // studio
	DiyTitle                          // Diy名称
	ResName                           // 素材名称
	ResDesc                           // 素材描述
	GameName                          // 游戏名称
	GameDesc                          // 游戏描述
	StoreName                         // 店铺名称
	StoreDesc                         // 店铺描述
	ImText                            // IM文本
)

// Deprecated: Plat定义的图片审核枚举类型
type PlatCheckImage int

const (
	StoreLogo PlatAspect = iota + 1 // 店铺Logo
	StoreFace                       // 店铺封面
	GameFace                        // 游戏封面
	ImImage                         // IM图片
	ResThumb                        // 素材图片
)

// ==================================Reworld审核枚举====================================
// 推荐标准
type CheckAspect int

// 素材
const (
	Aspect_Unknown   CheckAspect = iota // 0: 未知(用于参数校验)
	Aspect_ResName                      // 1: 素材名称
	Aspect_ResThumb                     // 2: 素材图片
	Aspect_ResDesc                      // 3: 素材描述
	Aspect_ResCarve                     // 4: 图片(浮雕)文字/DIY文本
	Aspect_ResModel                     // 5: 素材模型
	Aspect_ResAction                    // 6: 动作文件
)

// 游戏
const (
	Aspect_GameName    CheckAspect = iota + 10 // 10: 游戏名称
	Aspect_GameLogo                            // 11: 游戏Logo
	Aspect_GameFace                            // 12: 游戏封面(图片)
	Aspect_GameDesc                            // 13: 游戏描述
	Aspect_GameDiscuss                         // 14: 游戏评论
)

// 店铺
const (
	Aspect_StoreName CheckAspect = iota + 20 // 20: 店铺名称
	Aspect_StoreLogo                         // 21: 店铺Logo
	Aspect_StoreFace                         // 22: 店铺封面
	Aspect_StoreDesc                         // 23: 店铺描述
)

// 用户
const (
	Aspect_UserNick  CheckAspect = iota + 30 // 30: 用户昵称
	Aspect_UserCover                         // 31: 用户头像
	Aspect_UserSign                          // 32: 个性签名
)

// 聊天
const (
	Aspect_IMContent CheckAspect = iota + 40 // 40: IM内容
	Aspect_IMImage                           // 41: IM图片
)

// 聊天
const (
	Aspect_Post        CheckAspect = iota + 50 // 50: 帖子
	Aspect_PostDiscuss                         // 51: 帖子评论
)
