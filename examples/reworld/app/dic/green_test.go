package dic

import (
	"fmt"
	"testing"
)

func TestGetGreenRules(t *testing.T) {
	ids := "[1,27,281]"
	ret := GetGreenRules(ids)
	fmt.Printf("len=%d, titles=%v\n", len(ret), ret)

	ids = "[25,999]"
	ret = GetGreenRules(ids)
	fmt.Printf("len=%d, titles=%v\n", len(ret), ret)

	ids = ""
	ret = GetGreenRules(ids)
	fmt.Printf("len=%d, titles=%v\n", len(ret), ret)
}
