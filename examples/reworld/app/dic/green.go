package dic

import (
	"strconv"
	"strings"
)

// (素材)人工审核结果 - 绿网处理: 审核违规示例(与平台同步)
var CheckRules = map[int]string{
	1:  "Pornography",                                //色情
	2:  "Gamble",                                     //赌博
	3:  "Tobacco, Alcohol and Drugs",                 //烟酒毒品
	4:  "Personal Information",                       //个人信息
	5:  "Political Sensitivity & Reactionary Speech", //涉党涉政 反动言论
	6:  "Dangerous Information",                      //危险信息
	7:  "Infringement",                               //侵权行为
	10: "Nudity",                                     //裸露
	11: "Sex",                                        //性交
	12: "Contraceptives",                             //生计用品
	13: "Related Content",                            //相关内容
	20: "Gambling Behavior",                          //赌博行为
	21: "Gambling Supplies",                          //赌博道具
	30: "Tobacco",                                    //烟
	31: "Alcohol",                                    //酒
	32: "Drugs",                                      //毒品
	40: "Display personal information",               //展示个人信息
	41: "Display personal communications",            //展示通讯方式
	42: "Seek romantic relationship",                 //索求浪漫关系
	50: "Political Sensitivity",                      //涉党涉政
	51: "Discrimination",                             //表达歧视
	52: "Reactionary Speech",                         //反动言论
	53: "Reactionary Inclination",                    //反动意愿
	54: "Feudal Superstition",                        //封建迷信
	60: "Inappropriate Words/Insulting Pictures",     //不当词汇/侮辱画面
	61: "Violence",                                   //暴力血腥
	62: "Self-injury & Suicide",                      //自残自杀
	63: "Dangerous Behaviors",                        //危险行为
	64: "Crime Incitement",                           //教唆犯罪
	70: "Infringement",                               //侵权行为
	71: "Business Propaganda",                        //引流行为
	// 以下为机审意见
	1001: "色情",
	1002: "暴恐",
	1005: "政治敏感",
	1016: "敏感旗帜标志",
}

// 获取审核违规内容(ids的格式如:"[1,2,14,212]")
func GetGreenRules(ids string) []string {
	ret := make([]string, 0)
	raw := strings.Trim(ids, "[]")
	idsArr := strings.Split(raw, ",")
	for _, v := range idsArr {
		n, err := strconv.Atoi(v)
		if err != nil {
			continue
		}
		if transTitle, ok := CheckRules[n]; ok {
			ret = append(ret, transTitle)
		}
	}
	return ret
}
