package middle

import (
	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
)

// Http请求拦截
func Hook() gin.HandlerFunc {
	return func(c *gin.Context) {
		logrus.Tracef(" ==> [%s] %s", c.Request.Method, c.Request.URL.Path) // 记录请求地址
	}
}
