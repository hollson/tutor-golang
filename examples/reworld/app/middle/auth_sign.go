package middle

import (
	"crypto/sha256"
	"errors"
	"fmt"
	"strconv"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"

	"locojoy.com/app/api"
)

var AppClients map[int64]string

func init() {
	AppClients = map[int64]string{
		100000: "07AF343694253A223B6B59FD49A677986B3BBC9B1CA14B75C8EC5A9B4E70DC4A", // 测试
		100011: "21540AFF7C6DBB5043AEB5F676C51DBD8B122AC30D8E581485C0DB8511BE06EA", // 移动端
		100012: "C5EB30C4B0D15BCDF2CFA424A00E963413055CC34033C0A57E721A08E7CC15C7", // 编辑器
		100099: "4B6A2FF7EB8684AFB10D00FCA3289183BBE9E673D7C4F1CF63C53570A1285538", // 预留
	}
}

// OpenApi校验参数
// 参数要求：
//    1. 使用 application/x-www-form-urlencoded传输格式；
//    2. 可使用URL或Form传参(POST方法)，参数区分大小写；
//    3. FORM优先于URL，建议Auth参数使用URL，普通参数使用FORM；
// 如: curl -H "Content-Type:application/x-www-form-urlencoded" -X POST \
//     "http://localhost/a.html?appKey=111&timestamp=222&nonce=333&sign=ABC" \
//     -d "title=aaa&type=bbb"
type ApiAuth struct {
	AppKey    int64  `json:"appKey" form:"appKey"`
	Timestamp int64  `json:"timestamp" form:"timestamp"`
	Nonce     int64  `json:"nonce" form:"nonce"`
	Sign      string `json:"sign" form:"sign"`
}

// 中间件： 验证API签名
func Auth() gin.HandlerFunc {
	return func(c *gin.Context) {
		var (
			auth = new(ApiAuth)
			pong = api.DefaultResult()
			err  error
		)

		defer func() {
			if err != nil {
				logrus.WithField("module", "auth").Errorf("auth=%+v err=%v", auth, err)
				pong.Response(c)
				c.Abort()
			}
			if fail := recover(); fail != nil {
				logrus.WithField("module", "auth").Errorf("auth=%+v fail=%v", auth, fail)
				pong.Code = api.CODE_1102
				pong.Response(c)
				c.Abort()
			}
			// c.Next()
		}()

		// 分页参数验证 fixme:单独抽离为Check中间件
		if val, has := c.GetPostForm("pageNum"); has {
			if pn, _err := strconv.Atoi(val); _err != nil || pn < 1 {
				err = fmt.Errorf("pageNum error: %+v", _err)
				pong.Code = api.CODE_1003
				pong.Debug = "pageNum cannot be less than 1"
				return
			}
			if val, has := c.GetPostForm("pageSize"); has {
				if pn, _err := strconv.Atoi(val); _err != nil || pn < 1 {
					err = fmt.Errorf("pageSize error: %+v", _err)
					pong.Code = api.CODE_1003
					pong.Debug = "pageSize cannot be less than 1"
					return
				}
			} else {
				err = errors.New("err code 1003")
				pong.Code = api.CODE_1003
				return
			}
		}

		// 绑定URL
		err = c.BindQuery(auth)
		if err != nil {
			pong.Code = api.CODE_1005
			return
		}

		// 测试账号免授权验证
		if auth.AppKey == 100000 && auth.Sign == "!QAZ@WSX" {
			return
		}

		// 测试账号免授权验证
		if auth.AppKey == 100099 && auth.Sign == "22AC30D8E581485C" {
			return
		}

		// 参数校验
		err = reqValidate(auth)
		if err != nil {
			pong.Code = api.CODE_1005
			return
		}

		// 签名验证
		err = authValidate(auth)
		if err != nil {
			pong.Code = api.CODE_1005
			return
		}
	}
}

// 参数校验
func reqValidate(auth *ApiAuth) error {
	if auth.AppKey == 0 || len(auth.Sign) == 0 || auth.Timestamp < time.Now().Add(-time.Hour).Unix() {
		return errors.New("授权参数有误")
	}
	return nil
}

// 授权验证
func authValidate(auth *ApiAuth) error {
	var raw = fmt.Sprintf("%d.%d.%d.%s", auth.AppKey, auth.Timestamp, auth.Nonce, AppClients[auth.AppKey])
	if SHA256([]byte(raw)) == (auth.Sign) {
		return nil
	}
	return errors.New("授权验证失败")
}

func SHA256(data []byte) string {
	sum := sha256.Sum256(data)
	return fmt.Sprintf("%x", sum)
}
