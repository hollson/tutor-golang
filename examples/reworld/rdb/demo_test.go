package rdb

import (
	"fmt"
	"log"
	"math/rand"
	"testing"
	"time"
	
	"github.com/go-redis/redis/v8"
)

// var _rdb *redis.Client // 单机/哨兵
var _rdb *redis.ClusterClient // 集群
// var ctx = context.Background()

func TestRedis(t *testing.T) {
	// if err := initSingleClient(); err != nil {
	//     panic("initSingleClient error")
	// }
	// if err := initSentinelClient(); err != nil {
	// 	panic("initSentinelClient error")
	// }
	if err := initClusterClient(); err != nil {
		panic("initClusterClient error")
	}
	
	_rdb.FlushDB(ctx)
	
	General()
	ExampleString()
	ExampleList()
	ExampleHash()
	ExampleSet()
	ExampleSortset()
	ExampleHyperloglog()
	ExampleCmd()
	ExampleScan()
	ExampleTx()
	ExampleScript()
	ExamplePubsub()
}

// // 单机模式
// func initSingleClient() (err error) {
// 	_rdb = redis.NewClient(&redis.Options{
// 		Addr:        "10.0.0.20:6379",
// 		Password:    "", // no password set
// 		DB:          0,  // use default DB
// 		PoolSize:    500,
// 		PoolTimeout: time.Second * 3,
// 	})
// 	_, err = _rdb.Ping(ctx).Result()
//
// 	return err
// }

// // 哨兵模式
// func initSentinelClient() (err error) {
// 	_rdb = redis.NewFailoverClient(&redis.FailoverOptions{
// 		MasterName:    "mymaster",
// 		SentinelAddrs: []string{"10.0.0.20:26379", "10.0.0.20:26380", "10.0.0.20:26000"}, // 26000不存在
// 	})
// 	_, err = _rdb.Ping(ctx).Result()
// 	return err
// }

// 集群模式
func initClusterClient() (err error) {
	_rdb = redis.NewClusterClient(&redis.ClusterOptions{
		Addrs: []string{"10.0.0.20:7000", "10.0.0.20:7001", "10.0.0.20:7002"},
	})
	_, err = _rdb.Ping(ctx).Result()
	return err
}

func General() {
	log.Println("====================== General ======================")
	ret := _rdb.Do(ctx, "set", "k999", "999")
	
	log.Printf("写入数据: %+v\n", ret)
	
	ret = _rdb.Do(ctx, "incrby", "k999", "1000")
	ret = _rdb.Do(ctx, "get", "k999")
	log.Printf("读取数据: %+v\n", ret)
	
	ret = _rdb.Do(ctx, "incrbyxxx", "k999", "1000")
	if ret.Err() != nil {
		log.Printf("错误处理: %+v\n", ret.Err().Error())
	}
	
	ex := _rdb.Exists(ctx, "missing_key")
	fmt.Printf("是否存在: %+v\n", ex)
	
}

// 集群不能直接进行批量操作,可使用HashTag将数据写入同一个槽(但会引起数据倾斜)：
//  mset {t}test1 wang {t}test2 zhao {t}test3 liu
//  mget {t}test1 {t}test2 {t}test3
func ExampleString() {
	log.Println("====================== String ======================")
	err := _rdb.Set(ctx, "greet", "hello world", 30*time.Second).Err()
	log.Println("写入数据: ", err)
	
	val, err := _rdb.Type(ctx, "greet").Result()
	log.Println("数据类型: ", val, err)
	
	val, err = _rdb.Get(ctx, "greet").Result()
	log.Println("获取数据: ", val, err)
	
	tm, err := _rdb.TTL(ctx, "greet").Result()
	log.Println("过期时间: ", tm)
	
	err = _rdb.MSet(ctx, "k1001", "v1001", "k1002", "v1002", "k1003", "v1003").Err()
	log.Println("批量写入: ", err)
	
	keys := _rdb.Keys(ctx, "k1*")
	log.Println("Key名匹配: ", keys.Val())
	
	_, err = _rdb.Get(ctx, "missing_key").Result()
	if err == redis.Nil {
		log.Println("Key不存在:  missing_key")
	}
	
	value, err := _rdb.SetNX(ctx, "counter", 1, 1*time.Second).Result()
	log.Println("缺失创建: ", value, err) // Key不存在才设置
	
	result, err := _rdb.IncrBy(ctx, "counter", 10).Result()
	log.Println("数值自增: ", result, err)
	
	val, err = _rdb.ObjectEncoding(ctx, "counter").Result()
	log.Println("编码类型: ", val, err)
	
	log.Printf("线程池:  %+v\n", *_rdb.PoolStats())
}

func ExampleList() {
	log.Println("====================== List ======================")
	ret, err := _rdb.LPush(ctx, "list_test", "a").Result() // 左
	ret, err = _rdb.RPush(ctx, "list_test", "b", "a", "c", "a").Result()
	log.Println("列表插入: ", ret, err)
	
	log.Println("修改元素: ", _rdb.LSet(ctx, "list_test", 1, "b_xxx").Err())
	
	ret, err = _rdb.LRem(ctx, "list_test", -2, "a").Result() // 删除后两个a
	log.Println("移除数据: ", ret, err)
	
	rLen, err := _rdb.LLen(ctx, "list_test").Result()
	log.Println("列表长度: ", rLen, err)
	
	lists, err := _rdb.LRange(ctx, "list_test", 0, rLen-1).Result()
	log.Println("遍历列表: ", lists, err)
	
	// 弹出元素(如果列表为空,则阻塞N秒)
	result, err := _rdb.BLPop(ctx, time.Second*3, "list_test").Result()
	log.Println("阻塞POP: ", result, err)
}

func ExampleHash() {
	log.Println("====================== Hash ======================")
	data := map[string]interface{}{
		"name": "Jack",
		"sex":  1,
		"age":  28,
		"tel":  "18200010001",
	}
	
	ret, err := _rdb.HMSet(ctx, "hash_test", data).Result()
	log.Println("添加Hash对象: ", ret, err)
	
	retFiled, err := _rdb.HMGet(ctx, "hash_test", "name", "sex").Result()
	log.Println("获取Hash字段: ", retFiled, err)
	
	retAll, err := _rdb.HGetAll(ctx, "hash_test").Result()
	log.Println("获取Hash对象: ", retAll, err)
	
	exist, err := _rdb.HExists(ctx, "hash_test", "tel").Result()
	log.Println("字段是否存在: ", exist, err)
	
	nxSet, err := _rdb.HSetNX(ctx, "hash_test", "id", 1001).Result()
	log.Println("不存在时插入: ", nxSet, err)
	
	retInt, err := _rdb.HDel(ctx, "hash_test", "age").Result()
	log.Println("删除Hash字段: ", retInt, err)
	
	keys := _rdb.HKeys(ctx, "hash_test")
	log.Println("Hash对象字段: ", keys)
}

func ExampleSet() {
	log.Println("====================== Set  ======================")
	ret, err := _rdb.SAdd(ctx, "set_test", "aa", "bb", "cc", "cc", "dd").Result()
	log.Println("添加Set元素: ", ret, err)
	
	count, err := _rdb.SCard(ctx, "set_test").Result()
	log.Println("Set元素数量: ", count, err)
	
	ret, err = _rdb.SRem(ctx, "set_test", "aa", "cc").Result()
	log.Println("删除Set元素: ", ret, err)
	
	members, err := _rdb.SMembers(ctx, "set_test").Result()
	log.Println("获取Set成员: ", members, err)
	
	has, err := _rdb.SIsMember(ctx, "set_test", "ff").Result()
	log.Println("是否存在元素: ", has, err)
	
	// 集合操作：https://blog.csdn.net/Gusand/article/details/119876939
	// 交集：A & B,即A与B
	// 并集：A | B,即A或B
	// 差集：A - B,即A减B
	// 补集：A ^ B,即A异B
	_rdb.SAdd(ctx, "set_a", "1", "2", "3", "4")
	_rdb.SAdd(ctx, "set_b", "1", "2", "3", "5", "6", "7")
	inter, err := _rdb.SInter(ctx, "set_a", "set_b").Result()
	log.Println("A与B交集: ", inter, err)
	
	union, err := _rdb.SUnion(ctx, "set_a", "set_b").Result()
	log.Println("A与B并集: ", union, err)
	
	diff, err := _rdb.SDiff(ctx, "set_a", "set_b").Result()
	log.Println("A与B差集: ", diff, err)
	
	ret, err = _rdb.SDiffStore(ctx, "store_diff", "set_a", "set_b").Result()
	log.Println("差集保存: ", ret, err)
	
	rets, err := _rdb.SMembers(ctx, "store_diff").Result()
	log.Println("查询保存: ", rets, err)
}

// 用于实时统计,如人气排行榜,点赞榜单,热点新闻等
func ExampleSortset() {
	log.Println("====================== ZSet ======================")
	addArgs := make([]*redis.Z, 0)
	for i := 1; i < 100; i++ {
		addArgs = append(addArgs, &redis.Z{Score: float64(i), Member: fmt.Sprintf("a_%d", i)})
	}
	
	Shuffle := func(slice []*redis.Z) {
		r := rand.New(rand.NewSource(time.Now().Unix()))
		for len(slice) > 0 {
			n := len(slice)
			randIndex := r.Intn(n)
			slice[n-1], slice[randIndex] = slice[randIndex], slice[n-1]
			slice = slice[:n-1]
		}
	}
	
	// 随机打乱
	Shuffle(addArgs)
	
	// 添加
	ret, err := _rdb.ZAddNX(ctx, "zset_test", addArgs...).Result()
	log.Println(ret, err)
	
	// 获取指定成员score
	score, err := _rdb.ZScore(ctx, "zset_test", "a_10").Result()
	log.Println(score, err)
	
	// 获取制定成员的索引
	index, err := _rdb.ZRank(ctx, "zset_test", "a_50").Result()
	log.Println(index, err)
	
	count, err := _rdb.SCard(ctx, "zset_test").Result()
	log.Println(count, err)
	
	// 返回有序集合指定区间内的成员
	rets, err := _rdb.ZRange(ctx, "zset_test", 10, 20).Result()
	log.Println(rets, err)
	
	// 返回有序集合指定区间内的成员分数从高到低
	rets, err = _rdb.ZRevRange(ctx, "zset_test", 10, 20).Result()
	log.Println(rets, err)
	
	// 指定分数区间的成员列表
	rets, err = _rdb.ZRangeByScore(ctx, "zset_test", &redis.ZRangeBy{Min: "(30", Max: "(50", Offset: 1, Count: 10}).Result()
	log.Println(rets, err)
}

func ExampleHyperloglog() {
	log.Println("====================== HyperLogLog ======================")
	for i := 0; i < 1000; i++ {
		_rdb.PFAdd(ctx, "hll_test_1", fmt.Sprintf("xxx_%d", i))
	}
	ret, err := _rdb.PFCount(ctx, "hll_test_1").Result()
	log.Println("HLL.1总数：", ret, err)
	
	for i := 0; i < 1000; i++ {
		_rdb.PFAdd(ctx, "hll_test_2", fmt.Sprintf("yyy_%d", i))
	}
	ret, err = _rdb.PFCount(ctx, "hll_test_2").Result()
	log.Println("HLL.2总数：", ret, err)
	
	_rdb.PFMerge(ctx, "hll_test_3", "pf_test_2", "hll_test_1")
	ret, err = _rdb.PFCount(ctx, "hll_test_3").Result()
	log.Println("HLL.3总数：", ret, err)
}

func ExamplePubsub() {
	log.Println("====================== PubSub ======================")
	sub := _rdb.Subscribe(ctx, "topic_test")
	obj, err := sub.ReceiveTimeout(ctx, time.Second*3)
	if err != nil {
		panic(err)
	}
	log.Printf("订阅类型: %+v\n", *obj.(*redis.Subscription))
	
	// 发布消息
	time.AfterFunc(1*time.Second, func() { _rdb.Publish(ctx, "topic_test", "中国驻加拿大大使电话慰问孟晚舟") })
	time.AfterFunc(2*time.Second, func() { _rdb.Publish(ctx, "topic_test", "死刑!北大学子吴谢宇弑母案宣判") })
	time.AfterFunc(3*time.Second, func() { _rdb.Publish(ctx, "topic_test", "done") })
	
	for msg := range sub.Channel() {
		if msg.Payload == "done" {
			log.Println("订阅结束")
			sub.Unsubscribe(ctx, "topic_test")
			sub.Close()
			break
		}
		log.Printf("接收消息: %+v\n", msg)
	}
}

func ExampleCmd() {
	log.Println("====================== Command ======================")
	// myCommand := func(_rdb *redis.Client, key string) *redis.StringCmd {
	//     cmd := redis.NewStringCmd("get", key)
	//     _rdb.Process(cmd)
	//     return cmd
	// }
	//
	// v, err := myCommand(_rdb, "greet").Result()
	// log.Println("自定义命令: ", v, err)
	
	n, err := _rdb.Do(ctx, "dbsize").Int64()
	log.Println("系统Do命令：", n, err)
}

// https://zhuanlan.zhihu.com/p/46353221
// https://www.cnblogs.com/zhuyeshen/p/12496406.html
// COUNT只是参考数量(默认为10),实际可能会多或少；
// COUNT采用渐进式rehash机制,因为库会实时扩容缩容；
//
// keys和smembers用于获取全部元素,会阻塞服务
// scan采用增量式读取,但由于数据变化,可能没法读取所有Key
//
// SCAN (Key Scan)增量迭代当前数据库中的数据库键。
// SSCAN(Set Scan) 增量迭代集合键中的元素。
// HSCAN(Hash Scan) 增量迭代哈希键中的键值对。
// ZSCAN(Zset Scan) 增量迭代有序集合中的元素（包括元素成员和元素分值）。
func ExampleScan() {
	log.Println("====================== Scan ======================")
	
	// scan
	for i := 0; i < 20; i++ {
		_rdb.Set(ctx, fmt.Sprintf("skey_%d", i), i, 0)
	}
	cursor := uint64(0)
	for {
		keys, retCusor, err := _rdb.Scan(ctx, cursor, "skey_*", 5).Result()
		log.Println("游标遍历Key: ", keys, cursor, err)
		cursor = retCusor
		if cursor == 0 {
			break
		}
	}
}

func ExampleTx() {
	log.Println("====================== Tx ======================")
	pipe := _rdb.TxPipeline()
	incr := pipe.Incr(ctx, "tx_pipeline_counter")
	pipe.Expire(ctx, "tx_pipeline_counter", time.Hour)
	
	// Execute
	//
	//     MULTI
	//     INCR pipeline_counter
	//     EXPIRE pipeline_counts 3600
	//     EXEC
	//
	// using one _rdb-server roundtrip.
	_, err := pipe.Exec(ctx)
	fmt.Println(incr.Val(), err)
}

func ExampleScript() {
	log.Println("====================== Script ======================")
	err := _rdb.Set(ctx, "xx_counter", "40", 0).Err()
	if err != nil {
		panic(err)
	}
	
	script := redis.NewScript(`
        if redis.call("GET", KEYS[1]) ~= false then
            return redis.call("INCRBY", KEYS[1], ARGV[1])
        end
        return false
    `)
	
	n, err := script.Run(ctx, _rdb, []string{"xx_counter"}, 2).Result()
	log.Println("执行结果: ", n, err)
}
