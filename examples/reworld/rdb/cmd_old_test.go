package rdb

import (
	"fmt"
	"testing"

	"github.com/go-redis/redis/v8"
)

func init() {
	Rdb = redis.NewFailoverClient(&redis.FailoverOptions{
		MasterName:    "mymaster",
		SentinelAddrs: []string{"10.0.0.20:26379", "10.0.0.20:26380", "10.0.0.20:26000"},
	})
	// Rdb.FlushDB(ctx)
}

func TestExists(t *testing.T) {
	fmt.Println(Rdb.Exists(ctx, "keyName").Val())
	fmt.Println(Rdb.Exists(ctx, "keyN adme").Val())
	fmt.Println(Rdb.Exists(ctx, "hash_test1", "hash_test2").Val())

	fmt.Println(Exists("keyName"))
	fmt.Println(Exists("keyN adme"))
	fmt.Println(Exists("hash_test1", "hash_test2xx"))
	fmt.Println(Exists("hash_test1", "hash_test2"))
}

func TestHSet(t *testing.T) {
	err := HSet("hash_test1", "k1001", "v1001", "k1002", "v1002", "k1003", "v1003")
	err = HSet("hash_test2", []string{"key1", "value1", "key2", "value2"})
	err = HSet("hash_test3", map[string]interface{}{"mk1": "value1", "mk2": "value2"})
	fmt.Println(err)
}

func TestMGET(t *testing.T) {
	err := Rdb.MSet(ctx, "k1001", "v1001", "k1002", "v1002", "k1003", "v1003").Err()
	ret, err := MGET("k1001", "k1002", "K888")
	fmt.Println(ret, err)
}

// 总结: error不为nil时,返回 default,error
//      Key不存在时,   返回 default,nil
func TestINT(t *testing.T) {
	_, err := Rdb.Set(ctx, "counter", "99", 0).Result()
	if err != nil {
		panic(err)
	}

	// Int 正常
	n, err := INT("counter")
	if err != nil {
		fmt.Println("Sorry ===> ", n, err)
	} else {
		fmt.Println("Ok    ===> ", n)
	}

	// Int 不存在
	n, err = INT("dismiss_key")
	if err != nil {
		fmt.Println("Sorry ===> ", n, err)
	} else {
		fmt.Println("Ok   ===> ", n)
	}

	// Int64 异常
	_, err = Rdb.Set(ctx, "counter", "abc", 0).Result()
	if err != nil {
		panic(err)
	}

	m, err := INT64("counter")
	if err != nil {
		fmt.Println("Sorry ===> ", m, err)
	} else {
		fmt.Println("Ok   ===> ", m)
	}
}

func TestBYTES(t *testing.T) {
	_, err := Rdb.Set(ctx, "counter", "8", 0).Result()
	if err != nil {
		panic(err)
	}

	n, err := BYTES("counter")
	if err != nil {
		fmt.Println("Sorry ===> ", string(n), err)
	} else {
		fmt.Println("Ok    ===> ", string(n))
	}
}

func TestAMADD(t *testing.T) {
	_, err := Rdb.Set(ctx, "counter", "8", 0).Result()
	if err != nil {
		panic(err)
	}

	err = ZMADD("zmadd_test", map[int]string{-102: "aaa", 8: "bbb", 23: "ccc", 1004: "ddd"})
	if err != nil {
		fmt.Println("Sorry ===> ", err)
	}

	fmt.Println(ZRange("zmadd_test", 1, 999))
	fmt.Println(ZCARD("zmadd_test"))
	fmt.Println(ZCOUNTInterface("zmadd_test", "-inf", "100"))
	fmt.Println(ZCOUNTInterface("zmadd_test", "10", "+inf"))
}

func TestZRANGEUINT32(t *testing.T) {
	_, err := Rdb.Set(ctx, "counter", "8", 0).Result()
	if err != nil {
		panic(err)
	}
	err = ZMADD("zmadd_test", map[int]string{-122: "122", 888: "888.47", 11: "11", 1004: "1004"})
	if err != nil {
		fmt.Println("Sorry ===> ", err)
	}

	fmt.Println(ZRANGEUINT32("zmadd_test", 1, 99))
	fmt.Println(ZREVRANGEWITHSCORESINT64("zmadd_test", 1, 99))
}

func TestHMGET(t *testing.T) {
	err := HMSet("hash_test", "name", "tom", "age", 22, "vip", "true")
	if err != nil {
		panic(err)
	}

	fmt.Println(HMGET("hash_test", "name", "age"))
	fmt.Println(HMGET("hash_test"))
	fmt.Println(HMGET("hash_test", "name", "age", "unknown"))
}

func TestHMGET_MAP(t *testing.T) {
	err := HMSet("hash_test", "name", "tom", "age", 22, "vip", "true")
	if err != nil {
		panic(err)
	}

	fmt.Println(HMGET_MAP("hash_test", "name", "age"))
	fmt.Println(HMGET_MAP("hash_test"))
	fmt.Println(HMGET_MAP("hash_test", "name", "age", "unknown"))
}

func TestLua(t *testing.T) {
	lua := "if redis.call('get', KEYS[1]) == ARGV[1] then return redis.call('del', KEYS[1]) else return 0 end"
	fmt.Println(EVAL(lua, "test_lua", "12"))
	fmt.Println(Eval(lua, []string{"test_lua"}, "11").Result())
}

func TestGet(t *testing.T) {
	fmt.Println("1: ", Get("missing_key"))
	fmt.Println("2: ", Get("error key"))
	fmt.Println("3: ", Get("hello"))
	fmt.Println("4: ", Get("hash_test1")) // 不能读取hash
}

func TestSETNEX(t *testing.T) {
	var f = float64(99.222)
	var i int64 = int64(f)
	fmt.Println(i)
}

func TestIncr(t *testing.T) {
	// ExampleIncrN()
	ExampleIncrF()
}

func ExampleIncrN() {
	Set("msg", "1")                // 1
	fmt.Println(IncrN("msg"))      // 2
	fmt.Println(IncrN("msg", 1))   // 3
	fmt.Println(IncrN("msg", 20))  // 23
	fmt.Println(IncrN("msg", -1))  // 22
	fmt.Println(IncrN("msg", -30)) // -8
}

func ExampleIncrF() {
	Set("msg", "1.2")
	fmt.Println(IncrF("msg"))         // 2.2
	fmt.Println(IncrF("msg", 1.0))    // 3.2
	fmt.Println(IncrF("msg", 3.50))   // 6.7
	fmt.Println(IncrF("msg", 1.0e2))  // 106.7
	fmt.Println(IncrF("msg", -1.5))   // 105.2
	fmt.Println(IncrF("msg", -1.2e2)) // -14.8
}

func TestName(t *testing.T) {
	var f float64 = 10
	fmt.Println(f/0 - f/0)
}
