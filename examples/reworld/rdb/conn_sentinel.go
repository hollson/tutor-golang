// +build !redis_cluster

/*
	Redis哨兵说明:
		1. 哨兵模式解决了单点故障的问题,使Redis服务变得高可用;
		2. 数据量过小(如小于20G),建议使用哨兵模式,如1主2从3哨兵;
		3. 哨兵仅是个监控服务,不会侵入Redis实例和客户端(go-redis);


	go-redis客户端:
		1. 具有更多的Star和源码贡献者,被应用次数更高;
		2. 源码持续升级维护中,且有更全面的官方文档;
		3. sentinel,cluster集成度更高,代码封装更人性化;
		4. 具有较高的生态集成度,如RedisMock,分布式锁,缓存,限速等;
		5. 社区活跃度高,且有更全面的官方文档(redis.uptrace.dev);
*/

package rdb

import (
	"context"
	"time"

	"github.com/go-redis/redis/v8"

	"locojoy.com/system/config"
)

var (
	Rdb *redis.Client // Redis-Client
	ctx = context.Background()
)

// 单实例或哨兵模式
func Init() (model string, err error) {
	// 哨兵模式
	if config.Redis.Model == "sentinel" {
		if len(config.Redis.MasterName) == 0 {
			config.Redis.MasterName = "mymaster"
		}
		Rdb = redis.NewFailoverClient(&redis.FailoverOptions{
			MasterName:    config.Redis.MasterName,
			SentinelAddrs: config.Redis.Hosts,
			Password:      config.Redis.Password,
		})
		return "Sentinel", Rdb.Ping(ctx).Err()
	}

	// 单机模式
	Rdb = redis.NewClient(&redis.Options{
		Addr:        config.Redis.Hosts[0],
		Password:    config.Redis.Password,
		DB:          0, // use default DB
		PoolSize:    200,
		PoolTimeout: time.Second * 3,
	})

	return "Single", Rdb.Ping(ctx).Err()
}
