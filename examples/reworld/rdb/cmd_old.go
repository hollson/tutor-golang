// 兼容老代码(github.com/gomodule/redigo/redis)

package rdb

import (
	"fmt"
	"strconv"
	"strings"
	"time"
	
	"github.com/go-redis/redis/v8"
)

func MGET(k ...string) ([]string, error) {
	ret, err := Rdb.MGet(ctx, k...).Result()
	if len(ret) > 0 {
		var arr []string
		for _, v := range ret {
			vp := strings.Replace(fmt.Sprintf("%v", v), "<nil>", "", -1)
			arr = append(arr, fmt.Sprintf("%v", vp))
		}
		return arr, err
	}
	return nil, err
}

// Deprecated: (兼容旧方法,废弃)
//  expire:过期时间(秒)
func SETEX(k string, expire int32, v interface{}) error {
	return SetEX(k, v, time.Duration(expire)*time.Second)
}

// Deprecated: (兼容旧方法,废弃)
//  执行script脚本, key 和 v 为参数
func EVAL(script, key string, v interface{}) (int, error) {
	_, err := Eval(script, []string{key}, v).Result()
	if err != nil {
		return 0, err
	}
	return 1, nil
}

func INT(k string) (int, error) {
	return Rdb.Get(ctx, k).Int()
}

func INT64(k string) (int64, error) {
	return Rdb.Get(ctx, k).Int64()
}

func BYTES(k string) ([]byte, error) {
	return Rdb.Get(ctx, k).Bytes()
}

// Deprecated: (兼容旧方法,废弃,错误的封装,zset的score可重复,而map-key不可重复)
//  向zset中添加多个元素 比如 values 为map[int]string类型
func ZMADD(key string, values map[int]string) error {
	vs := make([]*redis.Z, 0)
	for k, v := range values {
		vs = append(vs, &redis.Z{
			Score:  float64(k),
			Member: v,
		})
	}
	return Rdb.ZAdd(ctx, key, vs...).Err()
}

// Deprecated: (兼容旧方法,废弃)
// 返回集合中元素个数
func ZCARD(key string) (int, error) {
	ret, err := ZCard(key)
	return int(ret), err
}

// Deprecated: (兼容旧方法,废弃)
//  返回集合中 score 在给定区间的数量
func ZCOUNTInterface(key string, start, end string) (int, error) {
	ret, err := Rdb.ZCount(ctx, key, start, end).Result()
	return int(ret), err
}

// Deprecated: (兼容旧方法,废弃)
func ZRANGEUINT32(key string, start, end uint32) ([]int64, error) {
	ret, err := ZRange(key, int64(start), int64(end))
	arr := []int64{}
	for _, v := range ret {
		if n, err := strconv.Atoi(v); err == nil {
			arr = append(arr, int64(n))
		}
	}
	return arr, err
}

// Deprecated: (兼容旧方法,废弃, 不严谨的封装)
//  返回名称为 key 的 zset(按 score 从大到小排序)中的 index 从 start 到 end 的所有元素，结果包含 socre
func ZREVRANGEWITHSCORESINT64(key string, start, end int64) (map[string]string, error) {
	ret, err := Rdb.ZRevRangeWithScores(ctx, key, start, end).Result()
	if err != nil {
		return nil, err
	}
	
	result := make(map[string]string, 0)
	for _, v := range ret {
		result[fmt.Sprintf("%.f", v.Score)] = fmt.Sprintf("%s", v.Member)
	}
	return result, nil
}

// 获取全部指定的 hash filed。
func HMGET(hashname string, keynames ...string) ([]string, error) {
	ret, err := HMGetOrAll(hashname, keynames...)
	if err != nil {
		return nil, err
	}
	result := make([]string, 0)
	for _, v := range ret {
		result = append(result, v)
	}
	return result, nil
}

// Deprecated: (兼容旧方法,废弃, 不严谨的封装)
//  同时设置 hash 的多个 field。
func HMSET_MAP(keyname string, mvals map[string]interface{}) error {
	vs := []interface{}{}
	for k, v := range mvals {
		vs = append(vs, k, v)
	}
	return Rdb.HMSet(ctx, keyname, vs...).Err()
}

// 通用, Hget或 HGetAll
func HMGetOrAll(hashname string, keynames ...string) (map[string]string, error) {
	result := make(map[string]string)
	// 获取全部
	if len(keynames) == 0 {
		ret, err := Rdb.HGetAll(ctx, hashname).Result()
		if err != nil {
			return nil, err
		}
		for k, v := range ret {
			vp := strings.Replace(fmt.Sprintf("%v", v), "<nil>", "", -1)
			result[k] = vp
		}
		return result, nil
	}
	
	// 指定字段
	ret, err := Rdb.HMGet(ctx, hashname, keynames...).Result()
	if err != nil {
		return nil, err
	}
	for i, v := range ret {
		vp := strings.Replace(fmt.Sprintf("%v", v), "<nil>", "", -1)
		result[keynames[i]] = vp
	}
	return result, nil
}

// 获取全部指定的 hash filed。
func HMGET_MAP(hashname string, keynames ...string) (map[string]string, error) {
	return HMGetOrAll(hashname, keynames...)
}
