package common

import (
	"encoding/json"
)

func JsonDump(o interface{}) string {
	data, err := json.MarshalIndent(o,"	", "\t")
	if err != nil {
		panic(err)
	}
	return string(data)
}
