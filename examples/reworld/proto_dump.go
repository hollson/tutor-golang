package common

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"

	"github.com/gin-gonic/gin"
	"github.com/golang/protobuf/proto"

	"locojoy.com/app/api"
)

// 测试proto请求
func ProtoDump(ping, pong proto.Message, uri string) {
	b, _ := proto.Marshal(ping)
	result, err := api.PostHandler(uri, b)
	if err != nil {
		log.Fatalf("发送请求失败:%v", err)
	}
	if result == nil {
		fmt.Println("响应结果为null")
		return
	}
	defer result.Body.Close()

	data, err := ioutil.ReadAll(result.Body)
	if err != nil {
		log.Fatalln("读取Body失败:", err)
	}
	err = proto.Unmarshal(data, pong)
	if err != nil {
		log.Fatalf("数据解码失败:%v,%v\n", string(data), err)
	}
	// 以json方式人性化输出
	dump, _ := json.MarshalIndent(pong, "", "\t")
	fmt.Println(string(dump))
}

// 将Json-API请求转换成Proto-API请求,并输出响应
func Json2ProtoApi(c *gin.Context, ping, pong proto.Message, uri string) error {
	pbd, _ := proto.Marshal(ping)
	result, err := api.PostHandler(uri, pbd)
	if err != nil {
		c.JSON(400, gin.H{
			"msg": fmt.Sprintf("PROXY-请求失败:%v", err.Error()),
		})
		c.Abort()
		return err
	}

	if result == nil {
		c.JSON(400, gin.H{
			"msg": fmt.Sprintf("PROXY-%v", "响应结果为nil"),
		})
		c.Abort()
		return err
	}
	defer result.Body.Close()

	data, err := ioutil.ReadAll(result.Body)
	if err != nil {
		c.JSON(400, gin.H{
			"msg": fmt.Sprintf("PROXY-BODY错误:%v", err.Error()),
		})
		c.Abort()
		return err
	}

	err = proto.Unmarshal(data, pong)
	if err != nil {
		c.JSON(400, gin.H{
			"msg": fmt.Sprintf("PROXY-解码失败:%v", err.Error()),
		})
		c.Abort()
		return err
	}
	c.JSON(200, pong)
	return nil
}
