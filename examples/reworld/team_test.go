package TeamworkServer

import (
	"fmt"
	"locojoy.com/common"
	"testing"

	"github.com/golang/protobuf/proto"

	"locojoy.com/packet/proto/go/pb"
)

var (
	teamUrl = "http://192.168.39.8:22000"
	//url = "http://192.168.39.8:22000"
	acc = "1CE97D0E90AC90AA733C08D3B4C398AE" // shs1-6
	// acc = "200519140049593678" // xxx
	md5 = "7a8911edb7206f9f20f7709ed9401f8e"

	hacc = "295B1628A772455ED6BD6828BA9C312B"
	hmd5 = "b3e35c7fb74fb236d91cf4876b23aa1f"
)

// 通用测试数据
var (
	pagenum   int32 = 1
	pagesize  int32 = 10
	name            = "ABC"
	type32    int32 = 32
	type64    int64 = 64
	mapId     int64 = 45046565886230787
	privilege int32 = 2
	versionId int64 = 10
)

//#####项目管理

// 项目列表
func TestItemList(t *testing.T) {

	var (
		uri  = "/team/item/list"
		ping = pb.Team_Req_ItemList{
			Account:  &acc,
			Md5:      &md5,
			TeamName: proto.String(""),
			Type:     proto.Int32(1),
			Sort:     proto.Int32(4),
		}
		pong pb.Team_Resp_ItemList
	)
	common.ProtoDump(&ping, &pong, fmt.Sprintf("%s/%s", teamUrl, uri))
}

// 获取项目信息
func TestItemGet(t *testing.T) {
	var (
		uri  = "/team/item/get"
		ping = pb.Team_Req_ItemGet{
			Account: &acc,
			Md5:     &md5,
			MapId:   proto.Int64(mapId),
		}
		pong pb.Team_Resp_ItemGet
	)
	common.ProtoDump(&ping, &pong, fmt.Sprintf("%s/%s", teamUrl, uri))
}

// 创建多人项目编辑
func TestItemCreate(t *testing.T) {
	var (
		uri  = "/team/item/create"
		ping = pb.Team_Req_ItemCreate{
			Account: &acc,
			Md5:     &md5,
			MapId:   proto.Int64(34242100287504642),
		}
		pong pb.Team_Resp_ItemCreate
	)
	common.ProtoDump(&ping, &pong, fmt.Sprintf("%s/%s", teamUrl, uri))
}

// 打开多人项目编辑
func TestItemOpen(t *testing.T) {
	var (
		uri  = "/team/item/open"
		ping = pb.Team_Req_ItemOpen{
			Account: &acc,
			Md5:     &md5,
			MapId:   proto.Int64(mapId),
		}
		pong pb.Team_Resp_ItemOpen
	)
	common.ProtoDump(&ping, &pong, fmt.Sprintf("%s/%s", teamUrl, uri))
}

// 删除多人项目编辑
func TestItemDelete(t *testing.T) {
	var (
		uri  = "/team/item/delete"
		ping = pb.Team_Req_ItemDelete{
			Account: &acc,
			Md5:     &md5,
			MapId:   proto.Int64(mapId),
			MapName: proto.String("hahhahaha"),
		}
		pong pb.Team_Resp_ItemDelete
	)
	common.ProtoDump(&ping, &pong, fmt.Sprintf("%s/%s", teamUrl, uri))
}

// 查看项目状态-是否在编辑
func TestItemCatStatus(t *testing.T) {
	var (
		uri  = "/team/item/state"
		ping = pb.Team_Req_ItemStatus{
			Account: &acc,
			Md5:     &md5,
			MapId:   proto.Int64(mapId),
		}
		pong pb.Team_Resp_ItemStatus
	)
	common.ProtoDump(&ping, &pong, fmt.Sprintf("%s/%s", teamUrl, uri))
}

//更新项目信息
func TestItemUpdate(t *testing.T) {
	var (
		uri  = "/team/item/update"
		ping = pb.Team_Req_ItemUpdate{
			Account:    &acc,
			Md5:        &md5,
			MapId:      proto.Int64(mapId),
			MapProfile: proto.String("这是多人编辑更新内容测试测试"),
			MapName:    proto.String("多人项目更新测试"),
		}
		pong pb.Team_Resp_ItemUpdate
	)
	common.ProtoDump(&ping, &pong, fmt.Sprintf("%s/%s", teamUrl, uri))
}

func TestItemVerNumUpdate(t *testing.T) {
	var (
		uri  = "/team/item/updateVerNum"
		ping = pb.Team_Req_UpdateVerNum{
			Account:  &acc,
			Md5:      &md5,
			MapId:    proto.Int64(mapId),
			VerNum:   proto.Int32(2),
			SaveType: proto.Int32(1),
		}
		pong pb.Team_Resp_UpdateVerNum
	)
	common.ProtoDump(&ping, &pong, fmt.Sprintf("%s/%s", teamUrl, uri))
}

//#########合作用户管理

// 合作用户管理-邀请成员
func TestMemberInvite(t *testing.T) {
	var (
		uri  = "/team/member/invite"
		ping = pb.Team_Req_MemberInvite{
			Account:           &acc,
			Md5:               &md5,
			MapId:             proto.Int64(mapId),
			InviteUserAccount: proto.String(hacc),
			InviteUserId:      proto.Int64(16847101459693826),
			PrivilegeLevel:    proto.Int32(2),
		}
		pong pb.Team_Resp_MemberInvite
	)
	common.ProtoDump(&ping, &pong, fmt.Sprintf("%s/%s", teamUrl, uri))
}

// 合作用户管理-移除成员
func TestMemberRemove(t *testing.T) {
	var (
		uri  = "/team/member/remove"
		ping = pb.Team_Req_MemberRemove{
			Account:           &acc,
			Md5:               &md5,
			MapId:             proto.Int64(mapId),
			RemoveUserAccount: proto.String("200828102310283534"),
		}
		pong pb.Team_Resp_MemberRemove
	)
	common.ProtoDump(&ping, &pong, fmt.Sprintf("%s/%s", teamUrl, uri))
}

// 合作用户管理-合作用户列表
func TestMemberList(t *testing.T) {
	var (
		uri  = "/team/member/list"
		ping = pb.Team_Req_MemberList{
			Account: &acc,
			Md5:     &md5,
			MapId:   proto.Int64(mapId),
		}
		pong pb.Team_Resp_MemberList
	)
	common.ProtoDump(&ping, &pong, fmt.Sprintf("%s/%s", teamUrl, uri))
}

// 合作用户管理-主动退出
func TestMemberQuit(t *testing.T) {
	var (
		uri  = "/team/member/quit"
		ping = pb.Team_Req_ItemDelete{
			Account: &hacc,
			Md5:     &hmd5,
			MapId:   proto.Int64(mapId),
			MapName: proto.String("hahhahaha"),
		}
		pong pb.Team_Resp_ItemDelete
	)
	common.ProtoDump(&ping, &pong, fmt.Sprintf("%s/%s", teamUrl, uri))
}

// 合作用户管理-修改成员权限
func TestMemberUpdate(t *testing.T) {
	var (
		uri  = "/team/member/update"
		ping = pb.Team_Req_MemberUpdate{
			Account:    &acc,
			Md5:        &md5,
			MapId:      proto.Int64(mapId),
			UpdateList: []*pb.Team_Member_UpdateInfo{&pb.Team_Member_UpdateInfo{UserAccount: &hacc, Privilege: proto.Int32(3)}},
		}
		pong pb.Team_Resp_MemberUpdate
	)
	common.ProtoDump(&ping, &pong, fmt.Sprintf("%s/%s", teamUrl, uri))
}

// 合作用户管理-获取本人权限
func TestMemberPrivilege(t *testing.T) {
	var (
		uri  = "/team/member/privilege"
		ping = pb.Team_Req_MemberPrivilege{
			Account: &hacc,
			Md5:     &hmd5,
			MapId:   proto.Int64(mapId),
		}
		pong pb.Team_Resp_MemberPrivilege
	)
	common.ProtoDump(&ping, &pong, fmt.Sprintf("%s/%s", teamUrl, uri))
}

// 合作用户管理-用户查询
func TestMemberSearch(t *testing.T) {
	var (
		uri  = "/team/member/search"
		ping = pb.Team_Req_MemberSearch{
			Account:       &acc,
			Md5:           &md5,
			MapId:         &mapId,
			SearchContent: proto.String("jie"),
		}
		pong pb.Team_Resp_MemberSearch
	)
	common.ProtoDump(&ping, &pong, fmt.Sprintf("%s/%s", teamUrl, uri))
}

//####### 版本管理

//版本自动保存  - 列表
func TestVerList(t *testing.T) {
	var (
		uri  = "/team/ver/list"
		ping = pb.Team_Req_VerList{
			Account: &acc,
			Md5:     &md5,
			MapId:   proto.Int64(mapId),
		}
		pong pb.Team_Resp_VerList
	)
	common.ProtoDump(&ping, &pong, fmt.Sprintf("%s/%s", teamUrl, uri))
}

//删除版本/自动保存
func TestVerDelete(t *testing.T) {
	var (
		uri  = "/team/ver/delete"
		ping = pb.Team_Req_VerDelete{
			Account: &acc,
			Md5:     &md5,
			MapId:   proto.Int64(41560231544619267),
			VerType: proto.Int32(1),
			VerNum:  proto.Int64(32),
		}
		pong pb.Team_Resp_VerDelete
	)
	common.ProtoDump(&ping, &pong, fmt.Sprintf("%s/%s", teamUrl, uri))
}

//更改自动保存时间
func TestVerUpdateTime(t *testing.T) {
	var (
		uri  = "/team/ver/uptime"
		ping = pb.Team_Req_UptimeAutoSave{
			Account:  &acc,
			Md5:      &md5,
			MapId:    proto.Int64(mapId),
			Interval: proto.Int32(60),
		}
		pong pb.Team_Resp_UptimeAutoSave
	)
	common.ProtoDump(&ping, &pong, fmt.Sprintf("%s/%s", teamUrl, uri))
}

//########### 脚本
//打开脚本
func TestScriptOpen(t *testing.T) {
	var (
		uri  = "/team/script/open"
		ping = pb.Team_Req_ScriptOpen{
			Account:    &acc,
			Md5:        &md5,
			TeamWorkId: &mapId,
			RoomId:     proto.Int64(1212212121),
			ScriptName: proto.String("1.lua"),
		}
		pong pb.Team_Resp_ScriptOpen
	)
	common.ProtoDump(&ping, &pong, fmt.Sprintf("%s/%s", teamUrl, uri))
}

//######状态管理

//当前编辑版本
func TestSttCurrent(t *testing.T) {
	var (
		uri  = "/team/stt/current"
		ping = pb.Team_Req_SttCurrent{
			Account:    &acc,
			Md5:        &md5,
			TeamWorkId: &mapId,
		}
		pong pb.Team_Resp_SttCurrent
	)
	common.ProtoDump(&ping, &pong, fmt.Sprintf("%s/%s", teamUrl, uri))
}

//锁定/解锁组件     广播
func TestSttModuleLock(t *testing.T) {
	var (
		uri  = "/team/stt/module/lock"
		ping = pb.Team_Req_SttModule{
			Account:  &acc,
			Md5:      &md5,
			ModuleId: proto.Int64(1212121212),
			Type:     proto.Int32(1),
		}
		pong pb.Team_Resp_SttModule
	)
	common.ProtoDump(&ping, &pong, fmt.Sprintf("%s/%s", teamUrl, uri))
}

//当用户进入房间时  状态获取
func TestSttEnter(t *testing.T) {
	var (
		uri  = "/team/stt/enter/order"
		ping = pb.Team_Req_SttEnterOrder{
			Account:    &acc,
			Md5:        &md5,
			TeamWorkId: &mapId,
			RoomId:     proto.Int64(12121211),
		}
		pong pb.Team_Resp_SttEnterOrder
	)
	common.ProtoDump(&ping, &pong, fmt.Sprintf("%s/%s", teamUrl, uri))
}

//############   多人测试

//创建多人测试
func TestTestCreate(t *testing.T) {
	var (
		uri  = "/team/test/create"
		ping = pb.Team_Req_TestCreate{
			Account:    &acc,
			Md5:        &md5,
			TeamWorkId: &mapId,
			VersionId:  proto.Int64(1),
		}
		pong pb.Team_Resp_TestCreate
	)
	common.ProtoDump(&ping, &pong, fmt.Sprintf("%s/%s", teamUrl, uri))
}

//查看多人测试状态
func TestTestView(t *testing.T) {
	var (
		uri  = "/team/test/view"
		ping = pb.Team_Req_TestView{
			Account:    &acc,
			Md5:        &md5,
			TeamWorkId: &mapId,
			VersionId:  proto.Int64(1),
		}
		pong pb.Team_Resp_TestView
	)
	common.ProtoDump(&ping, &pong, fmt.Sprintf("%s/%s", teamUrl, uri))
}

//加入多人测试
func TestTestJoin(t *testing.T) {
	var (
		uri  = "/team/test/join"
		ping = pb.Team_Req_TestJoin{
			Account:    &acc,
			Md5:        &md5,
			TeamWorkId: &mapId,
			VersionId:  proto.Int64(1),
			Privilege:  proto.Int32(2),
			TestId:     proto.Int64(1212121211),
		}
		pong pb.Team_Resp_TestJoin
	)
	common.ProtoDump(&ping, &pong, fmt.Sprintf("%s/%s", teamUrl, uri))
}

//退出多人测试
func TestTestQuit(t *testing.T) {
	var (
		uri  = "/team/test/quit"
		ping = pb.Team_Req_TestQuit{
			Account:    &acc,
			Md5:        &md5,
			TeamWorkId: &mapId,
			VersionId:  proto.Int64(1),
			TestId:     proto.Int64(1212121211),
		}
		pong pb.Team_Resp_TestQuit
	)
	common.ProtoDump(&ping, &pong, fmt.Sprintf("%s/%s", teamUrl, uri))
}
