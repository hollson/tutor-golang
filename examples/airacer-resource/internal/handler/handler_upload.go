package handler

import (
	"fmt"
	"github.com/Airacer-Inc/airacer-resource/internal/config"
	"github.com/Airacer-Inc/airacer-resource/utils"
	"github.com/Airacer-Inc/airacer-resource/utils/jwt"
	"github.com/Airacer-Inc/airacer-resource/utils/s3cli"

	"github.com/gin-gonic/gin"
	"path/filepath"
)

// UploadHandler 上传文件 (TODO：分布式锁)
func UploadHandler(c *gin.Context) {
	jwtModel, ok := c.MustGet("UserModel").(*jwt.UserModel)
	if !ok || jwtModel == nil {
		Error(c, fmt.Sprintf("jwt model invalid"))
		return
	}

	formFile, err := c.FormFile("file")
	if err != nil {
		Error(c, fmt.Sprintf("file param error：%s", err.Error()))
		return
	}

	obj, err := formFile.Open()
	if err != nil {
		Error(c, fmt.Sprintf("file opend error：%s", err.Error()))
		return
	}
	defer func() { _ = obj.Close() }()

	fileHash, err := utils.Md5(obj)
	if err != nil {
		Error(c, fmt.Sprintf("file md5 error：%s", err.Error()))
		return
	}

	//再回到文件头
	if _, err := obj.Seek(0, 0); err != nil {
		Error(c, fmt.Sprintf("file seek error：%s", err.Error()))
		return
	}

	if len(jwtModel.App) == 0 {
		jwtModel.App = jwt.Default
	}
	prefix := fmt.Sprintf("%v%v", jwtModel.UserId, jwtModel.App)
	urn := fmt.Sprintf("%s%s%s", prefix, fileHash, filepath.Ext(formFile.Filename))
	ret, err := s3cli.Default().Upload(config.S3.Bucket, fmt.Sprintf("res/%s", urn), obj)
	if err != nil {
		Error(c, fmt.Sprintf("s3 upload error: %s", err.Error()))
		return
	}

	OK(c, gin.H{"urn": urn, "url": ret.Location})
}
