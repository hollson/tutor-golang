package handler

import (
	"fmt"
	"github.com/Airacer-Inc/airacer-resource/internal/config"
	"github.com/Airacer-Inc/airacer-resource/utils"
	"github.com/Airacer-Inc/airacer-resource/utils/jwt"
	"github.com/Airacer-Inc/airacer-resource/utils/s3cli"
	"io"
	"net/http"
	"os"
	"strconv"

	"github.com/gin-gonic/gin"
	"path/filepath"
)

type UploadResp struct {
	File string `json:"file"`
	Urn  string `json:"urn"`
	Url  string `json:"url"`
}

// UploadHandler 上传文件 (TODO：接口调整 + 分布式锁 + 异步上传 + 等待图)
func UploadBatchHandler(c *gin.Context) {
	info := c.MustGet("UserModel")
	jwtModel, ok := info.(*jwt.UserModel)
	if !ok || jwtModel == nil {
		Error(c, "jwt model invalid")
		return
	}

	cli := s3cli.Default()
	var result []UploadResp
	for count := 0; ; count++ {
		var name = "file"
		if count > 0 {
			name = fmt.Sprintf("file%s", strconv.Itoa(count))
		}

		f, err := c.FormFile(name)
		if err != nil {
			break
		}

		fObj, err := f.Open()
		if err != nil {
			_ = fObj.Close()
			continue
		}

		fileHash, err := utils.Md5(fObj)
		if err != nil {
			_ = fObj.Close()
			continue
		}

		if _, err := fObj.Seek(0, 0); err != nil {
			_ = fObj.Close()
			continue
		}

		prefix := string(jwt.Default)
		if len(jwtModel.App) > 0 {
			prefix = fmt.Sprintf("%v%v", jwtModel.UserId, jwtModel.App)
		}

		urn := fmt.Sprintf("%s%s%s", prefix, fileHash, filepath.Ext(f.Filename))
		ret, err := cli.Upload(config.S3.Bucket, fmt.Sprintf("res/%s", urn), fObj)
		if err != nil {
			_ = fObj.Close()
			continue
		}

		result = append(result, UploadResp{
			File: f.Filename,
			Urn:  urn,
			Url:  ret.Location,
		})
	}
	OK(c, result)
}

// 测试
func xxx(c *gin.Context) {
	// 解析multipart表单数据
	err := c.Request.ParseMultipartForm(32 << 20) // 限制文件大小为32MB
	if err != nil {
		c.String(http.StatusBadRequest, fmt.Sprintf("解析表单数据失败：%s", err.Error()))
		return
	}

	// 获取上传的文件
	file, header, err := c.Request.FormFile("file")
	if err != nil {
		c.String(http.StatusBadRequest, fmt.Sprintf("获取文件失败：%s", err.Error()))
		return
	}
	fmt.Println(header.Filename, header.Size)
	defer file.Close()

	// 创建临时文件
	tempFile, err := os.CreateTemp("", "*")
	if err != nil {
		c.String(http.StatusInternalServerError, fmt.Sprintf("创建临时文件失败：%s", err.Error()))
		return
	}
	defer os.Remove(tempFile.Name())

	// 复制文件内容到临时文件
	_, err = io.Copy(tempFile, file)
	if err != nil {
		c.String(http.StatusInternalServerError, fmt.Sprintf("复制文件内容失败：%s", err.Error()))
		return
	}

	// 调用外部API，传递临时文件路径
	// ...

	c.String(http.StatusOK, "文件上传成功")
}
