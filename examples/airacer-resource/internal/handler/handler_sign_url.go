package handler

import (
	"fmt"
	"path"

	"github.com/Airacer-Inc/airacer-resource/internal/config"
	"github.com/Airacer-Inc/airacer-resource/utils"
	"github.com/Airacer-Inc/airacer-resource/utils/s3cli"
	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
)

type ReqPreSignUrl struct {
	Key string `json:"key" form:"key"`
}

// PreSignUrlHandler 获取预签名URL
// curl http://127.0.0.1:8080/preSignUrl
func PreSignUrlHandler(c *gin.Context) {
	var req ReqPreSignUrl
	if err := c.ShouldBind(&req); err != nil {
		Error(c, fmt.Sprintf("param invalid:%s", err.Error()))
		return
	}
	if len(req.Key) == 0 {
		Error(c, "key invalid")
		return
	}
	cli := s3cli.Default()
	urn := path.Join(fmt.Sprintf("%s/%s", utils.UUIDShort(uuid.New().String()), req.Key))
	url, err := cli.PreSignURL(config.S3.Bucket, urn)
	if err != nil {
		Error(c, fmt.Sprintf("presign error: %s", err.Error()))
		return
	}

	OK(c, gin.H{"urn": urn, "url": url})
}
