package handler

import (
	"github.com/Airacer-Inc/airacer-resource/utils/jwt"

	"github.com/gin-gonic/gin"
)

type LoginReq struct {
	UserName string `json:"username" form:"username"`
	Password string `json:"password" form:"password"`
}

type LoginResp struct {
	Token string `json:"token"`
}

// LoginHandler 登录【调试环境】
func LoginHandler(c *gin.Context) {
	var user LoginReq
	if err := c.ShouldBind(&user); err != nil {
		Error(c, err)
		return
	}

	// 模拟登录
	if user.UserName == "airacer" && user.Password == "Pass1234" {
		token := jwt.MustGenerateToken(&jwt.UserModel{App: jwt.Default, UserId: "1001"})
		OK(c, LoginResp{Token: token})
		return
	}

	Error(c, "account error")
	return
}
