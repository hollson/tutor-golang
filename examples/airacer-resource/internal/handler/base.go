package handler

import (
	"github.com/gin-gonic/gin"
)

func OK(c *gin.Context, data any) {
	c.JSON(200, gin.H{
		"code": 0,
		"data": data,
	})
}

func Error(c *gin.Context, msg any) {
	c.Abort()
	c.JSON(200, gin.H{
		"code": -1,
		"msg":  msg,
	})
}
