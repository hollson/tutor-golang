package middleware

import (
	"errors"
	"github.com/Airacer-Inc/airacer-resource/utils/jwt"
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
)

func JWTAuth() gin.HandlerFunc {
	return func(c *gin.Context) {
		token := c.Request.Header.Get("Authorization")
		if len(token) == 0 {
			c.AbortWithStatusJSON(http.StatusUnauthorized, gin.H{
				"code": -1,
				"msg":  "signature is invalid",
			})
			return
		}

		claims, err := jwt.ParseToken(token)
		if err != nil {
			if errors.Is(err, jwt.ErrExpired) {
				// Token过期不超过10分钟则给它续签
				if time.Since(claims.ExpiresAt.Time) < time.Minute*10 {
					newToken, err := jwt.GenerateToken(claims.UserModel)
					if err == nil && newToken != "" {
						c.Header("renew", newToken) //续签Token
						goto NEXT
					}
				}
			}

			// 鉴权失败
			c.Header("Location", "/login")
			c.AbortWithStatusJSON(http.StatusUnauthorized, gin.H{
				"code": -1,
				"msg":  "signature is invalid",
			})
			return
		}

	NEXT:
		c.Set("UserModel", claims.UserModel)
		c.Next()
	}
}
