package config

import (
	"fmt"
	"time"
)

// 全局Model
type configModel struct {
	App     *app      `toml:"app"`
	S3      *s3Config `toml:"s3"`
	JwtAuth *jwtAuth  `toml:"jwtAuth"`
}

func (p *configModel) String() string {
	return fmt.Sprintf("%+v", *p)
}

// =============================App Config==============================

// 应用
type app struct {
	Name    string `toml:"app_name"` // 应用名称
	Port    int    `toml:"app_port"` // 服务端口
	LogPath string `toml:"log_path"` // 日志目录
}

func (p *app) String() string {
	return fmt.Sprintf("%+v", *p)
}

// ============================ S3 Config ===============================

type s3Config struct {
	AccessKey string `toml:"accessKey"`
	SecretKey string `toml:"secretKey"`
	Region    string `toml:"region"`
	Bucket    string `toml:"bucket"`
}

func (c *s3Config) String() string {
	return fmt.Sprintf("%+v", *c)
}

// ============================  JwtAuth  ===============================

type jwtAuth struct {
	Secret string        `toml:"secret"`
	Expire time.Duration `toml:"expire"` //过期时间(秒)
}

func (c *jwtAuth) String() string {
	return fmt.Sprintf("%+v", *c)
}
