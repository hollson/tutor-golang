package config

import (
	"fmt"

	"github.com/Airacer-Inc/airacer-resource/utils"
	"github.com/BurntSushi/toml"

	"log"
	"os"
)

// 全局变量
var (
	App     *app
	S3      *s3Config
	JwtAuth *jwtAuth
)

// Load 配置文件加载优先级：
// 1. 指定启动参数, 如：go run main.go ./etc/config.toml
// 2. 缺省配置文件: $HOME/.airacer/resource-server/config.toml
// 3. 项目配置文件: ./etc/config.toml
func Load(path ...string) {
	_path := "./etc/config.toml"
	if len(path) > 0 {
		_path = path[0]
		goto NEXT
	}
	if withHome, err := utils.HomePath(); err == nil {
		withHome = fmt.Sprintf("%s/.airacer/resource-server/config.toml", withHome)
		if _, err := os.Stat(withHome); err == nil {
			_path = withHome
			goto NEXT
		}
	}

NEXT:
	var model configModel
	if _, err := toml.DecodeFile(_path, &model); err != nil {
		log.Fatalf("Config file read error(%s) %v\n", _path, err)
	}

	//S3_DEFAULT_BUCKET
	defaultBucket := os.Getenv("S3_DEFAULT_BUCKET")
	if len(defaultBucket) > 0 {
		model.S3.Bucket = defaultBucket
	}
	App = model.App
	S3 = model.S3
	JwtAuth = model.JwtAuth
}
