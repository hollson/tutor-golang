#!/bin/bash
# shellcheck source=/dev/null
source ./sdk.sh

# 全局变量
export APP_NAME="Aws Resource Server"        # 应用名称
export APP_VERSION="v1.0.0"                  # 应用版本
export APP_ROOT="$HOME/airacer/aws-resource" # 应用目录

#CMD install|ins|安装程序(不运行)
function install() {
    mkdir -p $APP_ROOT
    cp ./* $APP_ROOT
    rm -rf $APP_ROOT/*_test.sh
    logInfo "安装成功"
    ls -lh $APP_ROOT
    echo ""
    return 1
}

#CMD run|-|运行程序
function run() {
    if [ "$(arch)" == "arm64" ]; then
        #nohup $APP_ROOT/apx_resource_linux_arm64 > /dev/null 2>&1 &
        nohup $APP_ROOT/apx_resource_linux_arm64 2>&1 &
        status
        logInfo "运行成功"
        return 0
    fi

    if [ "$(arch)" == "x64" ]; then
        #nohup $APP_ROOT/apx_resource_linux_amd64 > /dev/null 2>&1 &
        nohup $APP_ROOT/apx_resource_linux_amd64 2>&1 &
        status
        logInfo "运行成功"
        return 0
    fi

    logErr "运行失败"
    return 1
}

#CMD stop|-|停止服务
function stop() {
    pkill -f "apx_resource_linux"
    if test $?; then
        logInfo "已停止运行"
        return
    fi
    logErr "执行失败"
}

#CMD restart|res|重启服务
function restart() {
    stop
    run
}

#CMD status|stt|查看服务状态
function status() {
    ps -ef | grep apx_resource_linux
}

#CMD info|-|查看Airacer应用信息
function info() {
    echox magenta 1 "应用名称：$APP_NAME"
    echox magenta 1 "安装目录：$APP_ROOT"
    echox magenta 1 "环境变量：S3_DEFAULT_BUCKET = $S3_DEFAULT_BUCKET"
    echox magenta 1 "          GIN_MODE = $GIN_MODE"
}

#CMD sys|-|查看系统信息
function sys() {
    sysInfo
    echo ""
    sysInspect
}

# 继承通用命令，并重载main函数
#CMD list|-|查看函数列表
#CMD version|ver|查看应用版本
#CMD help|*|查看帮助说明
main
