package jwt

import (
	"fmt"
)

// AppType 应用类型
type AppType string

const (
	Taxi    AppType = "a"
	TaxiMgr AppType = "b"
	DcMgr   AppType = "c"
	Bro     AppType = "d"
	Opr     AppType = "e"
	Default AppType = "z"
)

// UserModel Jwt业务模型
type UserModel struct {
	App    AppType `json:"app"`
	UserId string  `json:"userId,omitempty"`
	//UserName string   `json:"userName,omitempty"`
	//Roles    []string `json:"roles,omitempty"`
	//Email    string   `json:"email,omitempty"`
}

func (u *UserModel) String() string {
	return fmt.Sprintf("%+v", *u)
}
