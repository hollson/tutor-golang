package jwt

import (
	jwt5 "github.com/golang-jwt/jwt/v5"
	"github.com/google/uuid"
	"strings"
	"time"
)

var (
	_secret               = []byte("0000000000-0000000000-0000000000")
	_issuer               = "jwt.io"
	_expire time.Duration = time.Hour * 24
)

var (
	ErrExpired = jwt5.ErrTokenExpired
)

type Claims struct {
	*UserModel
	*jwt5.RegisteredClaims
}

func InitJwt(issuer string, secret string, expires time.Duration) {
	_secret = []byte(secret)
	_issuer = issuer
	_expire = expires
}

func GenerateToken(model *UserModel) (string, error) {
	claims := &Claims{
		UserModel: model,
		RegisteredClaims: &jwt5.RegisteredClaims{
			ID:        uuid.New().String(),                                   // JTI
			Issuer:    _issuer,                                               // 签发机构
			ExpiresAt: jwt5.NewNumericDate(time.Now().Add(_expire)),          // 过期时间
			NotBefore: jwt5.NewNumericDate(time.Now().Add(time.Second * 10)), // 生效时间
			IssuedAt:  jwt5.NewNumericDate(time.Now()),                       // 签发时间
			Subject:   model.UserId,                                          // 签发对象（用户、设备、应用程序或其他实体的唯一标识符）
			Audience:  jwt5.ClaimStrings{"DC", "Bro", "Opr"},                 // 签发受众
		},
	}

	token := jwt5.NewWithClaims(jwt5.SigningMethodHS256, claims)
	tokenString, err := token.SignedString(_secret)
	if err != nil {
		return "", err
	}
	return tokenString, nil
}

func MustGenerateToken(u *UserModel) string {
	t, _ := GenerateToken(u)
	return t
}

func ParseToken(token string) (*Claims, error) {
	token = strings.TrimPrefix(token, "Bearer ")
	claims := &Claims{}
	_, err := jwt5.ParseWithClaims(token, claims, func(t *jwt5.Token) (any, error) {
		return _secret, nil
	})
	return claims, err
}
