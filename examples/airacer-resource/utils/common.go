package utils

import (
	"bufio"
	"crypto/md5"
	"encoding/hex"
	"fmt"
	"io"
	"os"
	"os/user"
	"strconv"
	"strings"
)

func HomePath() (string, error) {
	currUser, err := user.Current()
	if err != nil {
		return "", fmt.Errorf("HomePath error %s", err)
	}
	return currUser.HomeDir, nil
}

func UUIDShort(s string) string {
	return strings.ReplaceAll(s, "-", "")
}

func GenPid() error {
	file, err := os.Create("apx_resource.pid")
	if err != nil {
		return err
	}
	defer file.Close()
	writer := bufio.NewWriter(file)
	if _, err = writer.WriteString(strconv.Itoa(os.Getpid())); err != nil {
		return err
	}

	return writer.Flush()
}

// ExePath 获取当前程序的可执行文件路径
func ExePath() string {
	executablePath, _ := os.Executable()
	return executablePath
}

func Md5(r io.Reader) (string, error) {
	hash := md5.New()
	_, err := io.Copy(hash, r)
	if err != nil {
		return "", err
	}
	return hex.EncodeToString(hash.Sum(nil)), nil
}
