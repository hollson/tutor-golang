# AwsS3应用

## 一. Aws客户端

> aws客户端工具使用教程可参考 [官方文档](https://docs.aws.amazon.com/zh_cn/cli/latest/userguide/cli-chap-welcome.html)。

### 1.1 安装配置

```shell
# 下载安装
curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscli.zip"
#curl "https://awscli.amazonaws.com/awscli-exe-linux-aarch64.zip" -o "awscli.zip"
unzip awscli.zip
sudo ./aws/install
aws --version

# 添加配置
aws configure
> AWS Access Key ID [None]: SYBSAKIAYXWTULWBIDOZTF45
> AWS Secret Access Key [None]: SYBSaGOtcX3af9G9UOhFWdu3Aym/dVf5684RF+tecw+d
> Default region name [None]: us-east-1
> Default output format [None]: json

# 查看配置
aws configure list

# 开启智能提示
export AWS_CLI_AUTO_PROMPT=on
```

### 1.2 简单命令

```shell
# 查看帮助
aws s3 help

# 创建桶
aws s3 mb s3://mybucket

# 查看桶或对象
aws s3 ls
aws s3 ls s3://mybucket
aws s3 ls s3://mybucket/object-name

# 删除桶
aws s3 rb s3://mybucket
aws s3 rb s3://mybucket --force

# 删除对象(recursive:递归)
aws s3 rm s3://mybucket/abc/test
aws s3 rm s3://mybucket/abc --recursive

# 移动对象
aws s3 mv s3://mybucket/abc s3://mu bucket/ # S3 -> S3
aws s3 mv test.zip s3://mybucket                # 本地 -> S3
aws s3 mv s3://mybucket/test.zip ./             # S3 -> 本地
aws s3 mv s3://bucketname/a/ s3://bucketname/b/ --recursive #a目录到b目录

# 复制(上传下载)对象
aws s3 cp s3://mybucket/abc s3://mybucket/                                    # S3 -> S3
aws s3 cp test.zip s3://mybucket                                                  # 本地 -> S3
aws s3 cp s3://mybucket/test.zip ./                                               # S3 -> 本地
echo "hello world"|aws s3 cp - s3://mybucket/test.zip                             # 文本 -> S3
aws s3 cp s3://mybucket/test.zip -                                                # S3 -> 控制台
aws s3 cp s3://mybucket/pre - |bzip2 --best|aws s3 cp - s3://mybucket/key.bz2 # S3 -> (控制台)压缩 -> S3

# 差异同步
aws s3 sync . s3://mybucket/abc                                       # 本地 -> S3
aws s3 sync . s3://mybucket/abc --delete                              # 本地 -> S3 (删除S3多余对象)
aws s3 sync s3://mybucket/abc . --delete                              # S3 -> 本地 (删除本地多余对象)
aws s3 sync . s3://mybucket/abc --storage-class STANDARD_IA           # 与不频繁访问存储类同步
aws s3 sync . s3://mybucket/abc --delete --exclude "abc/MyFile?.txt" # 排除文件

# 权限控制(公开读取)
aws s3 sync . s3://mybucket/abc --acl public-read
# 查看权限
aws s3api get-bucket-acl --bucket mybucket

# 查看文件大小和总数
aws s3 ls --summarize --human-readable --recursive s3://mafool-test  # 递归(不推荐)
aws s3api list-objects --bucket mafool-test --query "[sum(Contents[].Size), length(Contents[])]"


# S3Api接口
aws s3api list-objects --bucket text-content
aws s3api head-bucket --bucket mybucket
aws s3api put-object --bucket text-content --key dir-1/my_images.tar.bz2 --body my_images.tar.bz2

#============Reworld操作实例===============
aws s3 ls
aws s3 ls s3://sg-sourcefile.reworld.io/resource/
aws s3 sync ./resource s3://sg-sourcefile.reworld.io/resource --delete --acl public-read

http://sg-sourcefile.reworld.io.s3.ap-southeast-1.amazonaws.com/resource/hello.txt
http://sg-sourcefile.reworld.io.s3.ap-southeast-1.amazonaws.com/resource/T37HxTBvDT1RCvBVdK

```



## 跨站请求

```json
[
    {
        "AllowedHeaders": ["*"],
        "AllowedMethods": ["HEAD","GET", "PUT", "POST", "DELETE"],
        "AllowedOrigins": ["*"],
        "ExposeHeaders": ["ETag"],
        "MaxAgeSeconds": 3000
    }
]
```



## Options
**Bucket：**
```shell
s3://airacer-cn-test		# 测试  
s3://airacer-cn-beta		# 仿真
s3://airacer-cn-release		# 生产   
```

## 权限配置

> 需要创建桶存储策略之后， `Amazon S3 > Buckets > MyBucket`

对象所有权： 选择【ACL已启用】
阻止所有公开访问： 取消

```json
{
    "Version": "2012-10-17",
    "Id": "Policy202401100001",
    "Statement": [
        {
            "Sid": "Stmt202401100001",
            "Effect": "Allow",
            "Principal": "*",
            "Action": [
                "s3:Put*",
                "s3:Get*",
                "s3:List*",
                "s3:Delete*"
            ],
            "Resource": "arn:aws:s3:::airacer-cn-beta/*"
        }
    ]
}
```

## 生命周期



## 版本控制




## 对象标签
```shell
aws s3api list-objects-v2 --bucket BucketName --query 'Contents[?TagSet[?Key==`Key1` && Value==`Value1`]]'
```
```go
package main

import (
	"fmt"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/s3"
)

func main() {
	sess, err := session.NewSession(&aws.Config{
		Region: aws.String("us-west-2"), // 设置您的区域
	})

	if err != nil {
		fmt.Println("Failed to create session", err)
		return
	}

	svc := s3.New(sess)

	// 指定需要匹配的标签键和值
	tagKey := "Key1"
	tagValue := "Value1"

	input := &s3.ListObjectsV2Input{
		Bucket: aws.String("BucketName"), // 设置您的存储桶名称
		TagFilters: []*s3.Tag{
			{
				Key:   aws.String(tagKey),
				Value: aws.String(tagValue),
			},
		},
	}

	output, err := svc.ListObjectsV2(input)
	if err != nil {
		fmt.Println("Failed to list objects", err)
		return
	}

	for _, obj := range output.Contents {
		fmt.Println(*obj.Key)
	}
}
```
https://docs.aws.amazon.com/zh_cn/AmazonS3/latest/userguide/object-tagging.html



## 预签名URL

```shell
aws s3 presign s3://mafool-test/my-object --expires-in 3600
aws s3 presign s3://BUCKET1/mydoc.txt --expires-in 604800 --region af-south-1 --endpoint-url https://s3.af-south-1.amazonaws.com

aws s3 presign s3://airacer-cn-beta/test/a.txt --expires-in 604800
aws s3 presign s3://airacer-cn-beta/test/a.txt --expires-in 604800 --region us-east-1 --endpoint-url https://us-east-1.amazonaws.com

https://docs.aws.amazon.com/zh_cn/AmazonS3/latest/userguide/ShareObjectPreSignedURL.html
https://docs.aws.amazon.com/zh_cn/AmazonS3/latest/userguide/example_s3_Scenario_PresignedUrl_section.html
```

```html
<!DOCTYPE html>
<html>
<head>
  <style>
    body {display: flex;flex-direction: column;align-items: center;justify-content: center;height: 100vh;background-color: #f5f5f5;}
    #preUrl {width: 300px;padding: 10px;margin-bottom: 20px;}
    .file-upload {position: relative;margin-bottom: 20px;}
    .file-input {display: none;}
    .upload-button {padding: 10px 20px;background-color: #007bff;color: #fff;border: none;cursor:pointer;}
    .file-name {font-size: 18px;font-weight: bold;}
  </style>
</head>
<body>
  <span>URL: <input type="text" id="preUrl" placeholder="S3的预签名URL" /></span>
  <div class="file-upload">
    <input type="file" id="fileInput" class="file-input" onchange="showFileName()"/>
    <label for="fileInput" class="upload-button">选择文件</label>
  </div>
  <button onclick="uploadFile()" class="upload-button">上传</button>
  <div id="fileName" class="file-name"></div>

  <script>
    function showFileName() {
      var fileInput = document.getElementById("fileInput");
      var fileNameElement = document.getElementById("fileName");
      fileNameElement.innerText = fileInput.files[0].name;
    }

    function uploadFile() {
      var fileInput = document.getElementById("fileInput");
      var preUrl = document.getElementById("preUrl");
      var file = fileInput.files[0];
      var fileNameElement = document.getElementById("fileName");
      fileNameElement.innerHTML = "已选择文件：" + file.name;
      var xhr = new XMLHttpRequest();
      xhr.open("PUT", preUrl.value,true);
      xhr.setRequestHeader("Content-Type", file.type);
      xhr.onreadystatechange = function () {
        if (xhr.readyState === 4) {
          if (xhr.status === 200) {
            alert("上传成功");} 
          else {alert("上传失败");}
        }
      };
      xhr.send(file);
    }
  </script>
</body>
</html>
```



## 其他

创建访问密钥：`登录Aws -> (右上角)账户 -> 安全凭证 -> 访问密钥(创建访问密钥)`



## 参考链接：
https://www.python100.com/html/3JFUV6162UT7.html

https://blog.csdn.net/weixin_45565886/article/details/131239351

https://blog.csdn.net/hhhlkkk/article/details/126202894

https://aws.amazon.com/cn/s3/

https://docs.aws.amazon.com/zh_cn/AmazonS3/latest/userguide/example_s3_Scenario_PresignedUrl_section.html

