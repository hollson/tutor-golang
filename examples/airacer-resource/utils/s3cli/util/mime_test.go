// Copyright 2024 Hollson. All rights reserved.
// Use of this source code is governed by a MIT style
// license that can be found in the LICENSE file.

package main

import (
	"fmt"
	"io"
	"log"
	"os"
	"strings"
	"testing"
)

func TestContentType(t *testing.T) {
	var path = "./resource/"

	files, err := os.ReadDir(path)
	if err != nil {
		log.Fatal("Path not found", err)
	}

	for _, file := range files {
		f, err := os.Open(path + file.Name())
		if err != nil {
			log.Fatal("open file error:"+f.Name(), err)
		}

		contentType, err := ContentTypeV2(f)
		if err != nil {
			panic(err)
		}

		fmt.Printf("%s\t\t%s\n", file.Name(), contentType)
	}
}

func TestReader(t *testing.T) {
	// 假设原始对象是一个字符串
	input := "This is a sample string that contains more than 128 characters"

	// 创建一个包装原始对象的限制读取的io.Reader对象
	reader := io.LimitReader(strings.NewReader(input), 32)

	// 使用Read方法读取前128个字符
	buf := make([]byte, 32)
	n, err := reader.Read(buf)
	if err != nil && err != io.EOF {
		log.Fatal(err)
	}

	// 打印读取结果
	fmt.Println(string(buf[:n])) // 这里使用buf[:n]是因为Read方法可能会读取不满指定长度的数据

	// 原始对象未受影响
	fmt.Println(input)
}
