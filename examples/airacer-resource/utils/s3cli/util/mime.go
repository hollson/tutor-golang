// Copyright 2024 Hollson. All rights reserved.
// Use of this source code is governed by a MIT style
// license that can be found in the LICENSE file.

package main

import (
	"io"
	"net/http"
	"os"
)

// ContentType Use the net/http package's handy DectectContentType function. Always returns a valid
// content-type by returning "application/octet-stream" if no others seemed to match.
func ContentType(f *os.File) (string, error) {
	buffer := make([]byte, 512)

	_, err := f.Read(buffer)
	if err != nil {
		return "", err
	}
	contentType := http.DetectContentType(buffer)
	return contentType, nil
}

func ContentTypeV2(reader io.Reader) (string, error) {
	buffer := make([]byte, 128)
	lr := io.LimitReader(reader, 128)
	_, err := lr.Read(buffer)
	if err != nil {
		return "", err
	}
	contentType := http.DetectContentType(buffer)
	return contentType, nil
}
