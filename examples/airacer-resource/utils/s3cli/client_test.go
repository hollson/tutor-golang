package s3cli

import (
	"fmt"
	"github.com/Airacer-Inc/airacer-resource/internal/config"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/credentials"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/s3"
	"log"
	"testing"
	"time"
)

func init() {
	config.Load("D:\\code\\airacer\\airacer-resource\\etc\\config.toml")
}

func TestPreSignURL(t *testing.T) {
	var (
		accessKey = "AKIAYXWTULWBIDOZTF45"
		secretKey = "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"
		region    = "us-east-1"
	)
	// 创建 AWS 会话
	sess, err := session.NewSession(&aws.Config{
		Credentials: credentials.NewStaticCredentials(accessKey, secretKey, ""),
		Region:      aws.String(region),
	})
	if err != nil {
		log.Fatal(err)
	}

	// 创建 S3 服务客户端
	svc := s3.New(sess)
	bucket := "airacer-cn-beta"
	key := "test/xxx.png"

	// 创建预签名 URL 请求
	req, _ := svc.PutObjectRequest(&s3.PutObjectInput{
		Bucket: aws.String(bucket),
		Key:    aws.String(key),
		//ACL:    aws.String(s3.ObjectCannedACLPublicRead),
	})

	// 设置 URL 的有效期限
	duration := time.Hour * 24
	urlStr, err := req.Presign(duration)
	if err != nil {
		log.Fatal(err)
	}

	fmt.Println(urlStr)
}

//type Presigner struct {
//	PresignClient *s3.PresignClient
//}
//
//func (presigner Presigner) PutObject(
//	bucketName string, objectKey string, lifetimeSecs int64) (*v3.PresignedHTTPRequest, error) {
//	request, err := presigner.PresignClient.PresignPutObject(context.TODO(), &s3.PutObjectInput{
//		Bucket: aws.String(bucketName),
//		Key:    aws.String(objectKey),
//	}, func(opts *s3.PresignOptions) {
//		opts.Expires = time.Duration(lifetimeSecs * int64(time.Second))
//	})
//	if err != nil {
//		log.Printf("Couldn't get a presigned request to put %v:%v. Here's why: %v\n",
//			bucketName, objectKey, err)
//	}
//	return request, err
//}
