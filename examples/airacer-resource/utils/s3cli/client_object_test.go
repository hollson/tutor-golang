// Copyright 2021 Hollson. All rights reserved.
// Use of this source code is governed by a MIT style
// license that can be found in the LICENSE file.

package s3cli

import (
	"fmt"
	"os"
	"strings"
	"testing"
	"time"
)

func TestClient_PutObject(t *testing.T) {
	cli := Default()

	fmt.Println(cli.PutObject("airacer-cn-test", "test01", strings.NewReader("hello test01")))
	fmt.Println(cli.PutObject("airacer-cn-test", "test01", strings.NewReader("hello 覆盖01")))
	fmt.Println(cli.PutObject("airacer-cn-test", "test02", strings.NewReader("hello test02")))
	fmt.Println(cli.PutObject("airacer-cn-test", "test01/aaa", strings.NewReader("hello aaa")))
	fmt.Println(cli.PutObject("airacer-cn-test", "test01/bbb", strings.NewReader("hello_bbb")))

	f, _ := os.Open("/Users/sybs/Desktop/elephant.png")
	fmt.Println(cli.PutObject("airacer-cn-test", "elephant.png", f))
	// https://airacer-cn-test.s3.ap-southeast-1.amazonaws.com/elephant.png
}

func TestClient_Get(t *testing.T) {
	cli := Default()
	n, ret, err := cli.Get("airacer-cn-test", "test01")
	fmt.Println(n, string(ret), err)
	n, ret, err = cli.Get("airacer-cn-test", "test02")
	fmt.Println(n, string(ret), err)
	n, ret, err = cli.Get("airacer-cn-test", "test01/aaa")
	fmt.Println(n, string(ret), err)
	n, ret, err = cli.Get("airacer-cn-test", "test01/bbb")
	fmt.Println(n, string(ret), err)
}

func TestClient_ListObjects(t *testing.T) {
	cli := Default()
	fmt.Println(cli.ListObjects("airacer-cn-test"))
	fmt.Println(cli.ListObjects("airacer-cn-test", "test"))
	fmt.Println(cli.ListObjects("airacer-cn-test", "test01"))
}

func TestClient_DeleteObject(t *testing.T) {
	cli := Default()

	fmt.Println(cli.DeleteObject("airacer-cn-test", "elephant"))
}

func TestClient_DeleteObjects(t *testing.T) {
	cli := Default()
	fmt.Println(cli.DeleteObjects("airacer-cn-test", "elephant2", "test02"))
}

func TestClient_Upload(t *testing.T) {
	cli := Default()
	fmt.Println(cli.Upload("airacer-cn-test", "file01", strings.NewReader("hello01.jpg")))
	//fmt.Println(cli.Upload("airacer-cn-test", "file01", strings.NewReader("hello01.png")))
	//fmt.Println(cli.Upload("airacer-cn-test", "file02", strings.NewReader("hello02.txt")))
	//fmt.Println(cli.Upload("airacer-cn-test", "file01/aaa", strings.NewReader("hello_aaa.mp3")))
	//fmt.Println(cli.Upload("airacer-cn-test", "file01/bbb", strings.NewReader("hello_bbb.zip")))

	//f, _ := os.Open("/Users/sybs/Desktop/elephant.png")
	//fmt.Println(cli.Upload("airacer-cn-test", "postgres.png", f))

}

func TestClient_UploadWithContext(t *testing.T) {
	cli := Default()
	fmt.Println(cli.UploadWithContext("airacer-cn-test", "file01", strings.NewReader("hello01.jpg"), time.Second*5))
}

func TestClient_Download(t *testing.T) {
	cli := Default()
	fmt.Println(cli.Download("airacer-cn-test", "file01"))
	fmt.Println(cli.Download("airacer-cn-test", "file01/aaa"))
}

func TestClient_CopyObject(t *testing.T) {
	cli := Default()
	fmt.Println(cli.CopyObject("airacer-cn-test", "file01/aaa", "reworld-bucket-test-02", "hello_copy.mp3"))
	n, ret, err := cli.Get("reworld-bucket-test-02", "hello_copy.mp3")
	fmt.Println(n, string(ret), err)
}

func TestAracer(t *testing.T) {
	cli := Default()
	//fmt.Println(cli.PutObject("airacer-cn-beta", "当前为公测版或仿真版,major是业务资源根目录", strings.NewReader("当前为公测版或仿真版,major是业务资源根目录,所有跟业务相关的资源都放在major目录下")))
	//fmt.Println(cli.PutObject("airacer-cn-release", "当前为生产环境,major是业务资源根目录", strings.NewReader("当前为生产环境,major是业务资源根目录,所有跟业务相关的资源都放在major目录下")))

	f, _ := os.Open("D:\\code\\airacer\\solutions\\aws_s3\\util\\resource\\a.txt")
	fmt.Println(cli.Upload("airacer-cn-test", "/test/a.txt", f))

	n, ret, err := cli.Get("airacer-cn-test", "/test/a.txt")
	fmt.Println(n, string(ret), err)
}
