// Copyright 2021 Hollson. All rights reserved.
// Use of this source code is governed by a MIT style
// license that can be found in the LICENSE file.

package s3cli

/*
   S3客户端 - 桶管理.
*/
import (
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/s3"
)

// CreateBucket 创建桶
//
//	bucket命名规则：不能使用下划线，如：bucket_test_01
//	如果已存在，则返回BucketAlreadyExists或BucketAlreadyOwnedByYou错误
func (s *Client) CreateBucket(bucket string) (*s3.CreateBucketOutput, error) {
	_s3, err := s.NewS3()
	if err != nil {
		return nil, err
	}

	input := &s3.CreateBucketInput{
		Bucket: aws.String(bucket),
		// ACL:    aws.String(s3.BucketCannedACLPrivate),
		ACL: aws.String(s3.ObjectCannedACLBucketOwnerFullControl),
	}

	result, err := _s3.CreateBucket(input)
	if err != nil {
		return nil, err
	}

	// 等待回调结果
	err = _s3.WaitUntilBucketExists(&s3.HeadBucketInput{Bucket: aws.String(bucket)})
	return result, err
}

// BucketAcl 桶权限,参考s3.PutBucketAclInput
type BucketAcl struct {
	*s3.PutBucketAclInput
	ACL                 *string
	AccessControlPolicy *s3.AccessControlPolicy // 包含为每个被授权者设置对象的ACL权限的元素。
	ExpectedBucketOwner *string                 // 预期存储桶所有者的帐户ID。 如果存储桶由其他帐户拥有，则请求将失败并显示HTTP 403（拒绝访问）错误。
	GrantFullControl    *string                 // 允许被授予者对存储桶的读取、写入、读取ACP和写入ACP权限。Outposts上的Amazon S3不支持此操作。
	GrantRead           *string                 // 允许被授权者列出存储桶中的对象。
	GrantReadACP        *string                 // 允许受让人读取存储桶ACL。Outposts上的Amazon S3不支持此操作。
	GrantWrite          *string                 // 允许被授权者在存储桶中创建新对象,对于现有对象的存储桶和对象所有者，还允许删除和覆盖这些对象。
	GrantWriteACP       *string                 // 允许被授权者为适用的存储桶编写 ACL。 Outposts上的Amazon S3不支持此操作。
}

// SetBucketAcl 设置桶权限
//
//	默认ACL：s3.BucketCannedACLPrivate
func (s *Client) SetBucketAcl(bucket string, acl BucketAcl) (*s3.PutBucketAclOutput, error) {
	_s3, err := s.NewS3()
	if err != nil {
		return nil, err
	}
	input := &s3.PutBucketAclInput{
		Bucket:              aws.String(bucket),
		ACL:                 acl.ACL,
		AccessControlPolicy: acl.AccessControlPolicy,
		ExpectedBucketOwner: acl.ExpectedBucketOwner,
		GrantFullControl:    acl.GrantFullControl,
		GrantRead:           acl.GrantRead,
		GrantReadACP:        acl.GrantReadACP,
		GrantWrite:          acl.GrantWrite,
		GrantWriteACP:       acl.GrantWriteACP,
	}
	return _s3.PutBucketAcl(input)
}

// GetBucketAcl 获取桶权限
func (s *Client) GetBucketAcl(bucket string) (*s3.GetBucketAclOutput, error) {
	_s3, err := s.NewS3()
	if err != nil {
		return nil, err
	}
	input := &s3.GetBucketAclInput{Bucket: aws.String(bucket)}
	return _s3.GetBucketAcl(input)
}

// DeleteBucket 删除桶(fixme:处理超时)
func (s *Client) DeleteBucket(bucket string) (*s3.DeleteBucketOutput, error) {
	_s3, err := s.NewS3()
	if err != nil {
		return nil, err
	}

	input := &s3.DeleteBucketInput{
		Bucket: aws.String(bucket),
	}

	result, err := _s3.DeleteBucket(input)
	if err != nil {
		return nil, err
	}

	// 等待回调结果
	// err = _s3.WaitUntilBucketExists(&s3.HeadBucketInput{Bucket: aws.String(bucket)})
	return result, err
}

// ListBuckets 获取桶集合
//
//	结果示例：
//	{
//	  Buckets: [
//	        {
//	          CreationDate: 2021-09-23 06: 16: 40 +0000 UTC,
//	          Name: "bucket-test-01"
//	        },
//	        {
//	          CreationDate: 2021-10-13 05: 39: 19 +0000 UTC,
//	          Name: "bucket-test-02"
//	        },
//	    ],
//	  Owner: {
//	        ID: "8d084cc8905e1de3c53368065b23c872e92b64bc431b292107cbab4479f0a97b"
//	    }
//	}
func (s *Client) ListBuckets() (*s3.ListBucketsOutput, error) {
	_s3, err := s.NewS3()
	if err != nil {
		return nil, err
	}
	return _s3.ListBuckets(nil)
}
