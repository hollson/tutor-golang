// Copyright 2021 Hollson. All rights reserved.
// Use of this source code is governed by a MIT style
// license that can be found in the LICENSE file.

package s3cli

import (
	"context"
	"fmt"
	"io"
	"time"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/s3"
	"github.com/aws/aws-sdk-go/service/s3/s3manager"
)

// ListObjects 预览对象(递归遍历一个目录或子目录)
//
//	调用示例：
//	  cli.ListObjects("bucket-test-01")			//查询全部
//	  cli.ListObjects("bucket-test-01","key_1001")	//查询子目录
//
//	结果示例：
//	 {
//	   Contents: [
//	         {
//	           ETag: "\"bbb92b9f0c7920dd2890e6a5ccbc2af1\"",
//	           Key: "key_1001",
//	           LastModified: 2021-10-13 09: 53: 04 +0000 UTC,
//	           Size: 9,
//	           StorageClass: "STANDARD"
//	         }
//	     ],
//	   IsTruncated: false,
//	   KeyCount: 3,
//	   MaxKeys: 1000,
//	   Name: "bucket-test-01",
//	   Prefix: "key_1001"
//	 }
func (s *Client) ListObjects(bucket string, prefix ...string) (*s3.ListObjectsV2Output, error) {
	_s3, err := s.NewS3()
	if err != nil {
		return nil, err
	}

	var _prefix = ""
	if len(prefix) > 0 {
		_prefix = prefix[0]
	}
	return _s3.ListObjectsV2(&s3.ListObjectsV2Input{
		Bucket: aws.String(bucket),
		Prefix: aws.String(_prefix),
	})
}

// PutObject 写入对象
//
//	   默认ACL权限为可公开读取, 即：s3.ObjectCannedACLPublicRead。
//
//	调用示例：
//	  cli.PutObject("bucket-test-01","put_obj_1001",strings.NewReader("val_1001")))
//	  cli.PutObject("bucket-test-01","put_obj_1001/aaa",strings.NewReader("val_1001.aaa")))
//	  cli.PutObject("bucket-test-01","put_obj_1001/bbb",strings.NewReader("val_1001.bbb")))
func (s *Client) PutObject(bucket string, key string, r io.Reader, acl ...string) (*s3.PutObjectOutput, error) {
	_s3, err := s.NewS3()
	if err != nil {
		return nil, err
	}

	ctx := context.Background()
	var cancelFn func()
	if s.timeout > 0 {
		ctx, cancelFn = context.WithTimeout(ctx, s.timeout)
	}
	if cancelFn != nil {
		defer cancelFn()
	}

	input := &s3.PutObjectInput{
		Bucket:      aws.String(bucket),
		Key:         aws.String(key),
		ACL:         aws.String(s3.ObjectCannedACLPublicRead),
		ContentType: aws.String("image/png"),
		// Expires: aws.Time(time.Now().Add(time.Minute * 2)),
		Body: aws.ReadSeekCloser(r),
	}

	if len(acl) > 0 {
		input.ACL = aws.String(acl[0])
	}
	return _s3.PutObjectWithContext(ctx, input)
}

// GetObject 读取对象
//
//	结果示例：{ ETag: "49ec709412d5c157333fbfa27b81db3c" }
//	      -  ETag是由Web服务器分配给在URL中找到的资源的特定版本的不透明标识符
func (s *Client) GetObject(bucket, key string) (*s3.GetObjectOutput, error) {
	_s3, err := s.NewS3()
	if err != nil {
		return nil, err
	}

	input := &s3.GetObjectInput{
		Bucket: aws.String(bucket),
		Key:    aws.String(key),
	}
	return _s3.GetObject(input)
}

// Get 读取对象(GetObject的简化形式)
func (s *Client) Get(bucket, key string) (int64, []byte, error) {
	out, err := s.GetObject(bucket, key)
	if err != nil {
		return 0, nil, err
	}

	data, err := io.ReadAll(out.Body)
	return aws.Int64Value(out.ContentLength), data, err
}

// DeleteObject 删除对象
func (s *Client) DeleteObject(bucket, key string) (*s3.DeleteObjectOutput, error) {
	_s3, err := s.NewS3()
	if err != nil {
		return nil, err
	}

	input := &s3.DeleteObjectInput{
		Bucket: aws.String(bucket),
		Key:    aws.String(key)}

	return _s3.DeleteObject(input)
}

// DeleteObjects 批量删除对象
//
// 结果示例：
//
//	{
//	  Deleted: [{
//	      Key: "k1001"
//	    },{
//	      Key: "k1002"
//	    }]
//	}
func (s *Client) DeleteObjects(bucket string, key ...string) (*s3.DeleteObjectsOutput, error) {
	_s3, err := s.NewS3()
	if err != nil {
		return nil, err
	}
	objects := make([]*s3.ObjectIdentifier, len(key))
	for k, v := range key {
		objects[k] = &s3.ObjectIdentifier{Key: aws.String(v)}
	}

	input := &s3.DeleteObjectsInput{
		Bucket: aws.String(bucket),
		Delete: &s3.Delete{Objects: objects},
	}
	return _s3.DeleteObjects(input)
}

// Upload 大文件上传
//
//	说明：Upload会创建一个上传器，然后将大文件缓存到多个小块中并发上传，并可设置缓冲区大小和并发数量；
//	     默认ACL权限为：可公开读取, 即：s3.ObjectCannedACLPublicRead。
//
//	调用示例：
//	  cli.Upload("bucket-test-01","key_1001",strings.NewReader("val_1001")))
//	  cli.Upload("bucket-test-01","key_1001/aaa",strings.NewReader("val_1001.aaa")))
//	  cli.Upload("bucket-test-01","key_1001/bbb",strings.NewReader("val_1001.bbb")))
//
//	结果示例：
//	 ETag:"cc2c857f89648dbd139d7b2a6665957d"
//	 Location:https://bucket-test-01.s3.ap-south-1.amazonaws.com/key_1001/aaa
//	 UploadID:   //分段上传的ID
//	 VersionID: //上传的对象的版本，仅当S3 Bucket版本化时才会填充。
func (s *Client) Upload(bucket string, key string, data io.Reader, acl ...string) (*s3manager.UploadOutput, error) {
	_session, err := s.newSession()
	if err != nil {
		return nil, err
	}

	input := &s3manager.UploadInput{
		Bucket: aws.String(bucket),
		Key:    aws.String(key),
		ACL:    aws.String(s3.ObjectCannedACLPublicRead), // 可公开访问
		// Expires: aws.Time(time.Now().Add(time.Hour * 24)), // 对象不再可缓存的日期和时间。
		Body: data,
	}

	if len(acl) > 0 {
		input.ACL = aws.String(acl[0])
	}

	var uploader = s3manager.NewUploader(_session)
	result, err := uploader.Upload(input, func(u *s3manager.Uploader) {
		u.PartSize = 10 << 20 // 文件限制10M(超过这个限制，开始分片上传)
		u.MaxUploadParts = 10 // 同时几个队列上传
		u.LeavePartsOnError = false
	})
	return result, err
}

// UploadWithContext 带有请求超时的文件上传(同upload)
func (s *Client) UploadWithContext(bucket string, key string, data io.Reader, timeout time.Duration, acl ...string) (*s3manager.UploadOutput, error) {
	_session, err := s.newSession()
	if err != nil {
		return nil, err
	}
	if timeout < 5*time.Second {
		timeout = time.Duration(30) * time.Second
	}
	ctx, cancel := context.WithTimeout(context.Background(), timeout)
	defer cancel()

	input := &s3manager.UploadInput{
		Bucket: aws.String(bucket),
		Key:    aws.String(key),
		ACL:    aws.String(s3.ObjectCannedACLPublicRead), // 可公开访问
		Body:   data,
	}

	if len(acl) > 0 {
		input.ACL = aws.String(acl[0])
	}

	var uploader = s3manager.NewUploader(_session)
	result, err := uploader.UploadWithContext(ctx, input, func(u *s3manager.Uploader) {
		u.PartSize = 10 << 20 // 文件限制10M(超过这个限制，开始分片上传)
		u.MaxUploadParts = 10 // 同时几个队列上传
		u.LeavePartsOnError = false
	})
	return result, err
}

// Download 大文件下载
//
//	说明：Download会创建一个下载器，并以并发的方式下载文件。
func (s *Client) Download(bucket, key string) (int64, []byte, error) {
	_session, err := s.newSession()
	if err != nil {
		return 0, nil, err
	}

	input := &s3.GetObjectInput{
		Bucket: aws.String(bucket),
		Key:    aws.String(key),
	}

	var downloader = s3manager.NewDownloader(_session)
	buffer := aws.NewWriteAtBuffer([]byte{})

	n, err := downloader.Download(buffer, input, func(d *s3manager.Downloader) {
		d.PartSize = 10 << 20 // 10M
		d.Concurrency = 10
	})
	return n, buffer.Bytes(), err
}

// BatchDelete 批量删除大文件(兼容cleint.DeleteObjects方法)
func (s *Client) BatchDelete(bucket string, keys ...string) error {
	_session, err := s.newSession()
	if err != nil {
		return err
	}

	bat := s3manager.NewBatchDelete(_session, func(bat *s3manager.BatchDelete) { bat.BatchSize = 20 })
	objects := make([]s3manager.BatchDeleteObject, len(keys))
	for k, v := range keys {
		objects[k] = s3manager.BatchDeleteObject{
			Object: &s3.DeleteObjectInput{
				Key:    aws.String(v),
				Bucket: aws.String(bucket),
			},
		}
	}

	return bat.Delete(aws.BackgroundContext(), &s3manager.DeleteObjectsIterator{
		Objects: objects,
	})
}

// CopyObject 拷贝对象
//
//	sourceBucket:	源桶名称
//	sourceKey：	源key名称
//	destBucket：	目标桶名称
//	destKey：	目标Key名称
//
//	官方文档：https://docs.aws.amazon.com/AmazonS3/latest/API/API_CopyObject.html
func (s *Client) CopyObject(sourceBucket, sourceKey, destBucket, destKey string, acl ...string) (*s3.CopyObjectOutput, error) {
	_s3, err := s.NewS3()
	if err != nil {
		return nil, err
	}

	input := &s3.CopyObjectInput{
		Bucket:     aws.String(destBucket),
		ACL:        aws.String(s3.ObjectCannedACLPublicRead),
		CopySource: aws.String(fmt.Sprintf("%s/%s", sourceBucket, sourceKey)),
		Key:        aws.String(destKey),
	}
	if len(acl) > 0 {
		input.ACL = aws.String(acl[0])
	}
	return _s3.CopyObject(input)
}

// PreSignURL 获取预签名URL
func (s *Client) PreSignURL(bucket, key string) (string, error) {
	_s3, err := s.NewS3()
	if err != nil {
		return "", err
	}

	// 创建预签名 URL 请求
	req, _ := _s3.PutObjectRequest(&s3.PutObjectInput{
		Bucket: aws.String(bucket),
		Key:    aws.String(key),
		//ACL:    aws.String(s3.ObjectCannedACLPublicRead),
	})

	return req.Presign(time.Hour * 24)
}
