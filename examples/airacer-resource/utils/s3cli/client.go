// Copyright 2021 Hollson. All rights reserved.
// Use of this source code is governed by a MIT style
// license that can be found in the LICENSE file.

/**************************************************************************************
一. 关于S3：
	s3是aws提供的分布式文件服务，可用于日志的持久化存储，大数据处理结果的输入输出等

二. 安装SDK：
	go get -u github.com/aws/aws-sdk-go

三. 官网文档：
	https://docs.aws.amazon.com/AmazonS3/latest/API/API_Operations_Amazon_Simple_Storage_Service.html
	https://docs.aws.amazon.com/zh_cn/code-samples/latest/catalog/code-catalog-go-s3.html
	https://docs.aws.amazon.com/zh_cn/code-library/latest/ug/go_2_s3_code_examples.html
	https://docs.aws.amazon.com/zh_cn/cli/latest/userguide/cli-chap-install.html
**************************************************************************************/

package s3cli

import (
	"github.com/Airacer-Inc/airacer-resource/internal/config"
	"time"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/credentials"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/s3"
)

// const Prefix = "resource" // 资源存放的子目录

// Client S3-Helper客户端
type Client struct {
	accessKey string        // "SYBSAKIAXFELR7C5DCIKDPBT"
	secretKey string        // "SYBS/Xuj6O50X6i3/pUUGD+UnEcNnA4EhBKbOeghfT2l"
	region    string        // "ap-south-1" // "us-west-1"
	endpoint  string        // http://10.0.6.247:7480
	token     string        //
	timeout   time.Duration // 超时(默认30秒)
}

// Default 默认实例(fixme:从配置文件读取)
func Default() *Client {
	return &Client{
		accessKey: config.S3.AccessKey,
		secretKey: config.S3.SecretKey,
		region:    config.S3.Region,
	}
}

// New 新建实例
func New(accessKey, secretKey, region, endpoint, token string) *Client {
	return &Client{
		accessKey: accessKey,
		secretKey: secretKey,
		region:    region,
		endpoint:  endpoint,
		token:     token,
	}
}

func (s *Client) newSession() (*session.Session, error) {
	return session.NewSession(&aws.Config{
		Credentials: credentials.NewStaticCredentials(s.accessKey, s.secretKey, s.token),
		Region:      aws.String(s.region),
		// Endpoint:    aws.String("http://10.0.6.247:7480"),
	})
}

func (s *Client) NewS3() (*s3.S3, error) {
	_session, err := s.newSession()
	if err != nil {
		return nil, err
	}
	return s3.New(_session), nil
}
