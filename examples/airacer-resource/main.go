package main

import (
	"fmt"

	"github.com/Airacer-Inc/airacer-resource/internal/config"
	"github.com/Airacer-Inc/airacer-resource/internal/handler"
	"github.com/Airacer-Inc/airacer-resource/internal/middleware"
	"github.com/Airacer-Inc/airacer-resource/utils/jwt"
	"github.com/gin-gonic/gin"

	"os"
	"time"
)

func init() {
	config.Load(os.Args[1:]...)
	jwt.InitJwt("airacer.com", config.JwtAuth.Secret, config.JwtAuth.Expire*time.Second)
	//utils.GenPid()
}

//go:generate go run main.go ./etc/config-dev.toml
func main() {
	router := gin.Default()
	router.SetTrustedProxies([]string{"127.0.0.1"})
	//router.Use(middleware.Cors()) // 如果Nginx已经设置了跨域，则这里无需重复设置
	router.POST("login", handler.LoginHandler)

	// Auth
	auth := router.Use(middleware.JWTAuth())
	auth.POST("upload", handler.UploadHandler)
	auth.POST("upload/batch", handler.UploadBatchHandler)
	auth.GET("download", handler.DownloadHandler)
	auth.POST("preSignUrl", handler.PreSignUrlHandler)
	router.Run(fmt.Sprintf(":%d", config.App.Port))
}
