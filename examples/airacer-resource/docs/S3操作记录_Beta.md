## 服务器资源
```shell
# 服务器资源信息
cd /home/ubuntu/bin/airtaxi/resource60002/views
ll |wc -l
du -h --max-depth=1 ./
find . -type f | wc -l

# 同步到s3
aws s3 sync . s3://airacer-cn-beta/asset
```



## 文件整合

```shell
aws s3 cp s3://airacer-cn-beta/asset/banner s3://airacer-cn-beta/res/ --recursive

# 嵌套
aws s3 cp s3://airacer-cn-beta/asset/blog s3://airacer-cn-beta/res/ --recursive
aws s3 mv s3://airacer-cn-beta/res/other s3://airacer-cn-beta/res --recursive
aws s3 mv s3://airacer-cn-beta/res/propaganda s3://airacer-cn-beta/res --recursive

aws s3 cp s3://airacer-cn-beta/asset/channel s3://airacer-cn-beta/res/ --recursive
aws s3 cp s3://airacer-cn-beta/asset/city s3://airacer-cn-beta/res/ --recursive
aws s3 cp s3://airacer-cn-beta/asset/empty-leg s3://airacer-cn-beta/res/ --recursive
aws s3 cp s3://airacer-cn-beta/asset/hot s3://airacer-cn-beta/res/ --recursive

# 嵌套
aws s3 cp s3://airacer-cn-beta/asset/itinerary/active s3://airacer-cn-beta/res/ --recursive

aws s3 cp s3://airacer-cn-beta/asset/logo s3://airacer-cn-beta/res/ --recursive
aws s3 cp s3://airacer-cn-beta/asset/pet-flight s3://airacer-cn-beta/res/ --recursive
aws s3 cp s3://airacer-cn-beta/asset/plane s3://airacer-cn-beta/res/ --recursive

```

```shell
aws s3 mv s3://airacer-cn-beta/asset/banner s3://airacer-cn-beta/res/ --recursive

# 嵌套
aws s3 mv s3://airacer-cn-beta/asset/blog s3://airacer-cn-beta/res/ --recursive
aws s3 mv s3://airacer-cn-beta/res/other s3://airacer-cn-beta/res --recursive
aws s3 mv s3://airacer-cn-beta/res/propaganda s3://airacer-cn-beta/res --recursive

aws s3 mv s3://airacer-cn-beta/asset/channel s3://airacer-cn-beta/res/ --recursive
aws s3 mv s3://airacer-cn-beta/asset/city s3://airacer-cn-beta/res/ --recursive
aws s3 mv s3://airacer-cn-beta/asset/empty-leg s3://airacer-cn-beta/res/ --recursive
aws s3 mv s3://airacer-cn-beta/asset/hot s3://airacer-cn-beta/res/ --recursive

# 嵌套
aws s3 mv s3://airacer-cn-beta/asset/itinerary/active s3://airacer-cn-beta/res/ --recursive

aws s3 mv s3://airacer-cn-beta/asset/logo s3://airacer-cn-beta/res/ --recursive
aws s3 mv s3://airacer-cn-beta/asset/pet-flight s3://airacer-cn-beta/res/ --recursive
aws s3 mv s3://airacer-cn-beta/asset/plane s3://airacer-cn-beta/res/ --recursive


# 查看总数
aws s3api list-objects --bucket airacer-cn-beta --query "[sum(Contents[].Size), length(Contents[])]"
```
