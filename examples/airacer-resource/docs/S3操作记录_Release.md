## 服务器资源
```shell
# 服务器资源信息
cd /home/ubuntu/bin/airtaxi/resource60002/views
ll |wc -l
du -h --max-depth=1 ./
find . -type f | wc -l 

# 同步到s3
aws s3 sync . s3://airacer-cn-release/asset
```



## 文件整合

```shell
aws s3 mv s3://airacer-cn-release/asset/banner s3://airacer-cn-release/res/ --recursive

# 嵌套
aws s3 mv s3://airacer-cn-release/asset/blog s3://airacer-cn-release/res/ --recursive
aws s3 mv s3://airacer-cn-release/res/other s3://airacer-cn-release/res --recursive
aws s3 mv s3://airacer-cn-release/res/propaganda s3://airacer-cn-release/res --recursive

aws s3 mv s3://airacer-cn-release/asset/channel s3://airacer-cn-release/res/ --recursive
aws s3 mv s3://airacer-cn-release/asset/city s3://airacer-cn-release/res/ --recursive
aws s3 mv s3://airacer-cn-release/asset/empty-leg s3://airacer-cn-release/res/ --recursive
aws s3 mv s3://airacer-cn-release/asset/hot s3://airacer-cn-release/res/ --recursive

# 嵌套
aws s3 mv s3://airacer-cn-release/asset/itinerary/active s3://airacer-cn-release/res/ --recursive

aws s3 mv s3://airacer-cn-release/asset/logo s3://airacer-cn-release/res/ --recursive
aws s3 mv s3://airacer-cn-release/asset/pet-flight s3://airacer-cn-release/res/ --recursive
aws s3 mv s3://airacer-cn-release/asset/plane s3://airacer-cn-release/res/ --recursive

# 查看总数
aws s3api list-objects --bucket airacer-cn-beta --query "[sum(Contents[].Size), length(Contents[])]"
```



---

## 2024/02/20

```shell
# 当前统计
ubuntu@ip-172-31-48-13:~/bin/airtaxi/resource60002/views$ ll |wc -l
18
ubuntu@ip-172-31-48-13:~/bin/airtaxi/resource60002/views$ du -h --max-depth=1 ./
12M     ./logo
69M     ./city
4.0K    ./channel
55M     ./blog
1.4G    ./plane
44K     ./certificate
348K    ./doc
544K    ./pdf
8.0K    ./wx
1.1M    ./country
8.4M    ./hot
360M    ./vedio
3.2M    ./itinerary
32M     ./banner
168M    ./empty-leg
22M     ./pet-flight
20K     ./share
2.1G    ./
ubuntu@ip-172-31-48-13:~/bin/airtaxi/resource60002/views$ find . -type f | wc -l 
22003

# 结果汇总
aws s3api list-objects --bucket airacer-cn-release --query "[sum(Contents[].Size), length(Contents[])]"
#[
#    2120007072,
#    22015
#]
```

