# Arms(Aws Resource Manage System)
> 基于**Aws-S3** 的资源管理系统，参考：[Aws-S3应用说明](./utils/s3cli/README.md)。


<br/>


## DevelopAssistant 
```shell
$ make
Usage:
  make [command]

Available Commands:
 build         编译
 pack          编译并打包
 linter        代码检查
 clean         清理编译、日志和缓存等
 commit <msg>  Git提交，如:make push [msg=<message>]
 push <msg>    Git提交并推送，如:make push [msg=<message>]
 update        更新Git和Submodule
 run           运行程序
 help          查看make帮助

For more to see https://github.com/hollson
```


<br/>


## AutoScript
> 服务器自动化脚本，用于管理Airacer应用
```shell
./keeper.sh 
=========================================================
     欢迎使用Airacer Resource Server v1.0.0
=========================================================
用法：
 keeper.sh [command] <params>

Available Commands:
 命令    简称   说明
 install  ins  安装程序
 run      -    运行程序
 stop     -    停止服务
 restart  res  重启服务
 status   stt  查看服务状态
 info     -    查看Airacer应用信息
 sys      -    查看系统信息
 list     -    查看函数列表
 version  ver  查看应用版本
 help     *    查看帮助说明

更多详情，请参考 https://github.com/hollson
```

<br/>


## LogicSetting
```golang
// AppType 应用类型
type AppType string

const (
    Taxi    AppType = "a"
    TaxiMgr AppType = "b"
    DcMgr   AppType = "c"
    Bro     AppType = "d"
    Opr     AppType = "e"
    Default AppType = "z"
)

// JwtUser Jwt用户业务数据
type JwtUser struct {
    App    AppType `json:"app"`
    UserId string  `json:"userId,omitempty"`
}
```

<br/>


## TODO
- [x] Jwt授权验证
- [x] 统一ResponseResult
- [x] 解决图片ID哈希碰撞
- [ ] 大文件分片上传（断点续传等）
- [ ] 图片防盗链
- [ ] 独立安装sdk.sh
- [ ] 抽离公共组件(adk)
- [x] 解决服务与Nginx跨域问题
- [x] 添加平台类型
- [x] 添加`userId`、`app`分组标识
- [ ] 对象文件(二进制)下载
- [x] 添加日志功能
- [ ] 批量上传


<br/>


## Reference
https://www.lddgo.net/encrypt/jwt-generate

https://www.lddgo.net/encrypt/jwt-decrypt

https://www.jyshare.com/unit-conversion/6902/

https://blog.csdn.net/xiaotian2333333/article/details/122219618
