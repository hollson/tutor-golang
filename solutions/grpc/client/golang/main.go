package main

import (
	"context"
	"demo/rpcClient/pb"
	"fmt"
	"google.golang.org/protobuf/types/known/emptypb"
	"log"

	"google.golang.org/grpc"
)

func main() {
	conn, err := grpc.Dial("localhost:50051", grpc.WithInsecure())
	if err != nil {
		log.Fatalf("did not connect: %v", err)
	}
	defer conn.Close()

	client := pb.NewIEmployeeClient(conn)

	// ================  添加  ================
	resp, err := client.AddEmployee(context.Background(), &pb.Employee{
		Id:        1,
		Name:      "John Doe",
		Email:     "john@example.com",
		Active:    true,
		Timestamp: 1234567890,
		Salary:    50000.0,
		Phones:    []*pb.Employee_PhoneNumber{{Number: "123456789", Type: pb.Employee_MOBILE}},
	})
	fmt.Println(resp, err)
	fmt.Println()

	// ================  读取  ================
	ret, err := client.GetEmployee(context.Background(), &pb.EmployeeReq{EmployeeId: 1})
	fmt.Println(ret, err)
	fmt.Println()

	// =============  列表(单向流)  =============
	// 服务端可以向客户端发送多个消息，但客户端只能读取一次
	stream, err := client.ListEmployees(context.Background(), &emptypb.Empty{})
	if err != nil {
		log.Fatalf("Error calling ListPeople: %v", err)
	}

	for {
		employee, err := stream.Recv()
		if err != nil {
			log.Printf("Error receiving from ListPeople stream: %v", err)
			break
		}
		log.Printf("Received employee: %v", employee)
	}
}
