package pb;

import static io.grpc.MethodDescriptor.generateFullMethodName;

/**
 * <pre>
 * 员工服务
 * </pre>
 */
@javax.annotation.Generated(
    value = "by gRPC proto compiler (version 1.62.2)",
    comments = "Source: employee.proto")
@io.grpc.stub.annotations.GrpcGenerated
public final class IEmployeeGrpc {

  private IEmployeeGrpc() {}

  public static final java.lang.String SERVICE_NAME = "demoPackage.IEmployee";

  // Static method descriptors that strictly reflect the proto.
  private static volatile io.grpc.MethodDescriptor<pb.EmployeeOuterClass.Employee,
      pb.EmployeeOuterClass.Status> getAddEmployeeMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "AddEmployee",
      requestType = pb.EmployeeOuterClass.Employee.class,
      responseType = pb.EmployeeOuterClass.Status.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<pb.EmployeeOuterClass.Employee,
      pb.EmployeeOuterClass.Status> getAddEmployeeMethod() {
    io.grpc.MethodDescriptor<pb.EmployeeOuterClass.Employee, pb.EmployeeOuterClass.Status> getAddEmployeeMethod;
    if ((getAddEmployeeMethod = IEmployeeGrpc.getAddEmployeeMethod) == null) {
      synchronized (IEmployeeGrpc.class) {
        if ((getAddEmployeeMethod = IEmployeeGrpc.getAddEmployeeMethod) == null) {
          IEmployeeGrpc.getAddEmployeeMethod = getAddEmployeeMethod =
              io.grpc.MethodDescriptor.<pb.EmployeeOuterClass.Employee, pb.EmployeeOuterClass.Status>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(SERVICE_NAME, "AddEmployee"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  pb.EmployeeOuterClass.Employee.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  pb.EmployeeOuterClass.Status.getDefaultInstance()))
              .setSchemaDescriptor(new IEmployeeMethodDescriptorSupplier("AddEmployee"))
              .build();
        }
      }
    }
    return getAddEmployeeMethod;
  }

  private static volatile io.grpc.MethodDescriptor<com.google.protobuf.Empty,
      pb.EmployeeOuterClass.Employee> getGetEmployeeMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "GetEmployee",
      requestType = com.google.protobuf.Empty.class,
      responseType = pb.EmployeeOuterClass.Employee.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<com.google.protobuf.Empty,
      pb.EmployeeOuterClass.Employee> getGetEmployeeMethod() {
    io.grpc.MethodDescriptor<com.google.protobuf.Empty, pb.EmployeeOuterClass.Employee> getGetEmployeeMethod;
    if ((getGetEmployeeMethod = IEmployeeGrpc.getGetEmployeeMethod) == null) {
      synchronized (IEmployeeGrpc.class) {
        if ((getGetEmployeeMethod = IEmployeeGrpc.getGetEmployeeMethod) == null) {
          IEmployeeGrpc.getGetEmployeeMethod = getGetEmployeeMethod =
              io.grpc.MethodDescriptor.<com.google.protobuf.Empty, pb.EmployeeOuterClass.Employee>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(SERVICE_NAME, "GetEmployee"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.google.protobuf.Empty.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  pb.EmployeeOuterClass.Employee.getDefaultInstance()))
              .setSchemaDescriptor(new IEmployeeMethodDescriptorSupplier("GetEmployee"))
              .build();
        }
      }
    }
    return getGetEmployeeMethod;
  }

  private static volatile io.grpc.MethodDescriptor<pb.EmployeeOuterClass.Employee,
      pb.EmployeeOuterClass.Employee> getListPeopleMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "ListPeople",
      requestType = pb.EmployeeOuterClass.Employee.class,
      responseType = pb.EmployeeOuterClass.Employee.class,
      methodType = io.grpc.MethodDescriptor.MethodType.SERVER_STREAMING)
  public static io.grpc.MethodDescriptor<pb.EmployeeOuterClass.Employee,
      pb.EmployeeOuterClass.Employee> getListPeopleMethod() {
    io.grpc.MethodDescriptor<pb.EmployeeOuterClass.Employee, pb.EmployeeOuterClass.Employee> getListPeopleMethod;
    if ((getListPeopleMethod = IEmployeeGrpc.getListPeopleMethod) == null) {
      synchronized (IEmployeeGrpc.class) {
        if ((getListPeopleMethod = IEmployeeGrpc.getListPeopleMethod) == null) {
          IEmployeeGrpc.getListPeopleMethod = getListPeopleMethod =
              io.grpc.MethodDescriptor.<pb.EmployeeOuterClass.Employee, pb.EmployeeOuterClass.Employee>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.SERVER_STREAMING)
              .setFullMethodName(generateFullMethodName(SERVICE_NAME, "ListPeople"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  pb.EmployeeOuterClass.Employee.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  pb.EmployeeOuterClass.Employee.getDefaultInstance()))
              .setSchemaDescriptor(new IEmployeeMethodDescriptorSupplier("ListPeople"))
              .build();
        }
      }
    }
    return getListPeopleMethod;
  }

  /**
   * Creates a new async stub that supports all call types for the service
   */
  public static IEmployeeStub newStub(io.grpc.Channel channel) {
    io.grpc.stub.AbstractStub.StubFactory<IEmployeeStub> factory =
      new io.grpc.stub.AbstractStub.StubFactory<IEmployeeStub>() {
        @java.lang.Override
        public IEmployeeStub newStub(io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
          return new IEmployeeStub(channel, callOptions);
        }
      };
    return IEmployeeStub.newStub(factory, channel);
  }

  /**
   * Creates a new blocking-style stub that supports unary and streaming output calls on the service
   */
  public static IEmployeeBlockingStub newBlockingStub(
      io.grpc.Channel channel) {
    io.grpc.stub.AbstractStub.StubFactory<IEmployeeBlockingStub> factory =
      new io.grpc.stub.AbstractStub.StubFactory<IEmployeeBlockingStub>() {
        @java.lang.Override
        public IEmployeeBlockingStub newStub(io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
          return new IEmployeeBlockingStub(channel, callOptions);
        }
      };
    return IEmployeeBlockingStub.newStub(factory, channel);
  }

  /**
   * Creates a new ListenableFuture-style stub that supports unary calls on the service
   */
  public static IEmployeeFutureStub newFutureStub(
      io.grpc.Channel channel) {
    io.grpc.stub.AbstractStub.StubFactory<IEmployeeFutureStub> factory =
      new io.grpc.stub.AbstractStub.StubFactory<IEmployeeFutureStub>() {
        @java.lang.Override
        public IEmployeeFutureStub newStub(io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
          return new IEmployeeFutureStub(channel, callOptions);
        }
      };
    return IEmployeeFutureStub.newStub(factory, channel);
  }

  /**
   * <pre>
   * 员工服务
   * </pre>
   */
  public interface AsyncService {

    /**
     * <pre>
     * 添加一个员工
     * </pre>
     */
    default void addEmployee(pb.EmployeeOuterClass.Employee request,
        io.grpc.stub.StreamObserver<pb.EmployeeOuterClass.Status> responseObserver) {
      io.grpc.stub.ServerCalls.asyncUnimplementedUnaryCall(getAddEmployeeMethod(), responseObserver);
    }

    /**
     * <pre>
     * 获取一个员工
     *  rpc GetEmployee(Employee) returns (Employee);
     * </pre>
     */
    default void getEmployee(com.google.protobuf.Empty request,
        io.grpc.stub.StreamObserver<pb.EmployeeOuterClass.Employee> responseObserver) {
      io.grpc.stub.ServerCalls.asyncUnimplementedUnaryCall(getGetEmployeeMethod(), responseObserver);
    }

    /**
     * <pre>
     * 列出员工(stream:单向流,服务端可以向客户端发送多个消息，但客户端只能读取一次)
     * </pre>
     */
    default void listPeople(pb.EmployeeOuterClass.Employee request,
        io.grpc.stub.StreamObserver<pb.EmployeeOuterClass.Employee> responseObserver) {
      io.grpc.stub.ServerCalls.asyncUnimplementedUnaryCall(getListPeopleMethod(), responseObserver);
    }
  }

  /**
   * Base class for the server implementation of the service IEmployee.
   * <pre>
   * 员工服务
   * </pre>
   */
  public static abstract class IEmployeeImplBase
      implements io.grpc.BindableService, AsyncService {

    @java.lang.Override public final io.grpc.ServerServiceDefinition bindService() {
      return IEmployeeGrpc.bindService(this);
    }
  }

  /**
   * A stub to allow clients to do asynchronous rpc calls to service IEmployee.
   * <pre>
   * 员工服务
   * </pre>
   */
  public static final class IEmployeeStub
      extends io.grpc.stub.AbstractAsyncStub<IEmployeeStub> {
    private IEmployeeStub(
        io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected IEmployeeStub build(
        io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
      return new IEmployeeStub(channel, callOptions);
    }

    /**
     * <pre>
     * 添加一个员工
     * </pre>
     */
    public void addEmployee(pb.EmployeeOuterClass.Employee request,
        io.grpc.stub.StreamObserver<pb.EmployeeOuterClass.Status> responseObserver) {
      io.grpc.stub.ClientCalls.asyncUnaryCall(
          getChannel().newCall(getAddEmployeeMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     * <pre>
     * 获取一个员工
     *  rpc GetEmployee(Employee) returns (Employee);
     * </pre>
     */
    public void getEmployee(com.google.protobuf.Empty request,
        io.grpc.stub.StreamObserver<pb.EmployeeOuterClass.Employee> responseObserver) {
      io.grpc.stub.ClientCalls.asyncUnaryCall(
          getChannel().newCall(getGetEmployeeMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     * <pre>
     * 列出员工(stream:单向流,服务端可以向客户端发送多个消息，但客户端只能读取一次)
     * </pre>
     */
    public void listPeople(pb.EmployeeOuterClass.Employee request,
        io.grpc.stub.StreamObserver<pb.EmployeeOuterClass.Employee> responseObserver) {
      io.grpc.stub.ClientCalls.asyncServerStreamingCall(
          getChannel().newCall(getListPeopleMethod(), getCallOptions()), request, responseObserver);
    }
  }

  /**
   * A stub to allow clients to do synchronous rpc calls to service IEmployee.
   * <pre>
   * 员工服务
   * </pre>
   */
  public static final class IEmployeeBlockingStub
      extends io.grpc.stub.AbstractBlockingStub<IEmployeeBlockingStub> {
    private IEmployeeBlockingStub(
        io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected IEmployeeBlockingStub build(
        io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
      return new IEmployeeBlockingStub(channel, callOptions);
    }

    /**
     * <pre>
     * 添加一个员工
     * </pre>
     */
    public pb.EmployeeOuterClass.Status addEmployee(pb.EmployeeOuterClass.Employee request) {
      return io.grpc.stub.ClientCalls.blockingUnaryCall(
          getChannel(), getAddEmployeeMethod(), getCallOptions(), request);
    }

    /**
     * <pre>
     * 获取一个员工
     *  rpc GetEmployee(Employee) returns (Employee);
     * </pre>
     */
    public pb.EmployeeOuterClass.Employee getEmployee(com.google.protobuf.Empty request) {
      return io.grpc.stub.ClientCalls.blockingUnaryCall(
          getChannel(), getGetEmployeeMethod(), getCallOptions(), request);
    }

    /**
     * <pre>
     * 列出员工(stream:单向流,服务端可以向客户端发送多个消息，但客户端只能读取一次)
     * </pre>
     */
    public java.util.Iterator<pb.EmployeeOuterClass.Employee> listPeople(
        pb.EmployeeOuterClass.Employee request) {
      return io.grpc.stub.ClientCalls.blockingServerStreamingCall(
          getChannel(), getListPeopleMethod(), getCallOptions(), request);
    }
  }

  /**
   * A stub to allow clients to do ListenableFuture-style rpc calls to service IEmployee.
   * <pre>
   * 员工服务
   * </pre>
   */
  public static final class IEmployeeFutureStub
      extends io.grpc.stub.AbstractFutureStub<IEmployeeFutureStub> {
    private IEmployeeFutureStub(
        io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected IEmployeeFutureStub build(
        io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
      return new IEmployeeFutureStub(channel, callOptions);
    }

    /**
     * <pre>
     * 添加一个员工
     * </pre>
     */
    public com.google.common.util.concurrent.ListenableFuture<pb.EmployeeOuterClass.Status> addEmployee(
        pb.EmployeeOuterClass.Employee request) {
      return io.grpc.stub.ClientCalls.futureUnaryCall(
          getChannel().newCall(getAddEmployeeMethod(), getCallOptions()), request);
    }

    /**
     * <pre>
     * 获取一个员工
     *  rpc GetEmployee(Employee) returns (Employee);
     * </pre>
     */
    public com.google.common.util.concurrent.ListenableFuture<pb.EmployeeOuterClass.Employee> getEmployee(
        com.google.protobuf.Empty request) {
      return io.grpc.stub.ClientCalls.futureUnaryCall(
          getChannel().newCall(getGetEmployeeMethod(), getCallOptions()), request);
    }
  }

  private static final int METHODID_ADD_EMPLOYEE = 0;
  private static final int METHODID_GET_EMPLOYEE = 1;
  private static final int METHODID_LIST_PEOPLE = 2;

  private static final class MethodHandlers<Req, Resp> implements
      io.grpc.stub.ServerCalls.UnaryMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ServerStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ClientStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.BidiStreamingMethod<Req, Resp> {
    private final AsyncService serviceImpl;
    private final int methodId;

    MethodHandlers(AsyncService serviceImpl, int methodId) {
      this.serviceImpl = serviceImpl;
      this.methodId = methodId;
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public void invoke(Req request, io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        case METHODID_ADD_EMPLOYEE:
          serviceImpl.addEmployee((pb.EmployeeOuterClass.Employee) request,
              (io.grpc.stub.StreamObserver<pb.EmployeeOuterClass.Status>) responseObserver);
          break;
        case METHODID_GET_EMPLOYEE:
          serviceImpl.getEmployee((com.google.protobuf.Empty) request,
              (io.grpc.stub.StreamObserver<pb.EmployeeOuterClass.Employee>) responseObserver);
          break;
        case METHODID_LIST_PEOPLE:
          serviceImpl.listPeople((pb.EmployeeOuterClass.Employee) request,
              (io.grpc.stub.StreamObserver<pb.EmployeeOuterClass.Employee>) responseObserver);
          break;
        default:
          throw new AssertionError();
      }
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public io.grpc.stub.StreamObserver<Req> invoke(
        io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        default:
          throw new AssertionError();
      }
    }
  }

  public static final io.grpc.ServerServiceDefinition bindService(AsyncService service) {
    return io.grpc.ServerServiceDefinition.builder(getServiceDescriptor())
        .addMethod(
          getAddEmployeeMethod(),
          io.grpc.stub.ServerCalls.asyncUnaryCall(
            new MethodHandlers<
              pb.EmployeeOuterClass.Employee,
              pb.EmployeeOuterClass.Status>(
                service, METHODID_ADD_EMPLOYEE)))
        .addMethod(
          getGetEmployeeMethod(),
          io.grpc.stub.ServerCalls.asyncUnaryCall(
            new MethodHandlers<
              com.google.protobuf.Empty,
              pb.EmployeeOuterClass.Employee>(
                service, METHODID_GET_EMPLOYEE)))
        .addMethod(
          getListPeopleMethod(),
          io.grpc.stub.ServerCalls.asyncServerStreamingCall(
            new MethodHandlers<
              pb.EmployeeOuterClass.Employee,
              pb.EmployeeOuterClass.Employee>(
                service, METHODID_LIST_PEOPLE)))
        .build();
  }

  private static abstract class IEmployeeBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoFileDescriptorSupplier, io.grpc.protobuf.ProtoServiceDescriptorSupplier {
    IEmployeeBaseDescriptorSupplier() {}

    @java.lang.Override
    public com.google.protobuf.Descriptors.FileDescriptor getFileDescriptor() {
      return pb.EmployeeOuterClass.getDescriptor();
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.ServiceDescriptor getServiceDescriptor() {
      return getFileDescriptor().findServiceByName("IEmployee");
    }
  }

  private static final class IEmployeeFileDescriptorSupplier
      extends IEmployeeBaseDescriptorSupplier {
    IEmployeeFileDescriptorSupplier() {}
  }

  private static final class IEmployeeMethodDescriptorSupplier
      extends IEmployeeBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoMethodDescriptorSupplier {
    private final java.lang.String methodName;

    IEmployeeMethodDescriptorSupplier(java.lang.String methodName) {
      this.methodName = methodName;
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.MethodDescriptor getMethodDescriptor() {
      return getServiceDescriptor().findMethodByName(methodName);
    }
  }

  private static volatile io.grpc.ServiceDescriptor serviceDescriptor;

  public static io.grpc.ServiceDescriptor getServiceDescriptor() {
    io.grpc.ServiceDescriptor result = serviceDescriptor;
    if (result == null) {
      synchronized (IEmployeeGrpc.class) {
        result = serviceDescriptor;
        if (result == null) {
          serviceDescriptor = result = io.grpc.ServiceDescriptor.newBuilder(SERVICE_NAME)
              .setSchemaDescriptor(new IEmployeeFileDescriptorSupplier())
              .addMethod(getAddEmployeeMethod())
              .addMethod(getGetEmployeeMethod())
              .addMethod(getListPeopleMethod())
              .build();
        }
      }
    }
    return result;
  }
}
