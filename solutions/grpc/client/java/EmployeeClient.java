import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import pb.EmployeeServiceGrpc;
import pb.EmployeeServiceOuterClass;

public class EmployeeClient {
  public static void main(String[] args) {
    // 连接到 gRPC 服务器
    ManagedChannel channel =
        ManagedChannelBuilder.forAddress("localhost", 50051)
            .usePlaintext()
            .build();

    // 创建 gRPC 客户端
    EmployeeServiceGrpc.EmployeeServiceBlockingStub blockingStub =
        EmployeeServiceGrpc.newBlockingStub(channel);

    // 构建 Employee 请求对象
    EmployeeServiceOuterClass.Employee employee =
        EmployeeServiceOuterClass.Employee.newBuilder()
            .setId(1)
            .setName("John Doe")
            .setEmail("john.doe@example.com")
            .setActive(true)
            .setTimestamp(System.currentTimeMillis())
            .setSalary(50000.0)
            .build();

    // 调用 gRPC 服务的 AddEmployee 方法
    EmployeeServiceOuterClass.Status status =
        blockingStub.addEmployee(employee);

    // 打印服务器响应
    System.out.println("Add Employee Status: " + status.getCode() + " - " +
                       status.getMessage());

    // 调用 gRPC 服务的 GetEmployee 方法
    EmployeeServiceOuterClass.Employee retrievedEmployee =
        blockingStub.getEmployee(
            com.google.protobuf.Empty.getDefaultInstance()); //注意

    // 打印获取的员工信息
    System.out.println("Retrieved Employee: " + retrievedEmployee);

    // 关闭 gRPC 通道
    channel.shutdown();
  }
}