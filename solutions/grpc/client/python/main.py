import sys
sys.path.append('./pb')

import grpc
from pb import employee_pb2
from pb import employee_pb2_grpc
from google.protobuf import empty_pb2

def run():
    channel = grpc.insecure_channel('localhost:50051')
    stub = employee_pb2_grpc.IEmployeeStub(channel)

    # 调用添加员工的方法
    print("================= Add 请求 =================")
    response = stub.AddEmployee(employee_pb2.Employee(
        id=1,
        name="Alice",
        email="alice@example.com",
        active=True,
        timestamp=1234567890,
        salary=60000.0
    ))
    print(response)

    # 调用获取员工的方法
    print("================= Get 请求 =================")
    response = stub.GetEmployee(employee_pb2.EmployeeReq(EmployeeId=1))
    print(response)

    # 调用列出员工的方法
    print("================= List 请求 =================")
    response_iterator = stub.ListEmployees(employee_pb2.Employee())
    for response in response_iterator:
        print(response)

if __name__ == '__main__':
    run()