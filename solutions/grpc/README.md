# 工具栈
```shell
# 安装Golang插件
# protoc-gen-go:      用于生成结构体和方法代码，
# protoc-gen-go-grpc: 在上面基础上(继承)和扩展了服务端/客户端代码。
$ go install google.golang.org/protobuf/cmd/protoc-gen-go@latest
$ go get -u google.golang.org/grpc/cmd/protoc-gen-go-grpc@latest

# 查看版本
$ protoc --version
libprotoc 3.20.0
$ protoc-gen-go --version
protoc-gen-go v1.33.0
$ protoc-gen-go-grpc --version
protoc-gen-go-grpc 1.2.0
```
## 服务端

```shell
$ tree server/
server/
├── pb
│   ├── employee.pb.go			# Employee协议(基本类型与函数)
│   └── employee_grpc.pb.go		# Employee服务协议扩展
├── service
│   └── employee.go				# Employee协议实现(需要自己实现)
└── main.go						# RPC服务托管(服务注册与监听)
```

```shell
$ protoc --go_out=./server --go-grpc_out=./server employee.proto
$ cd server && go run main.go
```

## 客户端

### Golang

```shell
# 生成客户端代码(除了package命名，其他与server端的proto文件完全一致)
$ mkdir -p client/golang
$ protoc --go_out=./client/golang --go-grpc_out=./client/golang employee.proto

# 创建客户端调用示例
$ cd client/golang
$ go mod init demo/rpc_client
$ touch main.go
```

```go
package main

import (
	"context"
	"demo/rpcClient/pb"
	"fmt"
	"google.golang.org/protobuf/types/known/emptypb"
	"log"

	"google.golang.org/grpc"
)

func main() {
	conn, err := grpc.Dial("localhost:50051", grpc.WithInsecure())
	if err != nil {
		log.Fatalf("did not connect: %v", err)
	}
	defer conn.Close()

	client := pb.NewIEmployeeClient(conn)

	// ================  添加  ================
	resp, err := client.AddEmployee(context.Background(), &pb.Employee{
		Id:        1,
		Name:      "John Doe",
		Email:     "john@example.com",
		Active:    true,
		Timestamp: 1234567890,
		Salary:    50000.0,
		Phones:    []*pb.Employee_PhoneNumber{{Number: "123456789", Type: pb.Employee_MOBILE}},
	})
	fmt.Println(resp, err)
	fmt.Println()

	// ================  读取  ================
	ret, err := client.GetEmployee(context.Background(), &pb.EmployeeReq{EmployeeId: 1})
	fmt.Println(ret, err)
	fmt.Println()

	// =============  列表(单向流)  =============
	// 服务端可以向客户端发送多个消息，但客户端只能读取一次
	stream, err := client.ListEmployees(context.Background(), &emptypb.Empty{})
	if err != nil {
		log.Fatalf("Error calling ListPeople: %v", err)
	}

	for {
		employee, err := stream.Recv()
		if err != nil {
			log.Printf("Error receiving from ListPeople stream: %v", err)
			break
		}
		log.Printf("Received employee: %v", employee)
	}
}
```
### Python

```shell
# 安装gRPC工具包(Py版本的protoc，使用python命令调用)
$ pip install protobuf grpcio-tools

# 查看版本
$ python -m grpc_tools.protoc --version
libprotoc 25.1

# 生产pb文件
$ mkdir -p client/python/pb
$ python -m grpc_tools.protoc -I=. --python_out=client/python/pb --grpc_python_out=client/python/pb employee.proto
```

```python
# /client/python/main.py
import sys
sys.path.append('./pb')

import grpc
from pb import employee_pb2
from pb import employee_pb2_grpc
from google.protobuf import empty_pb2

def run():
    channel = grpc.insecure_channel('localhost:50051')
    stub = employee_pb2_grpc.IEmployeeStub(channel)

    # 调用添加员工的方法
    print("================= Add 请求 =================")
    response = stub.AddEmployee(employee_pb2.Employee(
        id=1,
        name="Alice",
        email="alice@example.com",
        active=True,
        timestamp=1234567890,
        salary=60000.0
    ))
    print(response)

    # 调用获取员工的方法
    print("================= Get 请求 =================")
    response = stub.GetEmployee(employee_pb2.EmployeeReq(EmployeeId=1))
    print(response)

    # 调用列出员工的方法
    print("================= List 请求 =================")
    response_iterator = stub.ListEmployees(employee_pb2.Employee())
    for response in response_iterator:
        print(response)

if __name__ == '__main__':
    run()
```

### CSharp

```shell
# 安装gRPC-C#工具包
$ dotnet new install Grpc.Tools

# 安装protoc-gen-grpc插件(用于生成gRPC服务端和客户端代码的插件)
$ dotnet tool install --global protobuf-net.Grpc.Generator

dotnet tool install -g dotnet-grpc
dotnet tool update -g dotnet-grpc
dotnet grpc --version
dotnet-grpc --version
dotnet tool list --global


# 创建客户端项目
$ dotnet new console -n RpcDemo -o client/csharp
$ cd client/csharp
$ dotnet add package Google.Protobuf
$ dotnet add package Grpc.Tools

protoc -I=. --csharp_out=client/csharp/ --grpc_out=client/csharp/ --plugin=protoc-gen-grpc=client/csharp/ --proto_path=. employee.proto


protoc --csharp_out=output_directory --grpc_out=output_directory --plugin=protoc-gen-grpc=%USERPROFILE%\.dotnet\tools\protobuf-net.Grpc.Generator.exe your_proto_file.proto
```

在C#中，`protoc-gen-grpc`实际上是一个用于生成gRPC服务端和客户端代码的插件，它需要与 Protocol Buffers 编译器（`protoc`）一起使用。下面是安装`protoc-gen-grpc`的步骤：





### Nodejs



### Java

```shell
# 安装grpc-java插件(下载并命名为protoc-gen-grpc-java,添加执行权限,放到protoc同目录)
export VERSION=1.62.2
https://repo1.maven.org/maven2/io/grpc/protoc-gen-grpc-java/${VERSION}

$ mkdir -p client/java
$ protoc -I=. --proto_path=client/java --java_out=client/java --grpc-java_out=client/java employee.proto

```

要在命令行中安装`io.grpc.ManagedChannel`，你可以使用Java的包管理工具Maven或Gradle。这些工具可以帮助你自动下载并管理Java项目的依赖库。

以下是使用Maven安装`io.grpc.ManagedChannel`的步骤：

1. 首先，确保你已经安装了Maven。你可以在命令行中输入`mvn -v`来检查是否已经安装。

2. 在你的Java项目中的`pom.xml`文件中添加`io.grpc.ManagedChannel`的依赖。示例：

```xml
<dependencies>
    <dependency>
        <groupId>io.grpc</groupId>
        <artifactId>grpc-netty</artifactId>
        <version>1.43.0</version>
    </dependency>
    <!-- 其他依赖 -->
</dependencies>

```

3. 打开命令提示符或终端，导航到包含`pom.xml`文件的项目目录。

4. 在项目目录中执行以下命令，让Maven自动下载并安装`io.grpc.ManagedChannel`依赖：

```bash
mvn install
```

这样Maven就会自动下载并安装所需的依赖库。安装完成后，你可以在你的Java代码中使用`io.grpc.ManagedChannel`类。如果你使用的是Gradle作为构建工具，相应的步骤也类似，只不过涉及到Gradle的相关配置文件。



- 客户端

```java
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import demoPackage.EmployeeServiceGrpc;
import demoPackage.EmployeeServiceOuterClass;

public class EmployeeClient {

    public static void main(String[] args) {
        // 连接到 gRPC 服务器
        ManagedChannel channel = ManagedChannelBuilder.forAddress("localhost", 50051).usePlaintext().build();

        // 创建 gRPC 客户端
        EmployeeServiceGrpc.EmployeeServiceBlockingStub blockingStub = EmployeeServiceGrpc.newBlockingStub(channel);

        // 构建 Employee 请求对象
        EmployeeServiceOuterClass.Employee employee = EmployeeServiceOuterClass.Employee.newBuilder()
                .setId(1)
                .setName("John Doe")
                .setEmail("john.doe@example.com")
                .setActive(true)
                .setTimestamp(System.currentTimeMillis())
                .setSalary(50000.0)
                .build();

        // 调用 gRPC 服务的 AddEmployee 方法
        EmployeeServiceOuterClass.Status status = blockingStub.addEmployee(employee);

        // 打印服务器响应
        System.out.println("Add Employee Status: " + status.getCode() + " - " + status.getMessage());

        // 调用 gRPC 服务的 GetEmployee 方法
        EmployeeServiceOuterClass.Employee retrievedEmployee = blockingStub.getEmployee(Empty.getDefaultInstance());

        // 打印获取的员工信息
        System.out.println("Retrieved Employee: " + retrievedEmployee);

        // 关闭 gRPC 通道
        channel.shutdown();
    }
}
```





### Rust


```shell
# Nodejs
protoc --js_out=import_style=commonjs:. --grpc_out=. --plugin=protoc-gen-grpc=/path/to/grpc-tools/bin/grpc_node_plugin demo.proto




# rust
prost --grpc demo.proto

```


## 参考链接
https://www.jianshu.com/p/40cc71a0ae76

https://learn.microsoft.com/zh-cn/aspnet/core/grpc/clientfactory?view=aspnetcore-8.0

https://learn.microsoft.com/zh-cn/aspnet/core/tutorials/grpc/grpc-start?view=aspnetcore-8.0&tabs=visual-studio


https://learn.microsoft.com/zh-cn/aspnet/core/grpc/test-tools?view=aspnetcore-8.0
