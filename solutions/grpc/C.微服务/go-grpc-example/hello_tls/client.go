package main

import (
	"fmt"

	pb "github.com/jergoo/go-grpc-example/proto/hello" // 引入proto包
	"google.golang.org/grpc/grpclog"

	"golang.org/x/net/context"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials" // 引入grpc认证包
)

func main() {
	const _addr = "127.0.0.1:9090"
	// TLS连接
	creds, err := credentials.NewClientTLSFromFile("../../keys/server.crt", "www.mafool.com")
	if err != nil {
		fmt.Printf("Failed to create TLS credentials %v", err)
	}

	conn, err := grpc.Dial(_addr, grpc.WithTransportCredentials(creds))
	if err != nil {
		grpclog.Fatalln(err)
	}
	defer conn.Close()

	// 初始化客户端
	c := pb.NewHelloClient(conn)

	// 调用方法
	req := &pb.HelloRequest{Name: "gRPC"}
	res, err := c.SayHello(context.Background(), req)
	if err != nil {
		grpclog.Fatalln(err)
	}

	fmt.Println(res.Message)
}
