package main

import (
	"fmt"
	"log"
	"net"

	pb "github.com/jergoo/go-grpc-example/proto/hello"

	"golang.org/x/net/context"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials" // 引入grpc认证包
)

// 定义helloService并实现约定的接口
type helloService struct{}

func (h helloService) SayHello(ctx context.Context, in *pb.HelloRequest) (*pb.HelloResponse, error) {
	resp := new(pb.HelloResponse)
	resp.Message = fmt.Sprintf("Hello %s.", in.Name)

	return resp, nil
}

func main() {
	const _addr = "127.0.0.1:9090"

	listen, err := net.Listen("tcp", _addr)
	if err != nil {
		log.Printf("Failed to listen: %v", err)
	}

	// TLS认证
	creds, err := credentials.NewServerTLSFromFile("../../keys/server.crt", "../../keys/server.key")
	if err != nil {
		log.Printf("Failed to generate credentials %v", err)
	}

	// 实例化grpc Server, 并开启TLS认证
	s := grpc.NewServer(grpc.Creds(creds))

	// 注册HelloService
	var HelloService = &helloService{}
	pb.RegisterHelloServer(s, HelloService)

	fmt.Println("Listen on " + _addr + " with TLS")

	s.Serve(listen)
}
