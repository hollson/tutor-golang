#!/bin/bash

function next() {
    read -r -p "是否继续?(y/n) " next
    [ "$next" = 'Y' ] || [ "$next" = 'y' ] || exit 1
}

echo "SAN证书:"
echo -e "  SAN(Subject Alternative Name)是SSL标准x509中定义的一个扩展。\n使用了SAN字段的SSL证书,可以扩展此证书支持的域名,使得一个证书\n可以支持多个不同域名的解析。"
echo -e "下一步：生成CA根证书"
echo
#===============================CA证书==============================
#next
openssl genrsa -out ca.key 4096
echo "🔥🔥🔥 CA根证书「密钥」已生成: ./ca.key"
echo


#next
openssl req -new -sha256 -out ca.csr -key ca.key -config ca.conf
echo "🔥🔥🔥 CA证书「签发请求」已生成: ca.csr"
echo

#next
openssl x509 -req -days 3650 -in ca.csr -signkey ca.key -out ca.crt
echo "🔥🔥🔥 CA证书签发请求「公钥」已生成: ca.crt"
echo

#===============================用户证书==============================
echo -e "下一步：生成用户证书"
#next
openssl genrsa -out server.key 2048
echo "🔥🔥🔥 用户证书「秘钥」已生成： server.key"
echo

#next
openssl req -new -sha256 -out server.csr -key server.key -config server.conf
echo "🔥🔥🔥 用户证书「签发请求」已生成: server.csr"
echo

#next
openssl x509 -req -days 3650 -CA ca.crt -CAkey ca.key -CAcreateserial -in server.csr \
-out server.crt -extensions req_ext -extfile server.conf
echo "🔥🔥🔥 用户证书「公钥」已生成: server.crt"
echo