package main

import (
	"fmt"

	pb "github.com/jergoo/go-grpc-example/proto/hello"
	"google.golang.org/grpc/credentials/insecure"

	"golang.org/x/net/context"
	"google.golang.org/grpc"
	"google.golang.org/grpc/grpclog"
)

//go:generate go run client.go
func main() {
	conn, err := grpc.Dial(":8443", grpc.WithTransportCredentials(insecure.NewCredentials()))
	if err != nil {
		grpclog.Fatalln(err)
	}
	defer conn.Close()
	client := pb.NewHelloClient(conn)

	// 3. 调用服务
	res, err := client.SayHello(context.Background(), &pb.HelloRequest{Name: "土拨鼠"})
	if err != nil {
		grpclog.Fatalln(err)
	}

	fmt.Println(res.Message)
}
