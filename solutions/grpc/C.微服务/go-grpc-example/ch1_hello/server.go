package main

import (
	"fmt"
	"net"

	pb "github.com/jergoo/go-grpc-example/proto/hello" // 引入编译生成的包

	"golang.org/x/net/context"
	"google.golang.org/grpc"
)

type HelloService struct{}

func (h HelloService) SayHello(ctx context.Context, in *pb.HelloRequest) (*pb.HelloResponse, error) {
	resp := new(pb.HelloResponse)
	resp.Message = fmt.Sprintf("你好 %s.", in.Name)
	return resp, nil
}

//go:generate go run server.go
func main() {
	server := grpc.NewServer()
	pb.RegisterHelloServer(server, HelloService{})

	listener, _ := net.Listen("tcp", ":8443")
	server.Serve(listener)
}
