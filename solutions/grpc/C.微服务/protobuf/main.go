package main

import (
    "log"
	"os"
	"reflect"

	"github.com/golang/protobuf/jsonpb"
	"github.com/golang/protobuf/proto"
)

func main() {
	p1 := &Person{
		ID: 123, Name: "Joh", Email: "joh@gmail.com",
		Phones: []*Person_PhoneNumber{
			{Number: "555-4321", Type: Person_HOME},
			{Number: "555-1234", Type: Person_WORK},
		},
	}
	buf, err := proto.Marshal(p1)
	if err != nil {
		log.Fatal(err)
	}
	log.Println(p1)

	p2 := &Person{}
	if err := proto.Unmarshal(buf, p2); err != nil {
		log.Fatal(err)
	}
	log.Println(p2)

	/////////////////////////////////////////////////////////

	f, err := os.Open("user.json")
	if err != nil {
		log.Fatal(err)
	}

	p3 := &Person{}
	jsonpb.Unmarshal(f, p3)
	log.Println(p3)

	p4 := reflect.New(proto.MessageType("main.Person").Elem()).Interface().(proto.Message)
	f, err = os.Open("user.json")
	if err := jsonpb.Unmarshal(f, p4); err != nil {
		log.Println(err)
	}
	log.Println(p4)

}
