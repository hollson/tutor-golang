package main

import (
	"fmt"
	"net"
	"net/rpc"
	"time"

	"myrpc/rpc/proto"
)

// Tcp服务
func RpcTcpServer() {
	// 创建服务
	rpcServer := rpc.NewServer()
	rpcServer.Register(new(proto.HelloService))

	// 监听服务
	listener, _ := net.Listen("tcp", ":8082")
	go rpcServer.Accept(listener) // Tcp服务
}

//go:generate go run rpc_tcp_server.go
func main() {
	RpcTcpServer()
	time.Sleep(2000 * time.Millisecond)

	client, _ := rpc.Dial("tcp", "127.0.0.1:8082")
	var reply string
	client.Call("HelloService.Greet", "Jack", &reply)
	fmt.Println(reply)
}
