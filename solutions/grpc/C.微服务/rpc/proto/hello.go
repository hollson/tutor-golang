// Copyright 2023 Hollson. All rights reserved.
// Use of this source code is governed by a MIT style
// license that can be found in the LICENSE file.

package proto

import (
	"fmt"
)

// 服务协议
type HelloService struct{}

func (r *HelloService) Greet(args *string, reply *string) error {
	*reply = fmt.Sprintf("Hello %s", *args)
	return nil
}
