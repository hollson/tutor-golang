package main

import (
	"fmt"
	"net"
	"net/http"
	"net/rpc"
	"time"

	"myrpc/rpc/proto"
)

// 服务端
func RpcHttpServer() {
	// 创建服务
	rpcServer := rpc.NewServer()
	rpcServer.Register(new(proto.HelloService))

	// 注册路由
	rpcServer.HandleHTTP("/hello", "/debug/hello")

	// 监听服务
	listener, _ := net.Listen("tcp", ":8081")
	go http.Serve(listener, nil) // Http服务
}

//go:generate go run rpc_http_server.go
func main() {
	RpcHttpServer()
	time.Sleep(2000 * time.Millisecond)

	// 客户端
	client, _ := rpc.DialHTTPPath("tcp", "127.0.0.1:8081", "/hello")
	var reply string
	client.Call("HelloService.Greet", "Lily", &reply)
	fmt.Println(reply)
}
