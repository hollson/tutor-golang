using Grpc.Core;
using test;

class Program
{
    static async Task Main(string[] args)
    {
        Channel channel = new Channel("localhost:50051", ChannelCredentials.Insecure);
        var client = new Tester.TesterClient(channel);

        // 调用 gRPC 服务的方法
        var replyUnary = client.SayHelloUnary(new HelloRequest { Name = "Alice" });

        // 等待响应
        Console.WriteLine("Unary RPC Response: " + replyUnary.Message);

        await channel.ShutdownAsync();
    }
}