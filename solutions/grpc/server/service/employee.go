package service

import (
	"demo/rpcServer/pb"
	"errors"
	"fmt"
	"golang.org/x/net/context"
	"google.golang.org/protobuf/types/known/emptypb"
	"log"
	"time"
)

// EmployeeService 员工服务，实现了员工pb接口
type EmployeeService struct {
	pb.UnimplementedIEmployeeServer
}

var employees = make(map[int32]*pb.Employee)

func (s *EmployeeService) AddEmployee(ctx context.Context, req *pb.Employee) (*pb.Reply, error) {
	log.Printf("Add请求： %v\n\n", req)

	if req == nil {
		return &pb.Reply{Status: -1}, errors.New("param error")
	}

	// 添加数据
	employees[req.Id] = req
	return &pb.Reply{Status: 1}, nil
}

func (s *EmployeeService) GetEmployee(ctx context.Context, req *pb.EmployeeReq) (*pb.Employee, error) {
	if req == nil {
		return nil, errors.New("param error")
	}

	v, ok := employees[req.EmployeeId]
	if !ok {
		return nil, errors.New("not exists")
	}
	return v, nil
}

func (s *EmployeeService) ListEmployees(empty *emptypb.Empty, server pb.IEmployee_ListEmployeesServer) error {
	log.Printf("List请求： %v\n\n")
	_ = empty
	// 模拟向客户端发送多个消息
	for i := 0; i < 3; i++ {
		employee := &pb.Employee{
			Id:        int32(i + 1),
			Name:      fmt.Sprintf("Employee%d", i+1),
			Email:     fmt.Sprintf("employee%d@gmail.com", i+1),
			Active:    true,
			Timestamp: 1234567890,
			Salary:    50000.0,
		}
		time.Sleep(time.Second)
		if err := server.Send(employee); err != nil {
			return err
		}
	}

	return nil
}
