package main

import (
	"demo/rpcServer/pb"
	"demo/rpcServer/service"
	"log"
	"net"

	"google.golang.org/grpc"
)

func main() {
	listener, err := net.Listen("tcp", ":50051")
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}

	log.Println("Server is running on port 50051")

	server := grpc.NewServer()
	pb.RegisterIEmployeeServer(server, &service.EmployeeService{})
	if err := server.Serve(listener); err != nil {
		log.Fatalf("failed to serve: %v", err)
	}
}

// rpc包路径是什么?
// rpc如何被宿主托管？
// 手写一个简单的HelloWorld示例
