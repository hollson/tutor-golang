package main

import (
	"fmt"

	"pattern/hook/pack"
)

// 报告服务状态信息
func report(event string, contents ...string) {
	var hooks = []string{
		"www.wechat.com/hook?token=xxx",
		"www.whatsapp.com/hook?token=xxx",
	}

	for _, hook := range hooks {
		if len(contents) > 0 {
			fmt.Printf("=> 向 %s 发送【%s】消息, %s\n", hook, event, contents[0])
			continue
		}
		fmt.Printf("=> 向 %s 发送【%s】消息\n", hook, event)
	}
}

func main() {
	// 注册Hook函数
	hook := &pack.Hook{}
	hook.RegisterOnStart(func(event string, content ...string) { report(event) })
	hook.RegisterOnRestart(func(event string, content ...string) { report(event, content[0]) })
	hook.RegisterOnError(func(event string, content ...string) { report(event, content[0]) })

	server := pack.NewServer("MyServer", hook)
	server.Start()
	server.Restart()
	server.Error()
}
