package pack

type HookFunc func(event string, content ...string)

type Hook struct {
	onStart   HookFunc
	onRestart HookFunc
	onError   HookFunc
	// onStop    HookFunc
}

func (h *Hook) RegisterOnStart(f HookFunc) {
	h.onStart = f
}

func (h *Hook) RegisterOnRestart(f HookFunc) {
	h.onRestart = f
}

func (h *Hook) RegisterOnError(f HookFunc) {
	h.onError = f
}
