package pack

import (
	"fmt"
)

type Server struct {
	name string
	hook *Hook
}

func (s *Server) Start() {
	fmt.Printf("【Server】%s started.\n", s.name)
	if s.hook.onStart != nil {
		s.hook.onStart("start")
	}
	fmt.Println()
}

func (s *Server) Restart() {
	fmt.Printf("【Server】%s restarted.\n", s.name)
	if s.hook.onRestart != nil {
		s.hook.onRestart("restart", "手动重启")
	}
	fmt.Println()
}

func (s *Server) Error() {
	fmt.Printf("【Server】%s encountered an error.\n", s.name)
	if s.hook.onError != nil {
		s.hook.onError("error", "网络异常")
	}
	fmt.Println()
}

func NewServer(name string, hook *Hook) *Server {
	return &Server{name, hook}
}
