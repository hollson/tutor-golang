package main

import (
	"fmt"

	"pattern/builder/pack"
)

/*
	Builder模式通过简化复杂对象的创建过程、提高灵活性和增强代码可读性等方面带来了显著的好处。
	与Option模式相比，Builder模式在参数管理的清晰度、对象构建的灵活性和代码可读性方面更具优势。

SSL
AUTH
Session

解码
校验

*/

func main() {
	builder := pack.NewServerBuilder()
	builder.SetModel("TCP")
	builder.Configure(func(sb *pack.ServerBuilder) {
		sb.Server.Name = "AwesomeServer"
		sb.Server.ListenOption = pack.ListenOption{Ip: "127.0.0.1", Port: 6018, NoDelay: true}
	})
	builder.UseAuth(func() { fmt.Println("Custom authorization verification has been used.") })

	server := builder.Build()
	server.Run()
}
