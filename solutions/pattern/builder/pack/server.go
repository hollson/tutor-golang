package pack

import (
	"fmt"
)

type ListenOption struct {
	Ip      string
	Port    int
	NoDelay bool
}

type Server struct {
	Name         string
	ListenOption ListenOption
	model        string
	auth         func()
}

func (s *Server) Run() {
	fmt.Printf("%s (%s) 服务已启动: Host=%s Port=%d \n", s.Name, s.model, s.ListenOption.Ip, s.ListenOption.Port)
	s.auth()
}
