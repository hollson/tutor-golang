package pack

type ServerBuilder struct {
	Server *Server
}

func NewServerBuilder() *ServerBuilder {
	return &ServerBuilder{Server: &Server{}}
}

func (sb *ServerBuilder) SetModel(model string) {
	sb.Server.model = model
}

func (sb *ServerBuilder) Configure(opt func(sb *ServerBuilder)) {
	opt(sb)
}

func (sb *ServerBuilder) UseAuth(auth func()) {
	sb.Server.auth = auth
}

func (sb *ServerBuilder) Build() *Server {
	return sb.Server
}
