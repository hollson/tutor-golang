// Copyright 2023 Hollson. All rights reserved.
// Use of this source code is governed by a MIT style
// license that can be found in the LICENSE file.

package pack

import (
	"fmt"
)

// UserServer 用户服务
type UserServer struct {
	port        int
	OnRunEvents []EventHandler // Run事件
}

// Name 服务名称
func (s *UserServer) Name() string {
	return "UserServer"
}

// Port 默认端口
func (s *UserServer) Port() int {
	if s.port > 0 {
		return s.port
	}
	return 10001
}

// SetPort 修改默认端口端口
func (s *UserServer) SetPort(port int) {
	if port > 0 {
		s.port = port
	}
	s.port = s.Port()
}

func (s *UserServer) Run() error {
	for _, handler := range s.OnRunEvents {
		if err := handler(s, ServerEventArgs{ServerName: s.Name(), ServerPort: s.Port()}); err != nil {
			return err
		}
	}
	fmt.Printf("【%v】服务已启动：%d\n", s.Name(), s.Port())
	return nil
}
