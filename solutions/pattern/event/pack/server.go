// Copyright 2023 Hollson. All rights reserved.
// Use of this source code is governed by a MIT style
// license that can be found in the LICENSE file.

package pack

type Server interface {
	Name() string // 服务名称
	Port() int    // 默认端口
	Run() error   // 运行服务
}

// ServerEventArgs 服务事件参数
type ServerEventArgs struct {
	ServerName string
	ServerPort int
}

// 通用args
// type EventHandler func(sender interface{}, args ...interface{}) error

type EventHandler func(sender any, args ServerEventArgs) error
