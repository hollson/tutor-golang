package main

import (
	"fmt"
	"reflect"

	"pattern/event/pack"
)

// LoadConfig 加载配置文件
func LoadConfig(sender any, args pack.ServerEventArgs) error {
	fmt.Printf("【%v】已加载配置文件,name=%v \n", reflect.TypeOf(sender), args.ServerName)
	return nil
}

// RegServer 注册服务
func RegServer(sender any, args pack.ServerEventArgs) error {
	// 向注册中心提交服务信息
	fmt.Printf("【%v】已完成服务注册,name=%v,port=%v \n", reflect.TypeOf(sender), args.ServerName, args.ServerPort)
	return nil
}

// AddMonitor 添加监控告警
func AddMonitor(sender any, args pack.ServerEventArgs) error {
	fmt.Printf("【%v】已添加监控告警,name=%v,port=%v \n", reflect.TypeOf(sender), args.ServerName, args.ServerPort)
	return nil
}

func main() {
	u := pack.UserServer{}
	u.OnRunEvents = append(u.OnRunEvents, LoadConfig)
	u.OnRunEvents = append(u.OnRunEvents, RegServer)
	u.Run() // 触发事件

	fmt.Println()

	o := pack.OrderServer{}
	o.OnRunEvents = append(o.OnRunEvents, LoadConfig)
	o.OnRunEvents = append(o.OnRunEvents, RegServer)
	o.OnRunEvents = append(o.OnRunEvents, AddMonitor)
	o.SetPort(10003) // 默认10002
	o.Run()
}
