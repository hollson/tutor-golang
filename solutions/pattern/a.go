package main

import (
	"fmt"
)

// 定义事件类型
type EventType int

const (
	StartEvent EventType = iota
	RegisterServiceEvent
	HealthCheckEvent
	RestartEvent
)

// 事件监听器接口
type Listener interface {
	HandleEvent(eventType EventType)
}

// 具体的事件监听器（邮件通知监听器）
type MailNotificationListener struct{}

func (mnl *MailNotificationListener) HandleEvent(eventType EventType) {
	if eventType == RestartEvent {
		fmt.Println("Sending mail notification for server restart.")
	}
}

// 自定义服务器
type MyServer struct {
	listeners map[EventType][]Listener
}

func NewMyServer() *MyServer {
	return &MyServer{
		listeners: make(map[EventType][]Listener),
	}
}

func (s *MyServer) AddListener(eventType EventType, listener Listener) {
	s.listeners[eventType] = append(s.listeners[eventType], listener)
}

func (s *MyServer) RemoveListener(eventType EventType, listener Listener) {
	for i, l := range s.listeners[eventType] {
		if l == listener {
			s.listeners[eventType] = append(s.listeners[eventType][:i], s.listeners[eventType][i+1:]...)
			break
		}
	}
}

func (s *MyServer) TriggerEvent(eventType EventType) {
	for _, listener := range s.listeners[eventType] {
		listener.HandleEvent(eventType)
	}
}

func main() {
	server := NewMyServer()
	mailListener := &MailNotificationListener{}

	// 注册监听器
	server.AddListener(StartEvent, nil)
	server.AddListener(RegisterServiceEvent, nil)
	server.AddListener(HealthCheckEvent, nil)
	server.AddListener(RestartEvent, mailListener)

	// 模拟服务启动
	server.TriggerEvent(StartEvent)
	server.TriggerEvent(RegisterServiceEvent)
	server.TriggerEvent(HealthCheckEvent)

	// 模拟服务重启
	server.TriggerEvent(RestartEvent)
}
