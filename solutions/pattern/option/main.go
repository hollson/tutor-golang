package main

import (
	"pattern/option/pack"
)

func main() {
	// 有三种方式初始化实例参数：
	// 1. 使用默认的实例参数
	// 2. 使用预设的选项函数
	// 3. 使用自定义的选项函数
	server := pack.NewServer(
		pack.WithName("AwesomeServer"),
		pack.WithSSL(true),
		func(s *pack.Server) { s.Addr = "127.0.0.1" },
	)
	println(server.Name, server.Addr, server.Port, server.SSL)
}
