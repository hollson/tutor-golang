package pack

// Option 定义一个函数类型的Option，用于设置服务器的选项
type Option func(*Server)

// WithName 定义一些选项函数，用于设置服务器的名称、地址和端口
func WithName(name string) Option {
	return func(c *Server) {
		c.Name = name
	}
}

func WithAddr(ip string) Option {
	return func(c *Server) {
		c.Addr = ip
	}
}

func WithPort(port int) Option {
	return func(c *Server) {
		c.Port = port
	}
}

func WithSSL(ssl bool) Option {
	return func(c *Server) {
		c.SSL = ssl
	}
}
