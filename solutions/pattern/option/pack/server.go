package pack

type Server struct {
	Name string
	Addr string
	Port int
	SSL  bool
}

func NewServer(opts ...Option) *Server {
	//  创建一个默认的服务器实例
	server := &Server{"Default Server", "0.0.0.0", 8080, false}

	//  遍历选项函数列表，并应用每个选项函数到服务器实例上
	for _, opt := range opts {
		opt(server)
	}
	return server
}
