package main

import (
	"fmt"
	"sync"
)

// EventBus 事件总线结构体
type EventBus struct {
	subscribers map[string][]func(interface{})
	mutex       sync.Mutex
}

// NewEventBus 创建一个新的事件总线
func NewEventBus() *EventBus {
	return &EventBus{
		subscribers: make(map[string][]func(interface{})),
	}
}

// Subscribe 订阅特定类型的事件
func (eb *EventBus) Subscribe(eventName string, subscriber func(interface{})) {
	eb.mutex.Lock()
	defer eb.mutex.Unlock()
	eb.subscribers[eventName] = append(eb.subscribers[eventName], subscriber)
}

// Unsubscribe 取消订阅特定类型的事件
func (eb *EventBus) Unsubscribe(eventName string, subscriber func(interface{})) {
	eb.mutex.Lock()
	defer eb.mutex.Unlock()
	if subscribers, ok := eb.subscribers[eventName]; ok {
		for i, s := range subscribers {
			if s == subscriber {
				eb.subscribers[eventName] = append(subscribers[:i], subscribers[i+1:]...)
				break
			}
		}
	}
}

// Publish 发布事件
func (eb *EventBus) Publish(eventName string, data interface{}) {
	eb.mutex.Lock()
	defer eb.mutex.Unlock()
	if subscribers, ok := eb.subscribers[eventName]; ok {
		for _, subscriber := range subscribers {
			go subscriber(data)
		}
	}
}

func main() {
	eventBus := NewEventBus()

	// 订阅事件
	eventBus.Subscribe("event1", func(data interface{}) {
		fmt.Println("Subscriber 1 received event1:", data)
	})
	eventBus.Subscribe("event1", func(data interface{}) {
		fmt.Println("Subscriber 2 received event1:", data)
	})

	// 发布事件
	eventBus.Publish("event1", "Hello, EventBus!")
}
