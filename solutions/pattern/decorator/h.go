package main

import (
	"fmt"
	"net/http"
)

// 定义一个HandlerFunc接口，用于HTTP处理函数
type HandlerFunc interface {
	ServeHTTP(w http.ResponseWriter, r *http.Request)
}

// 定义一个基本的处理函数结构体
type HelloHandler struct{}

func (h *HelloHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "Hello, World!")
}

// 定义一个日志记录装饰器结构体
type LoggingHandler struct {
	next HandlerFunc
}

func (h *LoggingHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	fmt.Println("Request received:", r.URL.Path)
	h.next.ServeHTTP(w, r)
	fmt.Println("Request processed:", r.URL.Path)
}

// 定义一个身份验证装饰器结构体
type AuthHandler struct {
	next HandlerFunc
}

func (h *AuthHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	token := r.Header.Get("Authorization")
	if token != "valid_token" {
		http.Error(w, "Unauthorized", http.StatusUnauthorized)
		return
	}
	h.next.ServeHTTP(w, r)
}

func main() {
	// 创建基本的处理函数实例
	helloHandler := &HelloHandler{}

	// 使用装饰器增强功能
	authHandler := &AuthHandler{next: helloHandler}
	loggingHandler := &LoggingHandler{next: authHandler}

	// 将增强后的处理函数注册到HTTP服务器
	http.Handle("/", loggingHandler)

	// 启动HTTP服务器
	fmt.Println("Server is running on port 8080...")
	http.ListenAndServe(":8080", nil)
}
