## Embed介绍
> embed是在Go 1.16中新加包。它通过`//go:embed`指令，可以在编译阶段将静态资源文件打包进编译好的程序中，并提供访问这些文件的能力。



<br/>




## 嵌入方式：

- 支持单文件读取：go:embed hello.txt。
- 支持多文件读取：go:embed hello1.txt、go:embed hello2.txt。
- 支持目录读取：go:embed helloworld。
- 支持贪婪匹配：go:embed helloworld/*。



<br/>



## 嵌入类型：


| 数据类型 | 说明          |
| -------- | ------------ |
| **[]byte** | 表示数据存储为二进制格式，如果只使用[]byte和string需要以import (_ "embed")的形式引入embed标准库 |
| **string** | 表示数据被编码成utf8编码的字符串，因此不要用这个格式嵌入二进制文件比如图片，引入embed的规则同[]byte |
| **embed.FS** | 表示存储多个文件和目录的结构，[]byte和string只能存储单个文件 |

**embed.FS方法：**

```go
type FS
func (f FS) Open(name string) (fs.File, error)
func (f FS) ReadDir(name string) ([]fs.DirEntry, error)
func (f FS) ReadFile(name string) ([]byte, error)
```







## 参考文档
https://github.red/go-embed/
https://segmentfault.com/a/1190000039359898
https://www.cnblogs.com/niuben/p/14461973.html