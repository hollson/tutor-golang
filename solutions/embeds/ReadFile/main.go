package main

import (
	"embed"
	"fmt"
)

//go:embed static/*
var embedFs embed.FS

func openFile() {
	// Open打开要读取的文件，并返回文件的fs.File结构.
	file, err := embedFs.Open("static/a.txt")
	if err != nil {
		panic(err)
	}
	defer file.Close()

	data := make([]byte, 100)
	n, err := file.Read(data)
	if err != nil {
		panic(err)
	}
	fmt.Println(string(data[:n]))
}

func readFile() {
	data, err := embedFs.ReadFile("static/b.txt")
	if err != nil {
		panic(err)
	}
	fmt.Println(string(data))
}

//go:generate go build -o ./bin/app.exe .\main.go
func main() {
	fmt.Println("========= Open ==========")
	openFile()
	fmt.Println("========= Read ==========")
	readFile()
	fmt.Scanln()
}
