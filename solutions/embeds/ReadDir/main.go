package main

import (
	"embed"
	"fmt"
	"io/fs"
)

//go:embed static
var embedFs embed.FS

//go:generate go build -o ./bin/app.exe .\main.go
func main() {
	fmt.Println("===== embed 直接读取目录 =====")
	static, err := embedFs.ReadDir("static")
	if err != nil {
		panic(err)
	}

	for _, entry := range static {
		if entry.IsDir() {
			fmt.Println("[DIR] :", entry.Name())
		} else {
			fmt.Println("[FILE]:", entry.Name())
		}
	}

	// embedFS即嵌入式文件系统，它可以传递给io.fs进行处理
	fmt.Println("===== embed 间接读取目录 =====")
	images, err := fs.ReadDir(embedFs, "static/images")
	if err != nil {
		panic(err)
	}

	for _, entry := range images {

		fmt.Println("[IMG]:", entry.Name())

	}

	fmt.Scanln()
}
