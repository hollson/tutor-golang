package main

import (
	"embed"
	"fmt"
	"io/fs"
	"net/http"
)

//go:embed static
var embedFs embed.FS

func embedFS() http.FileSystem {
	fsys, err := fs.Sub(embedFs, "static")
	if err != nil {
		panic(err)
	}
	return http.FS(fsys)
}

//go:generate go build -o ./bin/web.exe .\main.go
func main() {
	http.Handle("/", http.FileServer(embedFS()))

	fmt.Println("browser open => http://localhost:8080")
	http.ListenAndServe(":8080", nil)
}
