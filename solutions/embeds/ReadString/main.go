package main

import (
	_ "embed"
	"fmt"
)

//go:embed version.txt
var embedByte []byte

//go:embed version.txt
var embedStr string

//go:generate go build -o ./bin/app.exe .\main.go
func main() {
	fmt.Printf("version [bytes ] : %v\n", string(embedByte))
	fmt.Printf("version [string] : %v\n", embedStr)
	fmt.Scanln()
}
