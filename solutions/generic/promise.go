package ext

import (
	"sync"
	"sync/atomic"
	"time"
)

const (
	promisePending  = int32(0)
	promiseTimeout  = int32(-1)
	promiseComplete = int32(1)
)

type Promise[T any] struct {
	*promise[T]
}

type promise[T any] struct {
	wait    sync.WaitGroup
	pending atomic.Int32
	result  T
}

// Await 0未完成 1完成 -1超时
func (p Promise[T]) Await() (T, int8) {
	if p.pending.Load() == promisePending {
		p.wait.Wait()
	}
	return p.result, int8(p.pending.Load())
}

// TryGet 0未完成 1完成 -1超时
func (p Promise[T]) TryGet() (T, int8) {
	return p.result, int8(p.pending.Load())
}

func Appoint[T any](timeout time.Duration) (Promise[T], func(T)) {
	p := Promise[T]{&promise[T]{}}
	p.wait.Add(1)
	f := func(t T) {
		if p.pending.Load() == promisePending {
			p.result = t
			p.pending.Store(promiseComplete)
			p.wait.Done()
		}
	}
	if timeout > 0 {
		go func(timeout time.Duration) {
			time.Sleep(timeout)
			if p.pending.Load() == promisePending {
				p.pending.Store(promiseTimeout)
				p.wait.Done()
			}
		}(timeout)
	}
	return p, f
}
