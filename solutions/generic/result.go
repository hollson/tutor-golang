package ext

import "errors"

type Result[T any] interface {
	IsOk() bool
	IsErr() bool
	Get() T
	Get_() T
	GetOr(T) T
	GetElse(func() T) T
	Err() error
}

func ResOk[T any](t T) Result[T] {
	return resOk[T]{t}
}

func ResErr[T any](e error) Result[T] {
	return resErr[T]{e}
}

type resOk[T any] struct {
	t T
}

func (resOk[T]) IsOk() bool {
	return true
}

func (resOk[T]) IsErr() bool {
	return false
}

func (r resOk[T]) Get() T {
	return r.t
}

func (r resOk[T]) Get_() T {
	return r.t
}

func (r resOk[T]) GetOr(T) T {
	return r.t
}

func (r resOk[T]) GetElse(func() T) T {
	return r.t
}

func (resOk[T]) Err() error {
	return nil
}

type resErr[T any] struct {
	e error
}

func (resErr[T]) IsOk() bool {
	return false
}

func (resErr[T]) IsErr() bool {
	return true
}

func (resErr[T]) Get() T {
	panic(errors.New("result is err"))
}

func (resErr[T]) Get_() T {
	return *new(T)
}

func (resErr[T]) GetOr(t T) T {
	return t
}

func (r resErr[T]) GetElse(fn func() T) T {
	return fn()
}

func (r resErr[T]) Err() error {
	return r.e
}
