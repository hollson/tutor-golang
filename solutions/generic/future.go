package ext

import (
	"sync"
)

type Future[T any] struct {
	*future[T]
}

type future[T any] struct {
	run    func() T
	wait   sync.WaitGroup
	result T
}

func Spawn[T any](run func() T) Future[T] {
	f := Future[T]{&future[T]{
		run: run,
	}}
	f.wait.Add(1)
	go func() {
		f.result = f.run()
		f.run = nil
		f.wait.Done()
	}()
	return f
}

func (f Future[T]) Await() T {
	if f.run != nil {
		f.wait.Wait()
	}
	return f.result
}

func (f Future[T]) TryGet() (T, bool) {
	return f.result, f.run == nil
}
