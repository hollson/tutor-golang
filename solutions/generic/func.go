package ext

type ForEach[E any] interface {
	ForEach(func(E))
	Len() int
	Empty() bool
}

type appendSelf[E, ES any] interface {
	appendSelf(E) ES
}

// Map 将Vec[T]转成Vec[R]
func Map[T, R any](vec Vec[T], fn func(T) R) Vec[R] {
	rs := Vec_[R](vec.Len())
	for _, t := range vec {
		rs.Append(fn(t))
	}
	return rs
}

// MapTo 将 ForEach[T]转成ForEach[R]
func MapTo[T, R any, TS ForEach[T], RS appendSelf[R, RS]](
	ts TS, fn func(T) R, toFn func(int) RS) RS {
	rs := toFn(ts.Len())
	ts.ForEach(func(t T) {
		rs = rs.appendSelf(fn(t))
	})
	return rs
}

// Flatten 将Vec[Vec[T]]平铺成Vec[T]
func Flatten[T any](vec Vec[Vec[T]]) Vec[T] {
	len_ := 0
	for _, v := range vec {
		len_ += v.Len()
	}
	rs := Vec_[T](len_)
	for _, v := range vec {
		rs.Appends(v...)
	}
	return rs
}

func FlattenTo[T any, RS appendSelf[T, RS]](
	vec Vec[Vec[T]], toFn func(int) RS) RS {
	len_ := 0
	for _, v := range vec {
		len_ += v.Len()
	}
	rs := toFn(len_)
	for _, v := range vec {
		for _, t := range v {
			rs = rs.appendSelf(t)
		}
	}
	return rs
}

// FlatMap 将Vec[Vec[T]]平铺成Vec[Vec[R]]
func FlatMap[T, R any](vec Vec[T], fn func(T) Vec[R]) Vec[R] {
	rs := Vec_[Vec[R]](vec.Len())
	for _, t := range vec {
		rs.Append(fn(t))
	}
	return Flatten(rs)
}

func FlatMapTo[T, R any, TS ForEach[T], RS appendSelf[R, RS]](
	ts TS, fn func(T) Vec[R], toFn func(int) RS) RS {
	rs := Vec_[Vec[R]](ts.Len())
	ts.ForEach(func(t T) {
		rs.Append(fn(t))
	})
	return FlattenTo(rs, toFn)
}

// Filter 过滤Vec[T]中不需要的元素
func Filter[T any](vec Vec[T], fn func(T) bool) Vec[T] {
	rs := Vec_[T](halfLen(vec.Len()))
	for _, t := range vec {
		if fn(t) {
			rs.Append(t)
		}
	}
	return rs
}

func FilterTo[T any, TS ForEach[T], RS appendSelf[T, RS]](
	ts TS, fn func(T) bool, toFn func(int) RS) RS {
	rs := toFn(halfLen(ts.Len()))
	ts.ForEach(func(t T) {
		if fn(t) {
			rs = rs.appendSelf(t)
		}
	})
	return rs
}

// MapFilter 将Vec[T]转成Vec[RangeTo_] 并过滤不需要的元素
func MapFilter[T, R any](vec Vec[T], fn func(T) Opt[R]) Vec[R] {
	rs := Vec_[R](halfLen(vec.Len()))
	for _, t := range vec {
		if r, b := fn(t).D(); b {
			rs.Append(r)
		}
	}
	return rs
}

func MapFilterTo[T, R any, TS ForEach[T], RS appendSelf[R, RS]](ts TS, fn func(T) Opt[R], toFn func(int) RS) RS {
	rs := toFn(halfLen(ts.Len()))
	ts.ForEach(func(t T) {
		if r, b := fn(t).D(); b {
			rs = rs.appendSelf(r)
		}
	})
	return rs
}

// Reduce 对Vec[T]做合并操作 需要一个种子
func Reduce[T, R any, TS ForEach[T]](ts TS, seed R, fn func(R, T) R) R {
	ts.ForEach(func(t T) {
		seed = fn(seed, t)
	})
	return seed
}

// Any 判断切片中的值是否符合预期，一个符合为true
func Any[T any](vec Vec[T], fn func(T) bool) bool {
	for _, t := range vec {
		if fn(t) {
			return true
		}
	}
	return false
}

// All 判断切片中的值是否符合预期，全部符合为true
func All[T any](vec Vec[T], fn func(T) bool) bool {
	for _, t := range vec {
		if !fn(t) {
			return false
		}
	}
	return true
}

// ToDict 分组函数 可以对key映射
func ToDict[K comparable, T any](vec Vec[T], kFn func(T) K) Dict[K, T] {
	dict := Dict_[K, T](4)
	for _, t := range vec {
		k := kFn(t)
		dict.Store(k, t)
	}
	return dict
}

// VToDict 分组函数 可以对key和value映射
func VToDict[K comparable, V, T any](vec Vec[T], kvFn func(T) (K, V)) Dict[K, V] {
	dict := Dict_[K, V](4)
	for _, t := range vec {
		k, v := kvFn(t)
		dict.Store(k, v)
	}
	return dict
}

// GroupBy 分组函数 可以对key映射
func GroupBy[K comparable, T any](vec Vec[T], kFn func(T) K) Dict[K, Vec[T]] {
	dict := Dict_[K, Vec[T]](4)
	for _, t := range vec {
		k := kFn(t)
		dict[k] = append(dict[k], t)
	}
	return dict
}

// VGroupBy 分组函数 可以对key和value同时映射
func VGroupBy[K comparable, V, T any](vec Vec[T], kvFn func(T) (K, V)) Dict[K, Vec[V]] {
	dict := Dict_[K, Vec[V]](4)
	for _, t := range vec {
		k, v := kvFn(t)
		dict[k] = append(dict[k], v)
	}
	return dict
}

// FollowSort 跟随排序
func FollowSort[O comparable, T any](orders Vec[O], vec Vec[T], kFn func(T) O) Vec[T] {
	return MapFilter(orders, ToDict(vec, kFn).Load)
}

func halfLen(len_ int) int {
	len_ /= 2
	if len_%2 == 1 {
		len_ += 1
	}
	return len_
}

func Init_[T any]() T {
	return *new(T)
}
