package main

import (
	"context"
	"encoding/json"
	"fmt"
	"github.com/golang-jwt/jwt"
	"log"
	"net/http"
	"net/url"
	"os"
	"time"

	"github.com/go-oauth2/oauth2/v4/generates"

	"github.com/go-oauth2/oauth2/v4/errors"
	"github.com/go-oauth2/oauth2/v4/manage"
	"github.com/go-oauth2/oauth2/v4/models"
	"github.com/go-oauth2/oauth2/v4/server"
	"github.com/go-oauth2/oauth2/v4/store"
	"github.com/go-session/session"
)

var srv *server.Server

func main() {
	log.SetFlags(log.Lshortfile | log.Ldate | log.Lmicroseconds)
	manager := manage.NewDefaultManager()
	manager.SetAuthorizeCodeTokenCfg(manage.DefaultPasswordTokenCfg) //默认配置
	manager.MustTokenStorage(store.NewMemoryTokenStore())            //内存存储

	manager.MapAccessGenerate(generates.NewJWTAccessGenerate("", []byte("L4D1S9E4T3W3T9Z7R4R2Y5Y9R0K9V6E2E9W7"), jwt.SigningMethodHS256))

	clientStore := store.NewClientStore()
	clientStore.Set("12345", &models.Client{
		ID:     "12345",                 //"客户端id"
		Secret: "67890",                 // "客户端密码"
		Domain: "http://localhost:9094", // "客户端的域，也就是客户端的网址"
	})
	manager.MapClientStorage(clientStore)

	srv = server.NewServer(server.NewConfig(), manager)

	// 密码模式使用
	srv.SetPasswordAuthorizationHandler(func(ctx context.Context, clientID, username, password string) (userID string, err error) {
		if username == "aaa" && password == "bbb" {
			userID = "ccc"
		}
		return
	})
	srv.SetUserAuthorizationHandler(userAuthorizeHandler)

	srv.SetInternalErrorHandler(func(err error) (re *errors.Response) {
		log.Println("Internal Error:", err.Error())
		return
	})
	srv.SetResponseErrorHandler(func(re *errors.Response) {
		log.Println("Response Error:", re.Error.Error())
	})

	//	get请求返回登录页面，post请求获取登录页面的表单信息进行验证，
	//	通过就添加session"LoggedInUserID"，然后进行重定向到：/auth
	http.HandleFunc("/login", loginHandler)

	//	验证是否有session是否有LoggedInUserID，
	//	有的话就证明登录过且成功，呢就返回auth.html页面（确认授权页面）
	//	auth.html的表单提交是到：/oauth/authorize
	http.HandleFunc("/auth", authHandler)

	//	从第三方服务器跳转到到login时就会触发userAuthorizeHandler函数，在这个函数中保存信息store.Set("ReturnUri", r.Form)
	//	从session中获取到第三方服务器跳转过来携带的信息，并把这些信息添加到本次请求Request中
	//	然后删除掉session中的信息
	//	最后把这次请求的w，r交给srv.HandleAuthorizeRequest(w, r)进行判断返回err
	http.HandleFunc("/oauth/authorize", authorize)

	//	处理令牌请求，内部根据令牌进行判断是什么模式，是授权码模式还是密码或是又或者是其他然后再进行细分具体逻辑，
	//	err := srv.HandleTokenRequest(w, r)
	http.HandleFunc("/oauth/token", token)
	//	受保护的资源，必须有access_token才可以访问
	http.HandleFunc("/test", test)
	log.Fatal(http.ListenAndServe(":9096", nil))
}

// srv.SetUserAuthorizationHandler(userAuthorizeHandler)
func userAuthorizeHandler(w http.ResponseWriter, r *http.Request) (userID string, err error) {
	store, err := session.Start(r.Context(), w, r)
	if err != nil {
		return
	}

	uid, ok := store.Get("LoggedInUserID")
	if !ok {
		if r.Form == nil {
			r.ParseForm()
		}
		//	从第三方服务器跳转到到login时就会触发这个函数，同时r.Form里面会包含如下信息
		//	r.Form: map[
		//		client_id:[12345]
		//		redirect_uri:[http://localhost:9094/callbacks]
		//		response_type:[code]
		//		scope:[all]
		//		state:[xyz]
		//	]
		store.Set("ReturnUri", r.Form)
		store.Save()

		w.Header().Set("Location", "/login")
		w.WriteHeader(http.StatusFound)
		return
	}
	userID = uid.(string)
	store.Delete("LoggedInUserID")
	store.Save()
	return
}

// /login
func loginHandler(w http.ResponseWriter, r *http.Request) {
	store, err := session.Start(r.Context(), w, r)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	if r.Method == "POST" {
		if r.Form == nil {
			if err := r.ParseForm(); err != nil {
				http.Error(w, err.Error(), http.StatusInternalServerError)
				return
			}
		}
		fmt.Println("=====> ", r.Form.Get("username"), r.Form.Get("password"))

		store.Set("LoggedInUserID", r.Form.Get("username"))
		store.Save()

		//w.Header().Set("Location", "/auth") //客户端重定向
		w.WriteHeader(http.StatusFound)
		return
	}
	outputHTML(w, r, "example/server/static/login.html")
}

// /auth
func authHandler(w http.ResponseWriter, r *http.Request) {
	store, err := session.Start(nil, w, r)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	if _, ok := store.Get("LoggedInUserID"); !ok {
		w.Header().Set("Location", "/login")
		w.WriteHeader(http.StatusFound)
		return
	}

	outputHTML(w, r, "example/server/static/auth.html")
}

// /oauth/token
func token(w http.ResponseWriter, r *http.Request) {
	err := srv.HandleTokenRequest(w, r)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}

// /test
func test(w http.ResponseWriter, r *http.Request) {
	token, err := srv.ValidationBearerToken(r)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	data := map[string]interface{}{
		"expires_in": int64(token.GetAccessCreateAt().Add(token.GetAccessExpiresIn()).Sub(time.Now()).Seconds()),
		"client_id":  token.GetClientID(),
		"user_id":    token.GetUserID(),
	}
	e := json.NewEncoder(w)
	e.SetIndent("", "  ")
	e.Encode(data)
}

// /oauth/authorize
func authorize(w http.ResponseWriter, r *http.Request) {
	store, err := session.Start(r.Context(), w, r)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	var form url.Values
	if v, ok := store.Get("ReturnUri"); ok {
		form = v.(url.Values)
	}
	r.Form = form

	store.Delete("ReturnUri")
	store.Save()

	err = srv.HandleAuthorizeRequest(w, r)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
	}
}

// 返回html页面
func outputHTML(w http.ResponseWriter, req *http.Request, filename string) {
	file, err := os.Open(filename)
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}
	defer file.Close()
	fi, _ := file.Stat()
	http.ServeContent(w, req, file.Name(), fi.ModTime(), file)
}
