#!/bin/bash
# shellcheck source=/dev/null
source ./sdk.sh

# 全局变量
export APP_NAME=" GoZero Greet Services" # 应用名称
export APP_VERSION="v1.0.0"                # 应用版本

#CMD dock|-|使用docker+link构建服务
function dock() {
    echo "🚀 构建镜像..."
    docker rm -f greet
    docker rmi greet
    docker build --no-cache -t greet .
    echo

    # --add-host: 将宿主机解析为容器内IP地址
    # --link:  连接两个容器(ExternalContainerName:InternalContainerName)
    echo "🚀 启动容器..."
    #docker run -tid --name greet_redis redis:alpine
    docker run -tid \
        -p 18000:8000 \
        --network gozero_greet_default \
        --add-host host.docker.internal:host-gateway \
        --add-host greet_etcd:52.74.6.63 \
        --link greet_redis:greet_redis \
        --name greet \
        greet:latest
    echo

    echo "🚀 服务清单..."
    docker ps -a | grep greet
    echo

    echo "🚀 测试接口..."
    sleep 5
    echo -en "🌍 http://127.0.0.1:18000 \t\t => "
    curl -X GET "http://127.0.0.1:18000"
    echo
    echo -en "🌍 http://127.0.0.1:18000/ping \t\t => "
    curl -X GET "http://127.0.0.1:18000/ping"
    echo
    echo -en "🌍 http://127.0.0.1:18000/greet \t\t => "
    curl -X POST -H "Content-Type: application/json" "http://127.0.0.1:18000/greet" -d '{"name":"GoZero"}'
    echo
    echo -en "🌍 http://127.0.0.1:18000/redis/set \t => ok"
    curl -X POST -H "Content-Type: application/json" "http://127.0.0.1:18000/redis/set" -d '{"name":"Golang"}'
    echo
    echo -en "🌍 http://127.0.0.1:18000/redis/get \t => "
    curl "http://127.0.0.1:18000/redis/get"
    echo
}

#CMD run|-|使用docker-compose构建服务
function run() {
    echo "🚀 构建服务..."
    #--force-recreate 会强制停止并重新创建所有容器，而 --build 只会重新构建镜像而不会停止已经运行的容器。
    #--force-recreate 适用于需要重新创建整个容器组的情况，而 --build 适用于仅在需要时重新构建镜像的情况
    #docker compose up -d --force-recreate --build
    docker compose up -d --build
    echo

    echo "🚀 服务清单..."
    status
    echo

    echo "🚀 应用信息..."
    info
    echo

    echo "🚀 测试接口..."
    sleep 5
    apitest
    echo
}

#CMD apitest|test|查看应用信息
function apitest(){
     echo -en "🌍 http://127.0.0.1:8000 \t\t => "
     curl -X GET "http://127.0.0.1:8000"
     echo
     echo -en "🌍 http://127.0.0.1:8000/ping \t\t => "
     curl -X GET "http://127.0.0.1:8000/ping"
     echo
     echo -en "🌍 http://127.0.0.1:8000/greet \t\t => "
     curl -X POST -H "Content-Type: application/json" "http://127.0.0.1:8000/greet" -d '{"name":"DockerCompose"}'
     echo
     echo -en "🌍 http://127.0.0.1:8000/redis/set \t => ok"
     curl -X POST -H "Content-Type: application/json" "http://127.0.0.1:8000/redis/set" -d '{"name":"Docker-Compose"}'
     echo
     echo -en "🌍 http://127.0.0.1:8000/redis/get \t => "
     curl "http://127.0.0.1:8000/redis/get"
     echo
}


#CMD info|-|查看应用信息
function info() {
    echox magenta 1 "应用名称：$APP_NAME"
    echox magenta 1 "容器网络： $(docker inspect --format='{{.HostConfig.NetworkMode}}' greet_rpc) $(docker inspect --format='{{range .NetworkSettings.Networks}}(Gateway: {{.Gateway}} IP: {{.IPAddress}}){{end}}' greet_rpc)"
}

#CMD flush|flu|刷新Redis缓存
function flush() {
    echo "待实现..."
}

#CMD restart|res|重启服务
function restart() {
    echo "待实现..."
}

#CMD status|stt|查看服务状态
function status() {
    docker ps -a | grep greet
}

#CMD redis|-|打开当前服务关联的Redis
function redis() {
    # docker exec -ti greet_redis redis-cli set greet HelloWorld
    # docker exec -ti greet_redis redis-cli get greet
    docker exec -ti greet_redis redis-cli
}

#CMD remove|rmv|移除所有greet相关服务
function remove() {
    docker compose down
    # --rmi all: 移除所有镜像
    # --remove-orphans: 移除孤立的容器
    # --volumes: 移除与服务关联的匿名卷
    # docker compose down --rmi all --remove-orphans --volumes
    # docker rm -f greet_api greet_rpc greet_redis greet
}

#CMD prune|-|清理无效的镜像/容器/网络等(慎用 ⚠)
function prune() {
    echo "♻ 清理无效的容器..."
    docker rm "$(docker ps -qaf status=exited)"

    echo "♻ 清理无效的镜像..."
    docker rmi "$(docker images -qf dangling=true)"

    # 查看未被容器使用的镜像
    # docker images --format "{{.ID}}\t{{.Repository}}:{{.Tag}}" | grep -v "$(docker ps --format "{{.Image}}")"
    docker images --format "{{.Repository}}:{{.Tag}}" | grep -v "$(docker ps --format "{{.Image}}")" | grep apx_datacenter | xargs docker rmi

    # 删除所有未使用的镜像
    docker image ls -q --filter "dangling=true"
    docker image prune -f

    echo "♻ 清理无效的数据卷..."
    # 删除所有未使用的数据卷
    docker volume ls -qf dangling=true
    docker volume prune -f

    echo "♻ 清理无效的数网络..."
    docker network prune -f
}

#CMD sys|-|查看系统信息
function sys() {
    sysInfo
    echo ""
    sysInspect
}

# 继承通用命令，并重载main函数
#CMD list|-|查看函数列表
#CMD version|ver|查看应用版本
#CMD help|*|查看帮助说明
main
