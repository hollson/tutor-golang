package logic

import (
	"context"
	"gozero_greet/rpc/greet"

	"gozero_greet/api/internal/svc"
	"gozero_greet/api/internal/types"

	"github.com/zeromicro/go-zero/core/logx"
)

type HomeLogic struct {
	logx.Logger
	ctx    context.Context
	svcCtx *svc.ServiceContext
}

func NewHomeLogic(ctx context.Context, svcCtx *svc.ServiceContext) *HomeLogic {
	return &HomeLogic{
		Logger: logx.WithContext(ctx),
		ctx:    ctx,
		svcCtx: svcCtx,
	}
}

func (l *HomeLogic) Home() (resp *types.Response, err error) {
	ret, err := l.svcCtx.Rpc.Home(context.Background(), &greet.Empty{})
	return &types.Response{Message: ret.Message}, err
}
