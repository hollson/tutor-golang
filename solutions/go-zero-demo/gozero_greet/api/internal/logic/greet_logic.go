package logic

import (
	"context"
	"fmt"
	"gozero_greet/rpc/greet"

	"gozero_greet/api/internal/svc"
	"gozero_greet/api/internal/types"

	"github.com/zeromicro/go-zero/core/logx"
)

type GreetLogic struct {
	logx.Logger
	ctx    context.Context
	svcCtx *svc.ServiceContext
}

func NewGreetLogic(ctx context.Context, svcCtx *svc.ServiceContext) *GreetLogic {
	return &GreetLogic{
		Logger: logx.WithContext(ctx),
		ctx:    ctx,
		svcCtx: svcCtx,
	}
}

func (l *GreetLogic) Greet(req *types.Request) (resp *types.Response, err error) {
	if len(req.Name) == 0 {
		return nil, fmt.Errorf("param invalid")
	}
	ret, err := l.svcCtx.Rpc.Greet(context.Background(), &greet.Request{Name: req.Name})
	return &types.Response{Message: ret.Message}, err
}
