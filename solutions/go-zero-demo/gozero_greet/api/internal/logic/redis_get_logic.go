package logic

import (
	"context"
	"gozero_greet/rpc/greet"

	"gozero_greet/api/internal/svc"
	"gozero_greet/api/internal/types"

	"github.com/zeromicro/go-zero/core/logx"
)

type RedisGetLogic struct {
	logx.Logger
	ctx    context.Context
	svcCtx *svc.ServiceContext
}

func NewRedisGetLogic(ctx context.Context, svcCtx *svc.ServiceContext) *RedisGetLogic {
	return &RedisGetLogic{
		Logger: logx.WithContext(ctx),
		ctx:    ctx,
		svcCtx: svcCtx,
	}
}

func (l *RedisGetLogic) RedisGet() (resp *types.Response, err error) {
	ret, err := l.svcCtx.Rpc.CacheGet(l.ctx, &greet.Empty{})
	return &types.Response{Message: ret.Message}, err
}
