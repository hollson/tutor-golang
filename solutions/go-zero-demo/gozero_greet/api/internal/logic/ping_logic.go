package logic

import (
	"context"
	"gozero_greet/rpc/greet"

	"github.com/zeromicro/go-zero/core/logx"
	"gozero_greet/api/internal/svc"
)

type PingLogic struct {
	logx.Logger
	ctx    context.Context
	svcCtx *svc.ServiceContext
}

func NewPingLogic(ctx context.Context, svcCtx *svc.ServiceContext) *PingLogic {
	return &PingLogic{
		Logger: logx.WithContext(ctx),
		ctx:    ctx,
		svcCtx: svcCtx,
	}
}

func (l *PingLogic) Ping() (resp string, err error) {
	ret, err := l.svcCtx.Rpc.Ping(context.Background(), &greet.Empty{})
	return ret.Content, err
}
