package logic

import (
	"context"
	"fmt"
	"gozero_greet/rpc/greet"

	"gozero_greet/api/internal/svc"
	"gozero_greet/api/internal/types"

	"github.com/zeromicro/go-zero/core/logx"
)

type RedisSetLogic struct {
	logx.Logger
	ctx    context.Context
	svcCtx *svc.ServiceContext
}

func NewRedisSetLogic(ctx context.Context, svcCtx *svc.ServiceContext) *RedisSetLogic {
	return &RedisSetLogic{
		Logger: logx.WithContext(ctx),
		ctx:    ctx,
		svcCtx: svcCtx,
	}
}

func (l *RedisSetLogic) RedisSet(req *types.Request) error {
	if len(req.Name) == 0 {
		return fmt.Errorf("param invalid")
	}
	_, err := l.svcCtx.Rpc.CacheSet(l.ctx, &greet.Request{Name: req.Name})
	return err
}
