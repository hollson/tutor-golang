package svc

import (
	"github.com/zeromicro/go-zero/zrpc"
	"gozero_greet/api/internal/config"
	"gozero_greet/rpc/greet"
)

type ServiceContext struct {
	Config config.Config
	Rpc    greet.Greet
}

func NewServiceContext(c config.Config) *ServiceContext {
	return &ServiceContext{
		Config: c,
		Rpc:    greet.NewGreet(zrpc.MustNewClient(c.Rpc)),
	}
}
