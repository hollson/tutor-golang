package logic

import (
	"context"
	"fmt"
	"github.com/zeromicro/go-zero/core/stores/redis"
	"gozero_greet/rpc/internal/svc"
	"gozero_greet/rpc/pb"

	"github.com/zeromicro/go-zero/core/logx"
)

type CacheGetLogic struct {
	ctx    context.Context
	svcCtx *svc.ServiceContext
	logx.Logger
}

func NewCacheGetLogic(ctx context.Context, svcCtx *svc.ServiceContext) *CacheGetLogic {
	return &CacheGetLogic{
		ctx:    ctx,
		svcCtx: svcCtx,
		Logger: logx.WithContext(ctx),
	}
}

func (l *CacheGetLogic) CacheGet(in *pb.Empty) (*pb.Response, error) {
	rds, err := redis.NewRedis(l.svcCtx.Config.Cache)
	if err != nil {
		return nil, err
	}

	val, err := rds.Get(CacheGreet)
	if err != nil {
		return nil, err
	}

	return &pb.Response{Message: fmt.Sprintf("[Redis] %s", val)}, nil
}
