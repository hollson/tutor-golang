package logic

import (
	"context"
	"github.com/zeromicro/go-zero/core/stores/redis"

	"gozero_greet/rpc/internal/svc"
	"gozero_greet/rpc/pb"

	"github.com/zeromicro/go-zero/core/logx"
)

const CacheGreet = "Greet#Name"

type CacheSetLogic struct {
	ctx    context.Context
	svcCtx *svc.ServiceContext
	logx.Logger
}

func NewCacheSetLogic(ctx context.Context, svcCtx *svc.ServiceContext) *CacheSetLogic {
	return &CacheSetLogic{
		ctx:    ctx,
		svcCtx: svcCtx,
		Logger: logx.WithContext(ctx),
	}
}

func (l *CacheSetLogic) CacheSet(in *pb.Request) (*pb.Empty, error) {
	rds, err := redis.NewRedis(l.svcCtx.Config.Cache)
	if err != nil {
		return nil, err
	}
	return nil, rds.Setex(CacheGreet, in.Name, 600)
}
