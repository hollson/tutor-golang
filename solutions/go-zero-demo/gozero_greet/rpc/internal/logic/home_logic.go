package logic

import (
	"context"

	"gozero_greet/rpc/internal/svc"
	"gozero_greet/rpc/pb"

	"github.com/zeromicro/go-zero/core/logx"
)

type HomeLogic struct {
	ctx    context.Context
	svcCtx *svc.ServiceContext
	logx.Logger
}

func NewHomeLogic(ctx context.Context, svcCtx *svc.ServiceContext) *HomeLogic {
	return &HomeLogic{
		ctx:    ctx,
		svcCtx: svcCtx,
		Logger: logx.WithContext(ctx),
	}
}

func (l *HomeLogic) Home(in *pb.Empty) (*pb.Response, error) {
	return &pb.Response{Message: "Hello World"}, nil
}
