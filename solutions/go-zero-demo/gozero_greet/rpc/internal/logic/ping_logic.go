package logic

import (
	"context"

	"gozero_greet/rpc/internal/svc"
	"gozero_greet/rpc/pb"

	"github.com/zeromicro/go-zero/core/logx"
)

type PingLogic struct {
	ctx    context.Context
	svcCtx *svc.ServiceContext
	logx.Logger
}

func NewPingLogic(ctx context.Context, svcCtx *svc.ServiceContext) *PingLogic {
	return &PingLogic{
		ctx:    ctx,
		svcCtx: svcCtx,
		Logger: logx.WithContext(ctx),
	}
}

func (l *PingLogic) Ping(_ *pb.Empty) (*pb.Pong, error) {
	return &pb.Pong{Content: "PONG"}, nil
}
