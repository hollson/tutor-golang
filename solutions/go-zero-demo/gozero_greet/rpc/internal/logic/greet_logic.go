package logic

import (
	"context"
	"fmt"
	"gozero_greet/rpc/internal/svc"
	"gozero_greet/rpc/pb"

	"github.com/zeromicro/go-zero/core/logx"
)

type GreetLogic struct {
	ctx    context.Context
	svcCtx *svc.ServiceContext
	logx.Logger
}

func NewGreetLogic(ctx context.Context, svcCtx *svc.ServiceContext) *GreetLogic {
	return &GreetLogic{
		ctx:    ctx,
		svcCtx: svcCtx,
		Logger: logx.WithContext(ctx),
	}
}

func (l *GreetLogic) Greet(in *pb.Request) (*pb.Response, error) {
	return &pb.Response{Message: fmt.Sprintf("Hello %s", in.Name)}, nil
}
