// Code generated by goctl. DO NOT EDIT.
// Source: greet_rpc.proto

package server

import (
	"context"

	"gozero_greet/rpc/internal/logic"
	"gozero_greet/rpc/internal/svc"
	"gozero_greet/rpc/pb"
)

type GreetServer struct {
	svcCtx *svc.ServiceContext
	pb.UnimplementedGreetServer
}

func NewGreetServer(svcCtx *svc.ServiceContext) *GreetServer {
	return &GreetServer{
		svcCtx: svcCtx,
	}
}

func (s *GreetServer) Home(ctx context.Context, in *pb.Empty) (*pb.Response, error) {
	l := logic.NewHomeLogic(ctx, s.svcCtx)
	return l.Home(in)
}

func (s *GreetServer) Ping(ctx context.Context, in *pb.Empty) (*pb.Pong, error) {
	l := logic.NewPingLogic(ctx, s.svcCtx)
	return l.Ping(in)
}

func (s *GreetServer) Greet(ctx context.Context, in *pb.Request) (*pb.Response, error) {
	l := logic.NewGreetLogic(ctx, s.svcCtx)
	return l.Greet(in)
}

func (s *GreetServer) CacheSet(ctx context.Context, in *pb.Request) (*pb.Empty, error) {
	l := logic.NewCacheSetLogic(ctx, s.svcCtx)
	return l.CacheSet(in)
}

func (s *GreetServer) CacheGet(ctx context.Context, in *pb.Empty) (*pb.Response, error) {
	l := logic.NewCacheGetLogic(ctx, s.svcCtx)
	return l.CacheGet(in)
}
