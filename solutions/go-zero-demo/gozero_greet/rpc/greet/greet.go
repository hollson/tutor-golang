// Code generated by goctl. DO NOT EDIT.
// Source: greet_rpc.proto

package greet

import (
	"context"

	"gozero_greet/rpc/pb"

	"github.com/zeromicro/go-zero/zrpc"
	"google.golang.org/grpc"
)

type (
	Empty    = pb.Empty
	Pong     = pb.Pong
	Request  = pb.Request
	Response = pb.Response

	Greet interface {
		Home(ctx context.Context, in *Empty, opts ...grpc.CallOption) (*Response, error)
		Ping(ctx context.Context, in *Empty, opts ...grpc.CallOption) (*Pong, error)
		Greet(ctx context.Context, in *Request, opts ...grpc.CallOption) (*Response, error)
		CacheSet(ctx context.Context, in *Request, opts ...grpc.CallOption) (*Empty, error)
		CacheGet(ctx context.Context, in *Empty, opts ...grpc.CallOption) (*Response, error)
	}

	defaultGreet struct {
		cli zrpc.Client
	}
)

func NewGreet(cli zrpc.Client) Greet {
	return &defaultGreet{
		cli: cli,
	}
}

func (m *defaultGreet) Home(ctx context.Context, in *Empty, opts ...grpc.CallOption) (*Response, error) {
	client := pb.NewGreetClient(m.cli.Conn())
	return client.Home(ctx, in, opts...)
}

func (m *defaultGreet) Ping(ctx context.Context, in *Empty, opts ...grpc.CallOption) (*Pong, error) {
	client := pb.NewGreetClient(m.cli.Conn())
	return client.Ping(ctx, in, opts...)
}

func (m *defaultGreet) Greet(ctx context.Context, in *Request, opts ...grpc.CallOption) (*Response, error) {
	client := pb.NewGreetClient(m.cli.Conn())
	return client.Greet(ctx, in, opts...)
}

func (m *defaultGreet) CacheSet(ctx context.Context, in *Request, opts ...grpc.CallOption) (*Empty, error) {
	client := pb.NewGreetClient(m.cli.Conn())
	return client.CacheSet(ctx, in, opts...)
}

func (m *defaultGreet) CacheGet(ctx context.Context, in *Empty, opts ...grpc.CallOption) (*Response, error) {
	client := pb.NewGreetClient(m.cli.Conn())
	return client.CacheGet(ctx, in, opts...)
}
