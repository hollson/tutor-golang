# Gozero Demo

## 查看帮助

```shell
$ make
Usage:  make [command]

Available Commands:
 build                            编译全部
 buildwin                         Windows版本编译
 buildamd                         Linux-Amd版本编译
 api                              生成api模板代码
 rpc                              生成rpc模板代码
 linter                           代码检检查
 vet                              静态代码分析
 clean                            清理编译、日志和缓存等
 run                              启动服务(开发)
 stop                             停止服务
 commit <msg>                     提交Git(格式:make commit msg=备注内容,msg为可选参数)。
 push <msg>                       提交并推送到Git仓库(格式:make push msg=备注内容,msg为可选参数)。
 help                             查看make帮助

For more to see https://github.com/hollson
```

<br/>


## Docker构建

**方式 1. docker构建容器服务**

> 将rpc和api构建到同一个镜像

```shell
docker rm -f greet
docker rmi greet
docker build --no-cache -t greet .

docker run -tid \
  -p 18001:8001 \
  -p 18000:8000 \
  --add-host host.docker.internal:host-gateway \
  --link redis:redis \
  --name greet \
  greet:latest
  
#docker run --network bridge --add-host greet_etcd:52.74.3.34 your_image_name
```


**方式 2. docker compose构建容器服务**

```shell
# --build: 强制重新构建镜像
# --force-recreate: 强制重新构建容器
docker compose up -d --force-recreate --build


# -d: 后台运行服务
# --volumes: 移除与服务关联的匿名卷
# --remove-orphans: 移除孤立的容器，即移除未在 docker-compose.yml 文件中定义的容器
docker compose down --remove-orphans --volumes && docker compose up -d

# --rmi all: 移除所有镜像
# --remove-orphans: 移除孤立的容器
# --volumes: 移除与服务关联的匿名卷
# -d: 后台运行服务
# --build: 总是重建服务容器镜像
docker compose down --rmi all --remove-orphans --volumes && docker compose up -d --build
```

<br/>


## 接口测试
```shell
curl http://127.0.0.1:8000
curl http://127.0.0.1:8000/ping
curl http://127.0.0.1:8000/health
curl -H "Content-Type: application/json" http://127.0.0.1:8000/greet -d '{"name":"Golang"}'

curl -v -H "Content-Type: application/json" http://127.0.0.1:8000/redis/set -d '{"name":"shongsheng"}'
curl -X GET http://127.0.0.1:8000/redis/get
```

<br/>


## 相关资源
> https://discovery.etcd.io/new?size=3
> 
> https://juejin.cn/post/7044185614811398174